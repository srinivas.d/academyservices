package com.leonia.academy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.leonia.academy.entity.Certificatetaken;
import com.leonia.academy.isevice.IStudentAdmissionService;

@RestController
@RequestMapping("/document")
public class CheckInCheckOutController {

	@Autowired
	private IStudentAdmissionService iStudentAdmissionService;
	
	@RequestMapping(value="/getCheckedAndUnCheckedDocuments")
	@ResponseBody
	public ResponseEntity<List<Certificatetaken>> getCheckedAndUnCheckedDocuments()
	{
		List<Certificatetaken> batchlist=iStudentAdmissionService.getCheckedAndUnCheckedDocuments();
		if (batchlist.size() == 0) 
		{
			return new ResponseEntity<>(batchlist, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(batchlist, HttpStatus.OK);
		}

	}
	
	@RequestMapping(value="/getCheckincheckoutreportByDate",method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<List<Certificatetaken>> getCheckincheckoutreportByDate(@RequestBody Certificatetaken certificatetaken ) { 			
		List<Certificatetaken> list =iStudentAdmissionService.getCheckincheckoutreportByDate(certificatetaken);
		if (list.size() == 0) {
			return new ResponseEntity<>(list, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(list, HttpStatus.OK);
		}
	}	
}
