package com.leonia.academy.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.leonia.academy.entity.CompanyDetails;
import com.leonia.academy.entity.InterviewAssignToStudents;
import com.leonia.academy.entity.StatusMaster;
import com.leonia.academy.entity.VacancyDetails;
import com.leonia.academy.entity.VacancyInfo;
import com.leonia.academy.isevice.IPlacementsService;

@RestController
@RequestMapping(value="/placements")
public class PlacementsController {
	@Autowired
	private IPlacementsService iPlacementsService;
	
	@RequestMapping(value="/getdomainmasterservice")
	public ResponseEntity<List> getDomainMaster(){
	  List list =	iPlacementsService.getDomainMaster();
		return new ResponseEntity<List>(list,HttpStatus.OK);
	}
	@RequestMapping(value="/savecompanydetails")
	public ResponseEntity<String> saveCompanyDetails(@RequestBody CompanyDetails companyDetails){
		String message = iPlacementsService.saveCompanyDetails(companyDetails);
		return new ResponseEntity<>(message,HttpStatus.OK);
	}
	@RequestMapping(value="/getallcompanieslistservice")
	public ResponseEntity<List> getAllCompaniesList(){
		List list =	iPlacementsService.getAllCompaniesList();
		return new ResponseEntity<>(list,HttpStatus.OK);
	}
	@RequestMapping(value="/editcompanydetailsbycompanydetailsid/{companyDetailsId}")
	public List<CompanyDetails> editCompanyDetailsByCompanyDetailsId(@PathVariable Long companyDetailsId){
		List companyDet = new ArrayList<>();
		CompanyDetails companyDetails =	iPlacementsService.editCompanyDetailsByCompanyDetailsId(companyDetailsId);
		companyDet.add(companyDetails);
		return companyDet;
	}
	@RequestMapping(value="/updatecompanydetails")
	public ResponseEntity<String> updateCompanyDetails(@RequestBody CompanyDetails companyDetails){
		String message = iPlacementsService.updateCompanyDetails(companyDetails);
		return new ResponseEntity<>(message,HttpStatus.OK);
	}
	@RequestMapping(value="/savevacancydetails")
	public ResponseEntity<String> saveVacancyDetails(@RequestBody VacancyInfo vacancyInfo){
		String message = iPlacementsService.saveVacancyDetails(vacancyInfo);
		return new ResponseEntity<>(message,HttpStatus.OK);
	}
	
	@RequestMapping(value="/getdepartmentmasterservice")
	public ResponseEntity<List> getDepartmentMaster(){
	  List list =	iPlacementsService.getDepartmentMaster();
		return new ResponseEntity<List>(list,HttpStatus.OK);
	}
	
	@RequestMapping(value="/getdesignationmasterservice")
	public ResponseEntity<List> getDesignationMaster(){
	  List list =	iPlacementsService.getDesignationMaster();
		return new ResponseEntity<List>(list,HttpStatus.OK);
	}
	
	@RequestMapping(value="/getdesignationidbydepartmentid/{departmentId}")
	public ResponseEntity<List> getDesignationIdByDepartmentId(@PathVariable Long departmentId){
	  List list =	iPlacementsService.getDesignationIdByDepartmentId(departmentId);
		return new ResponseEntity<List>(list,HttpStatus.OK);
	}
	
	@RequestMapping(value="/getallvacancieslistservice")
	public ResponseEntity<List> getAllVacanciesList(){
		List list =	iPlacementsService.getAllVacanciesList();
		return new ResponseEntity<>(list,HttpStatus.OK);
	}
	
	@RequestMapping(value="/editvacancyinfobyvacancyinfoid/{vacancyInfoId}")
	public List<VacancyInfo> editVacancyInfoByVacancyInfoId(@PathVariable Long vacancyInfoId){
		List companyDet = new ArrayList<>();
		VacancyInfo vacancyInfo =	iPlacementsService.editVacancyInfoByVacancyInfoId(vacancyInfoId);
		companyDet.add(vacancyInfo);
		return companyDet;
	}
	@RequestMapping(value="/updatevacancydetails")
	public ResponseEntity<String> updateVacancyDetails(@RequestBody VacancyInfo vacancyInfo){
		
		String message = iPlacementsService.updateVacancyDetails(vacancyInfo);
		return new ResponseEntity<>(message,HttpStatus.OK);
	}
	
	
	
	
	
	
	
	
	@RequestMapping(value="/viewvacancyDetailsGrid")
	public ResponseEntity<List<VacancyDetails>> viewvacancyDetailsGrid(){
		List<VacancyDetails> list = iPlacementsService.viewvacancyDetailsGrid();
		return new ResponseEntity<>(list,HttpStatus.OK);
	}
	
	@RequestMapping(value="/getVacancyDetailsById/{vacanyDetailsId}")
	public ResponseEntity<List<VacancyDetails>> getVacancyDetailsById(@PathVariable Long vacanyDetailsId){
		List<VacancyDetails> list = iPlacementsService.getVacancyDetailsById(vacanyDetailsId);
		return new ResponseEntity<>(list,HttpStatus.OK);
	}
	@RequestMapping(value="/interviewAssignedToStudents/{vacanyDetailsId}/{admissionNumberId}")
	@ResponseBody
	public String interviewAssignedToStudents(@PathVariable("admissionNumberId") String admissionNumberId,@PathVariable Long vacanyDetailsId){
		String msg = iPlacementsService.interviewAssignedToStudents(vacanyDetailsId,admissionNumberId);
		return msg;
	}
	
	@RequestMapping(value="/getVacancyDetailsByCompanyDetailsId/{companyDetailsId}")
	public ResponseEntity<List<VacancyInfo>> getVacancyDetailsByCompanyDetailsId(@PathVariable Long companyDetailsId){
		List<VacancyInfo> list = iPlacementsService.getVacancyDetailsByCompanyDetailsId(companyDetailsId);
		return new ResponseEntity<>(list,HttpStatus.OK);
	}
	
			
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value="/getDepartmentsByVacancyInfoId/{vacancyInfoId}")
	public ResponseEntity<List<VacancyDetails>> getDepartmentsByVacancyInfoId(@PathVariable Long vacancyInfoId){
		List<VacancyDetails> list = iPlacementsService.getDepartmentsByVacancyInfoId(vacancyInfoId);
		return new ResponseEntity<>(list,HttpStatus.OK);
	}
	
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/getStudentsByVacancyDetailsId/{vacanyDetailsId}")
	public  ResponseEntity<List> getStudentsByVacancyDetailsId(@PathVariable Long vacanyDetailsId) {
		List list= iPlacementsService.getStudentsByVacancyDetailsId(vacanyDetailsId);
		return new ResponseEntity<>(list,HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/removeStudentsByVacancyDetailsId/{vacanyDetailsId}/{admissionNumberId}")
	public  ResponseEntity<String> removeStudentsByVacancyDetailsId(@PathVariable("admissionNumberId") String admissionNumberId,@PathVariable Long vacanyDetailsId) {
		String msg= iPlacementsService.removeStudentsByVacancyDetailsId(vacanyDetailsId,admissionNumberId);
		return new ResponseEntity<>(msg,HttpStatus.OK);
	}
	
	
	@RequestMapping(value="/getStatusMaster")
	public ResponseEntity<List> getStatusMaster(){
	  List<StatusMaster> list =	iPlacementsService.getStatusMaster();
		return new ResponseEntity<List>(list,HttpStatus.OK);
	}
	
	@SuppressWarnings("unused")
	@RequestMapping(value="/saveDeclaredResults",method=RequestMethod.POST)
	@ResponseBody
	public String saveDeclaredResults(@RequestBody List<InterviewAssignToStudents> interviewAssignToStudents){
		 String msg = null;
		for(int i=0;i<interviewAssignToStudents.size();i++){
        msg =iPlacementsService.saveDeclaredResults(interviewAssignToStudents.get(i));
		}
	return msg;
	}
	
	@RequestMapping(value="/getStudentsByVacancyDetailsIdFromAttendedInterViews/{vacanyDetailsId}")
    public ResponseEntity<List> getStudentsByVacancyDetailsIdFromAttendedInterViews(@PathVariable Long vacanyDetailsId){
		List list= iPlacementsService.getStudentsByVacancyDetailsIdFromAttendedInterViews(vacanyDetailsId);
		return new ResponseEntity<>(list,HttpStatus.OK);
	}
	
	@RequestMapping(value="/closingInterviewVacancyByVacancyInfoId/{vacancyDetailsId}/{vacancyInfoId}")
	public ResponseEntity<String> closingInterviewVacancyByVacancyInfoId(@PathVariable Long vacancyDetailsId,@PathVariable Long vacancyInfoId){
		String msg = iPlacementsService.closingInterviewVacancyByVacancyInfoId(vacancyDetailsId,vacancyInfoId);
		return new ResponseEntity<>(msg,HttpStatus.OK);
	}
}
