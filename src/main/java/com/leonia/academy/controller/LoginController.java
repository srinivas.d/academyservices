package com.leonia.academy.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.leonia.academy.entity.LoginEntity;
import com.leonia.academy.isevice.ILoginService;

@RestController
public class LoginController {
	
	@Autowired
	private ILoginService loginService;

	@RequestMapping(value = "/home")
	@ResponseBody
	public ResponseEntity<LoginEntity> displaylogin(@RequestBody LoginEntity loginentity,HttpServletRequest request,HttpSession session) {
		String userreq = loginentity.getUsername();
		String passreq = loginentity.getPassword();
		LoginEntity login = new LoginEntity();
		login.setUsername(userreq);
		login.setPassword(passreq);
		List<LoginEntity> list = loginService.loginCheck(login);
		LoginEntity myusername =null;
		if (list.size() != 0) {
			myusername = list.get(0); 
			Integer userId = myusername.getUesrId();
			String uname = myusername.getUsername();
			String password = myusername.getPassword();
			session.setAttribute("userId", userId);
			session.setAttribute("username", uname);
			
			if (uname.equals(userreq) && password.equals(passreq)) {
				myusername.setResult(1);
				return new ResponseEntity<>(myusername,HttpStatus.OK);
			}
	}
		return new ResponseEntity<>(myusername,HttpStatus.OK);
	}
}
