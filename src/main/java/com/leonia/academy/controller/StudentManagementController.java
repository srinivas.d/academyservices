package com.leonia.academy.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.leonia.academy.entity.BatchMaster;
import com.leonia.academy.entity.CalenderEvents;
import com.leonia.academy.entity.CourseMaster;
import com.leonia.academy.entity.EducationEntity;
import com.leonia.academy.entity.ParentGuardianEntity;
import com.leonia.academy.entity.RelationMaster;
import com.leonia.academy.entity.StudentAdmissionEntity;
import com.leonia.academy.isevice.IStudentManagementService;

@RestController
@RequestMapping(value="/studentmanagement")
public class StudentManagementController {
	@Autowired
	private IStudentManagementService iStudentManagementService;
	
	@RequestMapping(value="/getAdmissionNumberId")
	public ResponseEntity<Long> getAdmissionNumberId(){
	Long admissionNumberId =	iStudentManagementService.getAdmissionNumberId();
		return new ResponseEntity<>(admissionNumberId,HttpStatus.OK);
		
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="nationalitymasterservice")
	@ResponseBody
	public ResponseEntity<List> nationalityMaster(){
	List nationalitymaster =	iStudentManagementService.getNationalityMaster();
	return new ResponseEntity<List>(nationalitymaster,HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="bloodgroupmasterservice")
	@ResponseBody
	public ResponseEntity<List> bloodGroupMaster(){
	List bloodGroupMaster =	iStudentManagementService.bloodGroupMaster();
	return new ResponseEntity<List>(bloodGroupMaster,HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="staycategorymasterservice")
	@ResponseBody
	public ResponseEntity<List> stayCategoryMaster(){
	List stayCategoryMaster =	iStudentManagementService.stayCategoryMaster();
	return new ResponseEntity<List>(stayCategoryMaster,HttpStatus.OK);
	}
	
	
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="discountcategorymasterservice")
	@ResponseBody
	public ResponseEntity<List> discountCategoryMaster(){
	List discountCategoryMaster =	iStudentManagementService.discountCategoryMaster();
	return new ResponseEntity<List>(discountCategoryMaster,HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="religionmasterservice")
	@ResponseBody
	public ResponseEntity<List> religionMaster(){
	List religionMaster =	iStudentManagementService.religionMaster();
	return new ResponseEntity<List>(religionMaster,HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="communitymasterservice")
	@ResponseBody
	public ResponseEntity<List> communityMaster(){
	List communityMaster =	iStudentManagementService.communityMaster();
	return new ResponseEntity<List>(communityMaster,HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="languagemasterservice")
	@ResponseBody
	public ResponseEntity<List> languageMaster(){
	List languageMaster =	iStudentManagementService.languageMaster();
	return new ResponseEntity<List>(languageMaster,HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="feecategorymasterservice")
	@ResponseBody
	public ResponseEntity<List> feeCategoryMaster(){
	List feecategorymaster =	iStudentManagementService.feeCategoryMaster();
	return new ResponseEntity<List>(feecategorymaster,HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="citymasterservice")
	@ResponseBody
	public ResponseEntity<List> cityMaster(){
	List cityMaster =	iStudentManagementService.cityMaster();
	return new ResponseEntity<List>(cityMaster,HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="getstatecountrybycityidservice/{cityId}")
	@ResponseBody
	public ResponseEntity<List> getStateCountryByCityId(@PathVariable int cityId){
		List strings =	iStudentManagementService.getStateCountryByCityId(cityId);
	return new ResponseEntity<List>(strings,HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="coursemasterservice")
	@ResponseBody
	public ResponseEntity<List> courseMaster(){
	List courseMaster =	iStudentManagementService.courseMaster();
	return new ResponseEntity<List>(courseMaster,HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="getbatchnamebycourseid/{courseId}")
	@ResponseBody
	public ResponseEntity<List> getBatchNameByCourseId(@PathVariable Long courseId){
		List strings =	iStudentManagementService.getBatchNameByCourseId(courseId);
	return new ResponseEntity<List>(strings,HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="getbatchfeebybatchid/{batchId}")
	@ResponseBody
	public ResponseEntity<List> getBatchFeeByBatchId(@PathVariable Long batchId){
		List strings =	iStudentManagementService.getBatchFeeByBatchId(batchId);
	return new ResponseEntity<List>(strings,HttpStatus.OK);
	}
	
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="savestudentadmissiondetails",method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> saveStudentAdmissionDetails(@RequestBody StudentAdmissionEntity studentAdmissionEntity){
		String message =	iStudentManagementService.saveStudentAdmissionDetails(studentAdmissionEntity);
	return new ResponseEntity<>(message,HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/viewstudentDetails")
	@ResponseBody
	public ResponseEntity<List> getviewstudentDetails(){
		List list = iStudentManagementService.getviewstudentDetails();
		return new ResponseEntity<>(list,HttpStatus.OK);	
	}
	
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="editstudentadmissiondetails/{admissionNumberId}")
	@ResponseBody
	public List<StudentAdmissionEntity> editStudentAdmissionDetails(@PathVariable Long admissionNumberId){
		List list = new ArrayList<>();
		StudentAdmissionEntity studentAdmissionEntity = iStudentManagementService.editStudentAdmissionDetails(admissionNumberId);
		list.add(studentAdmissionEntity);
		return list;
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="updatestudentadmissiondetails",method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> updateStudentAdmissionDetails(@RequestBody StudentAdmissionEntity studentAdmissionEntity){
		String message =	iStudentManagementService.updateStudentAdmissionDetails(studentAdmissionEntity);
	return new ResponseEntity<>(message,HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="studentDetailsByAdmissionIdDetails/{admissionNumberId}")
	@ResponseBody
	public ResponseEntity<StudentAdmissionEntity> studentDetailsByAdmissionIdDetails(@PathVariable Long admissionNumberId){
		StudentAdmissionEntity studentAdmissionEntity = iStudentManagementService.editStudentAdmissionDetails(admissionNumberId);
		return new ResponseEntity<>(studentAdmissionEntity,HttpStatus.OK);
	}
	
	@RequestMapping(value="relationsList")
	public ResponseEntity<List> relationsList(){
		List<RelationMaster> list = iStudentManagementService.relationsList();
		return new ResponseEntity<List>(list,HttpStatus.OK);
	}
	
	@RequestMapping(value="ParentDetails/{admissionNumberId}")
	public ResponseEntity<List> parentDetails(@PathVariable Long admissionNumberId){
		List<ParentGuardianEntity> list = iStudentManagementService.parentDetails(admissionNumberId);
		return new ResponseEntity<List>(list,HttpStatus.OK);
	}
	
	
	@RequestMapping(value="saveorupdateParentGuardian")
	public ResponseEntity<String> saveorupdateParentGuardian(@RequestBody ParentGuardianEntity parentGuardianEntity){
		String msg = iStudentManagementService.saveorupdateParentGuardian(parentGuardianEntity);
		return new ResponseEntity<>(msg,HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/eventtypelist")
	@ResponseBody
	public ResponseEntity<List> geteventtypelist(){
		List list = iStudentManagementService.geteventtypelist();
		return new ResponseEntity<>(list,HttpStatus.OK);	
	}
	
	@RequestMapping(value="/listcalendereventdate")
	@ResponseBody
	public ResponseEntity<List<CalenderEvents>> listcalendereventdate(){
		List<CalenderEvents> list = iStudentManagementService.getlistcalendereventdate();
		return new ResponseEntity<>(list,HttpStatus.OK);	
	}
	
	@RequestMapping(value="/insertcalendereventform")
	@ResponseBody
	public ResponseEntity<String> insertcalendereventform(@RequestBody CalenderEvents calenderEvents){
		String  msg = iStudentManagementService.insertcalendereventform(calenderEvents);
		return new ResponseEntity<>(msg,HttpStatus.OK);	
	}
	
	@RequestMapping(value="/calendereventdatelist")
	@ResponseBody
	public ResponseEntity<List> calendereventdatelist(){
		List list =iStudentManagementService.calendereventdatelist();
		return new ResponseEntity<>(list,HttpStatus.OK);	
	}
	@RequestMapping(value="/studentDetailsById/{admissionId}")
	@ResponseBody
	public ResponseEntity<StudentAdmissionEntity> getStudentDetailsById(@PathVariable Long admissionId){
		StudentAdmissionEntity studentAdmission =iStudentManagementService.editStudentAdmissionDetails(admissionId);
		return new ResponseEntity<>(studentAdmission,HttpStatus.OK);	
	}
	@RequestMapping(value="/studentEduDetailsById/{admissionId}")
	@ResponseBody
	public ResponseEntity<List> getEducatioDetails(@PathVariable Long admissionId){
		List geteducationdetails=iStudentManagementService.getEducatioDetails(admissionId);
		return new ResponseEntity<>(geteducationdetails,HttpStatus.OK);	
	}
	@RequestMapping(value="storeeducationdetails/{admissionNumberId}")
	public ResponseEntity<String> admissionNumberId(@RequestBody EducationEntity educationEntity,@PathVariable Long admissionNumberId){
		StudentAdmissionEntity studentAdmissionEntity = new StudentAdmissionEntity();
		studentAdmissionEntity.setAdmissionNumberId(admissionNumberId);
		educationEntity.setStudentAdmissionEntity(studentAdmissionEntity);
		String msg = iStudentManagementService.storeeducationdetails(educationEntity);
		return new ResponseEntity<>(msg,HttpStatus.OK);
	}
	
	
	
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/getUniversityMaster")
	@ResponseBody
	public ResponseEntity<List> universityMaster(){
	List universityMaster =	iStudentManagementService.universityMaster();
	return new ResponseEntity<List>(universityMaster,HttpStatus.OK);
	}
	
	@RequestMapping(value="getCourseByUniversity/{universityMasterId}")
	public ResponseEntity<List> getCourseByUniversity(@PathVariable Long universityMasterId){
		List<CourseMaster> list = iStudentManagementService.getCourseByUniversity(universityMasterId);
		return new ResponseEntity<List>(list,HttpStatus.OK);
	}
	
	@RequestMapping(value="getBatchNameByCourse/{universityMasterId}")
	public ResponseEntity<List> getBatchNameByCourse(@PathVariable Long courseId){
		List<BatchMaster> list = iStudentManagementService.getBatchNameByCourseId(courseId);
		return new ResponseEntity<List>(list,HttpStatus.OK);
	}
	
	
	@RequestMapping(value="getStudentsByBatchForAssignInterviews/{vacanyDetailsId}/{batchId}")
	public ResponseEntity<List> getStudentsByBatchForAssignInterviews(@PathVariable Long vacanyDetailsId,@PathVariable Long batchId){
		List<StudentAdmissionEntity> list = iStudentManagementService.getStudentsByBatchForAssignInterviews(vacanyDetailsId,batchId);
		return new ResponseEntity<List>(list,HttpStatus.OK);
	}
	
	
}
