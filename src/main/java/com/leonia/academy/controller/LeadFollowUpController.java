package com.leonia.academy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.leonia.academy.entity.LeadFollowUpDTO;
import com.leonia.academy.entity.LoginEntity;
import com.leonia.academy.isevice.IFollowupDetailsService;

@RestController
@RequestMapping(value = "/leadlocation")
public class LeadFollowUpController {
	@Autowired
	private IFollowupDetailsService iFollowupDetailsService;
	
	@RequestMapping(value="/getLeadFollowUpDetails/{leadid}")
	@ResponseBody
	public ResponseEntity<List<LeadFollowUpDTO>> getLeadFollowUpDetails(@PathVariable Integer leadid) {
		
		List<LeadFollowUpDTO> leadFollowUpDTO =iFollowupDetailsService.getfollowupdetails(leadid);
		if (leadFollowUpDTO.size() == 0) {
			return new ResponseEntity<>(leadFollowUpDTO, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(leadFollowUpDTO, HttpStatus.OK);
		}

	}
	@RequestMapping(value="/getUsernameList")
	@ResponseBody
	public ResponseEntity<List<LoginEntity>> getUsernameList() {
		
		List<LoginEntity> loginEntity =iFollowupDetailsService.getUsernameList();
		if (loginEntity.size() == 0) {
			return new ResponseEntity<>(loginEntity, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(loginEntity, HttpStatus.OK);
		}

	}
	@RequestMapping(value="/saveNewFollowUp",method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> saveNewFollowUp(@RequestBody LeadFollowUpDTO leadFollowUpDTO){
    String	msg = iFollowupDetailsService.saveNewFollowUp(leadFollowUpDTO);
	return new ResponseEntity<>(msg,HttpStatus.OK);
	}
	
	@RequestMapping(value="/leadFollowUpReport")
	@ResponseBody
	public ResponseEntity<List<LeadFollowUpDTO>> getFollowUpDetails(){

		List<LeadFollowUpDTO> leadFollowUpDTO =iFollowupDetailsService.getFollowUpDetails();
		if (leadFollowUpDTO.size() == 0) {
			return new ResponseEntity<>(leadFollowUpDTO, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(leadFollowUpDTO, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value="/getNewFollowUpDetails",method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<List<LeadFollowUpDTO>> getNewFollowUpDetails(@RequestBody LeadFollowUpDTO leadFollowUpDTO ) { 			
		
		List<LeadFollowUpDTO> leadDetails =iFollowupDetailsService.getNewFollowUpDetails(leadFollowUpDTO);
		if (leadDetails.size() == 0) {
			return new ResponseEntity<>(leadDetails, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(leadDetails, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value="/getOpenLeadFoliowupDetails")
	@ResponseBody
	public ResponseEntity<List<LeadFollowUpDTO>> getOpenLeadFoliowupDetails(){

		List<LeadFollowUpDTO> leadFollowUpDTO =iFollowupDetailsService.getOpenLeadFoliowupDetails();
		if (leadFollowUpDTO.size() == 0) {
			return new ResponseEntity<>(leadFollowUpDTO, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(leadFollowUpDTO, HttpStatus.OK);
		}
	}
	@RequestMapping(value="/getFollowupDetailsByDate",method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<List<LeadFollowUpDTO>> getFollowupDetailsByDate(@RequestBody LeadFollowUpDTO leadFollowUpDTO ) { 			
		
		List<LeadFollowUpDTO> leadDetails =iFollowupDetailsService.getFollowupDetailsByDate(leadFollowUpDTO);
		if (leadDetails.size() == 0) {
			return new ResponseEntity<>(leadDetails, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(leadDetails, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value="/closeLeadFollowupDetails/{followupId}")
	@ResponseBody
	public ResponseEntity<List<LeadFollowUpDTO>> getFollowUpDetailsById(@PathVariable Integer followupId) {
		
		List<LeadFollowUpDTO> leadFollowUpDTO =iFollowupDetailsService.getFollowUpDetailsById(followupId);
		if (leadFollowUpDTO.size() == 0) {
			return new ResponseEntity<>(leadFollowUpDTO, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(leadFollowUpDTO, HttpStatus.OK);
		}

	}
	
	
	@RequestMapping(value="/endlead",method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<List> endlead(@RequestBody LeadFollowUpDTO leadFollowUpDTO) {
		List list =iFollowupDetailsService.endlead(leadFollowUpDTO);
		if (list.size() == 0) {
			return new ResponseEntity<>(list, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(list, HttpStatus.OK);
		}

	}
	
}
