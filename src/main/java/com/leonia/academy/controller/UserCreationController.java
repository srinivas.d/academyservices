package com.leonia.academy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.leonia.academy.entity.StudentRegistrationEntity;
import com.leonia.academy.isevice.IUserCreationService;

@RestController
public class UserCreationController {
	
	@Autowired
	public IUserCreationService iUserCreationService;
	
	@RequestMapping(value="/usercreated",method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> userformsave(@RequestBody StudentRegistrationEntity studentRegistrationEntity){
    String	msg = iUserCreationService.saveuserdetalies(studentRegistrationEntity);
	return new ResponseEntity<>(msg,HttpStatus.OK);
	}
}
