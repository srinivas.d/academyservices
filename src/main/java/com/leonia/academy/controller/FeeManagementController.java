package com.leonia.academy.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.leonia.academy.entity.CardTypeMaster;
import com.leonia.academy.entity.FeeAssignToStudents;
import com.leonia.academy.entity.FeeCreation;
import com.leonia.academy.entity.FeeGenerationBatchWise;
import com.leonia.academy.entity.HostelFeeAssingToStudent;
import com.leonia.academy.entity.PaymodeMaster;
import com.leonia.academy.entity.ReceiptGenerateDTO;
import com.leonia.academy.entity.StudentAdmissionEntity;
import com.leonia.academy.isevice.IFeeManagementService;

/**
 * @author srinivas.d
 *
 */
@RestController
@RequestMapping(value="/feemanagement")
public class FeeManagementController {
	
	@Autowired
	private IFeeManagementService iFeeManagementService;
	
	/**
	 * @param discountCategoryId
	 * @param searchName
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="getstudentsearchbydiscountcategory/{discountCategoryId}/{searchName}")
	@ResponseBody
	public ResponseEntity<List> getStudentSearchByDiscountCategory(@PathVariable Long discountCategoryId,@PathVariable String searchName){
		List<StudentAdmissionEntity> list =	iFeeManagementService.getStudentSearchByDiscountCategory(discountCategoryId,searchName);
	return new ResponseEntity<List>(list,HttpStatus.OK);
	}
	
	
	/**
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="discountapprovedmasterservice")
	@ResponseBody
	public ResponseEntity<List> discountCategoryMaster(){
	List discountApprovedMaster =	iFeeManagementService.discountApprovedMaster();
	return new ResponseEntity<List>(discountApprovedMaster,HttpStatus.OK);
	}

	/**
	 * @param bacthId
	 * @param admissionNumberId
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="studentfeediscountdetails/{bacthId}/{admissionNumberId}")
	@ResponseBody
	public ResponseEntity<List> studentFeediscountDetails(@PathVariable Long bacthId,@PathVariable Long admissionNumberId){
		List list =	iFeeManagementService.studentFeediscountDetails(bacthId,admissionNumberId);
	return new ResponseEntity<List>(list,HttpStatus.OK);
	}
	
	
	/**
	 * @param feeAssignToStudentsId
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="feeassigntostudentsamountparticularfee/{feeAssignToStudentsId}")
	@ResponseBody
	public ResponseEntity<List> feeAssignToStudentsAmountParticularFee(@PathVariable Long feeAssignToStudentsId){
		
		List list =	iFeeManagementService.feeAssignToStudentsAmountParticularFee(feeAssignToStudentsId);
	return new ResponseEntity<List>(list,HttpStatus.OK);
	}
	
	/**
	 * @param hostelFeeAssingToStudentId
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="hostalfeeassigntostudentsamountparticularfee/{hostelFeeAssingToStudentId}")
	@ResponseBody
	public ResponseEntity<List> hostalfeeAssignToStudentsAmountParticularFee(@PathVariable Long hostelFeeAssingToStudentId){
		List list =	iFeeManagementService.hostalfeeAssignToStudentsAmountParticularFee(hostelFeeAssingToStudentId);
	return new ResponseEntity<List>(list,HttpStatus.OK);
	}
	
	/**
	 * @param objStudentCategory
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="discountstuparticularfee")
	@ResponseBody
	public ResponseEntity<String> discountStuParticularfee(@RequestBody FeeAssignToStudents objStudentCategory){
		String message =	iFeeManagementService.discountStuParticularfee(objStudentCategory);
	return new ResponseEntity<String>(message,HttpStatus.OK);
	}
	
	/**
	 * @param objStudentCategory
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="discountstuhostelfee")
	@ResponseBody
	public ResponseEntity<String> discountStuHostelFee(@RequestBody FeeAssignToStudents objStudentCategory){
		String message =	iFeeManagementService.discountStuHostelFee(objStudentCategory);
	return new ResponseEntity<String>(message,HttpStatus.OK);
	}
	
	/**
	 * @param searchName
	 * @return
	 */
	@RequestMapping(value = "/getStudentListSearch/{searchName}")
	public List<StudentAdmissionEntity> getStudentListSearch(@PathVariable String searchName) {
		List<StudentAdmissionEntity> list = iFeeManagementService.getStudentListSearch(searchName);
		return list;
	}
	
	/**
	 * @param admissionNumberId
	 * @param bacthId
	 * @return
	 */
	@RequestMapping(value = "/getFeeGenerationdetails/{admissionNumberId}/{bacthId}")
	public List<FeeAssignToStudents> getFeeGenerationdetails(@PathVariable Long admissionNumberId,@PathVariable Long bacthId){
		List<FeeAssignToStudents> list = iFeeManagementService.getFeeGenerationdetails(admissionNumberId,bacthId);
		return list;
	}
	
	/**
	 * @param admissionNumberId
	 * @return
	 */
	@RequestMapping(value = "/gethostelFeeGenerationdetails/{admissionNumberId}")
	public List<HostelFeeAssingToStudent> gethostelFeeGenerationdetails(@PathVariable Long admissionNumberId) {
		List<HostelFeeAssingToStudent> list = iFeeManagementService.gethostelFeeGenerationdetails(admissionNumberId);
		return list;
	}
	
	/**
	 * @return
	 */
	@RequestMapping(value = "/getPaymentModeTypes")
	public List<PaymodeMaster> getPaymentModeTypes() {
		List<PaymodeMaster> list = iFeeManagementService.getPaymentModeTypes();
		return list;
	}
	
	/**
	 * @return
	 */
	@RequestMapping(value = "/getCardTypeMaster")
	public List<CardTypeMaster> getCardTypeMaster() {
		List<CardTypeMaster> list = iFeeManagementService.getCardTypeMaster();
		return list;
	}
	
	/**
	 * @param admissionNumberId
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getfeepaidreceptdetails/{admissionNumberId}")
	public List getfeepaidreceptdetails(@PathVariable Long admissionNumberId) {
		List list = iFeeManagementService.getfeepaidreceptdetails(admissionNumberId);
		return list;
	}
	
	/**
	 * @param receiptGenerateId
	 * @param admissionNumberId
	 * @return
	 */
	@RequestMapping(value = "/generateReceipt/{admissionNumberId}/{receiptGenerateId}")
	public ReceiptGenerateDTO generateReceipt(@PathVariable Long receiptGenerateId,@PathVariable Long admissionNumberId) {
		ReceiptGenerateDTO generateDTO = iFeeManagementService.generateReceipt(receiptGenerateId,admissionNumberId);
		return generateDTO;
	}
	
	/**
	 * @param receiptgeneratedtoObj
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "savePaymentDetails")
	public String savePaymentDetails(@RequestBody ReceiptGenerateDTO receiptgeneratedtoObj) {
		String msg = iFeeManagementService.savePaymentDetails(receiptgeneratedtoObj);
		return msg;
	}
	
	/**
	 * @param feesettlementdtolist
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "feeassigntostudentPaymentupdate")
	public String savedefineHostelfee(@RequestBody List<FeeAssignToStudents> feesettlementdtolist) {
		String msg = iFeeManagementService.feeassigntostudentPaymentupdate(feesettlementdtolist);
		return msg;
   }
	
	/**
	 * @param hostelFeeAssingToStudent
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "hostelFeeAssignToStudentPaymentUpdate")
	public String hostelFeeAssignToStudentPaymentUpdate(@RequestBody HostelFeeAssingToStudent hostelFeeAssingToStudent) {
		String msg = iFeeManagementService.hostelFeeAssignToStudentPaymentUpdate(hostelFeeAssingToStudent);
		return msg;
	}
	
	
	
	
	/**
	 * @param batchId
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/getFeeDetailsById/{batchId}")
	@ResponseBody
	public ResponseEntity<List> getFeeDetailsById(@PathVariable Integer batchId) {
		List list=iFeeManagementService.getFeeDetailsById(batchId);
		if (list.size() == 0) {
			return new ResponseEntity<>(list, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(list, HttpStatus.OK);
		}
	}
	/**
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/feeCategory")
	@ResponseBody
	public ResponseEntity<List> feeCategory() {
		List list=iFeeManagementService.feeCategory();
		if (list.size() == 0) {
			return new ResponseEntity<>(list, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(list, HttpStatus.OK);
		}
	}
	/**
	 * @param batchId
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/getCategoryNameByBatchId/{batchId}")
	@ResponseBody
	public ResponseEntity<List> getCategoryNameByBatchId(@PathVariable Integer batchId) {
		List categorylist=iFeeManagementService.getCategoryNameByBatchId(batchId);
		if (categorylist.size() == 0) {
			return new ResponseEntity<>(categorylist, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(categorylist, HttpStatus.OK);
		}
	}
	/**
	 * @param fee
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/studentfee",method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> saveStudentFee(@RequestBody FeeCreation fee,HttpServletRequest request){
		
    String	msg = iFeeManagementService.saveStudentFee(fee);
	return new ResponseEntity<>(msg,HttpStatus.OK);
	}
	
	/**
	 * @param fee
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/addextrafee",method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> addextrafee(@RequestBody FeeCreation fee,HttpServletRequest request){
		
    String	msg = iFeeManagementService.saveStudentFee(fee);
	return new ResponseEntity<>(msg,HttpStatus.OK);
	}
	/**
	 * @param batchId
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/getInstallmentTypesRec/{batchId}")
	@ResponseBody
	public ResponseEntity<List> getFeeCreationBatchWise(@PathVariable Long batchId) {
		List list=iFeeManagementService.getInstallmentTypes(batchId);
		if (list.size() == 0) {
			return new ResponseEntity<>(list, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(list, HttpStatus.OK);
		}
	}
	
	/**
	 * @param batchId
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/getInstallmentTypesdrop/{batchId}")
	@ResponseBody
	public ResponseEntity<List> getInstallmentTypesdrop(@PathVariable Long batchId) {
		List list=iFeeManagementService.getInstallmentTypesdrop(batchId);
		if (list.size() == 0) {
			return new ResponseEntity<>(list, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(list, HttpStatus.OK);
		}
	}
	
	/**
	 * @param batchId
	 * @param installmentTypeMasterId
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/getFeeCreationBatchWise/{batchId}/{installmentTypeMasterId}")
	@ResponseBody
	public ResponseEntity<List<FeeCreation>> getFeeCreationBatchWise(@PathVariable Long batchId,@PathVariable Long installmentTypeMasterId){
		List<FeeCreation> feeCreationsList = iFeeManagementService.getFeeCreationBatchWiseListByBatchId(batchId,installmentTypeMasterId);
		if (feeCreationsList.size() == 0) {
			return new ResponseEntity<>(feeCreationsList, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(feeCreationsList, HttpStatus.OK);
		}
		
	}
	/**
	 * @param feeCreationId
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/getAmountByParticularFee/{feeCreationId}")
	@ResponseBody
	public ResponseEntity<List> getAmountByParticularFee(@PathVariable Long feeCreationId){
		List amount = iFeeManagementService.getAmountByParticularFee(feeCreationId);		
	   if (amount.size() == 0) {
			return new ResponseEntity<>(amount, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(amount, HttpStatus.OK);
		}
		
	}
	/**
	 * @param generationBatchWise
	 * @return
	 */
	@RequestMapping(value="/feeGenarationBatchWise")
	@ResponseBody
	public ResponseEntity<String> generationBatchWise(@RequestBody FeeGenerationBatchWise generationBatchWise){
		String msg = iFeeManagementService.savefeeGenarationBatchWise(generationBatchWise);	
		return new ResponseEntity<>(msg, HttpStatus.OK);
	}
	
	/**
	 * @param batchId
	 * @param feeGenerationBatchWiseId
	 * @return
	 */
	@RequestMapping(value="/getFeeGenarationFeeListInstallment/{batchId}/{feeGenerationBatchWiseId}")
	@ResponseBody
	public ResponseEntity<List> getFeeGenarationFeeListInstallment(@PathVariable Long batchId,@PathVariable Long feeGenerationBatchWiseId){
		List list = iFeeManagementService.getFeeGenarationFeeListInstallment(batchId,feeGenerationBatchWiseId);	
		if (list.size() == 0) {
			return new ResponseEntity<>(list, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(list, HttpStatus.OK);
		}
	}
	
	/**
	 * @param batchId
	 * @param feeGenerationBatchWiseId
	 * @param installmentTypeMasterId
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/getFeeGenarationFeeListInstallment123/{batchId}/{feeGenerationBatchWiseId}/{installmentTypeMasterId}")
	@ResponseBody
	public ResponseEntity<List> getFeeGenarationFeeListInstallment123(@PathVariable Long batchId,@PathVariable Long feeGenerationBatchWiseId,@PathVariable Long installmentTypeMasterId){
	   List feelist = iFeeManagementService.getFeeGenarationFeeListInstallment123(batchId,feeGenerationBatchWiseId,installmentTypeMasterId);
	   if (feelist.size() == 0) {
			return new ResponseEntity<>(feelist, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(feelist, HttpStatus.OK);
		}
		
	}
	/**
	 * @param batchId
	 * @param feeGenerationBatchWiseId
	 * @param installmentTypeMasterId
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/getStudentsListByBatchId/{batchId}/{feeGenerationBatchWiseId}/{installmentTypeMasterId}")
	@ResponseBody
	public ResponseEntity<List> getStudentsListByBatchId(@PathVariable Long batchId,@PathVariable Long feeGenerationBatchWiseId,@PathVariable Long installmentTypeMasterId){
	   List studentList = iFeeManagementService.getStudentsListByBatchId(batchId,feeGenerationBatchWiseId,installmentTypeMasterId);
	   if (studentList.size() == 0) {
			return new ResponseEntity<>(studentList, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(studentList, HttpStatus.OK);
		}
		
	}	
	
	/**
	 * @param feeAssignToStudents
	 * @return
	 */
	@RequestMapping(value="/feeAssignToStudentsPost")
	@ResponseBody
	public ResponseEntity<String> feeAssignToStudentsPost(@RequestBody FeeAssignToStudents feeAssignToStudents){
		String msg = iFeeManagementService.feeAssignToStudentsPost(feeAssignToStudents);	
		return new ResponseEntity<>(msg, HttpStatus.OK);
	}
}
