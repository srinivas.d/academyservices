package com.leonia.academy.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.leonia.academy.entity.DropdownMasterCourseEntity;
import com.leonia.academy.entity.DropdownMasterSourceEntity;
import com.leonia.academy.entity.LeadFollowUpDTO;
import com.leonia.academy.isevice.ILeadinSertionService;

@RestController
@RequestMapping(value = "/leadlocation")
public class LeadinSertionController {

	@Autowired
	private ILeadinSertionService ileadinSertionService;

	@InitBinder
	public void DataFormat(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
		binder.registerCustomEditor(Date.class, "followupDate", new CustomDateEditor(dateFormat, true));
		binder.registerCustomEditor(Date.class, "leadDate", new CustomDateEditor(dateFormat, true));
		binder.registerCustomEditor(Date.class, "dob", new CustomDateEditor(dateFormat, true));
	}
	@RequestMapping("/getCourseList")
	@ResponseBody
	public ResponseEntity<List<DropdownMasterCourseEntity>> getCourseListDetails() {
		
		DropdownMasterCourseEntity dropdownMasterCourseDTO = new DropdownMasterCourseEntity();
		List<DropdownMasterCourseEntity> paymentmodetypes =ileadinSertionService.MasterCourse(dropdownMasterCourseDTO);
		if (paymentmodetypes.size() == 0) {
			return new ResponseEntity<>(paymentmodetypes, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(paymentmodetypes, HttpStatus.OK);
		}

	}
	@RequestMapping("/getSourceList")
	@ResponseBody
	public ResponseEntity<List<DropdownMasterSourceEntity>> getSourceListDetails() {
		DropdownMasterSourceEntity dropdownMasterSourceDTO = new DropdownMasterSourceEntity();
		List<DropdownMasterSourceEntity> dropdownMasterSourcelist =ileadinSertionService.MasterSource(dropdownMasterSourceDTO);
		if (dropdownMasterSourcelist.size() == 0) {
			return new ResponseEntity<>(dropdownMasterSourcelist, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(dropdownMasterSourcelist, HttpStatus.OK);
		}

	}
	@RequestMapping(value="/saveleaddetails",method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> userformsave(@RequestBody LeadFollowUpDTO leadFollowUpDTO,HttpServletRequest request){
		
    String	msg = ileadinSertionService.saveleaddetails(leadFollowUpDTO);
	return new ResponseEntity<>(msg,HttpStatus.OK);
	}
}