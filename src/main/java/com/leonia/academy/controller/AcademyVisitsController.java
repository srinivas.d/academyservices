package com.leonia.academy.controller;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.leonia.academy.entity.DropDownMasterEntity;
import com.leonia.academy.entity.DropDownSubMasterEntity;
import com.leonia.academy.entity.InstituteMasterEntity;
import com.leonia.academy.entity.VisitEntryEntity;
import com.leonia.academy.isevice.IAcademyVisitsService;

@Controller
public class AcademyVisitsController {
	
	@Autowired
	private IAcademyVisitsService iAcademyVisitsSevice;
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/institutemaster",method=RequestMethod.GET)
	@ResponseBody
	public  ResponseEntity<List> instituteMasterFormGet(){
		//List<DropDownMasterEntity> downMasterEntities =iAcademyVisitsSevice.getInstituteType();
		List<DropDownSubMasterEntity> dropDownSumMasterList = iAcademyVisitsSevice.getDropDownSumMasterList();
		return new ResponseEntity<List>(dropDownSumMasterList,HttpStatus.OK);
	}
	
	@RequestMapping(value="/institutemastercreate",method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> instituteMasterFormPost(@RequestBody InstituteMasterEntity instituteMasterentity){
		instituteMasterentity.setStatus("active");
		String message  = iAcademyVisitsSevice.saveInstituteMasterDetails(instituteMasterentity);
		return new ResponseEntity<>(message,HttpStatus.OK);
	}
	
	@RequestMapping(value="/updateinstitutemasterpost",method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> updateinstitutemasterpost(@RequestBody InstituteMasterEntity instituteMasterentity){
		String message  = iAcademyVisitsSevice.updateInstituteMasterDetails(instituteMasterentity);
		return new ResponseEntity<>(message,HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/institutemasterlist")
	@ResponseBody
	public ResponseEntity<List<InstituteMasterEntity>> instituteMasterList(){
		List<InstituteMasterEntity> list = iAcademyVisitsSevice.getInstituteMasterList("");
		return new ResponseEntity<>(list,HttpStatus.OK);
	}
	
	@RequestMapping(value="/updatestatus/{status}",method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> updateStatusAction(@PathVariable String status,@RequestBody String[] instituteId)
	{
		String msg = null;
		if(status.equals("Active")){
			for(int i=0;i<instituteId.length;i++)
			{
				int instituteId1   = Integer.parseInt(instituteId[i]);
				String status1 = status;
				InstituteMasterEntity entity = new InstituteMasterEntity();
				entity.setInstituteId(instituteId1);
				entity.setStatus(status1);
				msg = iAcademyVisitsSevice.deactivateInstitute(entity);
			}	
		}else if(status.equals("InActive")){
			String status2 = status;
			for(int i=0;i<instituteId.length;i++)
			{
				int instituteId1   = Integer.parseInt(instituteId[i]);
				InstituteMasterEntity entity = new InstituteMasterEntity();
				entity.setInstituteId(instituteId1);
				entity.setStatus(status2);
				msg = iAcademyVisitsSevice.deactivateInstitute(entity);
			}
		}
		return new ResponseEntity<>(msg,HttpStatus.OK);			
	}
		
	@RequestMapping(value="/institutemasterlist/{status}",method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<List<InstituteMasterEntity>> instituteMasterList(@PathVariable String status){
		List<InstituteMasterEntity> list = iAcademyVisitsSevice.getInstituteMasterList(status);
		return new ResponseEntity<>(list,HttpStatus.OK);
	}
	
	@RequestMapping(value="/getInstituteNames")
	public ResponseEntity<List<InstituteMasterEntity>> getInstituteNames(){
		List<InstituteMasterEntity> list = iAcademyVisitsSevice.getInstituteMasterList("");
		return new ResponseEntity<>(list,HttpStatus.OK);	
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value="/getContactPersons")
	public ResponseEntity getContactPersons(@PathParam("instituteId") Long instituteNameId){
	List list =	iAcademyVisitsSevice.getContactPersons(instituteNameId);
		return new ResponseEntity(list,HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/masterinfoedit/{instituteId}")
	@ResponseBody
	public  ResponseEntity<InstituteMasterEntity> editInstituteMaster(@PathVariable int instituteId){
		InstituteMasterEntity instituteMasterEntity = iAcademyVisitsSevice.editInstituteMaster(instituteId);
		return new ResponseEntity<>(instituteMasterEntity,HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/getContactInfo/{instituteId}")
	@ResponseBody
	public  ResponseEntity<List> getContactInfo(@PathVariable int instituteId){
		List list = iAcademyVisitsSevice.getContactInfo(instituteId);
		return new ResponseEntity<>(list,HttpStatus.OK);
	}
	
	@RequestMapping(value="/saveVisitEntryDetails",method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> saveVisitEntryDetails(@RequestBody VisitEntryEntity visitEntryEntity){
		String message  = iAcademyVisitsSevice.saveVisitEntryDetails(visitEntryEntity);
		return new ResponseEntity<>(message,HttpStatus.OK);
	}
	
	@RequestMapping(value="/visitreport")
	@ResponseBody
	public ResponseEntity<List<VisitEntryEntity>> getInstituteNames(@RequestBody VisitEntryEntity visitEntryEntity){
		List<VisitEntryEntity> list = iAcademyVisitsSevice.getvisitreport(visitEntryEntity);
		return new ResponseEntity<>(list,HttpStatus.OK);	
	}
	
	@RequestMapping(value="/scheduledvisitreport", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<List<VisitEntryEntity>> getscheduledvisitreport(@RequestBody VisitEntryEntity visitEntryEntity){
		List<VisitEntryEntity> list = iAcademyVisitsSevice.getscheduledvisitreport(visitEntryEntity);
		return new ResponseEntity<>(list,HttpStatus.OK);	
	}
	
}
