package com.leonia.academy.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.leonia.academy.entity.Certificatemaster;
import com.leonia.academy.entity.Certificatetaken;
import com.leonia.academy.entity.StudentAdmissionEntity;
import com.leonia.academy.isevice.IStudentAdmissionService;

@RestController
@RequestMapping("/document")
public class DocumenttakenController {
	
@Autowired
private IStudentAdmissionService iStudentAdmissionService;
@RequestMapping("/searchStudents")
@ResponseBody
public ResponseEntity<List<StudentAdmissionEntity>> getCourseListDetails() {
	StudentAdmissionEntity studentAdmission=new StudentAdmissionEntity();
	List<StudentAdmissionEntity> listOfstudentAdmission =iStudentAdmissionService.getStudents(studentAdmission);
	if (listOfstudentAdmission.size() == 0) {
		return new ResponseEntity<>(listOfstudentAdmission, HttpStatus.NO_CONTENT);
	} else {
		return new ResponseEntity<>(listOfstudentAdmission, HttpStatus.OK);
	}
}

@RequestMapping(value="/getCertificatesList/{admissionId}")
@ResponseBody
public ResponseEntity<List<Certificatemaster>> getCertificates(@PathVariable Long admissionId) {
	List<Certificatemaster> list =iStudentAdmissionService.getCertificates(admissionId);
	if (list.size() == 0) {
		return new ResponseEntity<>(list, HttpStatus.NO_CONTENT);
	} else {
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

}

@RequestMapping(value="/getStudentDetailsById/{admissionId}")
@ResponseBody
public ResponseEntity<List<StudentAdmissionEntity>> getStudentDetailsById(@PathVariable Long admissionId) {
	
	List<StudentAdmissionEntity> leadFollowUpDTO =iStudentAdmissionService.getStudentDetailsById(admissionId);
	if (leadFollowUpDTO.size() == 0) {
		return new ResponseEntity<>(leadFollowUpDTO, HttpStatus.NO_CONTENT);
	} else {
		return new ResponseEntity<>(leadFollowUpDTO, HttpStatus.OK);
	}

}

@RequestMapping(value="/getCertificatesByid/{admissionId}")
@ResponseBody
public ResponseEntity<List<Certificatetaken>> getCertificatesByid(@PathVariable Long admissionId) {
	List<Certificatetaken> list=iStudentAdmissionService.getCertificatesByid(admissionId);
	if (list.size() == 0) {
		return new ResponseEntity<>(list, HttpStatus.NO_CONTENT);
	} else {
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

}


@RequestMapping(value="/printCertificates/{admissionId}")
@ResponseBody
public ResponseEntity<List> printCertificates(@PathVariable Long admissionId) {
	@SuppressWarnings("rawtypes")
	List list=iStudentAdmissionService.printCertificates(admissionId);
	if (list.size() == 0) {
		return new ResponseEntity<>(list, HttpStatus.NO_CONTENT);
	} else {
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

}

@RequestMapping(value="/addCertificateDetails",method=RequestMethod.POST)
@ResponseBody
public ResponseEntity<String> addCertificateDetails(@RequestBody List<Certificatetaken> certificateArrayDetails,HttpServletRequest request){
	
	String	msg=null;
	HttpSession session1 = request.getSession();
	String username = (String) session1.getAttribute("username");
	Long addmissionid1=certificateArrayDetails.get(0).getSadmissionid();
	for(Certificatetaken certificatetaken:certificateArrayDetails)
	{
		String sname=certificatetaken.getStudentname();
		Long addmissionid=certificatetaken.getSadmissionid();
		String addmissionno=certificatetaken.getSadmissionno();
		String batchname=certificatetaken.getSbatchname();
		String certificatename=certificatetaken.getScertificatename();
		String certificateno=certificatetaken.getCertificateno();
		String imgname=certificatetaken.getFileName();
		String course=certificatetaken.getCourse();
		String username11 = (String) session1.getAttribute("username"); 
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
  	    Date date =Calendar.getInstance().getTime(); 
  	    certificatetaken.setReturnedon("NR");
	 	certificatetaken.setCourse(course);
	 	certificatetaken.setSadmissionid(addmissionid);
	 	certificatetaken.setSadmissionno(addmissionno);
	 	certificatetaken.setStudentname(sname);
	 	certificatetaken.setSbatchname(batchname);
	 	certificatetaken.setScertificatename(certificatename);
	 	certificatetaken.setCertificateno(certificateno); 
	 	certificatetaken.setFileName(imgname);
	    certificatetaken.setData(imgname.getBytes()); 
	    certificatetaken.setStatus("Held");
	    certificatetaken.setCreatdedon(date);	
	    msg = iStudentAdmissionService.addCertificateDetails(certificatetaken);
	}
	return new ResponseEntity<>(msg,HttpStatus.OK);
   
}
}
