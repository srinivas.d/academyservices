package com.leonia.academy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.leonia.academy.entity.LeadFollowUpDTO;
import com.leonia.academy.isevice.ILeadGeneratedReportService;


@RestController
@RequestMapping(value = "/leadlocation")
public class LeadGeneratedReportController {
	
	@Autowired
	private ILeadGeneratedReportService iLeadGeneratedReportService;
	
	@RequestMapping("/getLeadDetails")
	@ResponseBody
	public ResponseEntity<List<LeadFollowUpDTO>> getLeadDetails() {
		LeadFollowUpDTO leadFollowUpDTO=new LeadFollowUpDTO();
		List<LeadFollowUpDTO> dropdownMasterSourcelist =iLeadGeneratedReportService.getLeadDetails(leadFollowUpDTO);
		if (dropdownMasterSourcelist.size() == 0) {
			return new ResponseEntity<>(dropdownMasterSourcelist, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(dropdownMasterSourcelist, HttpStatus.OK);
		}

	}
	
	@RequestMapping(value="/searchLeadList",method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<List<LeadFollowUpDTO>> searchLeadList(@RequestBody LeadFollowUpDTO leadFollowUpDTO ) { 			
		
		List<LeadFollowUpDTO> leadDetails =iLeadGeneratedReportService.searchLeadList(leadFollowUpDTO);
		if (leadDetails.size() == 0) {
			return new ResponseEntity<>(leadDetails, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(leadDetails, HttpStatus.OK);
		}
	}

}
