package com.leonia.academy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.leonia.academy.entity.BatchMaster;
import com.leonia.academy.entity.Certificatetaken;
import com.leonia.academy.entity.StudentAdmissionEntity;
import com.leonia.academy.isevice.IStudentAdmissionService;

@RestController
@RequestMapping("/document")
public class DocumentGivenController {

	@Autowired
	private IStudentAdmissionService iStudentAdmissionService;
	
	@RequestMapping(value="/dropdownValueForBatch")
	@ResponseBody
	public ResponseEntity<List<BatchMaster>> dropdownValueForBatch()
	{
		List<BatchMaster> batchlist=iStudentAdmissionService.getBatchname();
		if (batchlist.size() == 0) {
			return new ResponseEntity<>(batchlist, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(batchlist, HttpStatus.OK);
		}

	}
	@RequestMapping(value="/dropdownValueForStudent/{batchid}")
	@ResponseBody
	public ResponseEntity<List<StudentAdmissionEntity>> dropdownValueForStudent(@PathVariable Long batchid) {
		List<StudentAdmissionEntity> list =iStudentAdmissionService.dropdownValueForStudent(batchid);
		if (list.size() == 0) {
			return new ResponseEntity<>(list, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(list, HttpStatus.OK);
		}

	}
	@RequestMapping(value="/getStudentDetailsByIdForReturn/{sid}")
	@ResponseBody
	public ResponseEntity<List<Certificatetaken>> getStudentDetailsByIdForReturn(@PathVariable Long sid)
	{
		List<Certificatetaken> list= iStudentAdmissionService.getStudentDetailsByIdForReturn(sid);		
		if (list.size() == 0) {
			return new ResponseEntity<>(list, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(list, HttpStatus.OK);
		}

	}
	
	
	@RequestMapping(value="/updatecertificatestatus/{sid}/{studdid}/{username}")
	@ResponseBody
	public ResponseEntity<List<Certificatetaken>> updatecertificatestatus(@PathVariable String sid,@PathVariable Long studdid,@PathVariable String username){
		String str[]=sid.split(",");
		for(int i=0;i<str.length;i++)
		{
			iStudentAdmissionService.updateStatusDate(str[i],username);
		}
		List<Certificatetaken> list= iStudentAdmissionService.getStudentDetailsByIdForReturn(studdid);	
		if (list.size() == 0) {
			return new ResponseEntity<>(list, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(list, HttpStatus.OK);
		}

	}
	

}
