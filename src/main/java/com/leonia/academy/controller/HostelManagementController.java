package com.leonia.academy.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.leonia.academy.entity.Bedmaster;
import com.leonia.academy.entity.Blockmaster;
import com.leonia.academy.entity.Buildingmaster;
import com.leonia.academy.entity.Floormaster;
import com.leonia.academy.entity.HostelFeeAssingToStudent;
import com.leonia.academy.entity.InstallmentHostelFee;
import com.leonia.academy.entity.InstallmentMaster;
import com.leonia.academy.entity.RoomAllotment;
import com.leonia.academy.entity.RoomCategory;
import com.leonia.academy.entity.Roomdeallocationmaster;
import com.leonia.academy.entity.Roommaster;
import com.leonia.academy.entity.StudentAdmissionEntity;
import com.leonia.academy.entity.Totalroomallocatiodetails;
import com.leonia.academy.isevice.IHostelManagementService;

@RestController
@RequestMapping("/hostelmanagement")
public class HostelManagementController {
	
	@Autowired
	private IHostelManagementService hostelManagementService;
	
	@RequestMapping("/searchStudents")
	@ResponseBody
	public ResponseEntity<List<StudentAdmissionEntity>> hostelregistrationForm() {
		List<StudentAdmissionEntity> list =hostelManagementService.getStayCategoryStudents();
		if (list.size() == 0) {
			return new ResponseEntity<>(list, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(list, HttpStatus.OK);
		}
	}
	
	@RequestMapping("/buildingdetails/{gender}")
	@ResponseBody
	public ResponseEntity<List<Buildingmaster>> getBuildingdetails(@PathVariable String gender) {
	 	List<Buildingmaster> list =hostelManagementService.getBuildingdetails(gender);
		if (list.size() == 0) {
			return new ResponseEntity<>(list, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(list, HttpStatus.OK);
		}
	}
	@RequestMapping("/blockdetails/{buildingid}")
	@ResponseBody
	public ResponseEntity<List<Blockmaster>> getBuildingdetails(@PathVariable Integer buildingid) {
	 	List<Blockmaster> list =hostelManagementService.getBlockdetails(buildingid);
		if (list.size() == 0) {
			return new ResponseEntity<>(list, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(list, HttpStatus.OK);
		}
	}
	@RequestMapping("/floordetails/{blockid}")
	@ResponseBody
	public ResponseEntity<List<Floormaster>> getFloordetails(@PathVariable Integer blockid) {
	 	List<Floormaster> list =hostelManagementService.getFloordetails(blockid);
		if (list.size() == 0) {
			return new ResponseEntity<>(list, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(list, HttpStatus.OK);
		}
	}
	@RequestMapping("/roomdetails/{floorid}")
	@ResponseBody
	public ResponseEntity<List<Roommaster>> getRoomdetails(@PathVariable Integer floorid) {
	 	List<Roommaster> list =hostelManagementService.getRoomdetails(floorid);
		if (list.size() == 0) {
			return new ResponseEntity<>(list, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(list, HttpStatus.OK);
		}
	}
	@RequestMapping("/beddetails/{roomid}")
	@ResponseBody
	public ResponseEntity<List<Bedmaster>> getBeddetails(@PathVariable Integer roomid) {
	 	List<Bedmaster> list =hostelManagementService.getBeddetails(roomid);
		if (list.size() == 0) {
			return new ResponseEntity<>(list, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(list, HttpStatus.OK);
		}
	}
	@RequestMapping("/allocate")
	@ResponseBody
	public ResponseEntity<String> allocateRoom(@RequestBody RoomAllotment roomallot) {
	   String msg=hostelManagementService.allocation(roomallot);
	   return new ResponseEntity<>(msg, HttpStatus.OK);
	}
	@RequestMapping("/totalroomallocationdetails/{admissionNumberId}")
	@ResponseBody
	public ResponseEntity<String> totalAllocateRoomDetails(@RequestBody Totalroomallocatiodetails totalroomallocationdetails,@PathVariable Long admissionNumberId) {
	   String msg=hostelManagementService.totalroomallocationdetails(totalroomallocationdetails,admissionNumberId);
	   return new ResponseEntity<>(msg, HttpStatus.OK);
	}
	@RequestMapping("/generatereceipt/{admissionid}")
	@ResponseBody
	public ResponseEntity<List<Map<String,Object>>> generatereceipt(@PathVariable Long admissionid) {
	  List<Map<String,Object>> list=hostelManagementService.generatereceipt(admissionid);
	   if (list.size() == 0) {
			return new ResponseEntity<>(list, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(list, HttpStatus.OK);
		}
	}
	@RequestMapping("/snapshot")
	@ResponseBody
	public ResponseEntity<List> getSnapshot() {
	  List list=hostelManagementService.getSnapshot();
	   if (list.size() == 0) {
			return new ResponseEntity<>(list, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(list, HttpStatus.OK);
		}
	}
	@RequestMapping("/getAllocatedStudents")
	@ResponseBody
	public ResponseEntity<List> getAllocatedStudents() {
	List list=hostelManagementService.getAllocatedStudents();
	   if (list.size() == 0) {
			return new ResponseEntity<>(list, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(list, HttpStatus.OK);
		}
	}
	@RequestMapping("/getItems/{roomno}")
	@ResponseBody
	public ResponseEntity<String> getItems(@PathVariable String roomno) {
	String items=hostelManagementService.getItems(roomno);
	 return new ResponseEntity<>(items, HttpStatus.OK);
	}
	@RequestMapping("/deallocate/{admissionid}/{roomno}/{bedno}")
	@ResponseBody
	public ResponseEntity<String> deallocate(@RequestBody Roomdeallocationmaster roomdeallocationmaster ,@PathVariable String roomno,@PathVariable String bedno,@PathVariable Long admissionid) {
	 String msg=hostelManagementService.deallocate(roomno,bedno,admissionid,roomdeallocationmaster);
	 return new ResponseEntity<>(msg, HttpStatus.OK);
	}
	@RequestMapping("/getFeeAmountdata/{admissionid}")
	@ResponseBody
	public ResponseEntity<List> getFeeAmountdata(@PathVariable Long admissionid) {
	List list=hostelManagementService.getFeeAmountdata(admissionid);
	   if (list.size() == 0) {
			return new ResponseEntity<>(list, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(list, HttpStatus.OK);
		}
	}
	
	@ResponseBody
	@RequestMapping(value="getBuildingMaster")
	public List<Buildingmaster> getBuildingMaster() {
		List<Buildingmaster> list = hostelManagementService.getBuildingMaster();
		return list;
	}
	
	
	@ResponseBody
	@RequestMapping(value="getRoomCategory")
	public List<RoomCategory> getRoomCategory() {
		List<RoomCategory> list = hostelManagementService.getRoomCategory();
		return list;
	}
	
	@ResponseBody
	@RequestMapping(value="getInstallment")
	public List<InstallmentMaster> getInstallment(){
		List<InstallmentMaster> list =hostelManagementService.getInstallment();
		return list;
	}
	
	@ResponseBody
	@RequestMapping(value="getbuildingdefinefee")
	public List<InstallmentHostelFee> getbuildingdefinefee(@RequestBody InstallmentHostelFee fee){
		List<InstallmentHostelFee> list =hostelManagementService.getbuildingdefinefee(fee);
		return list;
	}
	
	
	@ResponseBody
	@RequestMapping(value="savedefineHostelfee")
	public String savedefineHostelfee(@RequestBody InstallmentHostelFee fee){
		String msg = hostelManagementService.savedefineHostelfee(fee);
		return msg;
	}
	
	@ResponseBody
	@RequestMapping(value="getBlockdetails/{buildingid}")
	public List<Blockmaster> getBlockdetails(@PathVariable Integer buildingid ) {
		List<Blockmaster> list = hostelManagementService.getBlockdetails123(buildingid);
		return list;
	}
	
	@ResponseBody
	@RequestMapping(value="getbuildingfeeStudents")
	public List<StudentAdmissionEntity> getbuildingfeeStudents(@RequestBody InstallmentHostelFee fee){
		List<StudentAdmissionEntity> list = hostelManagementService.getbuildingfeeStudents(fee);
		return list;
	}
	
	
	@ResponseBody
	@RequestMapping(value="assignedtohostelfees")
	public String assignedtohostelfees(@RequestBody HostelFeeAssingToStudent[] hostelFeeAssingToStudent){
		String msg = hostelManagementService.assignedtohostelfees(hostelFeeAssingToStudent);
		return msg;
		
	}
	
	
}


