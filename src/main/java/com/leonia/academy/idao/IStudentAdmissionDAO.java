package com.leonia.academy.idao;

import java.util.List;

import com.leonia.academy.entity.BatchMaster;
import com.leonia.academy.entity.Certificatemaster;
import com.leonia.academy.entity.Certificatetaken;
import com.leonia.academy.entity.StudentAdmissionEntity;

public interface IStudentAdmissionDAO {

	List<StudentAdmissionEntity> getStudents(StudentAdmissionEntity studentAdmission);

	List<Certificatemaster> getCertificates(Long admissionId);

	List<StudentAdmissionEntity> getStudentDetailsById(Long admissionId);

	String addCertificateDetails(Certificatetaken certificatetaken);

	List<Certificatetaken> getCertificatesByid(Long admissionId);

	List printCertificates(Long admissionId);

	List<BatchMaster> getBatchname();

	List<StudentAdmissionEntity> dropdownValueForStudent(Long batchid);

	List<Certificatetaken> getStudentDetailsByIdForReturn(Long sid);

	void updateStatusDate(String string, String username);

	List<Certificatetaken> getCheckedAndUnCheckedDocuments();

	List<Certificatetaken> getCheckincheckoutreportByDate(String startdate, String enddate);

	List<Certificatetaken> getCheckincheckoutreportByDate(Certificatetaken certificatetaken);

	StudentAdmissionEntity getStudentDetail(Long admissionNumberId);

	BatchMaster getBatchName(long batchId);


	


	
}
