package com.leonia.academy.idao;

import java.util.List;

import com.leonia.academy.entity.DropdownMasterCourseEntity;
import com.leonia.academy.entity.DropdownMasterSourceEntity;
import com.leonia.academy.entity.LeadFollowUpDTO;
import com.leonia.academy.entity.LeadfollowupdetailsDTO;

public interface ILeadinSertionDAO {

	public void leadinsertion(LeadfollowupdetailsDTO leadfollowupdetailsDTO);

	public List<DropdownMasterSourceEntity> MasterSource(DropdownMasterSourceEntity dropdownMasterSourceDTO);

	public List<DropdownMasterCourseEntity> MasterCourse(DropdownMasterCourseEntity dropdownMasterCourseDTO);

	public String saveleaddetails(LeadFollowUpDTO leadFollowUpDTO);

}
