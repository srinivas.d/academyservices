package com.leonia.academy.idao;

import java.util.List;

import com.leonia.academy.entity.CompanyDetails;
import com.leonia.academy.entity.InterviewAssignToStudents;
import com.leonia.academy.entity.PlacementDesignationMaster;
import com.leonia.academy.entity.ReasonsMaster;
import com.leonia.academy.entity.StatusMaster;
import com.leonia.academy.entity.VacancyDetails;
import com.leonia.academy.entity.VacancyInfo;

public interface IPlacementsDAO {

	List getDomainMaster();

	String saveCompanyDetails(CompanyDetails companyDetails);

	List getAllCompaniesList();

	CompanyDetails editCompanyDetailsByCompanyDetailsId(Long companyDetailsId);

	String updateCompanyDetails(CompanyDetails companyDetails);

	String saveVacancyDetails(VacancyInfo vacancyInfo);

	List getDepartmentMaster();

	List getDesignationIdByDepartmentId(Long departmentId);

	List getAllVacanciesList();

	VacancyInfo editVacancyInfoByVacancyInfoId(Long vacancyInfoId);

	String updateVacancyDetails(VacancyInfo vacancyInfo);

	List<PlacementDesignationMaster> getDesignationMaster();

	List viewvacancyDetailsGrid();

	List getVacancyDetailsById(Long vacanyDetailsId);

	Integer AssignedStudentsCount(Long vacanyDetailsId);

	String interviewAssignedToStudents(InterviewAssignToStudents assignToStudents);

	List<VacancyInfo> getVacancyDetailsByCompanyDetailsId(Long companyDetailsId);

	List<VacancyDetails> getDepartmentsByVacancyInfoId(Long vacancyInfoId);

	List<InterviewAssignToStudents> getStudentsByVacancyDetailsId(Long vacanyDetailsId);

	ReasonsMaster getReasonsByDepartmentId(Long departmentId);

	List<StatusMaster> getStatusMaster();

	String saveDeclaredResults(InterviewAssignToStudents toStudents);

	String removeStudentsByVacancyDetailsId(Long id, Long vacanyDetailsId);

	List<InterviewAssignToStudents> getStudentsByVacancyDetailsIdFromAttendedInterViews(Long vacanyDetailsId);

	List getDepartmentsByVacancyInfoIdSize(Long vacancyInfoId);

	String closingInterviewVacancyByVacancyInfoId(Long vacancyInfoId, Long vacancyDetailsId);

	String closingInterviewVacancyByVacancyInfoIds(Long vacancyInfoId, Long vacancyDetailsId);

}
