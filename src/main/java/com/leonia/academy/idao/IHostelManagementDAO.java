package com.leonia.academy.idao;

import java.util.List;
import java.util.Map;

import com.leonia.academy.entity.Bedmaster;
import com.leonia.academy.entity.Blockmaster;
import com.leonia.academy.entity.Buildingmaster;
import com.leonia.academy.entity.Floormaster;
import com.leonia.academy.entity.HostelFeeAssingToStudent;
import com.leonia.academy.entity.InstallmentHostelFee;
import com.leonia.academy.entity.InstallmentMaster;
import com.leonia.academy.entity.RoomAllotment;
import com.leonia.academy.entity.RoomCategory;
import com.leonia.academy.entity.Roomdeallocationmaster;
import com.leonia.academy.entity.Roommaster;
import com.leonia.academy.entity.StudentAdmissionEntity;
import com.leonia.academy.entity.Totalroomallocatiodetails;

public interface IHostelManagementDAO {

	List<StudentAdmissionEntity> getStayCategoryStudents();

	List<Buildingmaster> getBuildingdetails(String gender);

	List<Blockmaster> getBlockdetails(Integer buildingid);

	List<Floormaster> getFloordetails(Integer blockid);

	List<Roommaster> getRoomdetails(Integer floorid);

	List<Bedmaster> getBeddetails(Integer roomid);

	String allocation(RoomAllotment roomallot);

	String totalroomallocationdetails(Totalroomallocatiodetails totalroomallocationdetails, Long admissionNumberId);

	List<Map<String, Object>> generatereceipt(Long admissionid);

	List getSnapshot();

	List getAllocatedStudents();

	String getItems(String roomno);

	String deallocate(String roomno, String bedno, Long admissionid, Roomdeallocationmaster roomdeallocationmaster);

	List getFeeAmountdata(Long admissionid);

	List<Buildingmaster> getBuildingMaster();

	List<RoomCategory> getRoomCategory();

	List<InstallmentMaster> getInstallment();

	List<InstallmentHostelFee> getbuildingdefinefee(InstallmentHostelFee fee);

	String savedefineHostelfee(InstallmentHostelFee fee);

	List<Blockmaster> getBlockdetails123(Integer buildingid);

	List getbuildingfeeStudents(InstallmentHostelFee fee);

	String assignedtohostelfees(HostelFeeAssingToStudent feeAssingToStudent);
	
}
