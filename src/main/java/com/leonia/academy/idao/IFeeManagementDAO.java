package com.leonia.academy.idao;

import java.util.List;

import com.leonia.academy.entity.CardTypeMaster;
import com.leonia.academy.entity.FeeAssignToStudents;
import com.leonia.academy.entity.FeeCreation;
import com.leonia.academy.entity.FeeGenerationBatchWise;
import com.leonia.academy.entity.HostelFeeAssingToStudent;
import com.leonia.academy.entity.PaymodeMaster;
import com.leonia.academy.entity.ReceiptGenerateDTO;
import com.leonia.academy.entity.StudentAdmissionEntity;

public interface IFeeManagementDAO {

	List<StudentAdmissionEntity> getStudentSearchByDiscountCategory(Long discountCategoryId, String searchName);

	List discountApprovedMaster();

	List studentFeediscountDetails(Long bacthId, Long admissionNumberId);

	List feeAssignToStudentsAmountParticularFee(Long feeAssignToStudentsId);

	List hostalfeeAssignToStudentsAmountParticularFee(Long hostelFeeAssingToStudentId);

	String discountStuParticularfee(FeeAssignToStudents objStudentCategory);

	String discountStuHostelFee(FeeAssignToStudents objStudentCategory);

	String hostelFeeAssignToStudentPaymentUpdate(HostelFeeAssingToStudent hostelFeeAssingToStudent);

	List<StudentAdmissionEntity> getStudentListSearch(String searchName);

	List getFeeGenerationdetails(Long admissionNumberId, Long bacthId);

	List<HostelFeeAssingToStudent> gethostelFeeGenerationdetails(Long admissionNumberId);

	List<CardTypeMaster> getCardTypeMaster();

	List<PaymodeMaster> getPaymentModeTypes();

	List getfeepaidreceptdetails(Long admissionNumberId);

	List<ReceiptGenerateDTO> getfeepaidreceptdetails(Long admissionNumberId, Long receiptGenerateId);

	String savePaymentDetails(ReceiptGenerateDTO receiptgeneratedtoObj);

	String feeassigntostudentPaymentupdate(List<FeeAssignToStudents> feesettlementdtolist);

	List getFeeDetailsById(Integer batchId);

	List feeCategory();

	List getCategoryNameByBatchId(Integer batchId);

	String saveStudentFee(FeeCreation fee);

	List getInstallmentTypes(Long batchId);

	List getInstallmentTypesdrop(Long batchId);

	List<FeeCreation> getFeeCreationBatchWiseListByBatchId(Long batchId, Long installmentTypeMasterId);

	List getAmountByParticularFee(Long feeCreationId);

	String savefeeGenarationBatchWise(FeeGenerationBatchWise generationBatchWise);

	List getFeeGenarationFeeListInstallment123(Long batchId, Long feeGenerationBatchWiseId,
			Long installmentTypeMasterId);

	List getFeeGenarationFeeListInstallment(Long batchId, Long feeGenerationBatchWiseId);

	List getStudentsListByBatchId(Long batchId, Long feeGenerationBatchWiseId, Long installmentTypeMasterId);

	String feeAssignToStudentsPost(FeeAssignToStudents feeAssignToStudents);

}
