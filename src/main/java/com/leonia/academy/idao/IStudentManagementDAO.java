package com.leonia.academy.idao;

import java.util.List;

import com.leonia.academy.entity.CalenderEvents;
import com.leonia.academy.entity.CourseMaster;
import com.leonia.academy.entity.EducationEntity;
import com.leonia.academy.entity.Eventtype;
import com.leonia.academy.entity.ParentGuardianEntity;
import com.leonia.academy.entity.RelationMaster;
import com.leonia.academy.entity.StudentAdmissionEntity;
import com.leonia.academy.entity.UniversityMaster;

public interface IStudentManagementDAO {

	List getNationalityMaster();

	List bloodGroupMaster();

	List stayCategoryMaster();

	List discountCategoryMaster();

	List religionMaster();

	List communityMaster();

	List languageMaster();

	List cityMaster();

	List getStateCountryByCityId(int cityId);

	List courseMaster();

	List getBatchNameByCourseId(Long courseId);

	List getBatchFeeByBatchId(Long batchId);

	String saveStudentAdmissionDetails(StudentAdmissionEntity studentAdmissionEntity);

	List getviewstudentDetails();

	StudentAdmissionEntity editStudentAdmissionDetails(Long admissionNumberId);

	String updateStudentAdmissionDetails(StudentAdmissionEntity studentAdmissionEntity);

	Long getAdmissionNumberId();

	List feeCategoryMaster();

	List<RelationMaster> relationsList();

	List<ParentGuardianEntity> parentDetails(Long admissionNumberId);

	String saveorupdateParentGuardian(ParentGuardianEntity parentGuardianEntity);

	List<Eventtype> geteventtypelist();

	List<CalenderEvents> getlistcalendereventdate();

	String insertcalendereventform(CalenderEvents calenderEvents);

	StudentAdmissionEntity getStudentDetailsByAdmissionId(Long admissionId);

	List getEducatioDetails(Long admissionId);

	String storeeducationdetails(EducationEntity educationEntity);

	List<UniversityMaster> universityMaster();

	List<StudentAdmissionEntity> getStudentsByBatchForAssignInterviews(Long vacanyDetailsId, Long batchId);

	List<CourseMaster> getCourseByUniversity(Long universityMasterId);

}
