package com.leonia.academy.idao;

import java.util.List;

import com.leonia.academy.entity.LoginEntity;

public interface ILoginDAO {

	LoginEntity getlogindetailes(String username);

	List<LoginEntity> loginCheck(LoginEntity login);

}
