package com.leonia.academy.idao;

import java.util.List;

import com.leonia.academy.entity.ActiveDTO;
import com.leonia.academy.entity.LeadFollowUpDTO;

public interface ILeadGeneratedReportDAO {

	List<ActiveDTO> activelist();

	List<LeadFollowUpDTO> getLeadDetails(LeadFollowUpDTO leadFollowUpDTO);

	//List<LeadFollowUpDTO> searchLeadList12(String sdate, String edate);

	List<LeadFollowUpDTO> searchLeadList(LeadFollowUpDTO leadFollowUpDTO);

}
