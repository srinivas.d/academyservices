package com.leonia.academy.idao;

import java.util.List;


import com.leonia.academy.entity.DropdownMasterCourseEntity;
import com.leonia.academy.entity.LeadFollowUpDTO;
import com.leonia.academy.entity.LoginEntity;

public interface IFollowupDetailsDAO {

	List<LeadFollowUpDTO> getfollowupdetails(Integer leadid);

	List<LoginEntity> getUsernameList();

	String saveNewFollowUp(LeadFollowUpDTO leadFollowUpDTO);

	List<LeadFollowUpDTO> getNewFollowUpDetails(LeadFollowUpDTO leadFollowUpDTO);

	List<LeadFollowUpDTO> getFollowUpDetails();

	List<LeadFollowUpDTO> getOpenLeadFoliowupDetails();

	List<LeadFollowUpDTO> getFollowupDetailsByDate(LeadFollowUpDTO leadFollowUpDTO);

	List<LeadFollowUpDTO> getFollowUpDetailsById(Integer followupId);

	List endlead(LeadFollowUpDTO leadFollowUpDTO);


	

	

	
}
