package com.leonia.academy.serviceimpl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leonia.academy.entity.BatchMaster;
import com.leonia.academy.entity.CardTypeMaster;
import com.leonia.academy.entity.FeeAssignToStudents;
import com.leonia.academy.entity.FeeCreation;
import com.leonia.academy.entity.FeeGenerationBatchWise;
import com.leonia.academy.entity.FeePaidTransactionsDTO;
import com.leonia.academy.entity.FeeSettlementDTO;
import com.leonia.academy.entity.HostelFeeAssingToStudent;
import com.leonia.academy.entity.PaymodeMaster;
import com.leonia.academy.entity.ReceiptGenerateDTO;
import com.leonia.academy.entity.StudentAdmissionEntity;
import com.leonia.academy.idao.IFeeManagementDAO;
import com.leonia.academy.idao.IStudentAdmissionDAO;
import com.leonia.academy.isevice.IFeeManagementService;

/**
 * @author srinivas.d
 *
 */
@Service
public class FeeManagementServiceImpl implements IFeeManagementService {
	
	@Autowired
	private IFeeManagementDAO iFeeManagementDAO;
	
	@Autowired
	private IStudentAdmissionDAO iStudentAdmissionDAO;
	
	private static final Logger logger = Logger.getLogger(FeeManagementServiceImpl .class);
	
	
	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#getStudentSearchByDiscountCategory(java.lang.Long, java.lang.String)
	 */
	@Override
	public List<StudentAdmissionEntity> getStudentSearchByDiscountCategory(Long discountCategoryId, String searchName) {
		return iFeeManagementDAO.getStudentSearchByDiscountCategory(discountCategoryId,searchName);
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#discountApprovedMaster()
	 */
	@Override
	public List discountApprovedMaster() {
		return iFeeManagementDAO.discountApprovedMaster();
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#studentFeediscountDetails(java.lang.Long, java.lang.Long)
	 */
	@Override
	public List studentFeediscountDetails(Long bacthId, Long admissionNumberId) {
		return iFeeManagementDAO.studentFeediscountDetails(bacthId,admissionNumberId);
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#feeAssignToStudentsAmountParticularFee(java.lang.Long)
	 */
	@Override
	public List feeAssignToStudentsAmountParticularFee(Long feeAssignToStudentsId) {
		// TODO Auto-generated method stub
		return iFeeManagementDAO.feeAssignToStudentsAmountParticularFee(feeAssignToStudentsId);
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#hostalfeeAssignToStudentsAmountParticularFee(java.lang.Long)
	 */
	@Override
	public List hostalfeeAssignToStudentsAmountParticularFee(Long hostelFeeAssingToStudentId) {
		return iFeeManagementDAO.hostalfeeAssignToStudentsAmountParticularFee(hostelFeeAssingToStudentId);
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#discountStuParticularfee(com.leonia.academy.entity.FeeAssignToStudents)
	 */
	@Override
	public String discountStuParticularfee(FeeAssignToStudents objStudentCategory) {
		return iFeeManagementDAO.discountStuParticularfee(objStudentCategory);
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#discountStuHostelFee(com.leonia.academy.entity.FeeAssignToStudents)
	 */
	@Override
	public String discountStuHostelFee(FeeAssignToStudents objStudentCategory) {
		return iFeeManagementDAO.discountStuHostelFee(objStudentCategory);
	}
	
	
	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#getStudentListSearch(java.lang.String)
	 */
	@Override
	public List<StudentAdmissionEntity> getStudentListSearch(String searchName) {
		logger.info("Fetching StudentAdmission Data using SearchName:"+searchName);
		List<StudentAdmissionEntity> list = iFeeManagementDAO.getStudentListSearch(searchName);
		List<StudentAdmissionEntity> list1 = new ArrayList<>();
		try{
		for(int i=0;i<list.size();i++){
			StudentAdmissionEntity admissionEntity = new StudentAdmissionEntity();
			admissionEntity.setAdmissionNumberId(list.get(i).getAdmissionNumberId());
			admissionEntity.setAdmissionNumber(list.get(i).getAdmissionNumber());
			admissionEntity.setStudentFirstName(list.get(i).getStudentFirstName());
			admissionEntity.setStudentMiddleName(list.get(i).getStudentMiddleName());
			admissionEntity.setStudentLastName(list.get(i).getStudentLastName());
			admissionEntity.setGender(list.get(i).getGender());
			BatchMaster batchMaster = new BatchMaster();
			batchMaster.setBatchId(list.get(i).getBatchMaster().getBatchId());
			batchMaster.setBatchName(list.get(i).getBatchMaster().getBatchName());
			admissionEntity.setBatchMaster(batchMaster);
			list1.add(admissionEntity);
		}
		logger.debug("Interacted with database to fatch StudentAdmission:"+list1);
		}catch (Exception e) {
			logger.error("Exception raised in getstudentlistsearch Method in FeeManagementServiceImpl:", e);
		}
		return list1;
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#getFeeGenerationdetails(java.lang.Long, java.lang.Long)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<FeeAssignToStudents> getFeeGenerationdetails(Long admissionNumberId, Long bacthId) {
		logger.info("Fetching FeeGeneration Data using admissionNumberId&bacthId:"+admissionNumberId+""+bacthId);
		List list = null;
		try{
			list =  iFeeManagementDAO.getFeeGenerationdetails(admissionNumberId,bacthId);
		logger.debug("Interacted with database to fatch FeeGeneration:"+list);
		}catch (Exception e) {
		logger.error("Exception raised in feeGenerationdetails Method in FeeManagementServiceImpl:", e);
	}
		return list; 
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#gethostelFeeGenerationdetails(java.lang.Long)
	 */
	@Override
	public List<HostelFeeAssingToStudent> gethostelFeeGenerationdetails(Long admissionNumberId) {
		logger.info("Fetching HostelFeeAssingToStudent Data using admissionNumberId:"+admissionNumberId);
		List<HostelFeeAssingToStudent> feeGenerationBatchWises2 =null;
		try{
		feeGenerationBatchWises2 =  iFeeManagementDAO.gethostelFeeGenerationdetails(admissionNumberId);
		if(feeGenerationBatchWises2.size() != 0){
			feeGenerationBatchWises2.get(0).setTermWiseMessage("Hostel Fee"+"-"+feeGenerationBatchWises2.get(0).getInstid());
		}
		logger.debug("Interacted with database to fatch HostelFeeAssingToStudent:"+feeGenerationBatchWises2);
		}catch (Exception e) {
			logger.error("Exception raised in gethostelFeeGenerationdetails Method in FeeManagementServiceImpl:", e);
		}
		return feeGenerationBatchWises2;
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#getPaymentModeTypes()
	 */
	@Override
	public List<PaymodeMaster> getPaymentModeTypes() {
		logger.info("Fetching PaymodeMaster Data");
		List<PaymodeMaster> list = null;
			try{	
				list = iFeeManagementDAO.getPaymentModeTypes();
				logger.debug("Interacted with database to fatch PaymodeMaster");
			}catch (Exception e) {
				logger.error("Exception raised in getPaymentModeTypes Method in FeeManagementServiceImpl:", e);
			}
		return list;
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#getCardTypeMaster()
	 */
	@Override
	public List<CardTypeMaster> getCardTypeMaster() {
		logger.info("Fetching CardTypeMaster Data");
		List<CardTypeMaster> list = null;
		try{	
			list = iFeeManagementDAO.getCardTypeMaster();
			logger.debug("Interacted with database to fatch CardTypeMaster");
		}catch (Exception e) {
			logger.error("Exception raised in getCardTypes Method in FeeManagementServiceImpl:", e);
		}
	return list;
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#getfeepaidreceptdetails(java.lang.Long)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public List getfeepaidreceptdetails(Long admissionNumberId) {
		logger.info("Fetching ReceiptGenerateEntity Data using admissionNumberId:"+admissionNumberId);
		List list = null;
		try{	
			list = iFeeManagementDAO.getfeepaidreceptdetails(admissionNumberId);
			logger.debug("Interacted with database to fatch ReceiptGenerateEntity:"+list);
		}catch (Exception e) {
			logger.error("Exception raised in getFeePaidReceiptsList Method in FeeManagementServiceImpl:", e);
		}
	return list;
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#generateReceipt(java.lang.Long, java.lang.Long)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public ReceiptGenerateDTO generateReceipt(Long receiptGenerateId, Long admissionNumberId) {
		logger.info("Fetching ReceiptGenerateEntity Data using admissionNumberId&receiptGenerateId:"+receiptGenerateId+""+admissionNumberId);
		ReceiptGenerateDTO receiptGenerateDTO  = null;
		try{
		List<ReceiptGenerateDTO> list= iFeeManagementDAO.getfeepaidreceptdetails(admissionNumberId,receiptGenerateId);
		for (ReceiptGenerateDTO receiptGenerateDTO2 : list) {
			receiptGenerateDTO = new ReceiptGenerateDTO();
			receiptGenerateDTO.setReceiptGenerateId(receiptGenerateDTO2.getReceiptGenerateId());
			receiptGenerateDTO.setPaidDate(receiptGenerateDTO2.getPaidDate());
			receiptGenerateDTO.setTotalAmount(receiptGenerateDTO2.getTotalAmount());
			List list1  =new ArrayList<>();
			for(int j=0;j<receiptGenerateDTO2.getFeePaidTransactionsDTO().size();j++){
			FeePaidTransactionsDTO dto = new FeePaidTransactionsDTO();
			dto.setTermWiseMessage(receiptGenerateDTO2.getFeePaidTransactionsDTO().get(j).getTermWiseMessage());
			dto.setPaidAmount(receiptGenerateDTO2.getFeePaidTransactionsDTO().get(j).getPaidAmount());
			list1.add(dto); 
			}
			receiptGenerateDTO.setFeePaidTransactionsDTO(list1);
			List list2  =new ArrayList<>();
			for(int k=0;k<receiptGenerateDTO2.getFeeSettlementDTO().size();k++){
			FeeSettlementDTO settlementDTO = new FeeSettlementDTO();
			PaymodeMaster master = new PaymodeMaster();
			master.setPayMode(receiptGenerateDTO2.getFeeSettlementDTO().get(k).getPaymodeMaster().getPayMode());
			settlementDTO.setPaymodeMaster(master);
			settlementDTO.setPaidAmount(receiptGenerateDTO2.getFeeSettlementDTO().get(k).getPaidAmount());
			list2.add(settlementDTO);
			}
			receiptGenerateDTO.setFeeSettlementDTO(list2);
		}
		StudentAdmissionEntity studentDetailsByAdmissionId = iStudentAdmissionDAO.getStudentDetail(admissionNumberId);
		long batchId = studentDetailsByAdmissionId.getBatchMaster().getBatchId();
		BatchMaster batchMaster = iStudentAdmissionDAO.getBatchName(batchId);
		receiptGenerateDTO.setBatchName(batchMaster.getBatchName());
		receiptGenerateDTO.setStudentName(studentDetailsByAdmissionId.getStudentFirstName()+" "+studentDetailsByAdmissionId.getStudentMiddleName()+" "+studentDetailsByAdmissionId.getStudentLastName());
		receiptGenerateDTO.setAdmissionNumber(studentDetailsByAdmissionId.getAdmissionNumber());
		logger.debug("Interacted with database to fatch ReceiptGenerateEntity:"+list);
		}catch (Exception e) {
			logger.error("Exception raised in generateReceiptByReceiptGenerateId Method in FeeManagementServiceImpl:", e);
		}
		return  receiptGenerateDTO;
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#savePaymentDetails(com.leonia.academy.entity.ReceiptGenerateDTO)
	 */
	@Override
	public String savePaymentDetails(ReceiptGenerateDTO receiptgeneratedtoObj) {
		logger.info("Save Data ReceiptGenerateEntity :"+receiptgeneratedtoObj);
		String msg = null;
		try{
		msg =  iFeeManagementDAO.savePaymentDetails(receiptgeneratedtoObj);
		logger.debug("Saveing Data ReceiptGenerateEntity");
		}catch (Exception e) {
			logger.error("Exception raised in savePaymentDetails Method in FeeManagementServiceImpl:", e);
		}
		return msg;
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#feeassigntostudentPaymentupdate(java.util.List)
	 */
	@Override
	public String feeassigntostudentPaymentupdate(List<FeeAssignToStudents> feesettlementdtolist) {
		logger.info("Update Data FeeAssignToStudents:"+feesettlementdtolist);
		String msg = null;
		try{
		msg =  iFeeManagementDAO.feeassigntostudentPaymentupdate(feesettlementdtolist);
		logger.debug("Updataing Data FeeAssignToStudents :"+msg);
		}catch (Exception e) {
			logger.error("Exception raised in feeassigntostudentPaymentupdate Method in FeeManagementServiceImpl:", e);
		}
		return msg;
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#hostelFeeAssignToStudentPaymentUpdate(com.leonia.academy.entity.HostelFeeAssingToStudent)
	 */
	@Override
	public String hostelFeeAssignToStudentPaymentUpdate(HostelFeeAssingToStudent hostelFeeAssingToStudent) {
		logger.info("Update Data HostelFeeAssingToStudent:"+hostelFeeAssingToStudent);
		String msg = null;
		try{
		msg =  iFeeManagementDAO.hostelFeeAssignToStudentPaymentUpdate(hostelFeeAssingToStudent);
		logger.debug("Updataing Data HostelFeeAssingToStudent :"+msg);
		}catch (Exception e) {
			logger.error("Exception raised in hostelFeeAssignToStudentPaymentUpdate  Method in FeeManagementServiceImpl:", e);
		}
		return msg;
	}
	
	
	//FeeManagementServiceImpl from ajay


	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#getFeeDetailsById(java.lang.Integer)
	 */
	@Override
	public List getFeeDetailsById(Integer batchId) {
		List list = new LinkedList();
		try {

			list = iFeeManagementDAO.getFeeDetailsById(batchId);
		} catch (Exception e) {

		} finally {

		}
		return list;
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#feeCategory()
	 */
	@Override
	public List feeCategory() {
		List list = new ArrayList();
		try {

			list = iFeeManagementDAO.feeCategory();
		} catch (Exception e) {

		} finally {

		}
		return list;
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#getCategoryNameByBatchId(java.lang.Integer)
	 */
	@Override
	public List getCategoryNameByBatchId(Integer batchId) {
		List list = new ArrayList();
		try {

			list = iFeeManagementDAO.getCategoryNameByBatchId(batchId);
		} catch (Exception e) {

		} finally {

		}
		return list;
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#saveStudentFee(com.leonia.academy.entity.FeeCreation)
	 */
	@Override
	public String saveStudentFee(FeeCreation fee) {
		String str = "";
		try {

			str = iFeeManagementDAO.saveStudentFee(fee);
		} catch (Exception e) {

		} finally {

		}
		return str;
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#getInstallmentTypes(java.lang.Long)
	 */
	@Override
	public List getInstallmentTypes(Long batchId) {
		List list = new LinkedList();
		try {

			list = iFeeManagementDAO.getInstallmentTypes(batchId);
		} catch (Exception e) {

		} finally {

		}
		return list;
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#getInstallmentTypesdrop(java.lang.Long)
	 */
	@Override
	public List getInstallmentTypesdrop(Long batchId) {
		List list = new LinkedList();
		try {

			list = iFeeManagementDAO.getInstallmentTypesdrop(batchId);
		} catch (Exception e) {

		} finally {

		}
		return list;
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#getFeeCreationBatchWiseListByBatchId(java.lang.Long, java.lang.Long)
	 */
	@Override
	public List<FeeCreation> getFeeCreationBatchWiseListByBatchId(Long batchId, Long installmentTypeMasterId) {
		List<FeeCreation> list = new LinkedList<FeeCreation>();
		try {

			list = iFeeManagementDAO.getFeeCreationBatchWiseListByBatchId(batchId, installmentTypeMasterId);
		} catch (Exception e) {

		} finally {

		}
		return list;
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#getAmountByParticularFee(java.lang.Long)
	 */
	@Override
	public List getAmountByParticularFee(Long feeCreationId) {
		List list = new LinkedList();
		try {

			list = iFeeManagementDAO.getAmountByParticularFee(feeCreationId);
		} catch (Exception e) {

		} finally {

		}
		return list;
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#savefeeGenarationBatchWise(com.leonia.academy.entity.FeeGenerationBatchWise)
	 */
	@Override
	public String savefeeGenarationBatchWise(FeeGenerationBatchWise generationBatchWise) {
		String msg = null;
		try {

			msg = iFeeManagementDAO.savefeeGenarationBatchWise(generationBatchWise);
		} catch (Exception e) {

		} finally {

		}
		return msg;
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#getFeeGenarationFeeListInstallment123(java.lang.Long, java.lang.Long, java.lang.Long)
	 */
	@Override
	public List getFeeGenarationFeeListInstallment123(Long batchId, Long feeGenerationBatchWiseId,
			Long installmentTypeMasterId) {
		List list = new LinkedList();
		try {

			list = iFeeManagementDAO.getFeeGenarationFeeListInstallment123(batchId, feeGenerationBatchWiseId,
					installmentTypeMasterId);
		} catch (Exception e) {

		} finally {

		}
		return list;
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#getFeeGenarationFeeListInstallment(java.lang.Long, java.lang.Long)
	 */
	@Override
	public List getFeeGenarationFeeListInstallment(Long batchId, Long feeGenerationBatchWiseId) {
		List list = new LinkedList();
		try {

			list = iFeeManagementDAO.getFeeGenarationFeeListInstallment(batchId, feeGenerationBatchWiseId);
		} catch (Exception e) {

		} finally {

		}
		return list;
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#getStudentsListByBatchId(java.lang.Long, java.lang.Long, java.lang.Long)
	 */
	@Override
	public List getStudentsListByBatchId(Long batchId, Long feeGenerationBatchWiseId, Long installmentTypeMasterId) {
		List list = new LinkedList();
		try {

			list = iFeeManagementDAO.getStudentsListByBatchId(batchId, feeGenerationBatchWiseId,
					installmentTypeMasterId);
		} catch (Exception e) {

		} finally {

		}
		return list;
	}

	/* (non-Javadoc)
	 * @see com.leonia.academy.isevice.IFeeManagementService#feeAssignToStudentsPost(com.leonia.academy.entity.FeeAssignToStudents)
	 */
	@Override
	public String feeAssignToStudentsPost(FeeAssignToStudents feeAssignToStudents) {
		String msg = null;
		try {

			msg = iFeeManagementDAO.feeAssignToStudentsPost(feeAssignToStudents);
		} catch (Exception e) {

		} finally {

		}
		return msg;
	}

}
