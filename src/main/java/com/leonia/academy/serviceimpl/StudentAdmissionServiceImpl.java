package com.leonia.academy.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leonia.academy.entity.BatchMaster;
import com.leonia.academy.entity.Certificatemaster;
import com.leonia.academy.entity.Certificatetaken;
import com.leonia.academy.entity.StudentAdmissionEntity;
import com.leonia.academy.idao.IStudentAdmissionDAO;
import com.leonia.academy.isevice.IStudentAdmissionService;

@Service
public class StudentAdmissionServiceImpl implements IStudentAdmissionService{
	
	@Autowired
	private IStudentAdmissionDAO iStudentAdmissionDAO;

	@Override
	public List<StudentAdmissionEntity> getStudents(StudentAdmissionEntity studentAdmission) {
	return iStudentAdmissionDAO.getStudents(studentAdmission);
	}

	@Override
	public List<Certificatemaster> getCertificates(Long admissionId) {
		return iStudentAdmissionDAO.getCertificates(admissionId);
	}

	@Override
	public List<StudentAdmissionEntity> getStudentDetailsById(Long admissionId) {
		return iStudentAdmissionDAO.getStudentDetailsById(admissionId);
	}

	@Override
	public String addCertificateDetails(Certificatetaken certificatetaken) {
		return iStudentAdmissionDAO.addCertificateDetails(certificatetaken);
	}

	@Override
	public List<Certificatetaken> getCertificatesByid(Long admissionId) {
		return iStudentAdmissionDAO.getCertificatesByid(admissionId);
	}

	@Override
	public List printCertificates(Long admissionId) {
		return iStudentAdmissionDAO.printCertificates(admissionId);
	}

	@Override
	public List<BatchMaster> getBatchname() {
		return iStudentAdmissionDAO.getBatchname();
	}

	@Override
	public List<StudentAdmissionEntity> dropdownValueForStudent(Long batchid) {
		return iStudentAdmissionDAO.dropdownValueForStudent(batchid);
	}

	@Override
	public List<Certificatetaken> getStudentDetailsByIdForReturn(Long sid) {
		return iStudentAdmissionDAO.getStudentDetailsByIdForReturn(sid);
	}

	@Override
	public void updateStatusDate(String string, String username) {
		
		iStudentAdmissionDAO.updateStatusDate(string,username);
	}

	@Override
	public List<Certificatetaken> getCheckedAndUnCheckedDocuments() {
		return iStudentAdmissionDAO.getCheckedAndUnCheckedDocuments();
	}

	@Override
	public List<Certificatetaken> getCheckincheckoutreportByDate(String startdate, String enddate) {
		return iStudentAdmissionDAO.getCheckincheckoutreportByDate(startdate,enddate);
	}

	@Override
	public List<Certificatetaken> getCheckincheckoutreportByDate(Certificatetaken certificatetaken) {
		return iStudentAdmissionDAO.getCheckincheckoutreportByDate(certificatetaken);
	}	

	}