package com.leonia.academy.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leonia.academy.entity.LeadFollowUpDTO;
import com.leonia.academy.entity.LoginEntity;
import com.leonia.academy.idao.IFollowupDetailsDAO;
import com.leonia.academy.isevice.IFollowupDetailsService;



@Service
public class FollowupDetailsServiceImpl implements IFollowupDetailsService {
	
	@Autowired
	private IFollowupDetailsDAO iFollowupDetailsDAO;

	@Override
	public List<LeadFollowUpDTO> getfollowupdetails(Integer leadid) {
		return iFollowupDetailsDAO.getfollowupdetails(leadid);
	}

	@Override
	public List<LoginEntity> getUsernameList() {
		return iFollowupDetailsDAO.getUsernameList();
	}

	@Override
	public String saveNewFollowUp(LeadFollowUpDTO leadFollowUpDTO) {
		return iFollowupDetailsDAO.saveNewFollowUp(leadFollowUpDTO);
	}

	@Override
	public List<LeadFollowUpDTO> getNewFollowUpDetails(LeadFollowUpDTO leadFollowUpDTO) {
		return iFollowupDetailsDAO.getNewFollowUpDetails(leadFollowUpDTO);
	}

	@Override
	public List<LeadFollowUpDTO> getFollowUpDetails() {
		return iFollowupDetailsDAO.getFollowUpDetails();
	}

	@Override
	public List<LeadFollowUpDTO> getOpenLeadFoliowupDetails() {
		return iFollowupDetailsDAO.getOpenLeadFoliowupDetails();
	}

	@Override
	public List<LeadFollowUpDTO> getFollowupDetailsByDate(LeadFollowUpDTO leadFollowUpDTO) {
		return iFollowupDetailsDAO.getFollowupDetailsByDate(leadFollowUpDTO);
	}

	@Override
	public List<LeadFollowUpDTO> getFollowUpDetailsById(Integer followupId) {
		return iFollowupDetailsDAO.getFollowUpDetailsById(followupId);
	}

	/*@Override
	public List<StudentAdmission> getStudents(StudentAdmission studentAdmission) {
		return iFollowupDetailsDAO.getStudents(studentAdmission);
	}*/
	@Override
	public List endlead(LeadFollowUpDTO leadFollowUpDTO) {
		return iFollowupDetailsDAO.endlead(leadFollowUpDTO);
	}


	

}
