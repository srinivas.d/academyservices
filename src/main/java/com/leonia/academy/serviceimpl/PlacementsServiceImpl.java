package com.leonia.academy.serviceimpl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leonia.academy.entity.BatchMaster;
import com.leonia.academy.entity.CompanyDetails;
import com.leonia.academy.entity.EmployerContactInformation;
import com.leonia.academy.entity.InterviewAssignToStudents;
import com.leonia.academy.entity.PlacementDepartmentMaster;
import com.leonia.academy.entity.PlacementDesignationMaster;
import com.leonia.academy.entity.ReasonsMaster;
import com.leonia.academy.entity.StatusMaster;
import com.leonia.academy.entity.StudentAdmissionEntity;
import com.leonia.academy.entity.VacancyDetails;
import com.leonia.academy.entity.VacancyInfo;
import com.leonia.academy.idao.IPlacementsDAO;
import com.leonia.academy.isevice.IPlacementsService;

@Service
public class PlacementsServiceImpl implements IPlacementsService {
	
	@Autowired
	private IPlacementsDAO iPlacementsDAO;

	@Override
	public List getDomainMaster() {
		return iPlacementsDAO.getDomainMaster();
	}

	@Override
	public String saveCompanyDetails(CompanyDetails companyDetails) {
		return iPlacementsDAO.saveCompanyDetails(companyDetails);
	}

	@Override
	public List getAllCompaniesList() {
		List<CompanyDetails> allCompaniesList = iPlacementsDAO.getAllCompaniesList();
		List<CompanyDetails> companiesList = new ArrayList<>();
		for (CompanyDetails companyDetails : allCompaniesList) {
			CompanyDetails details = new CompanyDetails();
			details.setCompanyDetailsId(companyDetails.getCompanyDetailsId());
			details.setCompanyName(companyDetails.getCompanyName());
			details.setCompanyShortName(companyDetails.getCompanyShortName());
			details.setCompanyAddress(companyDetails.getCompanyAddress());
			companiesList.add(details);
		}
		return companiesList;
	}

	@Override
	public CompanyDetails editCompanyDetailsByCompanyDetailsId(Long companyDetailsId) {
		CompanyDetails editCompanyDetails = iPlacementsDAO.editCompanyDetailsByCompanyDetailsId(companyDetailsId);
		CompanyDetails companyDetails = new CompanyDetails();
		companyDetails.setCompanyDetailsId(editCompanyDetails.getCompanyDetailsId());
		companyDetails.setCompanyName(editCompanyDetails.getCompanyName());
		companyDetails.setDomainMasterId(editCompanyDetails.getDomainMasterId());
		companyDetails.setCompanyShortName(editCompanyDetails.getCompanyShortName());
		companyDetails.setCompanyAddress(editCompanyDetails.getCompanyAddress());
		companyDetails.setCompanyLandMark(editCompanyDetails.getCompanyLandMark());
		companyDetails.setCompanyPhone(editCompanyDetails.getCompanyPhone());
		companyDetails.setCompanyEmail(editCompanyDetails.getCompanyEmail());
		companyDetails.setOtherInfo(editCompanyDetails.getOtherInfo());
		
		List<EmployerContactInformation> employerContactInformation = editCompanyDetails.getEmployerContactInformation();
		List<EmployerContactInformation> employerContactInfo = new ArrayList<>();
		for (EmployerContactInformation employerContactInformations : employerContactInformation) {
			EmployerContactInformation contactInformation = new EmployerContactInformation();
			contactInformation.setEmployerContactInformationId(employerContactInformations.getEmployerContactInformationId());
			contactInformation.setEmployerContactPersonName(employerContactInformations.getEmployerContactPersonName());
			contactInformation.setEmployerDesignation(employerContactInformations.getEmployerDesignation());
			contactInformation.setEmployerContactMobile(employerContactInformations.getEmployerContactMobile());
			contactInformation.setEmployerEmail(employerContactInformations.getEmployerEmail());
			employerContactInfo.add(contactInformation);
		}
		companyDetails.setEmployerContactInformation(employerContactInfo);
		return companyDetails;
	}

	@Override
	public String updateCompanyDetails(CompanyDetails companyDetails) {
		return iPlacementsDAO.updateCompanyDetails(companyDetails);
	}

	@Override
	public String saveVacancyDetails(VacancyInfo vacancyInfo) {
		return iPlacementsDAO.saveVacancyDetails(vacancyInfo);
	}

	@Override
	public List getDepartmentMaster() {
		List<PlacementDepartmentMaster> departmentMaster = iPlacementsDAO.getDepartmentMaster();
		List<PlacementDepartmentMaster> deptMaster = new ArrayList<>();
		for (PlacementDepartmentMaster placementDepartmentMaster : departmentMaster) {
			PlacementDepartmentMaster master = new PlacementDepartmentMaster();
			master.setDepartmentMasterId(placementDepartmentMaster.getDepartmentMasterId());
			master.setDepartmentName(placementDepartmentMaster.getDepartmentName());
			master.setDepartmentShortName(placementDepartmentMaster.getDepartmentShortName());
			deptMaster.add(master);
		}
		return deptMaster;
	}
	
	@Override
	public List getDesignationMaster() {
		List<PlacementDesignationMaster> designationMasters = iPlacementsDAO.getDesignationMaster();
		List<PlacementDesignationMaster> designMasters = new ArrayList<>();
		for (PlacementDesignationMaster placementDesignationMaster : designationMasters) {
			PlacementDesignationMaster master = new PlacementDesignationMaster();
			master.setDesignationMasterId(placementDesignationMaster.getDesignationMasterId());
			master.setDesignationName(placementDesignationMaster.getDesignationName());
			master.setDesignationShortName(placementDesignationMaster.getDesignationShortName());
			designMasters.add(master);
		}
		return designMasters;
	}

	@Override
	public List getDesignationIdByDepartmentId(Long departmentId) {
		List<PlacementDesignationMaster> designationMasters = iPlacementsDAO.getDesignationIdByDepartmentId(departmentId);
		List<PlacementDesignationMaster> designMasters =  new ArrayList<>();
		for (PlacementDesignationMaster placementDesignationMaster : designationMasters) {
			PlacementDesignationMaster designationMaster = new PlacementDesignationMaster();
			designationMaster.setDesignationMasterId(placementDesignationMaster.getDesignationMasterId());
			designationMaster.setDesignationName(placementDesignationMaster.getDesignationName());
			designationMaster.setDesignationShortName(placementDesignationMaster.getDesignationShortName());
			designMasters.add(designationMaster);
		}
		return designMasters;
	}

	@Override
	public List getAllVacanciesList() {
		List<VacancyInfo> allVacanciesList = iPlacementsDAO.getAllVacanciesList();
		List<VacancyInfo> allVacList = new ArrayList<>();
		
		for (VacancyInfo vacancyInfo : allVacanciesList) {
			VacancyInfo vacancyInf = new VacancyInfo();
			vacancyInf.setVacancyInfoId(vacancyInfo.getVacancyInfoId());
			vacancyInf.setVacancyName(vacancyInfo.getVacancyName());
			vacancyInf.setInterviewScheduleDate(vacancyInfo.getInterviewScheduleDate());
			CompanyDetails companyDetails = new CompanyDetails();
			if(vacancyInfo.getCompanyDetails()!=null){
			companyDetails.setCompanyDetailsId(vacancyInfo.getCompanyDetails().getCompanyDetailsId());
			//companyDetails.setCompanyName(vacancyInfo.getCompanyDetails().getCompanyName());
			vacancyInf.setCompanyDetails(companyDetails);
			}else{
				vacancyInf.setCompanyDetails(null);
			}
			//vacancyInf.setVacancyDetails(null);
			allVacList.add(vacancyInf);
		}
		return allVacList;
	}

	@Override
	public VacancyInfo editVacancyInfoByVacancyInfoId(Long vacancyInfoId) {
		VacancyInfo info = new VacancyInfo();
		VacancyInfo vacancyInfo = iPlacementsDAO.editVacancyInfoByVacancyInfoId(vacancyInfoId);
		CompanyDetails companyDetails = new CompanyDetails();
		if(vacancyInfo.getCompanyDetails()!=null){
			companyDetails.setCompanyDetailsId(vacancyInfo.getCompanyDetails().getCompanyDetailsId());
			//companyDetails.setCompanyName(vacancyInfo.getCompanyDetails().getCompanyName());
			vacancyInfo.setCompanyDetails(companyDetails);
			}else{
				vacancyInfo.setCompanyDetails(null);
			}
		
		
		/*CompanyDetails companyDetails = new CompanyDetails();
		companyDetails.setCompanyDetailsId(vacancyInfo.getCompanyDetails().getCompanyDetailsId());*/
		
		info.setVacancyInfoId(vacancyInfo.getVacancyInfoId());
		info.setVacancyName(vacancyInfo.getVacancyName());
		info.setInterviewScheduleDate(vacancyInfo.getInterviewScheduleDate());
		
		List<VacancyDetails> vacancyDetails = vacancyInfo.getVacancyDetails();
		List<VacancyDetails> vacDetails = new ArrayList<>();
		for (VacancyDetails vacancyDetails2 : vacancyDetails) {
			VacancyDetails details = new VacancyDetails();
			details.setVacanyDetailsId(vacancyDetails2.getVacanyDetailsId());
			details.setDepartmentId(vacancyDetails2.getDepartmentId());
			details.setDesignationId(vacancyDetails2.getDesignationId());
			details.setNoOfVacancies(vacancyDetails2.getNoOfVacancies());
			details.setRequirement(vacancyDetails2.getRequirement());
			details.setLocationOfWork(vacancyDetails2.getLocationOfWork());
			details.setAdditionalInfo(vacancyDetails2.getAdditionalInfo());
			vacDetails.add(details);
		}
		info.setVacancyDetails(vacDetails);
		info.setCompanyDetails(companyDetails);
		return info;
	}

	@Override
	public String updateVacancyDetails(VacancyInfo vacancyInfo) {
		return iPlacementsDAO.updateVacancyDetails(vacancyInfo);
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<VacancyDetails> viewvacancyDetailsGrid() {
		List list = iPlacementsDAO.viewvacancyDetailsGrid();
		List list1 = new ArrayList<>();
		for(int i=0;i<list.size();i++){
			Object object = list.get(i);
			Object[] objArray = (Object[])object;
			VacancyDetails details = new VacancyDetails();
			BigInteger id =(BigInteger)objArray[4];
			BigInteger DepartmentId =(BigInteger)objArray[5];
			BigInteger DesignationId =(BigInteger)objArray[6];
			details.setVacanyDetailsId(id.longValue());
			details.setDepartmentId(DepartmentId.longValue());
			details.setDesignationId(DesignationId.longValue());
			details.setNoOfVacancies((String)objArray[7]);
			details.setLocationOfWork((String)objArray[8]);
			VacancyInfo info = new VacancyInfo();
			BigInteger vacancyInfoId =(BigInteger)objArray[9];
			info.setVacancyInfoId(vacancyInfoId.longValue());
			info.setVacancyName((String)objArray[2]);
			info.setInterviewScheduleDate((Date)objArray[3]);
			CompanyDetails companyDetails = new CompanyDetails();
			BigInteger CompanyDetailsId=(BigInteger)objArray[0];
			companyDetails.setCompanyDetailsId(CompanyDetailsId.longValue());
			companyDetails.setCompanyName((String)objArray[1]);
			info.setCompanyDetails(companyDetails);
			details.setVacancyInfo(info);
			list1.add(details);
		}
		
		return list1;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<VacancyDetails> getVacancyDetailsById(Long vacanyDetailsId) {
		List list = iPlacementsDAO.getVacancyDetailsById(vacanyDetailsId);
		Integer count = iPlacementsDAO.AssignedStudentsCount(vacanyDetailsId);
		List list1 = new ArrayList<>();
		for(int i=0;i<list.size();i++){
			Object object = list.get(i);
			Object[] objArray = (Object[])object;
			VacancyDetails details = new VacancyDetails();
			BigInteger id =(BigInteger)objArray[4];
			BigInteger DepartmentId =(BigInteger)objArray[5];
			BigInteger DesignationId =(BigInteger)objArray[6];
			details.setVacanyDetailsId(id.longValue());
			details.setDepartmentId(DepartmentId.longValue());
			details.setDesignationId(DesignationId.longValue());
			details.setNoOfVacancies((String)objArray[7]);
			details.setLocationOfWork((String)objArray[8]);
			details.setAssignedStudentsCount(count);
			VacancyInfo info = new VacancyInfo();
			info.setVacancyName((String)objArray[2]);
			info.setInterviewScheduleDate((Date)objArray[3]);
			CompanyDetails companyDetails = new CompanyDetails();
			BigInteger CompanyDetailsId=(BigInteger)objArray[0];
			companyDetails.setCompanyDetailsId(CompanyDetailsId.longValue());
			companyDetails.setCompanyName((String)objArray[1]);
			info.setCompanyDetails(companyDetails);
			details.setVacancyInfo(info);
			list1.add(details);
		}
		return list1;
	}

	@Override
	public String interviewAssignedToStudents(Long vacanyDetailsId, String admissionNumberId) {
		String msg =null;
        String admissionNumberId1[]=admissionNumberId.split(",");
		for(int i=0;i<admissionNumberId1.length;i++){
			InterviewAssignToStudents assignToStudents = new InterviewAssignToStudents();
			VacancyDetails details = new  VacancyDetails();
			details.setVacanyDetailsId(vacanyDetailsId);
			StudentAdmissionEntity admissionEntity = new  StudentAdmissionEntity();
			admissionEntity.setAdmissionNumberId(new Long(admissionNumberId1[i]));
			assignToStudents.setVacancyDetails(details);
			assignToStudents.setStudentAdmissionEntity(admissionEntity);
			assignToStudents.setStatusReport("In Process");
			assignToStudents.setStatus(true);
			msg = iPlacementsDAO.interviewAssignedToStudents(assignToStudents);
		}
		return msg;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<VacancyInfo> getVacancyDetailsByCompanyDetailsId(Long companyDetailsId) {
		List<VacancyInfo> list = iPlacementsDAO.getVacancyDetailsByCompanyDetailsId(companyDetailsId);
		List list1= new ArrayList<>();
		for(int i=0;i<list.size();i++){
			VacancyInfo vacancyInfo =new VacancyInfo();
			vacancyInfo.setVacancyInfoId(list.get(i).getVacancyInfoId());
			vacancyInfo.setVacancyName(list.get(i).getVacancyName());
			list1.add(vacancyInfo);
		}
		return list1;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List getDepartmentsByVacancyInfoId(Long vacancyInfoId) {
		List<VacancyDetails> list = iPlacementsDAO.getDepartmentsByVacancyInfoId(vacancyInfoId);
		List<VacancyDetails> list1 = new ArrayList<>();
		for(int i=0;i<list.size();i++){
			VacancyDetails  details = new VacancyDetails();
			details.setVacanyDetailsId(list.get(i).getVacanyDetailsId());
			details.setDepartmentId(list.get(i).getDepartmentId());
			details.setDesignationId(list.get(i).getDesignationId());
			VacancyInfo vacancyInfo = new VacancyInfo();
			vacancyInfo.setVacancyInfoId(list.get(i).getVacancyInfo().getVacancyInfoId());
			vacancyInfo.setVacancyName(list.get(i).getVacancyInfo().getVacancyName());
			vacancyInfo.setCompanyDetails(null);
			details.setVacancyInfo(vacancyInfo);
			list1.add(details);
		}
		return list1;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List getStudentsByVacancyDetailsId(Long vacanyDetailsId) {
		List<InterviewAssignToStudents> list =  iPlacementsDAO.getStudentsByVacancyDetailsId(vacanyDetailsId);
		
		List list1= new ArrayList<>();
		for(int i=0;i<list.size();i++){
			Long departmentId = new Long(list.get(i).getVacancyDetails().getDepartmentId());
			ReasonsMaster reasonsMaster = iPlacementsDAO.getReasonsByDepartmentId(departmentId);
			InterviewAssignToStudents interviewAssignToStudents = new InterviewAssignToStudents();
			interviewAssignToStudents.setInterviewAssignToStudentsId(list.get(i).getInterviewAssignToStudentsId());
			interviewAssignToStudents.setStatusReport(list.get(i).getStatusReport());
			interviewAssignToStudents.setRemarks(list.get(i).getRemarks());
			VacancyDetails details =new VacancyDetails();
			details.setVacanyDetailsId(list.get(i).getVacancyDetails().getVacanyDetailsId());
			details.setDepartmentId(list.get(i).getVacancyDetails().getDepartmentId());
			StudentAdmissionEntity admissionEntity = new StudentAdmissionEntity();
			admissionEntity.setAdmissionNumberId(list.get(i).getStudentAdmissionEntity().getAdmissionNumberId());
			admissionEntity.setStudentFirstName(list.get(i).getStudentAdmissionEntity().getStudentFirstName());
			admissionEntity.setGender(list.get(i).getStudentAdmissionEntity().getGender());
			BatchMaster batchMaster = new BatchMaster();
			batchMaster.setBatchName(list.get(i).getStudentAdmissionEntity().getBatchMaster().getBatchName());
			admissionEntity.setBatchMaster(batchMaster);
			ReasonsMaster master = new ReasonsMaster();
			master.setReasonsMasterId(reasonsMaster.getReasonsMasterId());
			master.setReasonsName(reasonsMaster.getReasonsName());
			interviewAssignToStudents.setVacancyDetails(details);
			interviewAssignToStudents.setStudentAdmissionEntity(admissionEntity);
			interviewAssignToStudents.setReasonsMaster(master);
			list1.add(interviewAssignToStudents);
		}
		return list1;
	}

	@Override
	public List<StatusMaster> getStatusMaster() {
		return iPlacementsDAO.getStatusMaster();
	}

	@Override
	public String saveDeclaredResults(InterviewAssignToStudents toStudents) {
		return iPlacementsDAO.saveDeclaredResults(toStudents);
	}

	@Override
	public String removeStudentsByVacancyDetailsId(Long vacanyDetailsId, String admissionNumberId) {
		String msg =null;
        String admissionNumberId1[]=admissionNumberId.split(",");
		for(int i=0;i<admissionNumberId1.length;i++){
			Long id = new Long(admissionNumberId1[i]);
			msg =iPlacementsDAO.removeStudentsByVacancyDetailsId(id,vacanyDetailsId);
		}
		return msg;
	}

	@Override
	public List getStudentsByVacancyDetailsIdFromAttendedInterViews(Long vacanyDetailsId) {
		List<InterviewAssignToStudents> list = iPlacementsDAO.getStudentsByVacancyDetailsIdFromAttendedInterViews(vacanyDetailsId);
		List list1= new ArrayList<>();
		for(int i=0;i<list.size();i++){
			Long departmentId = new Long(list.get(i).getVacancyDetails().getDepartmentId());
			ReasonsMaster reasonsMaster = iPlacementsDAO.getReasonsByDepartmentId(departmentId);
			InterviewAssignToStudents interviewAssignToStudents = new InterviewAssignToStudents();
			interviewAssignToStudents.setInterviewAssignToStudentsId(list.get(i).getInterviewAssignToStudentsId());
			interviewAssignToStudents.setStatusReport(list.get(i).getStatusReport());
			interviewAssignToStudents.setRemarks(list.get(i).getRemarks());
			VacancyDetails details =new VacancyDetails();
			details.setVacanyDetailsId(list.get(i).getVacancyDetails().getVacanyDetailsId());
			details.setDepartmentId(list.get(i).getVacancyDetails().getDepartmentId());
			StudentAdmissionEntity admissionEntity = new StudentAdmissionEntity();
			admissionEntity.setAdmissionNumberId(list.get(i).getStudentAdmissionEntity().getAdmissionNumberId());
			admissionEntity.setStudentFirstName(list.get(i).getStudentAdmissionEntity().getStudentFirstName());
			admissionEntity.setGender(list.get(i).getStudentAdmissionEntity().getGender());
			BatchMaster batchMaster = new BatchMaster();
			batchMaster.setBatchName(list.get(i).getStudentAdmissionEntity().getBatchMaster().getBatchName());
			admissionEntity.setBatchMaster(batchMaster);
			ReasonsMaster master = new ReasonsMaster();
			master.setReasonsMasterId(reasonsMaster.getReasonsMasterId());
			master.setReasonsName(reasonsMaster.getReasonsName());
			interviewAssignToStudents.setVacancyDetails(details);
			interviewAssignToStudents.setStudentAdmissionEntity(admissionEntity);
			interviewAssignToStudents.setReasonsMaster(master);
			list1.add(interviewAssignToStudents);
		}
		return list1;
	}

	@Override
	public String closingInterviewVacancyByVacancyInfoId(Long vacancyDetailsId, Long vacancyInfoId) {
		String msg = null;
		List list =	iPlacementsDAO.getDepartmentsByVacancyInfoIdSize(vacancyInfoId);
		if(list.size() > 1){
			msg = iPlacementsDAO.closingInterviewVacancyByVacancyInfoId(vacancyInfoId,vacancyDetailsId);
		}
		else{
			msg = iPlacementsDAO.closingInterviewVacancyByVacancyInfoIds(vacancyInfoId,vacancyDetailsId);
		}
		return msg;
	}

	}
