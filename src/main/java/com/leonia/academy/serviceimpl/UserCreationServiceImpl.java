package com.leonia.academy.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.leonia.academy.entity.StudentRegistrationEntity;
import com.leonia.academy.idao.IUserCreationDao;
import com.leonia.academy.isevice.IUserCreationService;

@Service
public class UserCreationServiceImpl implements IUserCreationService{
	
	@Autowired
	public IUserCreationDao IUserCreationDao ;

	@Override
	public String saveuserdetalies(StudentRegistrationEntity studentRegistrationEntity) {
		return IUserCreationDao.saveuserdetalies(studentRegistrationEntity);
	}
}
