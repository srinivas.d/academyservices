package com.leonia.academy.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leonia.academy.entity.LoginEntity;
import com.leonia.academy.idao.ILoginDAO;
import com.leonia.academy.isevice.ILoginService;

@Service
public class LoginServiceImpl implements ILoginService{
	
	@Autowired
	public ILoginDAO iLoginDAo;

	@Override
	public LoginEntity getlogindetailes(String username) {
		return  iLoginDAo. getlogindetailes(username);
	}

	@Override
	public List<LoginEntity> loginCheck(LoginEntity login) {
		return iLoginDAo.loginCheck(login);
	}

}
