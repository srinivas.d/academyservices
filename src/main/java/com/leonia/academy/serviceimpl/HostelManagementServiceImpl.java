package com.leonia.academy.serviceimpl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leonia.academy.entity.Bedmaster;
import com.leonia.academy.entity.Blockmaster;
import com.leonia.academy.entity.Buildingmaster;
import com.leonia.academy.entity.Floormaster;
import com.leonia.academy.entity.HostelFeeAssingToStudent;
import com.leonia.academy.entity.InstallmentHostelFee;
import com.leonia.academy.entity.InstallmentMaster;
import com.leonia.academy.entity.RoomAllotment;
import com.leonia.academy.entity.RoomCategory;
import com.leonia.academy.entity.Roomdeallocationmaster;
import com.leonia.academy.entity.Roommaster;
import com.leonia.academy.entity.StudentAdmissionEntity;
import com.leonia.academy.entity.Totalroomallocatiodetails;
import com.leonia.academy.idao.IHostelManagementDAO;
import com.leonia.academy.isevice.IHostelManagementService;

@Service
public class HostelManagementServiceImpl implements IHostelManagementService{
	
	@Autowired
	private IHostelManagementDAO hostelManagementDAO;
	
	//private static final Logger logger = Logger.getLogger(HostelManagementServiceImpl.class);

	@Override
	public List<StudentAdmissionEntity> getStayCategoryStudents() {
		return hostelManagementDAO.getStayCategoryStudents();
	}

	@Override
	public List<Buildingmaster> getBuildingdetails(String gender) {
		return hostelManagementDAO.getBuildingdetails(gender);
	}

	@Override
	public List<Blockmaster> getBlockdetails(Integer buildingid) {
		return hostelManagementDAO.getBlockdetails(buildingid);
	}

	@Override
	public List<Floormaster> getFloordetails(Integer blockid) {
		return hostelManagementDAO.getFloordetails(blockid);
	}

	@Override
	public List<Roommaster> getRoomdetails(Integer floorid) {
		return hostelManagementDAO.getRoomdetails(floorid);
	}

	@Override
	public List<Bedmaster> getBeddetails(Integer roomid) {
		return hostelManagementDAO.getBeddetails(roomid);
	}

	@Override
	public String allocation(RoomAllotment roomallot) {
		return hostelManagementDAO.allocation(roomallot);
	}

	@Override
	public String totalroomallocationdetails(Totalroomallocatiodetails totalroomallocationdetails, Long admissionNumberId) {
		return hostelManagementDAO.totalroomallocationdetails(totalroomallocationdetails,admissionNumberId);
	}

	@Override
	public List<Map<String, Object>> generatereceipt(Long admissionid) {
		return hostelManagementDAO.generatereceipt(admissionid);
	}

	@Override
	public List getSnapshot() {
		return hostelManagementDAO.getSnapshot();
	}

	@Override
	public List getAllocatedStudents() {
		return hostelManagementDAO.getAllocatedStudents();
	}

	@Override
	public String getItems(String roomno) {
		return hostelManagementDAO.getItems(roomno);
	}

	@Override
	public String deallocate(String roomno, String bedno, Long admissionid,
			Roomdeallocationmaster roomdeallocationmaster) {
		return hostelManagementDAO.deallocate(roomno,bedno,admissionid,roomdeallocationmaster);
	}

	@Override
	public List getFeeAmountdata(Long admissionid) {
		return hostelManagementDAO.getFeeAmountdata(admissionid);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<Buildingmaster> getBuildingMaster() {
		List<Buildingmaster> list = hostelManagementDAO.getBuildingMaster();
		List list1= new ArrayList<>();
		for(int i = 0;i<list.size();i++){
			Buildingmaster buildingmaster = new  Buildingmaster();
			buildingmaster.setBuildingId(list.get(i).getBuildingId());
			buildingmaster.setBuildingName(list.get(i).getBuildingName());
			buildingmaster.setBuildingCode(list.get(i).getBuildingCode());
			buildingmaster.setGendertype(list.get(i).getGendertype());
			buildingmaster.setStatus(list.get(i).getStatus());
			buildingmaster.setBlockmaster(null);
			list1.add(buildingmaster);
		}
		return  list1;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<RoomCategory> getRoomCategory() {
		List<RoomCategory> list = hostelManagementDAO.getRoomCategory();
		List list1= new ArrayList<>();
		for(int i = 0;i<list.size();i++){
			RoomCategory roomCategory = new RoomCategory();
			roomCategory.setCategoryid(list.get(i).getCategoryid());
			roomCategory.setCategoryname(list.get(i).getCategoryname());
			roomCategory.setCategoryfee(list.get(i).getCategoryfee());
			roomCategory.setRoommaster(null);
			list1.add(roomCategory);
		}
		return list1;
	}


	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<InstallmentMaster> getInstallment() {
     List<InstallmentMaster> list = hostelManagementDAO.getInstallment();
     List list1 = new ArrayList<>();
     for(int i = 0;i<list.size();i++){
    	 InstallmentMaster installmentMaster = new InstallmentMaster();
    	 installmentMaster.setInstid(list.get(i).getInstid());
    	 installmentMaster.setInstallmentname(list.get(i).getInstallmentname());
    	 installmentMaster.setInstallmentHostelFee(null);
    	 list1.add(installmentMaster);
     }
     return list1;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<InstallmentHostelFee> getbuildingdefinefee(InstallmentHostelFee fee) {
		List<InstallmentHostelFee> list = hostelManagementDAO.getbuildingdefinefee(fee);
		List list1= new ArrayList<>();
		 for(int i = 0;i<list.size();i++){
			 InstallmentHostelFee installmentHostelFee = new InstallmentHostelFee();
			 installmentHostelFee.setInstallmentHostelFeeId(list.get(i).getInstallmentHostelFeeId());
			 installmentHostelFee.setHostelinstallAmount(list.get(i).getHostelinstallAmount());
			 installmentHostelFee.setDueDate(list.get(i).getDueDate());
			 RoomCategory roomCategory = new RoomCategory();
			 roomCategory.setCategoryid(list.get(i).getRoomCategory().getCategoryid());
			 roomCategory.setCategoryname(list.get(i).getRoomCategory().getCategoryname());
			 installmentHostelFee.setRoomCategory(roomCategory);
			 Buildingmaster buildingmaster = new  Buildingmaster();
			 buildingmaster.setBuildingId(list.get(i).getBuildingmaster().getBuildingId());
			 buildingmaster.setBuildingName(list.get(i).getBuildingmaster().getBuildingName());
			 installmentHostelFee.setBuildingmaster(buildingmaster);
			 InstallmentMaster installmentMaster = new InstallmentMaster();
			 installmentMaster.setInstid(list.get(i).getInstallmentMaster().getInstid());
			 installmentMaster.setInstallmentname(list.get(i).getInstallmentMaster().getInstallmentname());
			 installmentHostelFee.setInstallmentMaster(installmentMaster);
			 list1.add(installmentHostelFee);
		 }
		return list1;
	}



	@Override
	public String savedefineHostelfee(InstallmentHostelFee fee) {
		return hostelManagementDAO. savedefineHostelfee(fee);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<Blockmaster> getBlockdetails123(Integer buildingid) {
		List<Blockmaster> list = hostelManagementDAO.getBlockdetails123(buildingid);
		List list1 = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			Blockmaster blockmaster = new Blockmaster();
			blockmaster.setBlockId(list.get(i).getBlockId());
			blockmaster.setBlockName(list.get(i).getBlockName());
			blockmaster.setBlockCode(list.get(i).getBlockCode());
			blockmaster.setCreatedOn(list.get(i).getCreatedOn());
			blockmaster.setBuildingmaster(null);
			blockmaster.setFloormaster(null);
			list1.add(blockmaster);
		}
		return list1;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<StudentAdmissionEntity> getbuildingfeeStudents(InstallmentHostelFee fee) {
		List list = hostelManagementDAO.getbuildingfeeStudents(fee);
		List<StudentAdmissionEntity> list1= new ArrayList<>();
		for(int i=0;i<list.size();i++){
			Object object = list.get(i);
			Object[] objArray = (Object[])object;
			StudentAdmissionEntity  admissionEntity = new StudentAdmissionEntity();
			BigInteger id =(BigInteger)objArray[0];
			admissionEntity.setAdmissionNumberId(id.longValue());
			admissionEntity.setAdmissionNumber((String) objArray[1]);
			admissionEntity.setStudentFirstName((String) objArray[2]);
			admissionEntity.setStudentMiddleName((String) objArray[3]);
			admissionEntity.setStudentLastName((String) objArray[4]);
			admissionEntity.setGender((String) objArray[5]);
			List list2= new ArrayList<>();
			RoomAllotment allotment = new RoomAllotment();
			allotment.setAlltId((Integer)objArray[6]);
			list2.add(allotment);
			admissionEntity.setRoomAllotment(list2);
			list1.add(admissionEntity);
		}
		return list1;
	}

	@Override
	public String assignedtohostelfees(HostelFeeAssingToStudent[] hostelFeeAssingToStudent) {
		String msg = null;
		for(int i=0;i<hostelFeeAssingToStudent.length;i++){
			HostelFeeAssingToStudent feeAssingToStudent = new HostelFeeAssingToStudent();
			feeAssingToStudent = hostelFeeAssingToStudent[i];
			msg = hostelManagementDAO.assignedtohostelfees(feeAssingToStudent);
		}
		return msg; 
	}
	
}
