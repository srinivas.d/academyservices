package com.leonia.academy.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leonia.academy.entity.DropdownMasterCourseEntity;
import com.leonia.academy.entity.DropdownMasterSourceEntity;
import com.leonia.academy.entity.LeadFollowUpDTO;
import com.leonia.academy.entity.LeadfollowupdetailsDTO;
import com.leonia.academy.idao.ILeadinSertionDAO;
import com.leonia.academy.isevice.ILeadinSertionService;

@Service
public class LeadinSertionServiceImpl implements ILeadinSertionService {
	
	@Autowired
	private ILeadinSertionDAO ileadinSertiondao;

	@Override
	public void leadinsertion(LeadfollowupdetailsDTO leadfollowupdetailsDTO) {
		ileadinSertiondao.leadinsertion(leadfollowupdetailsDTO);
	}

	@Override
	public List<DropdownMasterSourceEntity> MasterSource(DropdownMasterSourceEntity dropdownMasterSourceDTO) {
		return ileadinSertiondao.MasterSource(dropdownMasterSourceDTO);
	}

	@Override
	public List<DropdownMasterCourseEntity> MasterCourse(DropdownMasterCourseEntity dropdownMasterCourseDTO) {
		return ileadinSertiondao.MasterCourse(dropdownMasterCourseDTO);
	}

	@Override
	public String saveleaddetails(LeadFollowUpDTO leadFollowUpDTO) {
		return ileadinSertiondao.saveleaddetails(leadFollowUpDTO);
	}
}
