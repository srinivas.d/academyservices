package com.leonia.academy.serviceimpl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leonia.academy.entity.BatchMaster;
import com.leonia.academy.entity.CalenderEvents;
import com.leonia.academy.entity.CityMaster;
import com.leonia.academy.entity.CountryMaster;
import com.leonia.academy.entity.CourseMaster;
import com.leonia.academy.entity.EducationEntity;
import com.leonia.academy.entity.Eventtype;
import com.leonia.academy.entity.FeeCategoryMaster;
import com.leonia.academy.entity.ParentGuardianEntity;
import com.leonia.academy.entity.RelationMaster;
import com.leonia.academy.entity.StateMaster;
import com.leonia.academy.entity.StudentAdmissionEntity;
import com.leonia.academy.entity.UniversityMaster;
import com.leonia.academy.idao.IStudentManagementDAO;
import com.leonia.academy.isevice.IStudentManagementService;

@Service
public class StudentManagementServiceImpl implements IStudentManagementService {

	@Autowired
	private IStudentManagementDAO iStudentManagementDAO;
	
	@Override
	public List getNationalityMaster() {
		return iStudentManagementDAO.getNationalityMaster();
	}

	@Override
	public List bloodGroupMaster() {
		return iStudentManagementDAO.bloodGroupMaster();
	}

	@Override
	public List stayCategoryMaster() {
		return iStudentManagementDAO.stayCategoryMaster();
	}

	@Override
	public List discountCategoryMaster() {
		return iStudentManagementDAO.discountCategoryMaster();
	}

	@Override
	public List religionMaster() {
		return iStudentManagementDAO.religionMaster();
	}

	@Override
	public List communityMaster() {
		return iStudentManagementDAO.communityMaster();
	}

	@Override
	public List languageMaster() {
		return iStudentManagementDAO.languageMaster();
	}

	@Override
	public List cityMaster() {
		List<CityMaster> cityMaster = iStudentManagementDAO.cityMaster();
		List<CityMaster> cityMasters = new ArrayList<>();
		for (CityMaster master : cityMaster) {
			CityMaster cityMaster2 = new CityMaster();
			cityMaster2.setCityid(master.getCityid());
			cityMaster2.setCityName(master.getCityName());
			cityMasters.add(cityMaster2);
		}
		
		return cityMasters;
	}

	@Override
	public List getStateCountryByCityId(int cityId) {
		List<CityMaster> masters = iStudentManagementDAO.getStateCountryByCityId(cityId);
		
		List<CityMaster> cityMastersTemp = new ArrayList<>();
		
		for (CityMaster cityMaster : masters) {
			CityMaster master = new CityMaster();
			StateMaster stateMaster = new StateMaster();
			CountryMaster countryMaster = new CountryMaster();
			
			
			master.setCityName(cityMaster.getCityName());
			
			String stateName = cityMaster.getStateMaster().getStateName();
			stateMaster.setStateName(stateName);
			
			String countryName = cityMaster.getStateMaster().getCountryMaster().getCountryName();
			countryMaster.setCountryName(countryName);
			stateMaster.setCountryMaster(countryMaster);
			master.setStateMaster(stateMaster);
			
			cityMastersTemp.add(master);
		}
		return cityMastersTemp;
	}

	@Override
	public List courseMaster() {
		List<CourseMaster> courseMaster = iStudentManagementDAO.courseMaster();
		List<CourseMaster> courseMasterTemp = new ArrayList<>();
		for (CourseMaster cmaster : courseMaster) {
			CourseMaster master = new CourseMaster();
			master.setCourseId(cmaster.getCourseId());
			master.setCourseName(cmaster.getCourseName());
			master.setShortCode(cmaster.getShortCode());
			courseMasterTemp.add(master);
			
		}
		return courseMasterTemp;
	}

	@Override
	public List getBatchNameByCourseId(Long courseId) {
		List<BatchMaster> batchList = iStudentManagementDAO.getBatchNameByCourseId(courseId);
		List<BatchMaster> batchMaster = new ArrayList<>();
		for (BatchMaster batchMaster2 : batchList) {
			BatchMaster master = new BatchMaster();
			master.setBatchId(batchMaster2.getBatchId());
			master.setBatchName(batchMaster2.getBatchName());
			batchMaster.add(master);
		}
		return batchMaster;
	}

	@Override
	public List getBatchFeeByBatchId(Long batchId) {
		List<BatchMaster> batchMasters = iStudentManagementDAO.getBatchFeeByBatchId(batchId);
		List<BatchMaster> masters = new ArrayList<>();
		for (BatchMaster batchMaster : batchMasters) {
			BatchMaster master = new BatchMaster();
			master.setFees(batchMaster.getFees());
			masters.add(master);
		}
		
		return masters;
	}

	@Override
	public String saveStudentAdmissionDetails(StudentAdmissionEntity studentAdmissionEntity) {
		// TODO Auto-generated method stub
		return iStudentManagementDAO.saveStudentAdmissionDetails(studentAdmissionEntity);
	}

	@Override
	public List getviewstudentDetails() {
		List<StudentAdmissionEntity> list=  iStudentManagementDAO.getviewstudentDetails();
		List list1= new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			StudentAdmissionEntity admissionEntity = new StudentAdmissionEntity();
			admissionEntity.setAdmissionNumber(list.get(i).getAdmissionNumber());
			admissionEntity.setAdmissionNumberId(list.get(i).getAdmissionNumberId());
			admissionEntity.setStudentFirstName(list.get(i).getStudentFirstName());
			admissionEntity.setStudentLastName(list.get(i).getStudentLastName());
			admissionEntity.setStudentMiddleName(list.get(i).getStudentMiddleName());
			admissionEntity.setCourseId(list.get(i).getCourseId());
			BatchMaster batchMaster = new BatchMaster();
			batchMaster.setBatchId(list.get(i).getBatchMaster().getBatchId());
			batchMaster.setBatchName(list.get(i).getBatchMaster().getBatchName());
			admissionEntity.setBatchMaster(batchMaster);
			admissionEntity.setDateOfBirth(list.get(i).getDateOfBirth());
			list1.add(admissionEntity);
		}
		return list1;
	}

	@Override
	public StudentAdmissionEntity editStudentAdmissionDetails(Long admissionNumberId) {
		StudentAdmissionEntity editStudentAdmissionDetails = iStudentManagementDAO.editStudentAdmissionDetails(admissionNumberId);
		
		StudentAdmissionEntity studentAdmissionEntity = new StudentAdmissionEntity();
		studentAdmissionEntity.setAdmissionNumberId(editStudentAdmissionDetails.getAdmissionNumberId());
		studentAdmissionEntity.setAdmissionNumber(editStudentAdmissionDetails.getAdmissionNumber());
		studentAdmissionEntity.setAdmissionDate(editStudentAdmissionDetails.getAdmissionDate());
		
		studentAdmissionEntity.setUniversityAdmissionNumber(editStudentAdmissionDetails.getUniversityAdmissionNumber());
		studentAdmissionEntity.setUniversityRegisterNumber(editStudentAdmissionDetails.getUniversityRegisterNumber());
		studentAdmissionEntity.setStudentFirstName(editStudentAdmissionDetails.getStudentFirstName());
		studentAdmissionEntity.setStudentMiddleName(editStudentAdmissionDetails.getStudentMiddleName());
		studentAdmissionEntity.setStudentLastName(editStudentAdmissionDetails.getStudentLastName());
		
		studentAdmissionEntity.setDateOfBirth(editStudentAdmissionDetails.getDateOfBirth());
		studentAdmissionEntity.setGender(editStudentAdmissionDetails.getGender());
		studentAdmissionEntity.setBirthPlace(editStudentAdmissionDetails.getBirthPlace());
		studentAdmissionEntity.setNationalityId(editStudentAdmissionDetails.getNationalityId());
		studentAdmissionEntity.setBloodGroupId(editStudentAdmissionDetails.getBloodGroupId());
		studentAdmissionEntity.setMotherTongueId(editStudentAdmissionDetails.getMotherTongueId());
		studentAdmissionEntity.setCourseId(editStudentAdmissionDetails.getCourseId());
		
		
		
		
		BatchMaster batchMaster = new BatchMaster();
		batchMaster.setBatchId(editStudentAdmissionDetails.getBatchMaster().getBatchId());
		batchMaster.setBatchName(editStudentAdmissionDetails.getBatchMaster().getBatchName());
		studentAdmissionEntity.setBatchMaster(batchMaster);
		studentAdmissionEntity.setFees(editStudentAdmissionDetails.getFees());
		studentAdmissionEntity.setStayCategoryId(editStudentAdmissionDetails.getStayCategoryId());
		studentAdmissionEntity.setDiscountCategoryId(editStudentAdmissionDetails.getDiscountCategoryId());
		studentAdmissionEntity.setReligionId(editStudentAdmissionDetails.getReligionId());
		studentAdmissionEntity.setCommunityId(editStudentAdmissionDetails.getCommunityId());
		studentAdmissionEntity.setStudentPhoneNumber(editStudentAdmissionDetails.getStudentPhoneNumber());
		studentAdmissionEntity.setStudentMobilenumber(editStudentAdmissionDetails.getStudentMobilenumber());
		studentAdmissionEntity.setStudentEmail(editStudentAdmissionDetails.getStudentEmail());
		studentAdmissionEntity.setAddressLine1(editStudentAdmissionDetails.getAddressLine1());
		studentAdmissionEntity.setAddressLine2(editStudentAdmissionDetails.getAddressLine2());
		studentAdmissionEntity.setCityId(editStudentAdmissionDetails.getCityId());
		studentAdmissionEntity.setState(editStudentAdmissionDetails.getState());
		studentAdmissionEntity.setCountry(editStudentAdmissionDetails.getCountry());
		studentAdmissionEntity.setPinCode(editStudentAdmissionDetails.getPinCode());
		studentAdmissionEntity.setReference(editStudentAdmissionDetails.getReference());
		studentAdmissionEntity.setFeeCategoryId(editStudentAdmissionDetails.getFeeCategoryId());
		studentAdmissionEntity.setRfidCardNumber(editStudentAdmissionDetails.getRfidCardNumber());
		studentAdmissionEntity.setAadhaarCardNumber(editStudentAdmissionDetails.getAadhaarCardNumber());
		studentAdmissionEntity.setIdentificationMarksOne(editStudentAdmissionDetails.getIdentificationMarksOne());
		studentAdmissionEntity.setIdentificationMarksTwo(editStudentAdmissionDetails.getIdentificationMarksTwo());
		studentAdmissionEntity.setAllergies(editStudentAdmissionDetails.getAllergies());
		studentAdmissionEntity.setBankName(editStudentAdmissionDetails.getBankName());
		studentAdmissionEntity.setBankBranchName(editStudentAdmissionDetails.getBankBranchName());
		studentAdmissionEntity.setBankAccNumber(editStudentAdmissionDetails.getBankAccNumber());
		studentAdmissionEntity.setBankBranchIfscCode(editStudentAdmissionDetails.getBankBranchIfscCode());
		
		return studentAdmissionEntity;
	}

	@Override
	public String updateStudentAdmissionDetails(StudentAdmissionEntity studentAdmissionEntity) {
		return iStudentManagementDAO.updateStudentAdmissionDetails(studentAdmissionEntity);
	}

	@Override
	public Long getAdmissionNumberId() {
		return iStudentManagementDAO.getAdmissionNumberId();
	}

	@Override
	public List feeCategoryMaster() {
		return iStudentManagementDAO.feeCategoryMaster();
	}

	@Override
	public List<RelationMaster> relationsList() {
		// TODO Auto-generated method stub
		return iStudentManagementDAO.relationsList();
	}

	@Override
	public List<ParentGuardianEntity> parentDetails(Long admissionNumberId) {
		List<ParentGuardianEntity> list = iStudentManagementDAO.parentDetails(admissionNumberId);
		List<ParentGuardianEntity> list1 = new ArrayList<>();
		for(int i=0;i<list.size();i++){
			ParentGuardianEntity parentGuardianEntity = new ParentGuardianEntity();
			parentGuardianEntity.setParentGuardianId(list.get(i).getParentGuardianId());
			parentGuardianEntity.setParentFirstName(list.get(i).getParentFirstName());
			parentGuardianEntity.setParentSureName(list.get(i).getParentSureName());
			parentGuardianEntity.setParentDateOfBirth(list.get(i).getParentDateOfBirth());
			parentGuardianEntity.setParentEducation(list.get(i).getParentEducation());
			parentGuardianEntity.setParentAddressLine1(list.get(i).getParentAddressLine1());
			parentGuardianEntity.setParentAddressLine2(list.get(i).getParentAddressLine2());
			parentGuardianEntity.setParentAnnualIncomeInLakhs(list.get(i).getParentAnnualIncomeInLakhs());
			parentGuardianEntity.setParentEmail(list.get(i).getParentEmail());
			parentGuardianEntity.setParentMobileNumber(list.get(i).getParentMobileNumber());
			parentGuardianEntity.setParentOccupation(list.get(i).getParentOccupation());
			parentGuardianEntity.setParentPhoneNumber1(list.get(i).getParentPhoneNumber1());
			parentGuardianEntity.setParentPhoneNumber2(list.get(i).getParentPhoneNumber2());
			parentGuardianEntity.setCity(list.get(i).getCity());
			parentGuardianEntity.setCountry(list.get(i).getCountry());
			parentGuardianEntity.setState(list.get(i).getState());
			parentGuardianEntity.setImpatient(list.get(i).getImpatient());
			parentGuardianEntity.setParentType(list.get(i).getParentType());
			parentGuardianEntity.setRelationWithStudent(list.get(i).getRelationWithStudent());
			parentGuardianEntity.setAdmissionNumberId(list.get(i).getAdmissionNumberId());
			//StudentAdmissionEntity admissionEntity = new StudentAdmissionEntity();
			/*admissionEntity.setAdmissionNumberId(list.get(i).getStudentAdmission().getAdmissionNumberId());
			parentGuardianEntity.setStudentAdmission(admissionEntity);*/
			list1.add(parentGuardianEntity);
		}
		return list1;
	}

	@Override
	public String saveorupdateParentGuardian(ParentGuardianEntity parentGuardianEntity) {
		// TODO Auto-generated method stub
		return iStudentManagementDAO.saveorupdateParentGuardian(parentGuardianEntity);
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List geteventtypelist() {
		List<Eventtype> list = iStudentManagementDAO.geteventtypelist();
		List list1 = new ArrayList<>();
		for(int i=0;i<list.size();i++){
			Eventtype eventtype =new Eventtype();
			eventtype.setEventid(list.get(i).getEventid());
			eventtype.setEventtype(list.get(i).getEventtype());
			eventtype.setColor(list.get(i).getColor());
			eventtype.setTextColor(list.get(i).getTextColor());
			list1.add(eventtype);
		}
		return list1;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<CalenderEvents> getlistcalendereventdate() {
		List<CalenderEvents> list = iStudentManagementDAO.getlistcalendereventdate();
		List list1 = new ArrayList<>();
		for(int i=0;i<list.size();i++){
			CalenderEvents calenderEvents = new CalenderEvents();
			calenderEvents.setCalenderId(list.get(i).getCalenderId());
			calenderEvents.setTitle(list.get(i).getTitle());
			calenderEvents.setStartDate(list.get(i).getStartDate());
			calenderEvents.setEndDate(list.get(i).getEndDate());
			Eventtype eventtype = new Eventtype();
			eventtype.setEventid(list.get(i).getEventtype().getEventid());
			calenderEvents.setEventtype(eventtype);
			list1.add(calenderEvents);
		}
		return list1;
	}

	@Override
	public String insertcalendereventform(CalenderEvents calenderEvents) {
		return iStudentManagementDAO.insertcalendereventform(calenderEvents);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List calendereventdatelist() {
		List<CalenderEvents> list = iStudentManagementDAO.getlistcalendereventdate();
		List list1 = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			Map<String, Object> map = new LinkedHashMap<String, Object>();
			String color = list.get(i).getEventtype().getColor();
			String textColor = list.get(i).getEventtype().getTextColor();
			Date start = list.get(i).getStartDate();
			Date end = list.get(i).getEndDate();
			String title = list.get(i).getTitle();
			map.put("color", color);
			map.put("textColor", textColor);
			map.put("title", title);
			map.put("start", start);
			map.put("end", end);
			list1.add(map);
		}
		return list1;
	}

	@Override
	public StudentAdmissionEntity getStudentDetailsByAdmissionId(Long admissionId) {
		return iStudentManagementDAO.getStudentDetailsByAdmissionId(admissionId);
	}

	@Override
	public List getEducatioDetails(Long admissionId) {
		List<EducationEntity> educatioDetails = iStudentManagementDAO.getEducatioDetails(admissionId);
		
		List<EducationEntity> educatioDetailsList = new ArrayList<>();
		for (EducationEntity educationEntity : educatioDetails) {
			
			EducationEntity entity = new EducationEntity();
			entity.setEducationId(educationEntity.getEducationId());
			entity.setDegreeboardname(educationEntity.getDegreeboardname());
			entity.setDegreecoursename(educationEntity.getDegreecoursename());
			entity.setDegreemarkobtained(educationEntity.getDegreemarkobtained());
			entity.setDegreepercentage(educationEntity.getDegreepercentage());
			entity.setDegreeyearofpassing(educationEntity.getDegreeyearofpassing());
			entity.setInterboardname(educationEntity.getInterboardname());
			entity.setIntercoursename(educationEntity.getIntercoursename());
			entity.setIntermarkobtained(educationEntity.getIntermarkobtained());
			entity.setInterpercentage(educationEntity.getInterpercentage());
			entity.setInteryearofpassing(educationEntity.getInteryearofpassing());
			entity.setSscboardname(educationEntity.getSscboardname());
			entity.setSsccoursename(educationEntity.getSsccoursename());
			entity.setSscmarkobtained(educationEntity.getSscmarkobtained());
			entity.setSscpercentage(educationEntity.getSscpercentage());
			entity.setSscyearofpassing(educationEntity.getSscyearofpassing());
			//entity.setStudentAdmissionEntity(educationEntity.getStudentAdmissionEntity());
			educatioDetailsList.add(entity);
		}
		
		return educatioDetailsList;
	}

	@Override
	public String storeeducationdetails(EducationEntity educationEntity) {
		return iStudentManagementDAO.storeeducationdetails(educationEntity);
	}

	@Override
	public List universityMaster() {
		List<UniversityMaster> list =  iStudentManagementDAO.universityMaster();
		List list1 = new ArrayList<>();
		for(int i=0;i<list.size();i++){
		UniversityMaster universityMaster = new  UniversityMaster();
		universityMaster .setUniversityMasterId(list.get(i).getUniversityMasterId());
		universityMaster .setUniversityName(list.get(i).getUniversityName());
		universityMaster.setCourseMaster(null);
		list1.add(universityMaster);
		}
		return list1;
	}

	@Override
	public List<CourseMaster> getCourseByUniversity(Long universityMasterId) {
		List<CourseMaster> list =  iStudentManagementDAO.getCourseByUniversity(universityMasterId);
		List list1= new ArrayList<>();
		for(int i=0;i<list.size();i++){
			CourseMaster courseMaster = new CourseMaster();
			courseMaster.setCourseId(list.get(i).getCourseId());
			courseMaster.setCourseName(list.get(i).getCourseName());
			courseMaster.setUniversityMaster(null);
			list1.add(courseMaster);
		}
		return list1;
	}

	@Override
	public List<StudentAdmissionEntity> getStudentsByBatchForAssignInterviews(Long vacanyDetailsId, Long batchId) {
		List<StudentAdmissionEntity> list =  iStudentManagementDAO.getStudentsByBatchForAssignInterviews(vacanyDetailsId,batchId);
		List list1= new ArrayList<>();
		for(int i=0;i<list.size();i++){
			Object object = list.get(i);
			Object[] objArray = (Object[])object;
			StudentAdmissionEntity studentAdmissionEntity = new StudentAdmissionEntity();
			BigInteger id =(BigInteger)objArray[0];
			studentAdmissionEntity.setAdmissionNumberId(id.longValue());
			studentAdmissionEntity.setAdmissionNumber((String)objArray[1]);
			studentAdmissionEntity.setStudentFirstName((String)objArray[2]);
			studentAdmissionEntity.setStudentMiddleName((String)objArray[3]);
			studentAdmissionEntity.setGender((String)objArray[4]);
			BatchMaster batchMaster = new BatchMaster();
			batchMaster.setBatchName((String)objArray[5]);
			studentAdmissionEntity.setBatchMaster(batchMaster);
			list1.add(studentAdmissionEntity);
		}
			
		return list1;
	}
}
