package com.leonia.academy.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leonia.academy.entity.ActiveDTO;
import com.leonia.academy.entity.LeadFollowUpDTO;
import com.leonia.academy.idao.ILeadGeneratedReportDAO;
import com.leonia.academy.isevice.ILeadGeneratedReportService;

@Service
public class LeadGeneratedReportServiceImpl implements ILeadGeneratedReportService {
	@Autowired
	private ILeadGeneratedReportDAO iLeadGeneratedReportDAO;

	@Override
	public List<ActiveDTO> activelist() {
		return	iLeadGeneratedReportDAO.activelist();
	}

	@Override
	public List<LeadFollowUpDTO> getLeadDetails(LeadFollowUpDTO leadFollowUpDTO) {
		
		return iLeadGeneratedReportDAO.getLeadDetails(leadFollowUpDTO);
	}

	/*@Override
	public List<LeadFollowUpDTO> searchLeadList(String sdate, String edate) {
		return iLeadGeneratedReportDAO.searchLeadList12(sdate,edate);
	}*/

	@Override
	public List<LeadFollowUpDTO> searchLeadList(LeadFollowUpDTO leadFollowUpDTO) {
		return iLeadGeneratedReportDAO.searchLeadList(leadFollowUpDTO);
	}

	

	
}
