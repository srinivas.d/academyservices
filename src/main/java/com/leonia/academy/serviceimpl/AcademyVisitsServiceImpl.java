package com.leonia.academy.serviceimpl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leonia.academy.entity.DropDownMasterEntity;
import com.leonia.academy.entity.DropDownSubMasterEntity;
import com.leonia.academy.entity.InstituteContactPersonEntity;
import com.leonia.academy.entity.InstituteMasterEntity;
import com.leonia.academy.entity.VisitEntryEntity;
import com.leonia.academy.idao.IAcademyVisitsDAO;
import com.leonia.academy.isevice.IAcademyVisitsService;

@Service
public class AcademyVisitsServiceImpl implements IAcademyVisitsService{
	
	@Autowired
	private IAcademyVisitsDAO iAcademyVisitsDAO;

	@SuppressWarnings("rawtypes")
	@Override
	public List<DropDownMasterEntity> getInstituteType() {
		List<DropDownMasterEntity> instituteType = iAcademyVisitsDAO.getInstituteType();
		
		List<DropDownMasterEntity> list2= new LinkedList<>();
		
		for(int i=0;i<instituteType.size();i++){
			DropDownMasterEntity entity = new DropDownMasterEntity();		
			entity.setDropdownmasterId(instituteType.get(i).getDropdownmasterId());
			entity.setDescription(instituteType.get(i).getDescription());
			
			//DropDownMasterEntity dropDownMasterEntity = instituteType.get(i);
			List<DropDownSubMasterEntity> downSubMasterEntity = instituteType.get(i).getDownSubMasterEntity();
			List<DropDownSubMasterEntity> dropDownSubMasterEntity = new ArrayList<>();
			for(int j=0;j<downSubMasterEntity.size();j++){
				
				DropDownSubMasterEntity dropDownSubMasterEntity1 = new DropDownSubMasterEntity();
				dropDownSubMasterEntity1.setDropDownSubMasterId(downSubMasterEntity.get(j).getDropDownSubMasterId());
				dropDownSubMasterEntity1.setDescription(downSubMasterEntity.get(j).getDescription());
				dropDownSubMasterEntity.add(dropDownSubMasterEntity1);
				
			}
			entity.setDownSubMasterEntity(dropDownSubMasterEntity);
			list2.add(entity);
			
		}
		return list2;
	}

	@Override
	public String saveInstituteMasterDetails(InstituteMasterEntity instituteMasterentity) {
		return iAcademyVisitsDAO.saveInstituteMasterDetails(instituteMasterentity);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<InstituteMasterEntity> getInstituteMasterList(String df) {
		List<InstituteMasterEntity> list =  iAcademyVisitsDAO.getInstituteMasterList(df);
		List<InstituteMasterEntity> list2= new LinkedList<>();
		for(int i=0;i<list.size();i++){
			InstituteMasterEntity entity = new InstituteMasterEntity();
			entity.setInstituteId(list.get(i).getInstituteId());
			entity.setInstituteName(list.get(i).getInstituteName());
			entity.setStatus(list.get(i).getStatus());
			list2.add(entity);
		}
		return  list2;
	}

	@SuppressWarnings({ "rawtypes", "unused", "unchecked" })
	@Override
	public List getContactPersons(Long instituteId) {
		InstituteContactPersonEntity contactPersonEntity = new InstituteContactPersonEntity();
		
		List<InstituteContactPersonEntity> contactPersons = iAcademyVisitsDAO.getContactPersons(instituteId);
		for (InstituteContactPersonEntity instituteContactPersonEntity : contactPersons) {
			contactPersonEntity = instituteContactPersonEntity;
		}
		
		return contactPersons;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public InstituteMasterEntity editInstituteMaster(int instituteId) {
		InstituteMasterEntity editInstituteMaster = iAcademyVisitsDAO.editInstituteMaster(instituteId);
		
		String instituteType2 = editInstituteMaster.getInstituteType();
		String allocationManager = editInstituteMaster.getAllocationManager();
		String instituteType =	null;
		if(instituteType2 != null )
		 instituteType =	getInstituteTypeByInstituteTypeId(new Integer(instituteType2));
		String allomng =	getInstituteTypeByInstituteTypeId(new Integer(allocationManager));
		InstituteMasterEntity instituteMasterEntity = new InstituteMasterEntity();
		
		instituteMasterEntity.setInstituteId(editInstituteMaster.getInstituteId());
		instituteMasterEntity.setInstituteName(editInstituteMaster.getInstituteName());
		instituteMasterEntity.setInstituteType(editInstituteMaster.getInstituteType());
		instituteMasterEntity.setInstituteTypeName(instituteType);
		instituteMasterEntity.setInstituteAddress(editInstituteMaster.getInstituteAddress());
		instituteMasterEntity.setLandMark(editInstituteMaster.getLandMark());
		instituteMasterEntity.setInstitutePhone(editInstituteMaster.getInstitutePhone());
		instituteMasterEntity.setFax(editInstituteMaster.getFax());
		instituteMasterEntity.setEmail(editInstituteMaster.getEmail());
		instituteMasterEntity.setAllocationManager(editInstituteMaster.getAllocationManager());
		instituteMasterEntity.setAllocmngName(allomng);
		
		List<InstituteContactPersonEntity> contactPersonEntities = editInstituteMaster.getContactPersonEntities();
		List<InstituteContactPersonEntity> contactPersonEntities2 = new ArrayList<>();
		for (InstituteContactPersonEntity instituteContactPersonEntity : contactPersonEntities) {
			InstituteContactPersonEntity instituteContactPersonEntity2 = new InstituteContactPersonEntity();
			instituteContactPersonEntity2.setInstituteContactPersonEntityId(instituteContactPersonEntity.getInstituteContactPersonEntityId());
			instituteContactPersonEntity2.setContactPersonName(instituteContactPersonEntity.getContactPersonName());
			instituteContactPersonEntity2.setContactPersonDesignation(instituteContactPersonEntity.getContactPersonDesignation());
			instituteContactPersonEntity2.setContactPersonPhone(instituteContactPersonEntity.getContactPersonPhone());
			instituteContactPersonEntity2.setContactPersonEmail(instituteContactPersonEntity.getContactPersonEmail());
			contactPersonEntities2.add(instituteContactPersonEntity2);
		}
		instituteMasterEntity.setContactPersonEntities(contactPersonEntities2);
//		editInstituteMaster.setContactPersonEntities(null);
		return instituteMasterEntity;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List getContactInfo(int instituteId) {
	List<InstituteContactPersonEntity> contactPersonEntities =	iAcademyVisitsDAO.getContactInfo(instituteId);
	List list1 = new ArrayList<>();
		for (InstituteContactPersonEntity instituteContactPersonEntity : contactPersonEntities) {
			List list = new ArrayList<>();
			list.add(instituteContactPersonEntity.getContactPersonName());
			list.add(instituteContactPersonEntity.getContactPersonDesignation());
			list.add(instituteContactPersonEntity.getContactPersonEmail());
			list.add(instituteContactPersonEntity.getContactPersonPhone());
			list1.add(list);
		}
		return list1;
	}

	@Override
	public String updateInstituteMasterDetails(InstituteMasterEntity instituteMasterentity) {
		return iAcademyVisitsDAO.updateInstituteMasterDetails(instituteMasterentity);
	}

	@Override
	public String saveVisitEntryDetails(VisitEntryEntity visitEntryEntity) {
		return iAcademyVisitsDAO.saveVisitEntryDetails(visitEntryEntity);
	}

	@Override
	public String getInstituteTypeByInstituteTypeId(Integer instituteTypeId) {
		return iAcademyVisitsDAO.getInstituteTypeByInstituteTypeId(instituteTypeId);
	}

	@Override
	public String deactivateInstitute(InstituteMasterEntity entity) {
		return iAcademyVisitsDAO.deactivateInstitute(entity);
	}

	@Override
	public List<VisitEntryEntity> getvisitreport(VisitEntryEntity visitEntryEntity) {
		List<VisitEntryEntity> getvisitreport = iAcademyVisitsDAO.getvisitreport(visitEntryEntity);
		
		List<DropDownSubMasterEntity> downSubMasterEntity = iAcademyVisitsDAO.getDropDownSubMasterList();
		List<InstituteMasterEntity> instituteMasterList = iAcademyVisitsDAO.getInstituteMasterList("");
		
		/*	for(int i=0;i<getvisitreport.size();i++){
			  l: for(int j=0;j<downSubMasterEntity.size();j++){
				  System.out.println("VisitPurpose==="+getvisitreport.get(i).getVisitPurpose());
				   Integer visitPurposeId =new Integer( getvisitreport.get(i).getVisitPurpose());
				   Integer downSubMasterEntityId=downSubMasterEntity.get(j).getDropDownSubMasterId();
				if(visitPurposeId.equals(downSubMasterEntityId)){
					getvisitreport.get(i).setVisitPurposeName(downSubMasterEntity.get(j).getDescription());
					break l;
				}
			 }
			
			 l: for(int k=0;k<instituteMasterList.size();k++){
				   Integer instituteNameId =new Integer( getvisitreport.get(i).getInstituteName());
				   Integer instId=instituteMasterList.get(k).getInstituteId();
				if(instituteNameId.equals(instId)){
					getvisitreport.get(i).setInstituteName(instituteMasterList.get(k).getInstituteName());
					break l;
				}
			 }
			 
			 l: for(int j=0;j<downSubMasterEntity.size();j++){
				   Integer VisitRatingId =new Integer( getvisitreport.get(i).getVisitRating());
				   Integer downSubMasterEntityId=downSubMasterEntity.get(j).getDropDownSubMasterId();
				if(VisitRatingId.equals(downSubMasterEntityId)){
					getvisitreport.get(i).setVisitRatingName(downSubMasterEntity.get(j).getDescription());
					break l;
				}
			 }
			
		}*/
		return getvisitreport;
	}

	@Override
	public List<VisitEntryEntity> getscheduledvisitreport(VisitEntryEntity visitEntryEntity) {
		
		
		
		return iAcademyVisitsDAO.getscheduledvisitreport(visitEntryEntity);
	}
	
	@Override
	public List<DropDownSubMasterEntity> getDropDownSumMasterList(){
	List<DropDownSubMasterEntity> downSubMasterEntity = iAcademyVisitsDAO.getDropDownSubMasterList();
	List<DropDownSubMasterEntity> downSubMasterEntityTemp = new ArrayList<>();
	for (DropDownSubMasterEntity dropDownSubMasterEntity : downSubMasterEntity) {
		DropDownSubMasterEntity downSubMasterEntity2 = new DropDownSubMasterEntity();
		downSubMasterEntity2.setDropDownSubMasterId(dropDownSubMasterEntity.getDropDownSubMasterId());
		downSubMasterEntity2.setDropdownmasterId(dropDownSubMasterEntity.getDropdownmasterId());
		downSubMasterEntity2.setDescription(dropDownSubMasterEntity.getDescription());
		downSubMasterEntityTemp.add(downSubMasterEntity2);
	}
	return  downSubMasterEntityTemp;
	}
	
}
