package com.leonia.academy.daoimpl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.leonia.academy.entity.BatchMaster;
import com.leonia.academy.entity.Bedmaster;
import com.leonia.academy.entity.Blockmaster;
import com.leonia.academy.entity.Buildingmaster;
import com.leonia.academy.entity.CourseMaster;
import com.leonia.academy.entity.Floormaster;
import com.leonia.academy.entity.HostelFeeAssingToStudent;
import com.leonia.academy.entity.InstallmentHostelFee;
import com.leonia.academy.entity.InstallmentMaster;
import com.leonia.academy.entity.RoomAllotment;
import com.leonia.academy.entity.RoomCategory;
import com.leonia.academy.entity.Roomdeallocationmaster;
import com.leonia.academy.entity.Roommaster;
import com.leonia.academy.entity.StudentAdmissionEntity;
import com.leonia.academy.entity.Totalroomallocatiodetails;
import com.leonia.academy.idao.IHostelManagementDAO;

@Repository
public class HostelManagementDAOImpl implements IHostelManagementDAO {

	@Autowired
	private SessionFactory sessionFactory;

	private static final Logger logger = Logger.getLogger(HostelManagementDAOImpl.class);

	@Override
	public List<StudentAdmissionEntity> getStayCategoryStudents() {
		Session openSession = null;
		List list = new LinkedList();
		try{
			openSession = sessionFactory.openSession();
			list=openSession.createSQLQuery("SELECT s.studentfirstname,s.admissionnumber,s.gender,r.allotmentcode,s.admissionnumberid FROM studentadmission s left join  roomallotment r   on s.admissionnumberid=r.admissionnumberId where s.staycategoryid=1 ORDER BY s.admissionnumberid;").list();
		}catch(Exception exception){
			logger.error("Exception raised in getStayCategoryStudents Method:", exception);
		}
		finally {
			openSession.close();
		}
		return list;
	}

	@Override
	public List<Buildingmaster> getBuildingdetails(String gender) {
		Session openSession = null;
		List<Buildingmaster> list = null;
		try{
			openSession = sessionFactory.openSession();
			Criteria criteria=openSession.createCriteria(Buildingmaster.class,"building");
			criteria.createAlias("building.blockmaster", "block");
			criteria.createAlias("block.floormaster", "floor");
			criteria.createAlias("floor.roommaster", "room");
			criteria.add(Restrictions.eq("building.gendertype",gender));
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("building.buildingId"))
					.add(Projections.groupProperty("building.buildingCode"))
				    .add(Projections.groupProperty("building.buildingName"))
				    .add(Projections.sum("room.capacity"))
				    .add(Projections.sum("room.countoccupancy")));
			list=criteria.list();	
		}catch(Exception exception){
			logger.error("Exception raised in getBuildingDetails Method:", exception);
		}
		finally {
			openSession.close();
		}
		return list;
	}

	@Override
	public List<Blockmaster> getBlockdetails(Integer buildingid) {
		Session openSession = null;
		List<Blockmaster> list = null;
		try{
			openSession = sessionFactory.openSession();
			Criteria criteria=openSession.createCriteria(Blockmaster.class,"block");
			criteria.createAlias("block.buildingmaster", "build");
			criteria.createAlias("block.floormaster", "floor");
			criteria.createAlias("floor.roommaster", "room");
			criteria.add(Restrictions.eq("build.buildingId",buildingid));
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("block.blockId"))
				    .add(Projections.groupProperty("block.blockName"))
				    .add(Projections.sum("room.capacity"))
				    .add(Projections.sum("room.countoccupancy")));
			
			list=criteria.list();	
		}catch(Exception exception){
			logger.error("Exception raised in getblockdetails Method:", exception);
		}
		finally {
			openSession.close();
		}
		return list;
	}

	@Override
	public List<Floormaster> getFloordetails(Integer blockid) {
		Session openSession = null;
		List<Floormaster> list = null;
		try{
			openSession = sessionFactory.openSession();
			Criteria criteria=openSession.createCriteria(Floormaster.class,"floor");
			criteria.createAlias("floor.blockmaster", "block");
			criteria.createAlias("floor.roommaster", "room");
			criteria.add(Restrictions.eq("block.blockId",blockid));
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("floor.floorId"))
					.add(Projections.groupProperty("floor.floorName"))
				    .add(Projections.sum("room.capacity"))
				    .add(Projections.sum("room.countoccupancy")));
			list=criteria.list();	
		}catch(Exception exception){
			logger.error("Exception raised in getFloordetails Method:", exception);
		}
		finally {
			openSession.close();
		}
		return list;
	}

	@Override
	public List<Roommaster> getRoomdetails(Integer floorid) {
		Session openSession = null;
		List<Roommaster> list = null;
		try{
			openSession = sessionFactory.openSession();
			Criteria criteria=openSession.createCriteria(Roommaster.class,"room");
			
			criteria.createAlias("room.roomCategory", "roomcategory");
			criteria.createAlias("room.floormaster", "floor");
			criteria.add(Restrictions.eq("floor.floorId",floorid));
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("room.roomId"))
					.add(Projections.groupProperty("room.roomNumber"))
				    .add(Projections.sum("room.capacity"))
				    .add(Projections.sum("room.countoccupancy"))
				    .add(Projections.groupProperty("room.items"))
				    .add(Projections.groupProperty("roomcategory.categoryname"))
				    .add(Projections.groupProperty("roomcategory.categoryid"))
				    );
			list=criteria.list();	
		}catch(Exception exception){
			logger.error("Exception raised in getRoomDetails Method:", exception);
		}
		finally {
			openSession.close();
		}
		return list;
	}

	@Override
	public List<Bedmaster> getBeddetails(Integer roomid) {
		Session openSession = null;
		List<Bedmaster> list = null;
		try{
			openSession = sessionFactory.openSession();
			Criteria criteria=openSession.createCriteria(Bedmaster.class,"bed");
			criteria.createAlias("bed.roommaster", "room");
			criteria.add(Restrictions.eq("room.roomId",roomid));
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("bed.bedid"))
					.add(Projections.groupProperty("bed.bedcode"))
					.add(Projections.groupProperty("bed.bedstatus")));
			list=criteria.list();	
		}catch(Exception exception){
			logger.error("Exception raised in getBedDetails Method:", exception);
		}
		finally {
			openSession.close();
		}
		return list;
	}

	@Override
	public String allocation(RoomAllotment roomallot) {
		Integer roomno=roomallot.getRoomId();
		Session openSession = null;
		Transaction beginTransaction=null;
		int bedid=roomallot.getBedId();
		int blockid=roomallot.getBlockId();
		int buildingid=roomallot.getBuildingId();
		int bflooridedid=roomallot.getFloorId();
		int roomid=roomallot.getRoomId();
		try{
			openSession = sessionFactory.openSession();
			beginTransaction = openSession.beginTransaction();
			
			Criteria criteria1=openSession.createCriteria(Buildingmaster.class);
			 criteria1.add(Restrictions.eq("buildingId", buildingid));
			 Buildingmaster bdto=(Buildingmaster)criteria1.uniqueResult();
			
				Criteria criteria2=openSession.createCriteria(Blockmaster.class);
				 criteria2.add(Restrictions.eq("blockId", blockid));
				 Blockmaster bldto=(Blockmaster)criteria2.uniqueResult();
					
			
				Criteria criteria3=openSession.createCriteria(Floormaster.class);
				 criteria3.add(Restrictions.eq("floorId", bflooridedid));
				 Floormaster fldto=(Floormaster)criteria3.uniqueResult();
					
				Criteria criteria4=openSession.createCriteria(Roommaster.class);
				 criteria4.add(Restrictions.eq("roomId", roomid));
				 Roommaster rodto=(Roommaster)criteria4.uniqueResult();
			
				Criteria criteria5=openSession.createCriteria(Bedmaster.class);
				 criteria5.add(Restrictions.eq("bedid", bedid));
				 Bedmaster bedto=(Bedmaster)criteria5.uniqueResult();
				roomallot.setAllotmentCode(bdto.getBuildingName()+"/"+bldto.getBlockName()+"/"+fldto.getFloorName()+"/"+rodto.getRoomNumber()+"/"+bedto.getBedcode());	
				
		    openSession.save(roomallot);	
			Criteria createCriteria=openSession.createCriteria(Roommaster.class);
			createCriteria.add(Restrictions.eq("roomId",roomno));
			Roommaster roommaster=(Roommaster)createCriteria.uniqueResult();
			Integer occupancy=roommaster.getCountoccupancy();
			if(occupancy==null)
			{
				roommaster.setCountoccupancy(1);
			}else{
			roommaster.setCountoccupancy(occupancy+1);
			}
			openSession.merge(roommaster);	
			SQLQuery query=openSession.createSQLQuery("update bedmaster set bedstatus='Allocated' where bed_id='"+bedid+"'");
			query.executeUpdate();
			
		}catch(Exception exception){
			logger.error("Exception raised in room allocation Method:", exception);
		}
		finally {
			beginTransaction.commit();
			openSession.close();
		}
		return "Successfully allocated";
	}

	@Override
	public String totalroomallocationdetails(Totalroomallocatiodetails totalroomallocationdetails, Long admissionNumberId) {
		Session openSession = null;
		Transaction beginTransaction=null;
		int bedid=totalroomallocationdetails.getBedId();
		int blockid=totalroomallocationdetails.getBlockId();
		int buildingid=totalroomallocationdetails.getBuildingId();
		int bflooridedid=totalroomallocationdetails.getFloorId();
		int roomid=totalroomallocationdetails.getRoomId();
		try{
			openSession = sessionFactory.openSession();
			beginTransaction = openSession.beginTransaction();
			
			Query query=openSession.createQuery("update Totalroomallocatiodetails set status=0 where status=1 and admissionNumberId='"+admissionNumberId+"' ");
			query.executeUpdate();
			
			     Criteria criteria1=openSession.createCriteria(Buildingmaster.class);
			     criteria1.add(Restrictions.eq("buildingId", buildingid));
			     Buildingmaster bdto=(Buildingmaster)criteria1.uniqueResult();
			
				 Criteria criteria2=openSession.createCriteria(Blockmaster.class);
				 criteria2.add(Restrictions.eq("blockId", blockid));
				 Blockmaster bldto=(Blockmaster)criteria2.uniqueResult();
					
				 Criteria criteria3=openSession.createCriteria(Floormaster.class);
				 criteria3.add(Restrictions.eq("floorId", bflooridedid));
				 Floormaster fldto=(Floormaster)criteria3.uniqueResult();
					
				 Criteria criteria4=openSession.createCriteria(Roommaster.class);
				 criteria4.add(Restrictions.eq("roomId", roomid));
				 Roommaster rodto=(Roommaster)criteria4.uniqueResult();
			
				 Criteria criteria5=openSession.createCriteria(Bedmaster.class);
				 criteria5.add(Restrictions.eq("bedid", bedid));
				 Bedmaster bedto=(Bedmaster)criteria5.uniqueResult();
				 totalroomallocationdetails.setAllotmentCode(bdto.getBuildingName()+"/"+bldto.getBlockName()+"/"+fldto.getFloorName()+"/"+rodto.getRoomNumber()+"/"+bedto.getBedcode());
				 openSession.save(totalroomallocationdetails);
		        }catch(Exception exception){
		        	logger.error("Exception raised in totalRoomAllocationdetails Method:", exception);
		        }
				finally {
					beginTransaction.commit();
					openSession.close();
				}
		return "Successfully allocated Totalroomallocatiodetails";
	}

	@Override
	public List<Map<String, Object>> generatereceipt(Long admissionid) {
		List<Map<String, Object>> list=new LinkedList<Map<String, Object>>();
		Map<String,Object> map=new LinkedHashMap<String,Object>();
		Session openSession = null;
		try{
			openSession = sessionFactory.openSession();
			Criteria criteria=openSession.createCriteria(RoomAllotment.class,"ra");
			criteria.createAlias("ra.studentAdmission", "sa");
			criteria.add(Restrictions.eq("sa.admissionNumberId", admissionid));
			criteria.setProjection(Projections.property("ra.allotmentCode"));
			String rr=(String)criteria.uniqueResult();
			criteria.setProjection(Projections.property("ra.allotdatetime"));
			Date date=(Date) criteria.uniqueResult();
			DateFormat dateFormat = new SimpleDateFormat(" MM/dd/yyyy hh:mm");
			String datestring=dateFormat.format(date);
			criteria.setProjection(Projections.property("ra.itemsallocated"));
			String items=(String)criteria.uniqueResult();
			String str[]=rr.split("/");
			map.put("buildingname", str[0]);
			map.put("blockname", str[1]);
			map.put("floorname", str[2]);
			map.put("roomno", str[3]);
			map.put("bedno", str[4]);
			map.put("allotdatetime", datestring);
			map.put("items", items);
			
			Criteria criteria2=openSession.createCriteria(StudentAdmissionEntity.class,"sa");
			criteria2.createAlias("sa.batchMaster", "bms");
			criteria2.add(Restrictions.eq("sa.admissionNumberId", admissionid));
			criteria2.setProjection(Projections.property("sa.admissionNumberId"));
			Long addmissionid=(Long)criteria2.uniqueResult();
			criteria2.setProjection(Projections.property("sa.studentFirstName"));
			String fname=(String)criteria2.uniqueResult();
			criteria2.setProjection(Projections.property("sa.studentMiddleName"));
			String mname=(String)criteria2.uniqueResult();
			criteria2.setProjection(Projections.property("sa.studentLastName"));
			String lname=(String)criteria2.uniqueResult();
			String studentname=fname+" "+mname+" "+lname;
			map.put("admissionid",addmissionid );
			map.put("studentname",studentname );
			
			criteria2.setProjection(Projections.property("sa.courseId"));
			Long courseid=(Long)criteria2.uniqueResult();
			criteria2.setProjection(Projections.property("bms.batchId"));
			Long batchid=(Long)criteria2.uniqueResult();
			
			Criteria criteria3=openSession.createCriteria(CourseMaster.class,"cm");
			criteria3.add(Restrictions.eq("cm.courseId", courseid));
			criteria3.setProjection(Projections.property("cm.shortCode"));
			String coursename=(String)criteria3.uniqueResult();
			Criteria criteria4=openSession.createCriteria(BatchMaster.class,"bm");
			criteria4.add(Restrictions.eq("bm.batchId", batchid));
			criteria4.setProjection(Projections.property("bm.batchName"));
			String batchname=(String)criteria4.uniqueResult();	
			map.put("course",coursename);
			map.put("batch",batchname);
			list.add(map);
		}catch(Exception exception){
			logger.error("Exception raised in generatereceipt for student Method:", exception);
		}
		finally {
			openSession.close();
		}
		return list;
	}

	@Override
	public List getSnapshot() {
		Session openSession = null;
		List<Buildingmaster> list = null;
		try{
			openSession = sessionFactory.openSession();
			Criteria criteria=openSession.createCriteria(Buildingmaster.class,"building");
			criteria.createAlias("building.blockmaster", "block");
			criteria.createAlias("block.floormaster", "floor");
			criteria.createAlias("floor.roommaster", "room");
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("building.buildingId"))
					.add(Projections.groupProperty("building.buildingCode"))
				    .add(Projections.groupProperty("building.buildingName"))
				    .add(Projections.sum("room.capacity"))
				    .add(Projections.sum("room.countoccupancy")));
			list=criteria.list();	
		}catch(Exception exception){
			logger.error("Exception raised in getSnapshot Method:", exception);
		}
		finally {
			openSession.close();
		}
		return list;
	}

	@Override
	public List getAllocatedStudents() {
		Session openSession = null;
		List list = null;
		try{
			openSession = sessionFactory.openSession();
			SQLQuery query=openSession.createSQLQuery("select r.allotmentcode,s.studentfirstname,s.admissionnumberid,c.shortcode,b.batchname from roomallotment r  INNER JOIN studentadmission s ON r.admissionnumberid=s.admissionnumberid INNER JOIN coursemaster c ON c.courseid=s.courseid INNER JOIN batchmaster b ON b.batchid=s.batchid;");
			list=query.list();
		}catch(Exception exception){
			logger.error("Exception raised in getAllocatedStudents Method:", exception);
		}
		finally {
			openSession.close();
		}
		return list;
	}

	@Override
	public String getItems(String roomno) {
		Session openSession = null;
		String items=null;
		try{
			openSession = sessionFactory.openSession();
			Criteria createcritera=openSession.createCriteria(Roommaster.class,"rd");
			createcritera.add(Restrictions.eq("rd.roomNumber", roomno));
			createcritera.setProjection(Projections.property("rd.items"));
			items=(String)createcritera.uniqueResult();
		}catch(Exception exception){
			logger.error("Exception raised in getItems Method:", exception);
		}
		finally {
			openSession.close();
		}
		return items;
	}

	@Override
	public String deallocate(String roomno, String bedno, Long admissionid,Roomdeallocationmaster master) {
		Session openSession = null;
		Transaction tx=null;
		String items=null;
		List list=new LinkedList();
		try{
			openSession = sessionFactory.openSession();
			tx=openSession.beginTransaction();
			Criteria createcriteria=openSession.createCriteria(RoomAllotment.class,"ra");
			createcriteria.createAlias("ra.studentAdmission", "sa");
			createcriteria.add(Restrictions.eq("sa.admissionNumberId", admissionid));
			createcriteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("sa.admissionNumberId")));
			list=createcriteria.list();
			if(list.size()>0){
			openSession.save(master);
			Query query2=openSession.createQuery("update Bedmaster b set b.bedstatus=null where b.bedcode='"+bedno+"'");
			query2.executeUpdate();
			Criteria criteria=openSession.createCriteria(Roommaster.class,"rd");
			criteria.add(Restrictions.eq("rd.roomNumber", roomno));
			criteria.setProjection(Projections.property("rd.countoccupancy"));
			Integer occupancy=(Integer) criteria.uniqueResult();	
			Query query4=openSession.createQuery("update Roommaster set countoccupancy='"+occupancy+"'-1 where roomnumber='"+roomno+"'");
			query4.executeUpdate();
			Query query3=openSession.createQuery("delete from RoomAllotment where admissionNumberId='"+admissionid+"' ");
			query3.executeUpdate();	
			}
		}catch(Exception exception){
			logger.error("Exception raised in deallocate Method:", exception);
		}
		finally {
			tx.commit();
			openSession.close();
		}
		return "succesfully deallocated";
	}

	@Override
	public List getFeeAmountdata(Long admissionid) {
		Session openSession = null;
		List list=null;
		try{
			openSession = sessionFactory.openSession();
			SQLQuery query=openSession.createSQLQuery("select h.paidamount,h.dueamount from leoacademy_leo_hostelfeeassingtostudent h  where admissionnumberid='"+admissionid+"'");
		    list=query.list();
		}catch(Exception exception){
			logger.error("Exception raised in getFeeAmountdata Method:", exception);
		}
		finally {
			openSession.close();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Buildingmaster> getBuildingMaster() {
		Session openSession = sessionFactory.openSession();
		List<Buildingmaster> list = null;
		try{
			Criteria criteria = openSession.createCriteria(Buildingmaster.class);
			list = criteria.list();
		}catch(Exception e){
			logger.error("Exception raised in BuildingMaster Method:", e);
		}finally {
			openSession.close();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RoomCategory> getRoomCategory() {
		Session openSession = sessionFactory.openSession();
		List<RoomCategory> list = null;
		try{
			Criteria criteria = openSession.createCriteria(RoomCategory.class);
			list = criteria.list();
		}catch(Exception e){
			logger.error("Exception raised in RoomCategory Method:", e);
		}finally {
			openSession.close();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<InstallmentMaster> getInstallment() {
		Session openSession = sessionFactory.openSession();
		List<InstallmentMaster> list = null;
		try{
			Criteria criteria = openSession.createCriteria(InstallmentMaster.class);
			list = criteria.list();
		}catch(Exception e){
			logger.error("Exception raised in Installment Method:", e);
		}finally {
			openSession.close();
		}
		return list;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<InstallmentHostelFee> getbuildingdefinefee(InstallmentHostelFee fee) {
		Session openSession = sessionFactory.openSession();
		List<InstallmentHostelFee> list = null;
		try{
			Criteria criteria = openSession.createCriteria(InstallmentHostelFee.class,"ihf")
			.setFetchMode("roomCategory",FetchMode.EAGER)
			.setFetchMode("buildingmaster",FetchMode.EAGER)
			.setFetchMode("installmentMaster",FetchMode.EAGER)
			.createAlias("ihf.buildingmaster", "buildingmaster")
			.createAlias("ihf.roomCategory", "roomCategory")
			.add(Restrictions.eq("buildingmaster.buildingId",fee.getBuildingmaster().getBuildingId() ))
			.add(Restrictions.eq("roomCategory.categoryid", fee.getRoomCategory().getCategoryid()));
			if(fee.getInstallmentMaster()!= null){
			criteria.createAlias("ihf.installmentMaster", "installmentMaster");
			criteria.add(Restrictions.eq("installmentMaster.instid", fee.getInstallmentMaster().getInstid()));
			}
			list = criteria.list();
		}catch(Exception e){
			logger.error("Exception raised in buildingdefinefee Method:", e);
			e.printStackTrace();
		}finally {
			openSession.close();
		}
		return list;
	}

	@Override
	public String savedefineHostelfee(InstallmentHostelFee fee) {
		Session openSession = sessionFactory.openSession();
		Transaction beginTransaction = openSession.beginTransaction();
		String msg =null;
		try{
			openSession.save(fee);
			beginTransaction.commit();
			msg="Successfully Saved";
		}catch (Exception e) {
			logger.error("Exception raised in savedefineHostelfee Method:", e);
		}finally {
			openSession.close();
		}
		return msg;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Blockmaster> getBlockdetails123(Integer buildingid) {
		Session openSession = sessionFactory.openSession();
		List<Blockmaster> list = null;
		try{
			Criteria criteria = openSession.createCriteria(Blockmaster.class,"blm")
					.setFetchMode("buildingmaster",FetchMode.EAGER)
					.createAlias("blm.buildingmaster", "buildingmaster")
			.add(Restrictions.eq("buildingmaster.buildingId", buildingid));
			list = criteria.list();
		}catch(Exception e){
			logger.error("Exception raised in RoomCategory Method:", e);
		}finally {
			openSession.close();
		}
		return list;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<StudentAdmissionEntity> getbuildingfeeStudents(InstallmentHostelFee fee) {
		Session opensession = null;
		List list = null;
		try{
			opensession=sessionFactory.openSession();
			String sql = "Select s.admissionnumberId,s.admissionNumber,s.studentfirstname,s.studentMiddleName,"
					+ "s.studentLastName,s.gender,r.alltId From studentadmission s,roomallotment r where "
					+ "(s.admissionnumberId not in (select coalesce(admissionnumberId,0) as colid from "
					+ "leoacademy_leo_hostelfeeassingtostudent where buildingid='"+fee.getBuildingmaster().getBuildingId()+"' and  categoryid ='"+fee.getRoomCategory().getCategoryid()+"' and instid='"+fee.getInstallmentMaster().getInstid()+"')) "
					+ "and  r.buildingid='"+fee.getBuildingmaster().getBuildingId()+"' and  r.categoryid ='"+fee.getRoomCategory().getCategoryid()+"' and s.admissionnumberId = r.admissionnumberId";
			SQLQuery createSQLQuery = opensession.createSQLQuery(sql);
			list=createSQLQuery.list();
		}catch(Exception exception){
			logger.error("Exception raised in getbuildingfeeStudents Method:", exception);
		}finally {
			opensession.close();
		}
		return list;
	}

	@Override
	public String assignedtohostelfees(HostelFeeAssingToStudent feeAssingToStudent) {
		Session openSession = sessionFactory.openSession();
		Transaction beginTransaction = openSession.beginTransaction();
		String msg =null;
		try{
			openSession.save(feeAssingToStudent);
			beginTransaction.commit();
			msg="Successfully Saved";
		}catch (Exception e) {
			logger.error("Exception raised in getbuildingfeeStudents Method:", e);
			e.printStackTrace();
		}finally {
			openSession.close();
		}
		return msg;
	}

	

	}
