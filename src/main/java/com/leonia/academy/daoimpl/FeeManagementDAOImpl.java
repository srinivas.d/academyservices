package com.leonia.academy.daoimpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.leonia.academy.entity.CardTypeMaster;
import com.leonia.academy.entity.DiscountApprovedMaster;
import com.leonia.academy.entity.FeeAssignToStudents;
import com.leonia.academy.entity.FeeCreation;
import com.leonia.academy.entity.FeeGenerationBatchWise;
import com.leonia.academy.entity.FeePaidTransactionsDTO;
import com.leonia.academy.entity.FeeSettlementDTO;
import com.leonia.academy.entity.FeesCategory;
import com.leonia.academy.entity.HostelFeeAssingToStudent;
import com.leonia.academy.entity.InstallmentTypeMaster;
import com.leonia.academy.entity.PaymodeMaster;
import com.leonia.academy.entity.ReceiptGenerateDTO;
import com.leonia.academy.entity.StudentAdmissionEntity;
import com.leonia.academy.idao.IFeeManagementDAO;

/**
 * @author srinivas.d
 *
 */
@Repository
public class FeeManagementDAOImpl implements IFeeManagementDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	private static final Logger logger = Logger.getLogger(FeeManagementDAOImpl.class);
	
	
	/* (non-Javadoc)
	 * @see com.leonia.academy.idao.IFeeManagementDAO#getStudentSearchByDiscountCategory(java.lang.Long, java.lang.String)
	 */
	@Override
	public List<StudentAdmissionEntity> getStudentSearchByDiscountCategory(Long discountCategoryId, String searchName) {
		
		Session openSession = sessionFactory.openSession();
		List<StudentAdmissionEntity> studentlistsearchlist = null;
		try{
			Criteria createCriteria = openSession.createCriteria(StudentAdmissionEntity.class,"sa")
					.createAlias("sa.batchMaster", "batchMaster");
			if(searchName != ""){
				createCriteria.add(Restrictions.or(Restrictions.ilike("admissionNumber",searchName,MatchMode.ANYWHERE))
						.add(Restrictions.ilike("studentFirstName",searchName,MatchMode.ANYWHERE)));
			}
			if(discountCategoryId != null ){
				createCriteria.add(Restrictions.eq("discountCategoryId",discountCategoryId));
			}
			createCriteria.addOrder(Order.asc("admissionNumberId"));
			createCriteria.setProjection(Projections.projectionList()
					.add(Projections.property("admissionNumberId"),"admissionNumberId")
					.add(Projections.property("admissionNumber"),"admissionNumber")
					.add(Projections.property("studentFirstName"),"studentFirstName")
					.add(Projections.property("studentMiddleName"),"studentMiddleName")
					.add(Projections.property("studentLastName"),"studentLastName")
					.add(Projections.property("gender"),"gender")
					.add(Projections.property("batchMaster.batchName"),"batchName")
					.add(Projections.property("batchMaster.batchId"),"batchId"));
			createCriteria.setResultTransformer(Transformers.aliasToBean(StudentAdmissionEntity.class));
			
			studentlistsearchlist = createCriteria.list();
		}catch(Exception exception){
			logger.error("Exception raised in getStudentSearchByDiscountCategory Method:", exception);
		}
		finally {
			openSession.close();
		}
		return studentlistsearchlist;

	}


	/* (non-Javadoc)
	 * @see com.leonia.academy.idao.IFeeManagementDAO#discountApprovedMaster()
	 */
	@Override
	public List discountApprovedMaster() {
		Session openSession = sessionFactory.openSession();
		List<DiscountApprovedMaster> discountApprovedMasters = null;
		try{
			Criteria createCriteria = openSession.createCriteria(DiscountApprovedMaster.class);
			createCriteria.addOrder(Order.asc("discountApprovedmasterId"));
			createCriteria.setProjection(Projections.projectionList().add(Projections.property("discountApprovedmasterId"),"discountApprovedmasterId")
					.add(Projections.property("discountApprovedmasterName"),"discountApprovedmasterName"));
			createCriteria.setResultTransformer(Transformers.aliasToBean(DiscountApprovedMaster.class));
			discountApprovedMasters =  createCriteria.list();
	}catch(Exception e){
		logger.error("Exception raised in discountApprovedMaster Method:", e);
	}
	finally {
		openSession.close();
	}
	return  discountApprovedMasters;
	}


	/* (non-Javadoc)
	 * @see com.leonia.academy.idao.IFeeManagementDAO#studentFeediscountDetails(java.lang.Long, java.lang.Long)
	 */
	@Override
	public List studentFeediscountDetails(Long bacthId, Long admissionNumberId) {
		Session openSession= sessionFactory.openSession();
		List list = new LinkedList<>(); 
		try{
			Criteria createCriteria = openSession.createCriteria(FeeAssignToStudents.class,"fats")
					.createAlias("fats.studentAdmissionEntity","studentAdmission")
			.add(Restrictions.eq("fats.batchId", bacthId))
			.add(Restrictions.eq("studentAdmission.admissionNumberId", admissionNumberId)).addOrder(Order.asc("fats.feeAssignToStudentsId"));
			createCriteria.setProjection(Projections.projectionList().add(Projections.property("feeAssignToStudentsId"),"feeAssignToStudentsId")
					.add(Projections.property("termWiseMessage"),"termWiseMessage")
					.add(Projections.property("termWiseAmount"),"termWiseAmount")
					);
			createCriteria.setResultTransformer(Transformers.aliasToBean(FeeAssignToStudents.class));

			List list1 = createCriteria.list();
			list.add(list1);
			
			 Criteria createCriteria2 = openSession.createCriteria(HostelFeeAssingToStudent.class,"hfats")
					 .createAlias("hfats.StudentAdmission", "studentAdmission")
					 .add(Restrictions.eq("studentAdmission.admissionNumberId", admissionNumberId));
			 createCriteria2.setProjection(Projections.projectionList().add(Projections.property("hostelFeeAssingToStudentId"),"hostelFeeAssingToStudentId")
					 .add(Projections.property("instid"),"instid")
						.add(Projections.property("termWiseAmount"),"termWiseAmount"));
			 createCriteria2.setResultTransformer(Transformers.aliasToBean(HostelFeeAssingToStudent.class));
			 List list2 = createCriteria2.list();
			 list.add(list2);
		}catch(Exception exception){
			logger.error("Exception raised in studentFeediscountDetails Method:", exception);
			exception.printStackTrace();
			}
		finally {
			openSession.close();
		}
		return list;
	}


	/* (non-Javadoc)
	 * @see com.leonia.academy.idao.IFeeManagementDAO#feeAssignToStudentsAmountParticularFee(java.lang.Long)
	 */
	@Override
	public List feeAssignToStudentsAmountParticularFee(Long feeAssignToStudentsId) {
		Session openSession = null;
		Criteria createCriteria = null;
		List list = null;
		try{
			openSession= sessionFactory.openSession();
			createCriteria = openSession.createCriteria(FeeAssignToStudents.class)
					.add(Restrictions.eq("feeAssignToStudentsId", feeAssignToStudentsId));
			createCriteria.setProjection(Projections.projectionList()
					.add(Projections.property("termWiseAmount"),"termWiseAmount")
					.add(Projections.property("dueAmount"),"dueAmount")
					.add(Projections.property("netAmount"),"netAmount")
					.add(Projections.property("discountAmount"),"discountAmount"));
			createCriteria.setResultTransformer(Transformers.aliasToBean(FeeAssignToStudents.class));
			list = createCriteria.list();
		}catch(Exception exception){
			logger.error("Exception raised in feeAssignToStudentsAmountParticularFee Method:", exception);
		}finally {
			openSession.close();
		}
		return list;	
	}


	/* (non-Javadoc)
	 * @see com.leonia.academy.idao.IFeeManagementDAO#hostalfeeAssignToStudentsAmountParticularFee(java.lang.Long)
	 */
	@Override
	public List hostalfeeAssignToStudentsAmountParticularFee(Long hostelFeeAssingToStudentId) {
		Session openSession = null;
		Criteria createCriteria = null;
		List list = null;
		try{
			openSession= sessionFactory.openSession();
			createCriteria = openSession.createCriteria(HostelFeeAssingToStudent.class)
					.add(Restrictions.eq("hostelFeeAssingToStudentId", hostelFeeAssingToStudentId));
			createCriteria.setProjection(Projections.projectionList()
					.add(Projections.property("termWiseAmount"),"termWiseAmount")
					.add(Projections.property("dueAmount"),"dueAmount")
					.add(Projections.property("netAmount"),"netAmount")
					.add(Projections.property("discountAmount"),"discountAmount"));
			createCriteria.setResultTransformer(Transformers.aliasToBean(HostelFeeAssingToStudent.class));
			list = createCriteria.list();
		}catch(Exception exception){
			logger.error("Exception raised in hostalfeeAssignToStudentsAmountParticularFee Method:", exception);
		}finally {
			openSession.close();
		}
		return list;	
	}


	/* (non-Javadoc)
	 * @see com.leonia.academy.idao.IFeeManagementDAO#discountStuParticularfee(com.leonia.academy.entity.FeeAssignToStudents)
	 */
	@Override
	public String discountStuParticularfee(FeeAssignToStudents objStudentCategory) {
		Session openSession = sessionFactory.openSession();
		Transaction beginTransaction = openSession.beginTransaction();
		String message = null;
		try{
			
			FeeAssignToStudents assignToStudents =(FeeAssignToStudents) openSession.get(FeeAssignToStudents.class, objStudentCategory.getFeeAssignToStudentsId());
			assignToStudents.setFeeAssignToStudentsId(objStudentCategory.getFeeAssignToStudentsId());
			assignToStudents.setDiscountApprovedMaster(objStudentCategory.getDiscountApprovedMaster());
			assignToStudents.setDiscountCategoryMaster(objStudentCategory.getDiscountCategoryMaster());
			assignToStudents.setNetAmount(objStudentCategory.getNetAmount());
			assignToStudents.setDueAmount(objStudentCategory.getDueAmount());
			assignToStudents.setDiscountAmount(objStudentCategory.getDiscountAmount());
			assignToStudents.setRemarks(objStudentCategory.getRemarks());
			//openSession.merge(assignToStudents);
			message ="Discount Data Successfully Saved...";
			beginTransaction.commit();
			}catch(Exception e){
				logger.error("Exception raised in discountStuParticularfee Method:", e);
				message = "Discount Data not saved due to Exception!";
				e.printStackTrace();
		}finally {
			openSession.close();
		}
		return message;
	}


	/* (non-Javadoc)
	 * @see com.leonia.academy.idao.IFeeManagementDAO#discountStuHostelFee(com.leonia.academy.entity.FeeAssignToStudents)
	 */
	@Override
	public String discountStuHostelFee(FeeAssignToStudents objStudentCategory) {
		Session openSession = sessionFactory.openSession();
		Transaction beginTransaction = openSession.beginTransaction();
		String message = null;
		try{
			FeeAssignToStudents assignToStudents =(FeeAssignToStudents) openSession.get(FeeAssignToStudents.class, objStudentCategory.getFeeAssignToStudentsId());
			assignToStudents.setFeeAssignToStudentsId(objStudentCategory.getFeeAssignToStudentsId());
			assignToStudents.setDiscountApprovedMaster(objStudentCategory.getDiscountApprovedMaster());
			assignToStudents.setDiscountCategoryMaster(objStudentCategory.getDiscountCategoryMaster());
			assignToStudents.setNetAmount(objStudentCategory.getNetAmount());
			assignToStudents.setDueAmount(objStudentCategory.getDueAmount());
			assignToStudents.setDiscountAmount(objStudentCategory.getDiscountAmount());
			assignToStudents.setRemarks(objStudentCategory.getRemarks());
			//openSession.merge(objStudentCategory);
			message ="Discount Data Successfully Saved...";
			beginTransaction.commit();
			}catch(Exception e){
				logger.error("Exception raised in discountStuHostelFee Method:", e);
				message = "Discount Data not saved due to Exception!";
				e.printStackTrace();
			}finally {
			openSession.close();
		}
		return message;
	}

	
	/* (non-Javadoc)
	 * @see com.leonia.academy.idao.IFeeManagementDAO#getStudentListSearch(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<StudentAdmissionEntity> getStudentListSearch(String searchName) {
		Session openSession = sessionFactory.openSession();
		List<StudentAdmissionEntity> studentlistsearchlist = null;
		logger.info("Fetching StudentAdmission Data using SearchName:"+searchName);
		try{
			Criteria createCriteria = openSession.createCriteria(StudentAdmissionEntity.class,"sa")
					.createAlias("sa.batchMaster", "batchMaster");
			if(searchName != ""){
				createCriteria.add(Restrictions.or(Restrictions.ilike("admissionNumber",searchName,MatchMode.ANYWHERE))
						.add(Restrictions.ilike("studentFirstName",searchName,MatchMode.ANYWHERE)));
			}
			createCriteria.addOrder(Order.asc("admissionNumberId"));
			studentlistsearchlist = createCriteria.list();
			logger.debug("Interacted with database to fatch StudentAdmission:"+studentlistsearchlist);
		}catch(Exception exception){
			logger.error("Exception raised in getstudentlistsearch Method:", exception);
		}
		finally {
			openSession.close();
		}
		return studentlistsearchlist;
	}


	/* (non-Javadoc)
	 * @see com.leonia.academy.idao.IFeeManagementDAO#getFeeGenerationdetails(java.lang.Long, java.lang.Long)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<FeeAssignToStudents> getFeeGenerationdetails(Long admissionNumberId, Long bacthId) {
		Session openSession= sessionFactory.openSession();
		List list = null;
		logger.info("Fetching FeeGeneration Data using admissionNumberId&bacthId:"+admissionNumberId+""+bacthId);
		try{
			Criteria createCriteria = openSession.createCriteria(FeeAssignToStudents.class,"fats")
					.createAlias("fats.studentAdmissionEntity","studentAdmission")
			.add(Restrictions.eq("fats.batchId", bacthId))
			.add(Restrictions.eq("studentAdmission.admissionNumberId", admissionNumberId)).addOrder(Order.asc("fats.feeAssignToStudentsId"));
			createCriteria.setProjection(Projections.projectionList()
					.add(Projections.property("feeAssignToStudentsId"),"feeAssignToStudentsId")
					.add(Projections.property("batchId"),"batchId")
					.add(Projections.property("paidAmount"),"paidAmount")
					.add(Projections.property("dueAmount"),"dueAmount")
					.add(Projections.property("discountAmount"),"discountAmount")
					.add(Projections.property("termWiseMessage"),"termWiseMessage")
					.add(Projections.property("termWiseAmount"),"termWiseAmount")
					.add(Projections.property("netAmount"),"netAmount"));
			createCriteria.setResultTransformer(Transformers.aliasToBean(FeeAssignToStudents.class));
			list = createCriteria.list();
			logger.debug("Interacted with database to fatch FeeGeneration:"+list);
		}catch(Exception exception){
			logger.error("Exception raised in feeGenerationdetails Method:", exception);
			exception.printStackTrace();
		}
		finally {
			openSession.close();
		}
		return list;
	}


	/* (non-Javadoc)
	 * @see com.leonia.academy.idao.IFeeManagementDAO#gethostelFeeGenerationdetails(java.lang.Long)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<HostelFeeAssingToStudent> gethostelFeeGenerationdetails(Long admissionNumberId) {
		Session opensession=sessionFactory.openSession();
		List list = null;
		logger.info("Fetching HostelFeeAssingToStudent Data using admissionNumberId:"+admissionNumberId);
		try{
			Criteria createCriteria = opensession.createCriteria(HostelFeeAssingToStudent.class,"hfats")
					.createAlias("hfats.StudentAdmission", "studentAdmission")
					.add(Restrictions.eq("studentAdmission.admissionNumberId",admissionNumberId));
			createCriteria.setProjection(Projections.projectionList()
					.add(Projections.property("hostelFeeAssingToStudentId"),"hostelFeeAssingToStudentId")
					.add(Projections.property("paidAmount"),"paidAmount")
					.add(Projections.property("dueAmount"),"dueAmount")
					.add(Projections.property("discountAmount"),"discountAmount")
					.add(Projections.property("instid"),"instid")
					.add(Projections.property("termWiseAmount"),"termWiseAmount")
					.add(Projections.property("netAmount"),"netAmount"));
			createCriteria.setResultTransformer(Transformers.aliasToBean(HostelFeeAssingToStudent.class));
			list = createCriteria.list();
			logger.debug("Interacted with database to fatch HostelFeeAssingToStudent:"+list);
		}catch(Exception exception){
			logger.error("Exception raised in hostelFeeGenerationdetails Method:", exception);
			exception.printStackTrace();
		}finally {
			opensession.close();
		}
		return list;
	}


	/* (non-Javadoc)
	 * @see com.leonia.academy.idao.IFeeManagementDAO#getPaymentModeTypes()
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<PaymodeMaster> getPaymentModeTypes() {
		Session sessionObj = sessionFactory.openSession();
		Criteria createCriteria = null;
		List list = null;
		logger.info("Fetching PaymodeMaster Data");
		try{
			createCriteria = sessionObj.createCriteria(PaymodeMaster.class);
			createCriteria.setProjection(Projections.projectionList()
					.add(Projections.property("paymodeId"),"paymodeId")
					.add(Projections.property("payMode"),"payMode"));
			createCriteria.setResultTransformer(Transformers.aliasToBean(PaymodeMaster.class));
			list = createCriteria.list();
			logger.debug("Interacted with database to fatch PaymodeMaster");
		}catch(Exception exception){
			logger.error("Exception raised in getPaymentModeTypes Method:", exception);
		}finally{
			sessionObj.close();
		}
        return list;
	}


	/* (non-Javadoc)
	 * @see com.leonia.academy.idao.IFeeManagementDAO#getCardTypeMaster()
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<CardTypeMaster> getCardTypeMaster() {
		Session  sessionObj = sessionFactory.openSession();
		Criteria createCriteria = null;
		List list = null;
		logger.info("Fetching CardTypeMaster Data");
		try{
		 createCriteria = sessionObj.createCriteria(CardTypeMaster.class);
		 list = createCriteria.list();
		 logger.debug("Interacted with database to fatch CardTypeMaster");
		}catch(Exception exception){
			logger.error("Exception raised in getCardTypes Method:", exception);
		}finally{
			sessionObj.close();
		}
        return list;
	}


	/* (non-Javadoc)
	 * @see com.leonia.academy.idao.IFeeManagementDAO#getfeepaidreceptdetails(java.lang.Long)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List getfeepaidreceptdetails(Long admissionNumberId) {
		Session openSession = null;
		List<ReceiptGenerateDTO> list = null;
		logger.info("Fetching ReceiptGenerateEntity Data using admissionNumberId:"+admissionNumberId);
		try{
			openSession = sessionFactory.openSession();
			Criteria criteria = openSession.createCriteria(ReceiptGenerateDTO.class)
					.add(Restrictions.eq("admissionNumberId", admissionNumberId)).addOrder(Order.desc("receiptGenerateId"));
			criteria.setProjection(Projections.projectionList()
					.add(Projections.property("receiptGenerateId"),"receiptGenerateId")
					.add(Projections.property("totalAmount"),"totalAmount")
					.add(Projections.property("paidDate"),"paidDate"));
			criteria.setResultTransformer(Transformers.aliasToBean(ReceiptGenerateDTO.class));
			list = criteria.list();
			logger.debug("Interacted with database to fatch ReceiptGenerateEntity:"+list);
			}catch(Exception exception){
				logger.error("Exception raised in getFeePaidReceiptsList Method:", exception);
			}finally {
				openSession.close();
			}
		return list;
	}


	/* (non-Javadoc)
	 * @see com.leonia.academy.idao.IFeeManagementDAO#getfeepaidreceptdetails(java.lang.Long, java.lang.Long)
	 */
	@SuppressWarnings({ "unchecked"})
	@Override
	public List<ReceiptGenerateDTO> getfeepaidreceptdetails(Long admissionNumberId, Long receiptGenerateId) {
		Session openSession = null;
		List<ReceiptGenerateDTO> list = null;
		logger.info("Fetching ReceiptGenerateEntity Data using admissionNumberId&receiptGenerateId:"+receiptGenerateId+""+admissionNumberId);
		try{
			openSession = sessionFactory.openSession();
			Criteria criteria = openSession.createCriteria(ReceiptGenerateDTO.class,"rg")
					.createAlias("rg.feePaidTransactionsDTO", "feePaidTransactionsDTO")
					.createAlias("rg.feeSettlementDTO", "feeSettlementDTO");
			criteria.add(Restrictions.eq("rg.receiptGenerateId", receiptGenerateId));
			criteria.add(Restrictions.eq("rg.admissionNumberId", admissionNumberId));
			list = criteria.list();
			logger.debug("Interacted with database to fatch ReceiptGenerateEntity:"+list);
			}catch(Exception exception){
				logger.error("Exception raised in generateReceiptByReceiptGenerateId Method:", exception);
			}finally {
				//openSession.close();
			}
		return list;
	}


	/* (non-Javadoc)
	 * @see com.leonia.academy.idao.IFeeManagementDAO#savePaymentDetails(com.leonia.academy.entity.ReceiptGenerateDTO)
	 */
	@Override
	public String savePaymentDetails(ReceiptGenerateDTO receiptgeneratedtoObj) {
		Session openSession = sessionFactory.openSession();
		Transaction beginTransaction = openSession.beginTransaction();
		String msg = null;
		logger.info("Save Data ReceiptGenerateEntity :"+receiptgeneratedtoObj);
		try {
			for (FeePaidTransactionsDTO abc : receiptgeneratedtoObj.getFeePaidTransactionsDTO()) {
				abc.setReceiptGenerateDTO(receiptgeneratedtoObj);
			}
			for (FeeSettlementDTO abc1 : receiptgeneratedtoObj.getFeeSettlementDTO()){
				abc1.setReceiptGenerateDTO(receiptgeneratedtoObj);
			}
			openSession.save(receiptgeneratedtoObj);
			beginTransaction.commit();
			msg="Payment Details Successfully Saved...";
			logger.debug("Saveing Data ReceiptGenerateEntity");
		} catch (Exception exception) {
			beginTransaction.rollback();
			logger.error("Exception raised in savePaymentDetails Method:", exception);
		} finally {
			openSession.close();
		}
		return msg;
	}


	/* (non-Javadoc)
	 * @see com.leonia.academy.idao.IFeeManagementDAO#feeassigntostudentPaymentupdate(java.util.List)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String feeassigntostudentPaymentupdate(List<FeeAssignToStudents> feesettlementdtolist) {
		Session openSession = sessionFactory.openSession();
		Transaction beginTransaction = openSession.beginTransaction();
		List<FeeAssignToStudents> list = null;
		Criteria createCriteria = null;
		FeeAssignToStudents feeAssignToStudents = null;
		String msg = null;
		logger.info("Update Data FeeAssignToStudents:"+feesettlementdtolist);
		try {
			Long batchId = feesettlementdtolist.get(0).getBatchId();
			Long admissionNumberId = feesettlementdtolist.get(0).getStudentAdmissionEntity().getAdmissionNumberId();
			createCriteria = openSession.createCriteria(FeeAssignToStudents.class,"fats")
					.createAlias("fats.studentAdmission", "studentAdmission")
			.add(Restrictions.eq("batchId", batchId))
			.add(Restrictions.eq("studentAdmission.admissionNumberId",admissionNumberId)).addOrder(Order.asc("fats.feeAssignToStudentsId"));
			list = createCriteria.list();
			for (int i = 0; i < list.size(); i++) {
				if(list.get(i).getFeeAssignToStudentsId().equals(feesettlementdtolist.get(i).getFeeAssignToStudentsId())){
					feeAssignToStudents = list.get(i);
					BigDecimal paidAmount = feesettlementdtolist.get(i).getPaidAmount();
					BigDecimal dueAmount = feesettlementdtolist.get(i).getDueAmount();
					feeAssignToStudents.setPaidAmount(paidAmount);
					feeAssignToStudents.setDueAmount(dueAmount);
					openSession.merge(feeAssignToStudents);
				}
			}
			beginTransaction.commit();
			logger.debug("Updataing Data FeeAssignToStudents");
			msg="Student Payment  Successfully updated...";
		} catch (Exception e) {
			logger.error("Exception raised in feeassigntostudentPaymentupdate Method:", e);
		}finally {
			openSession.close();
		}
		return msg;
	}


	/* (non-Javadoc)
	 * @see com.leonia.academy.idao.IFeeManagementDAO#hostelFeeAssignToStudentPaymentUpdate(com.leonia.academy.entity.HostelFeeAssingToStudent)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String hostelFeeAssignToStudentPaymentUpdate(HostelFeeAssingToStudent hostelFeeAssingToStudent1) {
		Session opensession=sessionFactory.openSession();
		Transaction transaction = null;
		List<HostelFeeAssingToStudent> list = null;
		HostelFeeAssingToStudent hostelFeeAssingToStudent = new HostelFeeAssingToStudent();
		String msg = null;
		logger.info("Update Data HostelFeeAssingToStudent:"+hostelFeeAssingToStudent);
		try{
			transaction=opensession.beginTransaction();
			Criteria createCriteria = opensession.createCriteria(HostelFeeAssingToStudent.class,"hfats")
					.createAlias("hfats.StudentAdmission", "studentAdmission")
					.add(Restrictions.eq("studentAdmission.admissionNumberId",hostelFeeAssingToStudent1.getStudentAdmission().getAdmissionNumberId()));
			list = createCriteria.list();
			for (int i = 0; i < list.size(); i++) {
				if(list.get(i).getHostelFeeAssingToStudentId().equals(hostelFeeAssingToStudent1.getHostelFeeAssingToStudentId())){
				hostelFeeAssingToStudent = list.get(i);
				BigDecimal dueAmount = hostelFeeAssingToStudent1.getDueAmount();
				BigDecimal paidAmount = hostelFeeAssingToStudent1.getPaidAmount();
				hostelFeeAssingToStudent.setDueAmount(dueAmount);
				hostelFeeAssingToStudent.setPaidAmount(paidAmount);
				opensession.merge(hostelFeeAssingToStudent);
				}
			}
			transaction.commit();
			logger.debug("Updataing Data HostelFeeAssingToStudent");
			msg="HostelFee Update Payment  Successfully updated...";
		}catch(Exception exception){
			logger.error("Exception raised in hostelFeeAssignToStudentPaymentUpdate Method:", exception);
			exception.printStackTrace();
		}finally {
			opensession.close();
		}
		return msg;
	}



	
	
	
	
	
	
	//FeeManagementDAOImpl from ajay

	@Override
		public List getFeeDetailsById(Integer batchId) {
			Session openSession = sessionFactory.openSession();
			List feeCreationsList = null;
			try{
				Criteria createCriteria = openSession.createCriteria(FeeCreation.class,"fc");
				createCriteria.createAlias("fc.batchMaster", "batchMaster");
				createCriteria.add(Restrictions.eq("batchMaster.batchId",new Long(batchId)));
				createCriteria.createAlias("fc.feesCategory", "feesCategory");
				createCriteria.setProjection(Projections.projectionList()
						.add(Projections.property("feesCategory.feesCategoryId"))
						.add(Projections.property("feesCategory.feesCategoryName"))
						.add(Projections.property("fc.feeAmount")));
				feeCreationsList = createCriteria.list();
			}catch(Exception e){
				logger.info("Exception raised in getFeeDetailsById Method:", e);
			    logger.debug("Exception raised in getFeeDetailsById Method:", e);
			    logger.error("Exception raised in getFeeDetailsById Method:", e);
			}finally {
				openSession.close();
			}
			return feeCreationsList;
		}
		
		@Override
		public List feeCategory() {
			Session sessionObj = sessionFactory.openSession();
			Criteria createCriteria = null;
			List feecategory = null;
			try{
				 createCriteria = sessionObj.createCriteria(FeesCategory.class);
			 feecategory=createCriteria.list();
			}catch(Exception e){
				logger.info("Exception raised in getFeeDetailsById Method:", e);
			    logger.debug("Exception raised in getFeeDetailsById Method:", e);
			    logger.error("Exception raised in getFeeDetailsById Method:", e);
			}
			finally {
				sessionObj.close();
			}
			return feecategory;
		}

		@Override
		public List getCategoryNameByBatchId(Integer batchId) {
			Session openSession = sessionFactory.openSession();
			List<FeesCategory> feeCreationsList = null;
			try{
				String sql = "Select feescategoryid, feescategoryname from feescategory where feescategoryid not in "
						+ "(select feescategoryid from feecreation where batchid='"+batchId+"')";
				feeCreationsList =openSession.createSQLQuery(sql).list();
			}
			catch(Exception e)
			{
				logger.info("Exception raised in getFeeDetailsById Method:", e);
			    logger.debug("Exception raised in getFeeDetailsById Method:", e);
			    logger.error("Exception raised in getFeeDetailsById Method:", e);
			}finally{
				openSession.close();
			}
			return feeCreationsList;
		}

		@Override
		public String saveStudentFee(FeeCreation fee) {
			Session sessionObj = sessionFactory.openSession();
			Transaction tx = sessionObj.beginTransaction();
			try{
			sessionObj.save(fee);
			tx.commit();
			}catch(Exception e)
			{
				logger.info("Exception raised in getFeeDetailsById Method:", e);
			    logger.debug("Exception raised in getFeeDetailsById Method:", e);
			    logger.error("Exception raised in getFeeDetailsById Method:", e);
			}finally{
				sessionObj.close();
			}
			return "Feecreated Successfully.......";
		}

		@Override
		public List getInstallmentTypes(Long batchId) {
			Session openSession= sessionFactory.openSession();
			List list =new LinkedList();
			try{
			Criteria criteria= openSession.createCriteria(FeeGenerationBatchWise.class,"fc");
			criteria.add(Restrictions.eq("batchId",batchId));
			criteria.createAlias("fc.feeCreation","feeCreation")
			.createAlias("feeCreation.feesCategory","feesCategory")
			.createAlias("feeCreation.batchMaster", "batchMaster")
			.createAlias("fc.installmentTypeMaster","installmentTypeMaster" )
			.createAlias("installmentTypeMaster.installmentTypes", "installmentTypes");
			criteria.setProjection(Projections.projectionList()
					.add(Projections.property("batchMaster.batchName"))
					.add(Projections.property("feesCategory.feesCategoryName"))
					.add(Projections.property("termWiseMessage"))
					.add(Projections.property("termWiseAmount"))
					/*.add(Projections.property("installmentTypes.installmentTypeName"))*/
					.add(Projections.property("installmentTypeMaster.installmentTypeMasterId")));
	        list=criteria.list();
	       
			}catch(Exception exception){
				logger.error("Exception raised in getInstallmentTypes Method:", exception);
			}
			finally {
				openSession.close();
			}
			return list;
		}

		@Override
		public List getInstallmentTypesdrop(Long batchId) {
			Session openSession = sessionFactory.openSession();
			Criteria createCriteria = null;
			List installmentTypeMasterList =new ArrayList();
			try{
				 createCriteria = openSession.createCriteria(InstallmentTypeMaster.class,"imtm");
				 createCriteria.createAlias("imtm.installmentTypes", "imt");
				 createCriteria.createAlias("imtm.batchMaster", "bm");
				 createCriteria.add(Restrictions.eq("bm.batchId", batchId));
				 createCriteria.setProjection(Projections.projectionList()
						 .add(Projections.property("imtm.installmentTypeMasterId"))
						 .add(Projections.property("bm.batchName"))
						 .add(Projections.property("imt.installmentTypeName")));
				 installmentTypeMasterList =  createCriteria.list();
			}catch(Exception exception){
				logger.error("Exception raised in getInstallmentTypesdrop Method:", exception);
			}
			finally {
				openSession.close();
			}
			return installmentTypeMasterList;
		}

		@Override
		public List<FeeCreation> getFeeCreationBatchWiseListByBatchId(Long batchId, Long installmentTypeMasterId) {
			Session openSession = sessionFactory.openSession();
			List<FeeCreation> feeCreationsList = new LinkedList<FeeCreation>();
			try{
			String	sql ="Select distinct(fc.feecreationid),fcg.feescategoryname,fcg.feesShortForm from "
					+ "feecreation fc,feescategory fcg where (fcg.feescategoryid=fc.feescategoryid) and"
					+ " fc.batchid='"+batchId+"'  and fc.feecreationid not in(select COALESCE(feecreationid,0) from "
					+ "feegenerationbatchwise where batchid='"+batchId+"'and installmenttypemasterid= '"+installmentTypeMasterId+"' )";
				SQLQuery sqlQuery = openSession.createSQLQuery(sql);
				feeCreationsList = sqlQuery.list();
			}catch(Exception exception){
				logger.error("Exception raised in getFeeCreationBatchWiseListByBatchId Method:", exception);
			}
			finally {
				openSession.close();
			}
			return feeCreationsList;
		}

		@Override
		public List getAmountByParticularFee(Long feeCreationId) {
			Session openSession = sessionFactory.openSession();
			List<FeeCreation> feeCreationsList = null;
			try{
				Criteria createCriteria = openSession.createCriteria(FeeCreation.class,"fc");
				createCriteria.createAlias("fc.feesCategory", "feesCategory");
				createCriteria.add(Restrictions.eq("feeCreationId",feeCreationId));
				createCriteria.setProjection(Projections.projectionList()
						.add(Projections.property("feesCategory.feesShortForm"))
						.add(Projections.property("feeAmount")));
				feeCreationsList = createCriteria.list();
			}catch(Exception exception){
				logger.error("Exception raised in getAmountByParticularFee Method:", exception);
			}finally {
				openSession.close();
			}
			return feeCreationsList;
		}

		@Override
		public String savefeeGenarationBatchWise(FeeGenerationBatchWise generationBatchWise) {
			Session openSession = sessionFactory.openSession();
			Transaction beginTransaction = openSession.beginTransaction();
			try{
				generationBatchWise.setAction(true);
				openSession.save(generationBatchWise);
				beginTransaction.commit();
			}catch(Exception exception){
				logger.error("Exception raised in savefeeGenarationBatchWise Method:", exception);
			}finally {
				openSession.close();
			}
			return "Saved Successfully......";
		}

		@Override
		public List getFeeGenarationFeeListInstallment123(Long batchId, Long feeGenerationBatchWiseId,Long installmentTypeMasterId){
			Session openSession = sessionFactory.openSession();
			Criteria createCriteria = null;
			List installmentTypeMasterList =null;
			try{
				createCriteria = openSession.createCriteria(FeeGenerationBatchWise.class,"fgbw");
				createCriteria.add(Restrictions.eq("batchId", batchId));
				createCriteria.createAlias("fgbw.installmentTypeMaster", "installmentTypeMaster");
				createCriteria.createAlias("fgbw.feeCreation", "feeCreation");
				createCriteria.add(Restrictions.eq("installmentTypeMaster.installmentTypeMasterId", installmentTypeMasterId));
				createCriteria.add(Restrictions.eq("fgbw.feeGenerationBatchWiseId",feeGenerationBatchWiseId));
				
				createCriteria.setProjection(Projections.projectionList().add(Projections.property("termWiseMessage"))
						.add(Projections.property("feeGenerationBatchWiseId"))
						.add(Projections.property("batchId"))
						.add(Projections.property("dueDate"))
						.add(Projections.property("installmentTypeMaster.installmentTypeMasterId"))
						.add(Projections.property("termWiseAmount"))
						.add(Projections.property("feeCreation.feeCreationId")));
				
				 installmentTypeMasterList = createCriteria.list();
			}catch(Exception exception){
				logger.error("Exception raised in getFeeGenarationFeeListInstallment123 Method:", exception);
			}finally {
				openSession.close();
			}
			return installmentTypeMasterList;
			
		}

		@Override
		public List getFeeGenarationFeeListInstallment(Long batchId, Long feeGenerationBatchWiseId) {
			Session openSession = sessionFactory.openSession();
			Criteria createCriteria = null;
			List installmentTypeMasterList =null;
			try{
				createCriteria = openSession.createCriteria(FeeGenerationBatchWise.class,"fgbw");
				createCriteria.add(Restrictions.eq("batchId", batchId));
				createCriteria.createAlias("fgbw.installmentTypeMaster", "installmentTypeMaster");
				createCriteria.createAlias("fgbw.feeCreation", "feeCreation");
				createCriteria.add(Restrictions.eq("installmentTypeMaster.installmentTypeMasterId", feeGenerationBatchWiseId));
				createCriteria.setProjection(Projections.projectionList().add(Projections.property("termWiseMessage"))
						.add(Projections.property("feeGenerationBatchWiseId"))
						.add(Projections.property("batchId"))
						.add(Projections.property("dueDate"))
						.add(Projections.property("installmentTypeMaster.installmentTypeMasterId"))
						.add(Projections.property("termWiseAmount"))
						.add(Projections.property("feeCreation.feeCreationId")));
				 installmentTypeMasterList = createCriteria.list();
			}catch(Exception exception){
				logger.error("Exception raised in getFeeGenarationFeeListInstallment Method:", exception);
			}finally {
				openSession.close();
			}
			return installmentTypeMasterList;
		}

		@Override
		public List getStudentsListByBatchId(Long batchId, Long feeGenerationBatchWiseId, Long installmentTypeMasterId) {
			Session openSession = sessionFactory.openSession();
			List studentsList = null;
			try{
				String sql = "Select s.admissionnumberId,s.admissionNumber,s.studentfirstname,s.studentMiddleName,s.studentLastName,"
						+ "s.gender From studentadmission s where (s.admissionnumberId not in (select coalesce(admissionnumberId,0)"
						+ " as colid from feeassigntostudents where batchid='"+batchId+"' and  installmenttypemasterid ='"+installmentTypeMasterId+"' and feeGenerationBatchWiseId='"+feeGenerationBatchWiseId+"')) and  s.batchid='"+batchId+"'";
				studentsList = openSession.createSQLQuery(sql).list();
			}catch(Exception exception){
				logger.error("Exception raised in getStudentsListByBatchId Method:", exception);
			}finally {
				openSession.close();
			}
			return studentsList;
		}

		@Override
		public String feeAssignToStudentsPost(FeeAssignToStudents feeAssignToStudents) {
			Session openSession = sessionFactory.openSession();
			try{
				Transaction beginTransaction = openSession.beginTransaction();
				openSession.save(feeAssignToStudents);
				beginTransaction.commit();
			}catch(Exception e){
				logger.error("Exception raised in feeassignToStudentsPost Method:", e);
			}finally {
				openSession.close();
			}
			return "Assigned Successfully....";
		}
		
	
}
