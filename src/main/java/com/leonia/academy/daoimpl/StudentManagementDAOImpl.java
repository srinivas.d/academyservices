package com.leonia.academy.daoimpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.leonia.academy.entity.BatchMaster;
import com.leonia.academy.entity.BloodGroupMaster;
import com.leonia.academy.entity.CalenderEvents;
import com.leonia.academy.entity.CityMaster;
import com.leonia.academy.entity.CommunityMaster;
import com.leonia.academy.entity.CourseMaster;
import com.leonia.academy.entity.DiscountCategoryMaster;
import com.leonia.academy.entity.EducationEntity;
import com.leonia.academy.entity.Eventtype;
import com.leonia.academy.entity.FeeCategoryMaster;
import com.leonia.academy.entity.LanguageMaster;
import com.leonia.academy.entity.NationalityMaster;
import com.leonia.academy.entity.ParentGuardianEntity;
import com.leonia.academy.entity.RelationMaster;
import com.leonia.academy.entity.ReligionMaster;
import com.leonia.academy.entity.StayCategoryMaster;
import com.leonia.academy.entity.StudentAdmissionEntity;
import com.leonia.academy.entity.UniversityMaster;
import com.leonia.academy.idao.IStudentManagementDAO;

@Repository
public class StudentManagementDAOImpl implements IStudentManagementDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	private static final Logger logger = Logger.getLogger(StudentManagementDAOImpl.class);

	@Override
	public List getNationalityMaster() {
		Session openSession = sessionFactory.openSession();
		List<NationalityMaster> nationalityMasters = null;
		try{
			Criteria createCriteria = openSession.createCriteria(NationalityMaster.class);
			createCriteria.addOrder(Order.asc("nationalityId"));
			nationalityMasters =  createCriteria.list();
	}catch(Exception e){
		logger.error("Exception raised in getNationalityMaster Method:", e);
	}
	finally {
		openSession.close();
	}
	return  nationalityMasters;
		
	}

	@Override
	public List bloodGroupMaster() {
		Session openSession = sessionFactory.openSession();
		List<BloodGroupMaster> bloodGroupMasters = null;
		try{
			Criteria createCriteria = openSession.createCriteria(BloodGroupMaster.class);
			createCriteria.addOrder(Order.asc("bloodGroupId"));
			bloodGroupMasters =  createCriteria.list();
	}catch(Exception e){
		logger.error("Exception raised in bloodGroupMaster Method:", e);
	}
	finally {
		openSession.close();
	}
	return  bloodGroupMasters;
	}

	@Override
	public List stayCategoryMaster() {
		Session openSession = sessionFactory.openSession();
		List<StayCategoryMaster> stayCategoryMasters = null;
		try{
			Criteria createCriteria = openSession.createCriteria(StayCategoryMaster.class);
			createCriteria.addOrder(Order.asc("stayCategoryMasterId"));
			stayCategoryMasters =  createCriteria.list();
	}catch(Exception e){
		logger.error("Exception raised in stayCategoryMaster Method:", e);
	}
	finally {
		openSession.close();
	}
	return  stayCategoryMasters;
	}

	@Override
	public List discountCategoryMaster() {
		Session openSession = sessionFactory.openSession();
		List<DiscountCategoryMaster> discountCategoryMasters = null;
		try{
			Criteria createCriteria = openSession.createCriteria(DiscountCategoryMaster.class);
			createCriteria.addOrder(Order.asc("discountCategoryId"));
			discountCategoryMasters =  createCriteria.list();
	}catch(Exception e){
		logger.error("Exception raised in discountCategoryMaster Method:", e);
	}
	finally {
		openSession.close();
	}
	return  discountCategoryMasters;
	}

	@Override
	public List religionMaster() {
		Session openSession = sessionFactory.openSession();
		List<ReligionMaster> religionMasters = null;
		try{
			Criteria createCriteria = openSession.createCriteria(ReligionMaster.class);
			createCriteria.addOrder(Order.asc("religionId"));
			religionMasters =  createCriteria.list();
	}catch(Exception e){
		logger.error("Exception raised in religionMaster Method:", e);
	}
	finally {
		openSession.close();
	}
	return  religionMasters;
	}

	@Override
	public List communityMaster() {
		Session openSession = sessionFactory.openSession();
		List<CommunityMaster> communityMasters = null;
		try{
			Criteria createCriteria = openSession.createCriteria(CommunityMaster.class);
			createCriteria.addOrder(Order.asc("communityId"));
			communityMasters =  createCriteria.list();
	}catch(Exception e){
		logger.error("Exception raised in communityMaster Method:", e);
	}
	finally {
		openSession.close();
	}
	return  communityMasters;
	}

	@Override
	public List languageMaster() {
		Session openSession = sessionFactory.openSession();
		List<LanguageMaster> languageMasters = null;
		try{
			Criteria createCriteria = openSession.createCriteria(LanguageMaster.class);
			createCriteria.addOrder(Order.asc("languageId"));
			languageMasters =  createCriteria.list();
	}catch(Exception e){
		logger.error("Exception raised in languageMaster Method:", e);
	}
	finally {
		openSession.close();
	}
	return  languageMasters;
	}

	@Override
	public List cityMaster() {
		Session openSession = sessionFactory.openSession();
		List<CityMaster> cityMasters = null;
		try{
			Criteria createCriteria = openSession.createCriteria(CityMaster.class);
			createCriteria.addOrder(Order.asc("cityid"));
			cityMasters =  createCriteria.list();
	}catch(Exception e){
		logger.error("Exception raised in cityMaster Method:", e);
	}
	finally {
		openSession.close();
	}
	return  cityMasters;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List getStateCountryByCityId(int cityId) {
		Session openSession = sessionFactory.openSession();
		List cityMasters = new ArrayList<>();
		try{
			Criteria createCriteria = openSession.createCriteria(CityMaster.class,"city");
			createCriteria.add(Restrictions.eq("city.cityid", cityId));
			createCriteria.add(Restrictions.eq("city.cityStatus", true));
			CityMaster citymaster = (CityMaster) createCriteria.uniqueResult();
			cityMasters.add(citymaster.getCityName());
			cityMasters.add(citymaster.getStateMaster().getStateName());
			cityMasters.add(citymaster.getStateMaster().getCountryMaster().getCountryName());
			cityMasters =  createCriteria.list();
	}catch(Exception e){
		logger.error("Exception raised in getStateCountryByCityId Method:", e);
	}
	finally {
		openSession.close();
	}
	return  cityMasters;
	}

	@Override
	public List courseMaster() {
		Session openSession = sessionFactory.openSession();
		List<CourseMaster> courseMasters = null;
		try{
			Criteria createCriteria = openSession.createCriteria(CourseMaster.class);
			createCriteria.addOrder(Order.asc("courseId"));
			courseMasters =  createCriteria.list();
	}catch(Exception e){
		logger.error("Exception raised in courseMaster Method:", e);
	}
	finally {
		openSession.close();
	}
	return  courseMasters;
	}

	@Override
	public List getBatchNameByCourseId(Long courseId) {
		Session openSession = sessionFactory.openSession();
		List<BatchMaster> batchMasters = null;
		try{
			Criteria createCriteria = openSession.createCriteria(BatchMaster.class,"bm");
			createCriteria.createAlias("bm.courseMaster", "courseMaster");
			createCriteria.add(Restrictions.eq("courseMaster.courseId", courseId));
			createCriteria.addOrder(Order.asc("batchId"));
			batchMasters =  createCriteria.list();
	}catch(Exception e){
		logger.error("Exception raised in getBatchNameByCourseId Method:", e);
	}
	finally {
		openSession.close();
	}
	return  batchMasters;
	}

	@Override
	public List getBatchFeeByBatchId(Long batchId) {
		Session openSession = sessionFactory.openSession();
		List<BatchMaster> batchMasters = null;
		try{
			Criteria createCriteria = openSession.createCriteria(BatchMaster.class,"bm");
			createCriteria.add(Restrictions.eq("bm.batchId", batchId));
			batchMasters =  createCriteria.list();
	}catch(Exception e){
		logger.error("Exception raised in getBatchFeeByBatchId Method:", e);
	}
	finally {
		openSession.close();
	}
	return  batchMasters;
	}

	@Override
	public String saveStudentAdmissionDetails(StudentAdmissionEntity studentAdmissionEntity) {
		Session openSession = sessionFactory.openSession();
		Transaction beginTransaction = openSession.beginTransaction();
		String message = null;
		try{
			openSession.save(studentAdmissionEntity);
			beginTransaction.commit();
			message = "Student Admission Details Successfully Saved...";
		}catch(Exception e){
			logger.error("Exception raised in saveStudentAdmissionDetails Method:", e);
			message = "Student Admission Details not saved due to Exception!";
			e.printStackTrace();
		}
		finally {
			openSession.close();
		}
		return message;
	}

	@Override
	public List getviewstudentDetails() {
		Session openSession = sessionFactory.openSession();
		List<StudentAdmissionEntity> studentAdmissionsList = null;
		try{
			Criteria createCriteria = openSession.createCriteria(StudentAdmissionEntity.class)
					.setFetchMode("batchMaster", FetchMode.EAGER);
		createCriteria.addOrder(Order.asc("admissionNumberId"));
		studentAdmissionsList = createCriteria.list();
		}catch(Exception exception){
			logger.error("Exception raised in getAllStudents Method:", exception);
			exception.printStackTrace();
		}
		finally {
			openSession.close();
		}
		return studentAdmissionsList;

	}

	public StudentAdmissionEntity editStudentAdmissionDetailsNU(Long admissionNumberId) {
		Session openSession = sessionFactory.openSession();
		StudentAdmissionEntity admissionEntity = null;
		try{
			 admissionEntity = (StudentAdmissionEntity) openSession.get(StudentAdmissionEntity.class, admissionNumberId);
		}catch(Exception e){
			logger.error("Exception raised in editStudentAdmissionDetails Method:", e);
			e.printStackTrace();
		}
		finally {
			openSession.close();
		}
		return admissionEntity;
	}
	
	@Override
	public StudentAdmissionEntity editStudentAdmissionDetails(Long admissionNumberId) {
		Session openSession = sessionFactory.openSession();
		StudentAdmissionEntity admissionEntity = new StudentAdmissionEntity();
		try{
			// admissionEntity = (StudentAdmissionEntity) openSession.get(StudentAdmissionEntity.class, admissionNumberId);
			Criteria createCriteria = openSession.createCriteria(StudentAdmissionEntity.class);
			createCriteria.add(Restrictions.eq("admissionNumberId", admissionNumberId));
			admissionEntity = (StudentAdmissionEntity)createCriteria.uniqueResult();
		}catch(Exception e){
			logger.error("Exception raised in editStudentAdmissionDetails Method:", e);
			e.printStackTrace();
		}
		finally {
			openSession.close();
		}
		return admissionEntity;
	}


	@Override
	public String updateStudentAdmissionDetails(StudentAdmissionEntity studentAdmissionEntity) {
		Session openSession = sessionFactory.openSession();
		Transaction beginTransaction = openSession.beginTransaction();
		String message = null;
		//StudentAdmissionEntity admissionEntity= new StudentAdmissionEntity();
		try{
			/*StudentAdmissionEntity admissionEntityTemp = (StudentAdmissionEntity) openSession.get(StudentAdmissionEntity.class, studentAdmissionEntity.getAdmissionNumberId());
			admissionEntityTemp.setAadhaarCardNumber(studentAdmissionEntity.getAadhaarCardNumber());
			admissionEntityTemp.setRfidCardNumber(studentAdmissionEntity.getRfidCardNumber());
			admissionEntityTemp.setStudentFirstName(studentAdmissionEntity.getStudentFirstName());
			admissionEntityTemp.setStudentLastName(studentAdmissionEntity.getStudentLastName());*/
			openSession.update(studentAdmissionEntity);
			beginTransaction.commit();
			message = "Student Admission Details Successfully Updated...";
		}catch(Exception e){
			logger.error("Exception raised in updateStudentAdmissionDetails Method:", e);
			message = "Student Admission Details not updated due to Exception!";
			e.printStackTrace();
		}
		finally {
			openSession.close();
		}
		return message;
	}

	@Override
	public Long getAdmissionNumberId() {
		Session openSession = sessionFactory.openSession();
		Long index  = null;
		try{
		 Criteria createCriteria = openSession.createCriteria(StudentAdmissionEntity.class);
		 createCriteria.setProjection(Projections.max("admissionNumberId"));
			index  = (Long) createCriteria.uniqueResult();
			if(index == null){
				index = (long) 1;
			}else{
				index = index++;
			}
		}catch(Exception exception){
			logger.error("Exception raised in getAdmissionNumberId Method:", exception);
		}
		finally {
			openSession.close();
		}
		return index;
	}

	@Override
	public List feeCategoryMaster() {
		Session openSession = sessionFactory.openSession();
		List<FeeCategoryMaster> feeCategoryMasters = null;
		try{
			Criteria createCriteria = openSession.createCriteria(FeeCategoryMaster.class);
			createCriteria.addOrder(Order.asc("feeCategoryMasterId"));
			feeCategoryMasters =  createCriteria.list();
	}catch(Exception e){
		logger.error("Exception raised in feeCategoryMaster Method:", e);
	}
	finally {
		openSession.close();
	}
	return  feeCategoryMasters;
	}

	@Override
	public List<RelationMaster> relationsList() {
		Session openSession = sessionFactory.openSession();
		List<RelationMaster> list = null;
		try{
			Criteria createCriteria = openSession.createCriteria(RelationMaster.class);
			list = createCriteria.list();
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			openSession.close();
		}
		return list;
	}

	@Override
	public List<ParentGuardianEntity> parentDetails(Long admissionNumberId) {
		Session openSession = sessionFactory.openSession();
		List<ParentGuardianEntity> list = null;
		try{
			Criteria createCriteria = openSession.createCriteria(ParentGuardianEntity.class)
					.add(Restrictions.eq("admissionNumberId", admissionNumberId));	
			list=createCriteria.list();
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			openSession.close();
		}
		return list;
		}

	@Override
	public String saveorupdateParentGuardian(ParentGuardianEntity parentGuardianEntity) {
		Session openSession = sessionFactory.openSession();
		Transaction beginTransaction = openSession.beginTransaction();
		try{
			openSession.saveOrUpdate(parentGuardianEntity);
			beginTransaction.commit();
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			openSession.close();
		}
		return null;
	}

	@Override
	public List geteventtypelist() {
		Session session=sessionFactory.openSession();
		 List list = null;
		 try {
			Criteria criteria  = session.createCriteria(Eventtype.class);
			list = criteria.list();
		 }catch(Exception exception){
			 exception.printStackTrace();
		 }finally {
			session.close();
		}
		return list;
	}

	@Override
	public List<CalenderEvents> getlistcalendereventdate() {
		Session openSession = sessionFactory.openSession();
		List list= null;
		try {
			Criteria createCriteria = openSession.createCriteria(CalenderEvents.class)
					.setFetchMode("eventtype",FetchMode.EAGER);
			list = createCriteria.list();
		}catch(Exception exception){
			exception.printStackTrace();
			//logger.error("Exception raised in listcalendereventdate Method:", exception);
		}finally {
			openSession.close();
		}
		return list;
	}

	@Override
	public String insertcalendereventform(CalenderEvents calenderEvents) {
		
		Session openSession = sessionFactory.openSession();
		Transaction beginTransaction = openSession.beginTransaction();
		String msg = null;
		try{
			openSession.saveOrUpdate(calenderEvents);
			beginTransaction.commit();
			msg = "Successfully Saved";
		}catch(Exception e){
			//logger.error("Exception raised in saveInstituteMasterDetails Method:", e);
			msg = "Not Saved Successfully";
			e.printStackTrace();
		}
		finally {
			openSession.close();
		}
		return msg;
	}

	@Override
	public StudentAdmissionEntity getStudentDetailsByAdmissionId(Long admissionId) {
		Session openSession = sessionFactory.openSession();
		StudentAdmissionEntity studentAdmissionEntity = new StudentAdmissionEntity();
        try{
			Criteria createCriteria = openSession.createCriteria(StudentAdmissionEntity.class);
			createCriteria.add(Restrictions.eq("admissionNumberId", admissionId));
			studentAdmissionEntity = (StudentAdmissionEntity)createCriteria.uniqueResult();
		}catch(Exception exception){
			logger.error("Exception raised in getStudentDetailsByAdmissionId Method:", exception);
		}
		finally {
			openSession.close();
		}
		return studentAdmissionEntity;
	}

	@Override
	public List getEducatioDetails(Long admissionId) {
		Session sessionObj = sessionFactory.openSession();
		Transaction tx = sessionObj.beginTransaction();
		List educationdetails = null ;
		try{
			Criteria createCriteria = sessionObj.createCriteria(EducationEntity.class,"ed");
			createCriteria.createAlias("ed.studentAdmissionEntity", "studentAdmissionEntity");
			createCriteria.add(Restrictions.eq("studentAdmissionEntity.admissionNumberId", admissionId));
			educationdetails=createCriteria.list();
			 //educationdetails = sessionObj.createSQLQuery("select * from educationdetails where admissionnumberid=?").setParameter(0, admissionId).list();
			 tx.commit();
		}catch(Exception exception){
			logger.error("Exception raised in getEducatioDetails Method:", exception);
		}finally{
			sessionObj.close();
		}
		return educationdetails;
	}

	@Override
	public String storeeducationdetails(EducationEntity educationEntity) {
		  Session session=sessionFactory.openSession();
			Transaction tx= session.beginTransaction();
			try{
			session.merge(educationEntity);
			tx.commit();
			}catch(Exception exception){
				logger.error("Exception raised in storeEducationDetails Method:", exception);
			}finally{
			session.close();
			}
			return "Successfully Stored";
	}

	@Override
	public List universityMaster() {
		Session openSession = sessionFactory.openSession();
		List<UniversityMaster> universityMasters = null;
		try{
			Criteria createCriteria = openSession.createCriteria(UniversityMaster.class);
			createCriteria.addOrder(Order.asc("universityMasterId"));
			universityMasters =  createCriteria.list();
	}catch(Exception e){
		logger.error("Exception raised in bloodGroupMaster Method:", e);
	}
	finally {
		openSession.close();
	}
	return  universityMasters;
	}

	@Override
	public List<CourseMaster> getCourseByUniversity(Long universityMasterId) {
		Session openSession = sessionFactory.openSession();
		List<CourseMaster> courseMasters = null;
		try{
			Criteria createCriteria = openSession.createCriteria(CourseMaster.class,"cm")
					.createAlias("cm.universityMaster","universityMaster")
					.add(Restrictions.eq("universityMaster.universityMasterId", universityMasterId)).addOrder(Order.asc("courseId"));
			courseMasters =  createCriteria.list();
	}catch(Exception e){
		logger.error("Exception raised in courseMaster Method:", e);
	}
	finally {
		openSession.close();
	}
	return  courseMasters;
	}

	@Override
	public List<StudentAdmissionEntity> getStudentsByBatchForAssignInterviews(Long vacanyDetailsId, Long batchId) {
		Session openSession = sessionFactory.openSession();
		List admissions = new ArrayList<>();
		try{
			String sql = "select s.admissionNumberId,s.admissionNumber,s.studentFirstName,s.studentMiddleName,s.gender,b.batchName "
					+ "from studentAdmission s,BatchMaster b where  s.admissionNumberId not in "
					+ " (select admissionNumberId from leoacademy_tbl_interviewassigntostudents where vacanyDetailsId="+vacanyDetailsId+")  and (s.batchId=b.batchId) and s.batchId ="+batchId+"";
			SQLQuery createSQLQuery = openSession.createSQLQuery(sql);
			admissions = createSQLQuery.list();
		}catch(Exception e){
			logger.error("Exception raised in getStudentsByBatchForAssignInterviews Method:", e);
		}finally {
			openSession.close();
		}
		return admissions;
	}


}
