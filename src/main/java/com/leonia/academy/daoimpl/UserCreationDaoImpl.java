package com.leonia.academy.daoimpl;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.leonia.academy.entity.LoginEntity;
import com.leonia.academy.entity.StudentRegistrationEntity;
import com.leonia.academy.idao.IUserCreationDao;

@Repository
public class UserCreationDaoImpl implements IUserCreationDao {
	
	@Autowired
	public SessionFactory sessionFactory;

	@SuppressWarnings("rawtypes")
	@Override
	public String saveuserdetalies(StudentRegistrationEntity studentRegistrationEntity) {
		String msg ;
		 Session session = sessionFactory.openSession();
		 Transaction tx=session.beginTransaction();
		List list = session.createQuery("from StudentRegistrationEntity e where e.email='"+studentRegistrationEntity.getEmail()+"'and e.phoneNumber='"+studentRegistrationEntity.getPhoneNumber()+"'").list();
		if(list.size()==0){
		 session.save(studentRegistrationEntity);
		 LoginEntity lt = new LoginEntity();
		 lt.setUsername(studentRegistrationEntity.getUsername());
		 lt.setPassword(studentRegistrationEntity.getPassword());
		 session.save(lt);
		 msg="successfully saved";
		}
		else{
			msg="Alerady existed";
		}
		 tx.commit();
		 session.close();
		return msg;
		}
}
