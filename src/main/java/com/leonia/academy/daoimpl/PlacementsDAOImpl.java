package com.leonia.academy.daoimpl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.leonia.academy.entity.CompanyDetails;
import com.leonia.academy.entity.DomainMaster;
import com.leonia.academy.entity.EmployerContactInformation;
import com.leonia.academy.entity.InterviewAssignToStudents;
import com.leonia.academy.entity.PlacementDepartmentMaster;
import com.leonia.academy.entity.PlacementDesignationMaster;
import com.leonia.academy.entity.ReasonsMaster;
import com.leonia.academy.entity.StatusMaster;
import com.leonia.academy.entity.VacancyDetails;
import com.leonia.academy.entity.VacancyInfo;
import com.leonia.academy.idao.IPlacementsDAO;

@Repository
public class PlacementsDAOImpl implements IPlacementsDAO{
	
	@Autowired
	private SessionFactory sessionFactory;
	private static final Logger logger = Logger.getLogger(PlacementsDAOImpl.class);

	@Override
	public List getDomainMaster() {
		Session openSession = sessionFactory.openSession();
		List<DomainMaster> domainMasters = null;
		try{
			Criteria createCriteria = openSession.createCriteria(DomainMaster.class);
			createCriteria.addOrder(Order.asc("domainMasterId"));
			domainMasters =  createCriteria.list();
	}catch(Exception e){
		logger.error("Exception raised in domainMasters Method:", e);
	}
	finally {
		openSession.close();
	}
	return  domainMasters;
	}

	@Override
	public String saveCompanyDetails(CompanyDetails companyDetails) {
		Session openSession = sessionFactory.openSession();
		Transaction beginTransaction = openSession.beginTransaction();
		String message = null;
		try{
			List<EmployerContactInformation> employerContactInformation = companyDetails.getEmployerContactInformation();
			for (EmployerContactInformation employerContactInfo : employerContactInformation) {
				employerContactInfo.setCompanyDetails(companyDetails);
			}
			openSession.save(companyDetails);
			beginTransaction.commit();
			message = "Comapany Details Successfully Saved...";
		}catch(Exception e){
			logger.error("Exception raised in saveCompanyDetails Method:", e);
			message = "Comapany Details not saved due to Exception!";
			e.printStackTrace();
		}
		finally {
			openSession.close();
		}
		return message;
	}

	@Override
	public List getAllCompaniesList() {
		Session openSession = sessionFactory.openSession();
		List<CompanyDetails> companiesList= null;
		try{
			Criteria createCriteria = openSession.createCriteria(CompanyDetails.class);
					//.setFetchMode("employerContactInformation", FetchMode.EAGER);
		createCriteria.addOrder(Order.asc("companyDetailsId"));
		companiesList = createCriteria.list();
		}catch(Exception exception){
			logger.error("Exception raised in getAllCompaniesList Method:", exception);
			exception.printStackTrace();
		}
		finally {
			openSession.close();
		}
		return companiesList;
	}

	@Override
	public CompanyDetails editCompanyDetailsByCompanyDetailsId(Long companyDetailsId) {
		
		Session openSession = sessionFactory.openSession();
		List<CompanyDetails> companiesList= null;
		CompanyDetails companyDetails = new CompanyDetails();
		try{
			Criteria createCriteria = openSession.createCriteria(CompanyDetails.class)
					.setFetchMode("employerContactInformation", FetchMode.EAGER);
			createCriteria.add(Restrictions.eq("companyDetailsId", companyDetailsId));
			
		createCriteria.addOrder(Order.asc("companyDetailsId"));
		companiesList = createCriteria.list();
		
		for (CompanyDetails companyDetail : companiesList) {
			companyDetails = companyDetail;
		}
		}catch(Exception exception){
			logger.error("Exception raised in editCompanyDetailsByCompanyDetailsId Method:", exception);
			exception.printStackTrace();
		}
		finally {
			openSession.close();
		}
		return companyDetails;
	}

	@Override
	public String updateCompanyDetails(CompanyDetails companyDetails) {
		Session openSession = sessionFactory.openSession();
		Transaction beginTransaction = openSession.beginTransaction();
		String message = null;
		try{
			CompanyDetails companyDetailsinfo =(CompanyDetails) openSession.get(CompanyDetails.class, companyDetails.getCompanyDetailsId());
		companyDetailsinfo.setCompanyName(companyDetails.getCompanyName());
		companyDetailsinfo.setCompanyShortName(companyDetails.getCompanyShortName());
		companyDetailsinfo.setDomainMasterId(companyDetails.getDomainMasterId());
		companyDetailsinfo.setCompanyAddress(companyDetails.getCompanyAddress());
		companyDetailsinfo.setCompanyLandMark(companyDetails.getCompanyLandMark());
		companyDetailsinfo.setCompanyPhone(companyDetails.getCompanyPhone());
		companyDetailsinfo.setCompanyEmail(companyDetails.getCompanyEmail());
		companyDetailsinfo.setOtherInfo(companyDetailsinfo.getOtherInfo());
		
		//	openSession.update(companyDetails);
		List<EmployerContactInformation> employerContactInformation = companyDetails.getEmployerContactInformation(); //front end values
		Criteria createCriteria = openSession.createCriteria(EmployerContactInformation.class,"eci");
		createCriteria.createAlias("companyDetails", "cd");
		createCriteria.add(Restrictions.eq("cd.companyDetailsId",companyDetails.getCompanyDetailsId()));
		List<EmployerContactInformation> list = createCriteria.list();											//backend values
		
		List<EmployerContactInformation> employerContactInformationDummy = new LinkedList<EmployerContactInformation>(employerContactInformation); //front end values
		List<EmployerContactInformation> listDummy = new LinkedList<EmployerContactInformation>(list);                      //backend values
		
		for(int i=0;i<employerContactInformation.size();i++){ 
			EmployerContactInformation empContactInfo = employerContactInformation.get(i);
			for(int j=0;j<list.size();j++){
				if(empContactInfo.getEmployerContactInformationId()==list.get(j).getEmployerContactInformationId()){
					EmployerContactInformation employerContactInfo = list.get(j);
					employerContactInfo.setEmployerContactPersonName(empContactInfo.getEmployerContactPersonName());
					employerContactInfo.setEmployerDesignation(empContactInfo.getEmployerDesignation());
					employerContactInfo.setEmployerContactMobile(empContactInfo.getEmployerContactMobile());
					employerContactInfo.setEmployerEmail(empContactInfo.getEmployerEmail());
					openSession.update(employerContactInfo);
					employerContactInformationDummy.remove(empContactInfo);
					listDummy.remove(employerContactInfo);
				}
			}
		}
		for(int i=0;i<employerContactInformationDummy.size();i++){
			EmployerContactInformation contactInformation = employerContactInformationDummy.get(i);
			contactInformation.setCompanyDetails(companyDetails);
			
			openSession.save(contactInformation);
		}
		for(int i=0;i<listDummy.size();i++){
		    EmployerContactInformation employerContactInformation3 = listDummy.get(i);
			openSession.delete(employerContactInformation3);
		}
		message ="Company Details Successfully Updated...";
		beginTransaction.commit();
	
		}catch(Exception e){
			logger.error("Exception raised in updateCompanyDetails Method:", e);
			message = "Comapany Details not updated due to Exception!";
			e.printStackTrace();
		}
		finally {
			openSession.close();
		}
		return message;
	}

	@Override
	public String saveVacancyDetails(VacancyInfo vacancyInfo) {
		Session openSession = sessionFactory.openSession();
		Transaction beginTransaction = openSession.beginTransaction();
		String message = null;
		try{
			List<VacancyDetails> vacancyDetails = vacancyInfo.getVacancyDetails();
			for (VacancyDetails vacancyDetail : vacancyDetails) {
				vacancyDetail.setVacancyInfo(vacancyInfo);
				vacancyDetail.setStatus(true);
			}
			vacancyInfo.setStatus(true);
			openSession.save(vacancyInfo);
			beginTransaction.commit();
			message = "Vacancy Details Successfully Saved...";
		}catch(Exception e){
			logger.error("Exception raised in saveVacancyDetails Method:", e);
			message = "Vacancy Details not saved due to Exception!";
			e.printStackTrace();
		}
		finally {
			openSession.close();
		}
		return message;
	}

	@Override
	public List getDepartmentMaster() {
		Session openSession = sessionFactory.openSession();
		List<PlacementDepartmentMaster> departmentMasters = null;
		try{
			Criteria createCriteria = openSession.createCriteria(PlacementDepartmentMaster.class);
			createCriteria.addOrder(Order.asc("departmentMasterId"));
			departmentMasters =  createCriteria.list();
	}catch(Exception e){
		logger.error("Exception raised in getDepartmentMaster Method:", e);
	}
	finally {
		openSession.close();
	}
	return  departmentMasters;
	}

	@Override
	public List getDesignationIdByDepartmentId(Long departmentId) {
		Session openSession = sessionFactory.openSession();
		List<PlacementDesignationMaster> designationMasters = null;
		try{
			Criteria createCriteria = openSession.createCriteria(PlacementDesignationMaster.class,"pdm");
			createCriteria.createAlias("pdm.departmentMaster", "departmentMaster");
			createCriteria.add(Restrictions.eq("departmentMaster.departmentMasterId", departmentId));
			createCriteria.addOrder(Order.asc("designationMasterId"));
			designationMasters =  createCriteria.list();
	}catch(Exception e){
		logger.error("Exception raised in getDesignationIdByDepartmentId Method:", e);
	}
	finally {
		openSession.close();
	}
	return  designationMasters;
	}

	@Override
	public List getAllVacanciesList() {
		Session openSession = sessionFactory.openSession();
		List<VacancyInfo> vacancyInfos= null;
		try{
			Criteria createCriteria = openSession.createCriteria(VacancyInfo.class);
					createCriteria.setFetchMode("companyDetails", FetchMode.EAGER);
					//.setFetchMode("vacancyDetails", FetchMode.EAGER);
		createCriteria.addOrder(Order.asc("vacancyInfoId"));
		vacancyInfos = createCriteria.list();
		}catch(Exception exception){
			logger.error("Exception raised in getAllVacanciesList Method:", exception);
			exception.printStackTrace();
		}
		finally {
			openSession.close();
		}
		return vacancyInfos;
	}

	@Override
	public VacancyInfo editVacancyInfoByVacancyInfoId(Long vacancyInfoId) {
		Session openSession = sessionFactory.openSession();
		List<VacancyInfo> vacancyList= null;
		VacancyInfo vacInfo = new VacancyInfo();
		try{
			Criteria createCriteria = openSession.createCriteria(VacancyInfo.class);
					createCriteria.setFetchMode("companyDetails", FetchMode.EAGER)
					.setFetchMode("vacancyDetails", FetchMode.EAGER);
			createCriteria.add(Restrictions.eq("vacancyInfoId", vacancyInfoId));
		createCriteria.addOrder(Order.asc("vacancyInfoId"));
		vacancyList = createCriteria.list();
		
		for (VacancyInfo vacancyInfo : vacancyList) {
			vacInfo = vacancyInfo;
		}
		}catch(Exception exception){
			logger.error("Exception raised in editVacancyInfoByVacancyInfoId Method:", exception);
			exception.printStackTrace();
		}
		finally {
			openSession.close();
		}
		return vacInfo;
	}
	@Override
	public String updateVacancyDetails(VacancyInfo vacancyInfo) {
		Session openSession = sessionFactory.openSession();
		Transaction beginTransaction = openSession.beginTransaction();
		String message = null;
		try{
			vacancyInfo.setStatus(true);
			openSession.update(vacancyInfo);
			List<VacancyDetails> vacancyDetailsfrontList = vacancyInfo.getVacancyDetails();					//frontend
			Criteria createCriteria = openSession.createCriteria(VacancyDetails.class,"vacDet");
			createCriteria.createAlias("vacDet.vacancyInfo", "vacancyInfo");
			createCriteria.add(Restrictions.eq("vacancyInfo.vacancyInfoId", vacancyInfo.getVacancyInfoId()));
			List<VacancyDetails> vacancyDetailsbackList =	createCriteria.list();							//backend
			List<VacancyDetails> vacancyDetailsfrontDummy = new ArrayList<VacancyDetails>(vacancyDetailsfrontList);   //frontend
			List<VacancyDetails>  vacancyDetailsbackDummy = new ArrayList<VacancyDetails>(vacancyDetailsbackList);   //backend
			
			
			for(int i=0;i<vacancyDetailsfrontList.size();i++){
				VacancyDetails vacancyDetails = vacancyDetailsfrontList.get(i);
				for(int j=0;j<vacancyDetailsbackList.size();j++)
				if(vacancyDetails.getVacanyDetailsId() == vacancyDetailsbackList.get(j).getVacanyDetailsId()){
					VacancyDetails vacancyDetails2 = vacancyDetailsbackList.get(j);
					vacancyDetails2.setStatus(true);
					openSession.update(vacancyDetails2);
					vacancyDetailsfrontDummy.remove(vacancyDetails);
					vacancyDetailsbackDummy.remove(vacancyDetails2);
				}
			}
			for(int i=0;i<vacancyDetailsfrontDummy.size();i++){
				VacancyDetails vacancyDetails = vacancyDetailsfrontDummy.get(i);
				vacancyDetails.setVacancyInfo(vacancyInfo);
				
				openSession.save(vacancyDetails);
			}
			for(int i=0;i<vacancyDetailsbackDummy.size();i++){
			    VacancyDetails vacancyDetails = vacancyDetailsbackDummy.get(i);
				openSession.delete(vacancyDetails);
			}
			
			message ="Vacancy Details Successfully Updated...";
			beginTransaction.commit();
		
			}catch(Exception e){
				logger.error("Exception raised in updateVacancyDetails Method:", e);
				message = "Vacancy Details not updated due to Exception!";
				e.printStackTrace();
			}finally {
			openSession.close();
		}
		return message;
	}

	
	
	
	
	
	
	
	@SuppressWarnings("rawtypes")
	@Override
	public List viewvacancyDetailsGrid() {
		Session openSession = sessionFactory.openSession();
		List list= null;
		try{
			openSession=sessionFactory.openSession();
			String sql ="Select c.companydetailsid,c.companyname,vi.vacancyname,vi.interviewscheduledate,"
					+ "vd.vacanydetailsid,vd.departmentid,vd.designationid,"
					+ "vd.noofvacancies,vd.locationofwork,vi.vacancyinfoid From leoacademy_tbl_vacancyinfo vi,"
					+ "leoacademy_leo_companyvacancydetails vd,leoacademy_tbl_companydetails c "
                    + "where (c.companydetailsid = vi.companydetailsid)and (vi.vacancyinfoid=vd.vacancyinfoid) and vd.status="+true+"";
			SQLQuery createSQLQuery = openSession.createSQLQuery(sql);
			list=createSQLQuery.list();
		}catch(Exception exception){
			logger.error("Exception raised in getAllVacanciesList Method:", exception);
			exception.printStackTrace();
		}
		finally {
			openSession.close();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PlacementDesignationMaster> getDesignationMaster() {
		Session openSession = sessionFactory.openSession();
		List<PlacementDesignationMaster> designationMasters = null;
		try{
			Criteria createCriteria = openSession.createCriteria(PlacementDesignationMaster.class);
			createCriteria.addOrder(Order.asc("designationMasterId"));
			designationMasters =  createCriteria.list();
	}catch(Exception e){
		logger.error("Exception raised in getDesignationMaster Method:", e);
	}
	finally {
		openSession.close();
	}
	return  designationMasters;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getVacancyDetailsById(Long vacanyDetailsId) {
		Session openSession = sessionFactory.openSession();
		List list= null;
		try{
			openSession=sessionFactory.openSession();
			String sql ="Select c.companydetailsid,c.companyname,vi.vacancyname,vi.interviewscheduledate,"
					+ "vd.vacanydetailsid,vd.departmentid,vd.designationid,"
					+ "vd.noofvacancies,vd.locationofwork,vi.vacancyinfoid From leoacademy_tbl_vacancyinfo vi,"
					+ "leoacademy_leo_companyvacancydetails vd,leoacademy_tbl_companydetails c "
                    + "where (c.companydetailsid = vi.companydetailsid)and (vi.vacancyinfoid=vd.vacancyinfoid) "
                    + "and vd.status="+true+" and vd.vacanydetailsid="+vacanyDetailsId+" ";
			SQLQuery createSQLQuery = openSession.createSQLQuery(sql);
			list=createSQLQuery.list();
			
		}catch(Exception exception){
			logger.error("Exception raised in getAllVacanciesList Method:", exception);
			exception.printStackTrace();
		}
		finally {
			openSession.close();
		}
		return list;
	}

	@Override
	public Integer AssignedStudentsCount(Long vacanyDetailsId) {
		Session openSession = sessionFactory.openSession();
		Integer count =null;
		List list = null;
		try{
			openSession=sessionFactory.openSession();
            Criteria  criteria = openSession.createCriteria(InterviewAssignToStudents.class,"ias")
            		.createAlias("ias.vacancyDetails", "vacancyDetails")
            		.add(Restrictions.eq("vacancyDetails.vacanyDetailsId", vacanyDetailsId));
            list=criteria.list();
         count = list.size();
		}catch(Exception exception){
			logger.error("Exception raised in getAllVacanciesList Method:", exception);
			exception.printStackTrace();
		}
		finally {
			openSession.close();
		}
		return count;
	}

	@Override
	public String interviewAssignedToStudents(InterviewAssignToStudents assignToStudents) {
		Session openSession = sessionFactory.openSession();
		Transaction beginTransaction = openSession.beginTransaction();
		String message = null;
		try{
			openSession.save(assignToStudents);
			beginTransaction.commit();
			message = "Interview Assigned To Students Details Successfully Saved...";
		}catch(Exception e){
			logger.error("Exception raised in interviewAssignedToStudents Method:", e);
			message = "Interview Assigned To Students  Details not saved due to Exception!";
			e.printStackTrace();
		}
		finally {
			openSession.close();
		}
		return message;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<VacancyInfo> getVacancyDetailsByCompanyDetailsId(Long companyDetailsId) {
		Session openSession = sessionFactory.openSession();
		List<VacancyInfo> compList = null;
		try{
		Criteria createCriteria = openSession.createCriteria(VacancyInfo.class,"vi")
				.setFetchMode("companyDetails", FetchMode.EAGER);
		createCriteria.add(Restrictions.eq("status", true))
		.createAlias("vi.companyDetails", "companyDetails")
		.add(Restrictions.eq("companyDetails.companyDetailsId", companyDetailsId));
		createCriteria.addOrder(Order.asc("vacancyInfoId"));
		compList = createCriteria.list();
		}catch(Exception exception){
			logger.error("Exception raised in getVacancyDetailsByCompanyDetailsId Method:", exception);
		}finally {
			openSession.close();
		}
		return compList;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List getDepartmentsByVacancyInfoId(Long vacancyInfoId) {
		Session openSession = sessionFactory.openSession();
		List  deptNames = null;
		try{
			Criteria createCriteria = openSession.createCriteria(VacancyDetails.class,"vd")
					.createAlias("vd.vacancyInfo", "vacancyInfo");
			createCriteria.add(Restrictions.eq("status", true));
			createCriteria.add(Restrictions.eq("vacancyInfo.vacancyInfoId", vacancyInfoId));
			deptNames = createCriteria.list();
		}catch(Exception exception){
			logger.error("Exception raised in getDepartmentsByVacancyDetailsId Method:", exception);
		}finally {
			openSession.close();
		}
		return deptNames;
	}

	@Override
	public List getStudentsByVacancyDetailsId(Long vacanyDetailsId) {
		Session openSession = sessionFactory.openSession();
		List<InterviewAssignToStudents> assignToStudents = null;
		try{
			Criteria createCriteria = openSession.createCriteria(InterviewAssignToStudents.class,"iats")
					.setFetchMode("vacancyDetails",FetchMode.EAGER )
					.setFetchMode("studentAdmissionEntity",FetchMode.EAGER)
					.setFetchMode("reasonsMaster",FetchMode.EAGER)
					.createAlias("iats.vacancyDetails", "vacancyDetails");
			createCriteria.add(Restrictions.eq("vacancyDetails.vacanyDetailsId", vacanyDetailsId));
			createCriteria.add(Restrictions.eq("vacancyDetails.status", true));
			createCriteria.add(Restrictions.eq("iats.statusReport", "In Process"));
			assignToStudents = createCriteria.list();
		}catch(Exception e){
			logger.error("Exception raised in getStudentsByVacancyDetailsIdFromAttendedInterViews Method:", e);
		}finally {
			openSession.close();
		}
		return assignToStudents;
	}

	@Override
	public List<StatusMaster> getStatusMaster() {
		Session openSession = sessionFactory.openSession();
		Transaction beginTransaction = openSession.beginTransaction();
		List statusMasters = null;
		try{
			Criteria createCriteria = openSession.createCriteria(StatusMaster.class);
			createCriteria.addOrder(Order.asc("statusId"));
			statusMasters = createCriteria.list();
			beginTransaction.commit();
		}catch(Exception e){
			logger.error("Exception raised in getStatusMaster Method:", e);
		}finally {
			openSession.close();
		}
		return statusMasters;
	}

	@Override
	public ReasonsMaster getReasonsByDepartmentId(Long departmentId) {
		Session openSession = sessionFactory.openSession();
		ReasonsMaster reasonNames = null;
		try{
			Criteria createCriteria = openSession.createCriteria(ReasonsMaster.class,"rm")
					.createAlias("rm.departmentMaster", "departmentMaster");
			createCriteria.add(Restrictions.eq("departmentMaster.departmentMasterId", departmentId));
			reasonNames = (ReasonsMaster) createCriteria.uniqueResult();
		}catch(Exception exception){
			logger.error("Exception raised in getReasonsByDepartmentId Method:", exception);
		}finally {
			openSession.close();
		}
		return reasonNames;
	}

	@Override
	public String saveDeclaredResults(InterviewAssignToStudents toStudents) {
		String message=null;
		Session openSession = sessionFactory.openSession();
		Transaction beginTransaction = openSession.beginTransaction();
		try{
			if(toStudents.getReasonsMaster().getReasonsMasterId()==null){
				ReasonsMaster master = new ReasonsMaster();
				toStudents.setReasonsMaster(null);
			}
			openSession.update(toStudents);
			message ="Declared Results Successfully...";
			beginTransaction.commit();
		}catch(Exception e){
			message ="not Declared due to Exception!";
			logger.error("Exception raised in saveDeclaredResults Method:", e);
		}finally {
			openSession.close();
		}
		return message;
	}

	@Override
	public String removeStudentsByVacancyDetailsId(Long id, Long vacanyDetailsId) {
		String msg = null;
		Session openSession = sessionFactory.openSession();
		Transaction beginTransaction = openSession.beginTransaction();
		try{
		String sql ="delete from leoacademy_tbl_interviewassigntostudents where admissionnumberid = "+id+" and vacanydetailsid="+vacanyDetailsId+"";
		SQLQuery createSQLQuery = openSession.createSQLQuery(sql);
		createSQLQuery.executeUpdate();
		beginTransaction.commit();
		msg ="Removed Successfully....";
		}catch(Exception e){
			msg ="Removing Failed Due to Exception....";
			logger.error("Exception raised in removeStudentsByVacancyDetailsId Method:", e);
		}
		finally {
			openSession.close();
		}
		return msg;
	}

	@Override
	public List getStudentsByVacancyDetailsIdFromAttendedInterViews(Long vacanyDetailsId) {
		Session openSession = sessionFactory.openSession();
		List<InterviewAssignToStudents> assignToStudents = null;
		try{
			Criteria createCriteria = openSession.createCriteria(InterviewAssignToStudents.class,"iats")
					.setFetchMode("vacancyDetails",FetchMode.EAGER )
					.setFetchMode("studentAdmissionEntity",FetchMode.EAGER)
					.setFetchMode("reasonsMaster",FetchMode.EAGER)
					.createAlias("iats.vacancyDetails", "vacancyDetails");
			createCriteria.add(Restrictions.eq("vacancyDetails.vacanyDetailsId", vacanyDetailsId));
			createCriteria.add(Restrictions.eq("vacancyDetails.status", true));
			assignToStudents = createCriteria.list();
		}catch(Exception e){
			logger.error("Exception raised in getStudentsByVacancyDetailsIdFromAttendedInterViews Method:", e);
		}finally {
			openSession.close();
		}
		return assignToStudents;
	}

	@Override
	public List getDepartmentsByVacancyInfoIdSize(Long vacancyInfoId) {
		Session openSession = sessionFactory.openSession();
		List  size = null;
		try{
			Criteria createCriteria = openSession.createCriteria(VacancyDetails.class,"vd")
					.createAlias("vd.vacancyInfo", "vacancyInfo");
			createCriteria.add(Restrictions.eq("status", true));
			createCriteria.add(Restrictions.eq("vacancyInfo.vacancyInfoId", vacancyInfoId));
			size = createCriteria.list();
		}catch(Exception exception){
			logger.error("Exception raised in getDepartmentsByVacancyDetailsIdsize Method:", exception);
		}finally {
			openSession.close();
		}
		return size;
	}

	@Override
	public String closingInterviewVacancyByVacancyInfoId(Long vacancyInfoId, Long vacancyDetailsId) {
		Session openSession = sessionFactory.openSession();
		Transaction beginTransaction = openSession.beginTransaction();
		String msg =null;
		try{
			SQLQuery createSQLQuery = openSession.createSQLQuery("update  leoacademy_leo_companyvacancydetails SET status= false where vacancyInfoId ="+vacancyInfoId+" and vacanyDetailsId ="+vacancyDetailsId+"");
			createSQLQuery.executeUpdate();
			beginTransaction.commit();
			msg = "Close Successfully....";
		}catch(Exception e){
			logger.error("Exception raised in closingInterviewVacancyByVacancyInfoId Method:", e);
		}finally {
			openSession.close();
		}
		return msg;
	}

	@Override
	public String closingInterviewVacancyByVacancyInfoIds(Long vacancyInfoId, Long vacancyDetailsId) {
		Session openSession = sessionFactory.openSession();
		Transaction beginTransaction = openSession.beginTransaction();
		VacancyDetails datails = null;
		VacancyInfo info = null;
		String msg =null;
		  
		try{
			Criteria add = openSession.createCriteria(VacancyDetails.class)
			.add(Restrictions.eq("vacanyDetailsId", vacancyDetailsId));
			datails = (VacancyDetails) add.uniqueResult();
			info = datails.getVacancyInfo();
			datails.setStatus(false);
			info.setStatus(false);
			datails.setVacancyInfo(info);
			beginTransaction.commit();
			 msg ="Close Successfully....";
		}catch(Exception e){
			logger.error("Exception raised in closingInterviewVacancyByVacancyInfoId Method:", e);
		}finally {
			openSession.close();
		}
		return msg;
	}
	
}
