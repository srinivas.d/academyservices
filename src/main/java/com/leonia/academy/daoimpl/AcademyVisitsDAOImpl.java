package com.leonia.academy.daoimpl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;


import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.leonia.academy.entity.DropDownMasterEntity;
import com.leonia.academy.entity.DropDownSubMasterEntity;
import com.leonia.academy.entity.InstituteContactPersonEntity;
import com.leonia.academy.entity.InstituteMasterEntity;
import com.leonia.academy.entity.VisitEntryEntity;
import com.leonia.academy.idao.IAcademyVisitsDAO;

@Repository
public class AcademyVisitsDAOImpl implements IAcademyVisitsDAO {

	@Autowired
	private SessionFactory sessionFactory;

	private static final Logger logger = Logger.getLogger(AcademyVisitsDAOImpl.class);
	
	@SuppressWarnings("rawtypes")
	@Override
	public List<DropDownMasterEntity> getInstituteType() {
		Session openSession = sessionFactory.openSession();
		List<DropDownMasterEntity> list = null;
		try{
			Criteria createCriteria = openSession.createCriteria(DropDownMasterEntity.class);
			createCriteria.setFetchMode("downSubMasterEntity", FetchMode.EAGER);
			        // createCriteria.createAlias("downSubMasterEntity", "downSubMasterEntity");
			         /* createCriteria.setProjection(Projections.projectionList()
			        		  .add(Projections.property("abcd.dropDownSubMasterId"))
			        		  .add(Projections.property("abcd.description"))
			        		  .add(Projections.property("master.dropdownmasterId"))
			        		  );*/
			list = createCriteria.list();
		}catch(Exception e){
			logger.error("Exception raised in getInstituteType Method:", e);
		}finally {
			openSession.close();
		}
		return list;
	}

	@Override
	public String saveInstituteMasterDetails(InstituteMasterEntity instituteMasterentity) {
		Session openSession = sessionFactory.openSession();
		Transaction beginTransaction = openSession.beginTransaction();
		String message = null;
		try{
			List<InstituteContactPersonEntity> contactPersonEntities = instituteMasterentity.getContactPersonEntities();
			for (InstituteContactPersonEntity instituteContactPersonEntity : contactPersonEntities) {
				instituteContactPersonEntity.setInstituteMasterEntity(instituteMasterentity);
			}
			openSession.save(instituteMasterentity);
			beginTransaction.commit();
			message = "Institute Master Details Successfully Saved...";
		}catch(Exception e){
			logger.error("Exception raised in saveInstituteMasterDetails Method:", e);
			message = "Institute Master Details not saved due to Exception!";
			e.printStackTrace();
		}
		finally {
			openSession.close();
		}
		return message;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<InstituteMasterEntity> getInstituteMasterList(String status) {
		Session openSession = sessionFactory.openSession();
		List<InstituteMasterEntity> masterEntities = null;
		try{
			Criteria createCriteria = openSession.createCriteria(InstituteMasterEntity.class);
			if(status.equals("Active")){
				createCriteria.add(Restrictions.ilike("status", status));
			}else if(status.equals("InActive")){
				createCriteria.add(Restrictions.ilike("status", status));
			}
			masterEntities = createCriteria.list();
		}catch(Exception e){
			logger.error("Exception raised in getInstituteMasterList Method:", e);
		}
		finally {
			openSession.close();
		}
		return masterEntities;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List getContactPersons(Long instituteId) {
		Session openSession = sessionFactory.openSession();
		List<InstituteContactPersonEntity> list = null;
		try{
		Criteria createCriteria = openSession.createCriteria(InstituteContactPersonEntity.class,"icp")
				.createAlias("icp.instituteMasterEntity", "instituteMasterEntity")
				.add(Restrictions.eq("instituteMasterEntity.instituteId",instituteId));
		list = createCriteria.list();
		}catch(Exception e){
			logger.error("Exception raised in editInstituteMaster Method:", e);
		}
		finally {
			openSession.close();
		}
		return  list;
	}

	@SuppressWarnings({ "rawtypes", "unused", "unchecked" })
	@Override
	public InstituteMasterEntity editInstituteMaster(int instituteId) {
		Session openSession = sessionFactory.openSession();
		List<InstituteMasterEntity> entity = null;
		InstituteMasterEntity uniqueResult = null;
		try{
			Criteria createCriteria = openSession.createCriteria(InstituteMasterEntity.class,"ime")
					.setFetchMode("contactPersonEntities", FetchMode.EAGER)
					.add(Restrictions.eq("instituteId",instituteId));
			entity =  createCriteria.list();
			
			for (InstituteMasterEntity instituteMasterEntity : entity) {
				uniqueResult = instituteMasterEntity;
			}
		}catch(Exception e){
			logger.error("Exception raised in editInstituteMaster Method:", e);
		}
		finally {
			openSession.close();
		}
		return  uniqueResult;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List getContactInfo(int instituteId) {
		Session openSession = sessionFactory.openSession();
		List<InstituteContactPersonEntity> contactPersonEntities = null;
		try{
			Criteria createCriteria = openSession.createCriteria(InstituteContactPersonEntity.class,"icpe")
					.createAlias("icpe.instituteMasterEntity", "instituteMasterEntity")
					.add(Restrictions.eq("instituteMasterEntity.instituteId",instituteId));
			contactPersonEntities =  createCriteria.list();
	}catch(Exception e){
		logger.error("Exception raised in getContactInfo Method:", e);
	}
	finally {
		openSession.close();
	}
	return  contactPersonEntities;

}

	@SuppressWarnings("unchecked")
	@Override
	public String updateInstituteMasterDetails(InstituteMasterEntity instituteMasterentity) {
		Session openSession = sessionFactory.openSession();
		Transaction beginTransaction = openSession.beginTransaction();
		String message = null;
		try{
		InstituteMasterEntity masterEntity =(InstituteMasterEntity) openSession.get(InstituteMasterEntity.class, instituteMasterentity.getInstituteId());
		masterEntity.setInstituteId(instituteMasterentity.getInstituteId());
		masterEntity.setInstituteName(instituteMasterentity.getInstituteName());
		masterEntity.setInstituteAddress(instituteMasterentity.getInstituteAddress());
		masterEntity.setFax(instituteMasterentity.getFax());
		masterEntity.setEmail(instituteMasterentity.getEmail());
		masterEntity.setLandMark(instituteMasterentity.getLandMark());
		List<InstituteContactPersonEntity> employerContactInformation = instituteMasterentity.getContactPersonEntities();
		Criteria createCriteria = openSession.createCriteria(InstituteContactPersonEntity.class,"icp");
		createCriteria.createAlias("instituteMasterEntity", "cd");
		createCriteria.add(Restrictions.eq("cd.instituteId",instituteMasterentity.getInstituteId()));
		List<InstituteContactPersonEntity> list = createCriteria.list();
		
		List<InstituteContactPersonEntity> employerContactInformationDummy = new LinkedList<InstituteContactPersonEntity>(employerContactInformation);
		List<InstituteContactPersonEntity> listDummy = new LinkedList<InstituteContactPersonEntity>(list);
		for(int i=0;i<employerContactInformation.size();i++){ 
			InstituteContactPersonEntity empContactInfo = employerContactInformation.get(i);
			for(int j=0;j<list.size();j++){
				if(empContactInfo.getInstituteContactPersonEntityId()==list.get(j).getInstituteContactPersonEntityId()){
					InstituteContactPersonEntity employerContactInfo = list.get(j);
					employerContactInfo.setContactPersonName(empContactInfo.getContactPersonName());
					employerContactInfo.setContactPersonDesignation(empContactInfo.getContactPersonDesignation());
					employerContactInfo.setContactPersonEmail(empContactInfo.getContactPersonEmail());
					employerContactInfo.setContactPersonPhone(empContactInfo.getContactPersonPhone());
					
					openSession.update(employerContactInfo);
					employerContactInformationDummy.remove(empContactInfo);
					listDummy.remove(employerContactInfo);
				}
			}
		}
		for(int i=0;i<employerContactInformationDummy.size();i++){
			InstituteContactPersonEntity contactInformation = employerContactInformationDummy.get(i);
			contactInformation.setInstituteMasterEntity(instituteMasterentity);
			
			openSession.save(contactInformation);
		}
		for(int i=0;i<listDummy.size();i++){
			InstituteContactPersonEntity employerContactInformation3 = listDummy.get(i);
			openSession.delete(employerContactInformation3);
		}
		//openSession.update(masterEntity);
		message = "Institute Master Details Updated Successfully..";
		beginTransaction.commit();
		
		}catch(Exception e){
			message = "Institute Master Details not Updated due to Exception!";
			logger.error("Exception raised in getContactInfo Method:", e);
		}finally {
			openSession.close();
		}
		return message;
	}

	@Override
	public String saveVisitEntryDetails(VisitEntryEntity visitEntryEntity) {
		Session openSession = sessionFactory.openSession();
		Transaction beginTransaction = openSession.beginTransaction();
		String message = null;
		try{
			openSession.save(visitEntryEntity);
			beginTransaction.commit();
			message = "Visit Entry Details Successfully Saved...";
		}catch(Exception e){
			logger.error("Exception raised in saveVisitEntryDetails Method:", e);
			message = "Visit Entry Details not saved due to Exception!";
			e.printStackTrace();
		}
		finally {
			openSession.close();
		}
		return message;
	}

	@Override
	public String getInstituteTypeByInstituteTypeId(Integer long1) {
		Session openSession = sessionFactory.openSession();
		String description =  null;
		try{
			DropDownSubMasterEntity entity = (DropDownSubMasterEntity)openSession.get(DropDownSubMasterEntity.class,long1);
			 description = entity.getDescription();
		}catch(Exception e){
			logger.error("Exception raised in getInstituteTypeByInstituteTypeId Method:", e);
			e.printStackTrace();
		}
		finally {
			openSession.close();
		}
		return description;
	}
	@Override
	public String deactivateInstitute(InstituteMasterEntity entity) {
		Session openSession = sessionFactory.openSession();
		Transaction beginTransaction = openSession.beginTransaction();
		try{
			Criteria criteria = openSession.createCriteria(InstituteMasterEntity.class)
					.add(Restrictions.eq("instituteId", entity.getInstituteId()));
			InstituteMasterEntity institutemaster = (InstituteMasterEntity) criteria.uniqueResult();
			institutemaster.setStatus(entity.getStatus());
			beginTransaction.commit();
		}catch(Exception e){
			logger.error("Exception raised in deactivateInstitute Method:", e);
			e.printStackTrace();
		}
		finally {
			openSession.close();
		}
		return null;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<VisitEntryEntity> getvisitreport(VisitEntryEntity visitEntryEntity) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		List<VisitEntryEntity> list = null;
		try{
		Criteria criteria = session.createCriteria(VisitEntryEntity.class);
		if (visitEntryEntity.getFromDate() != null && !(visitEntryEntity.getFromDate().isEmpty())) {
			Date startdate1 = null;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				startdate1 = sdf.parse(visitEntryEntity.getFromDate());
			} catch (Exception e) {
				logger.error("Exception raised in getVisitReportDetails Method:", e);
				e.printStackTrace();
			}
			criteria.add(Restrictions.ge("nextVisitDate", startdate1));
		}
		if (visitEntryEntity.getToDate() != null && !(visitEntryEntity.getToDate().isEmpty())) {
			Date enddate1 = null;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				enddate1 = sdf.parse(visitEntryEntity.getToDate());
			} catch (Exception e) {
				logger.error("Exception raised in getVisitReportDetails Method:", e);
				e.printStackTrace();
			}
			criteria.add(Restrictions.le("nextVisitDate", enddate1));
		}
		/*if(visitEntryEntity.getUsername() !=null && !(visitEntryEntity.getUsername().isEmpty())){
			criteria.add(Restrictions.ilike("username", visitEntryEntity.getUsername()));
		}*/
		Disjunction disjunction = Restrictions.disjunction();
		criteria.add(disjunction);
		criteria.addOrder(Order.asc("nextVisitDate"));
		list = criteria.list();
		transaction.commit();
		}catch(Exception e){
			logger.error("Exception raised in getVisitReportDetails Method:", e);
			e.printStackTrace();
		}finally{
		session.close();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VisitEntryEntity> getscheduledvisitreport(VisitEntryEntity visitEntryEntity) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		List<VisitEntryEntity> list = null;
		try{
		Criteria criteria = session.createCriteria(VisitEntryEntity.class);
		if (visitEntryEntity.getFromDate() != null && !(visitEntryEntity.getFromDate().isEmpty())) {
			Date startdate1 = null;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				startdate1 = sdf.parse(visitEntryEntity.getFromDate());
			} catch (Exception e) {
				logger.error("Exception raised in getScheduledVisitReportDetails Method:", e);
				e.printStackTrace();
			}
			criteria.add(Restrictions.ge("visitDate", startdate1));
		}
		if (visitEntryEntity.getToDate() != null && !(visitEntryEntity.getToDate().isEmpty())) {
			Date enddate1 = null;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				enddate1 = sdf.parse(visitEntryEntity.getToDate());
			} catch (Exception e) {
				logger.error("Exception raised in getScheduledVisitReportDetails Method:", e);
				e.printStackTrace();
			}
			criteria.add(Restrictions.le("visitDate", enddate1));
		}
		/*if(visitEntryEntity.getUsername() !=null && !(visitEntryEntity.getUsername().isEmpty())){
			criteria.add(Restrictions.ilike("username", visitEntryEntity.getUsername()));
		}*/
		Disjunction disjunction = Restrictions.disjunction();
		criteria.add(disjunction);
		criteria.addOrder(Order.asc("visitDate"));
		list = criteria.list();
		transaction.commit();
		}catch(Exception e){
			logger.error("Exception raised in getScheduledVisitReportDetails Method:", e);
			e.printStackTrace();
		}finally{
			session.close();
		}
		return list;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DropDownSubMasterEntity> getDropDownSubMasterList() {
		Session openSession = sessionFactory.openSession();
		List<DropDownSubMasterEntity> downSubMasterEntity = null;
		try{
			Criteria createCriteria = openSession.createCriteria(DropDownSubMasterEntity.class);
			downSubMasterEntity = createCriteria.list();
		}catch(Exception e){
			logger.error("Exception raised in getDropDownSumMasterList Method:", e);
		}finally {
			openSession.close();
		}
		return downSubMasterEntity;
	}
}
