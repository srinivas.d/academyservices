
package com.leonia.academy.daoimpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.Order;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.leonia.academy.entity.LeadFollowUpDTO;
import com.leonia.academy.idao.ILeadGeneratedReportDAO;
@Repository
public class LeadGeneratedReportDAOImpl implements ILeadGeneratedReportDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	private static final Logger logger = Logger.getLogger(LeadGeneratedReportDAOImpl.class);
	@Override
	public List<com.leonia.academy.entity.ActiveDTO> activelist() {
		return null;
	}
	@Override
	public List<LeadFollowUpDTO> getLeadDetails(LeadFollowUpDTO leadFollowUpDTO) {
		    Session openSession = null;
			List<LeadFollowUpDTO> list = null;
			try{
				openSession = sessionFactory.openSession();
				Criteria criteria=openSession.createCriteria(LeadFollowUpDTO.class,"leadFollowUpDTO");
				//criteria.createAlias("leadFollowUpDTO.loginEntity", "loginuser");
				criteria.setProjection(Projections.projectionList()
					    .add(Projections.groupProperty("leadFollowUpDTO.followUpId"))
					    .add(Projections.groupProperty("leadFollowUpDTO.studentName"))
					    .add(Projections.groupProperty("leadFollowUpDTO.source"))
					    .add(Projections.groupProperty("leadFollowUpDTO.course"))
					    .add(Projections.groupProperty("leadFollowUpDTO.leadDate"))
					    .add(Projections.groupProperty("leadFollowUpDTO.datefollowup"))
					    .add(Projections.groupProperty("leadFollowUpDTO.allocatedUserName"))
					    .add(Projections.groupProperty("leadFollowUpDTO.status")));
				//allocatedUserName
				list=criteria.list();
			}catch(Exception exception){
				logger.error("Exception raised in getBuildingDetails Method:", exception);
			}
			finally {
				openSession.close();
			}
			return list;
		}
	
	@Override
	public List<LeadFollowUpDTO> searchLeadList(LeadFollowUpDTO leadFollowUpDTO) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		List<LeadFollowUpDTO> list = null;
		Criteria criteria = session.createCriteria(LeadFollowUpDTO.class, "lead");
		if(leadFollowUpDTO.getEndDate() != null &&leadFollowUpDTO.getStartDate() != null )
		{
			Date enddate1 = null;
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
			try {
				enddate1 = sdf1.parse(leadFollowUpDTO.getEndDate());
			} catch (Exception exception) {
				logger.error("Exception raised in leadgeneratedreportdetails Method:", exception);
			}
			
			
			Date startdate2 = null;
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
			try {
				startdate2 = sdf2.parse(leadFollowUpDTO.getStartDate());
			} catch (Exception e) {
				e.printStackTrace();
			}
			criteria.add(Restrictions.ge("leadDate", startdate2));
			criteria.add(Restrictions.le("leadDate", enddate1));
			criteria.setProjection(Projections.projectionList()
				    .add(Projections.groupProperty("lead.followUpId"))
				    .add(Projections.groupProperty("lead.studentName"))
				    .add(Projections.groupProperty("lead.source"))
				    .add(Projections.groupProperty("lead.course"))
				    .add(Projections.groupProperty("lead.leadDate"))
				    .add(Projections.groupProperty("lead.datefollowup"))
				    .add(Projections.groupProperty("lead.allocatedUserName"))
				    .add(Projections.groupProperty("lead.status")));		
			 list = criteria.list();
		
			
		}else if (leadFollowUpDTO.getStartDate() != null && !(leadFollowUpDTO.getStartDate().isEmpty())) {
			Date startdate1 = null;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				startdate1 = sdf.parse(leadFollowUpDTO.getStartDate());
			} catch (Exception e) {
				e.printStackTrace();
			}
			criteria.add(Restrictions.ge("leadDate", startdate1));
			criteria.setProjection(Projections.projectionList()
				    .add(Projections.groupProperty("lead.followUpId"))
				    .add(Projections.groupProperty("lead.studentName"))
				    .add(Projections.groupProperty("lead.source"))
				    .add(Projections.groupProperty("lead.course"))
				    .add(Projections.groupProperty("lead.leadDate"))
				    .add(Projections.groupProperty("lead.datefollowup"))
				    .add(Projections.groupProperty("lead.allocatedUserName"))
				    .add(Projections.groupProperty("lead.status")));		
			 list = criteria.list();	
		}
		else if (leadFollowUpDTO.getEndDate() != null && !(leadFollowUpDTO.getEndDate().isEmpty())) {
			Date enddate1 = null;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				enddate1 = sdf.parse(leadFollowUpDTO.getEndDate());
			} catch (Exception exception) {
				logger.error("Exception raised in leadgeneratedreportdetails Method:", exception);
			}
			criteria.add(Restrictions.le("leadDate", enddate1));
			criteria.setProjection(Projections.projectionList()
				    .add(Projections.groupProperty("lead.followUpId"))
				    .add(Projections.groupProperty("lead.studentName"))
				    .add(Projections.groupProperty("lead.source"))
				    .add(Projections.groupProperty("lead.course"))
				    .add(Projections.groupProperty("lead.leadDate"))
				    .add(Projections.groupProperty("lead.datefollowup"))
				    .add(Projections.groupProperty("lead.allocatedUserName"))
				    .add(Projections.groupProperty("lead.status")));		
			 list = criteria.list();
		}
		
		else {
			criteria.setProjection(Projections.projectionList()
				    .add(Projections.groupProperty("lead.followUpId"))
				    .add(Projections.groupProperty("lead.studentName"))
				    .add(Projections.groupProperty("lead.source"))
				    .add(Projections.groupProperty("lead.course"))
				    .add(Projections.groupProperty("lead.leadDate"))
				    .add(Projections.groupProperty("lead.datefollowup"))
				    .add(Projections.groupProperty("lead.allocatedUserName"))
				    .add(Projections.groupProperty("lead.status")));		
			 list = criteria.list();
			
		}
		transaction.commit();
		session.close();
		return list;
	}
}
