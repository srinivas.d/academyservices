package com.leonia.academy.daoimpl;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.leonia.academy.entity.BatchMaster;
import com.leonia.academy.entity.Certificatemaster;
import com.leonia.academy.entity.Certificatetaken;
import com.leonia.academy.entity.StudentAdmissionEntity;
import com.leonia.academy.idao.IStudentAdmissionDAO;

@Repository
public class StudentAdmissionDAOImpl implements IStudentAdmissionDAO{
	@Autowired
	private SessionFactory sessionFactory;
	private static final Logger logger = Logger.getLogger(StudentAdmissionDAOImpl.class);

	@Override
	public List<StudentAdmissionEntity> getStudents(StudentAdmissionEntity studentAdmission) {
		Session openSession = null;
		List list=null;
		try{
			openSession = sessionFactory.openSession();
			list=openSession.createSQLQuery("select s.admissionnumberid,s.studentfirstname,b.batchname,s.studentmiddlename,s.studentlastname from studentadmission s INNER JOIN batchmaster b on s.batchid=b.batchid;").list();
		}catch(Exception exception){
			logger.error("Exception raised in getStudents Method:", exception);
		}
		finally {
			openSession.close();
		}
		return list;
	}

	@Override
	public List<Certificatemaster> getCertificates(Long admissionId) {
		Session openSession = null;
		List<Certificatemaster> list2=null;
		try{
			openSession = sessionFactory.openSession();
			Criteria criteria=openSession.createCriteria(Certificatetaken.class,"ct");
			criteria.add(Restrictions.eq("ct.sadmissionid",admissionId));
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("ct.scertificatename")));
			List<String> list1=criteria.list();
			
			Criteria criteria2=openSession.createCriteria(Certificatemaster.class);
			for(int i=0;i<list1.size();i++){
				criteria2.add(Restrictions.ne("certificatename", (String)list1.get(i)));
			}
			
			criteria2.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("certificatename")));
			 list2=criteria2.list();
		}catch(Exception exception){
			logger.error("Exception raised in getCertificates Method:", exception);
		}
		finally {
			openSession.close();
		}
		return list2;
	}

	@Override
	public List<StudentAdmissionEntity> getStudentDetailsById(Long admissionId) {
		Long studentid=new Long(admissionId);
		Session openSession = null;
		List list=null;
		List list2=null;
		try{
			openSession = sessionFactory.openSession();
			SQLQuery query=openSession.createSQLQuery("select courseid from studentadmission where admissionnumberid="+studentid+"");
			BigInteger courseid = (BigInteger)query.uniqueResult();
			Criteria criteria=openSession.createCriteria(StudentAdmissionEntity.class,"sa");
			criteria.createAlias("sa.batchMaster", "batchmaster");
			criteria.add(Restrictions.eq("sa.admissionNumberId",studentid));
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("sa.admissionNumberId"))
					.add(Projections.groupProperty("sa.admissionNumber"))
					.add(Projections.groupProperty("sa.studentFirstName"))
					.add(Projections.groupProperty("batchmaster.batchName")));
			list=criteria.list();
			SQLQuery query2=openSession.createSQLQuery("select shortcode from coursemaster where courseid="+courseid+"");
			String coursename=(String)query2.uniqueResult();
			list.add(coursename);
		}catch(Exception exception){
			logger.error("Exception raised in getStudentDetails Method:", exception);
		}
		finally {
			openSession.close();
		}
		return list;
	}

	@Override
	public String addCertificateDetails(Certificatetaken certificatetaken) {
		Session ses=sessionFactory.openSession();
		Transaction tx= ses.beginTransaction();
		List list=ses.createQuery("select scertificatename from Certificatetaken where sadmissionid='"+certificatetaken.getSadmissionid()+"' and scertificatename='"+certificatetaken.getScertificatename()+"'").list();
		if(list.size()>0)
		{
			return certificatetaken.getScertificatename()+" have already taken..";
		}
		else{
			Serializable save = ses.save(certificatetaken);
			tx.commit();
			 if(save!=null){
					list = ses.createQuery("from Certificatetaken where documentid="+save).list();
					Object object = list.get(0);
					Certificatetaken certificatetaken1=(Certificatetaken)object;
					byte[] data = certificatetaken1.getData();
					int documentid = certificatetaken1.getDocumentid();
					String studentname = certificatetaken1.getStudentname();
				     FileOutputStream fos;
					try {
						fos = new FileOutputStream("D:/images/"+documentid+""+studentname+".jpg",false);
						fos.write(data);
						   fos.flush();
					        fos.close();
					
					} catch (IOException e) {
						logger.error("Exception raised in savedocument Method:", e);
					}
			 }
			return "Saved successfully";
			}
	}

	@Override
	public List<Certificatetaken> getCertificatesByid(Long admissionId) {
		List<Certificatetaken>  list=null;
		Session sessionObj=null;
		try{
		sessionObj=sessionFactory.openSession();
		list= sessionObj.createQuery("select scertificatename,certificateno,status,sadmissionid from Certificatetaken where sadmissionid="+admissionId+"").list();
	}catch(Exception exception){
		logger.error("Exception raised in listOfDocument Method:", exception);
	}
	finally {
		sessionObj.close();
	}
		
		return list;
	}

	@Override
	public List printCertificates(Long admissionId) {
		List list=new ArrayList();
				Session sessionObj = sessionFactory.openSession();
				Transaction tx = sessionObj.beginTransaction();
				int intercount =0, ssccount = 0,incomecount = 0,degreecount =0,castecount = 0;
				try{
				  list = sessionObj.createQuery("select scertificatename,certificateno,status from Certificatetaken where sadmissionid=?").setParameter(0, admissionId).list();
				  ListIterator<Certificatetaken> listIterator = list.listIterator();
				  while(listIterator.hasNext()){
					Object obj = listIterator.next();
					Object[] objArray = (Object[])obj;
					if(objArray[0].equals("INTER")){
						intercount++;
					}
					if(objArray[0].equals("SSC")){
						ssccount++;
					}
					if(objArray[0].equals("INCOME")){
						incomecount++;
					}
					if(objArray[0].equals("DEGREE")){
						degreecount++;
					}
					if(objArray[0].equals("CASTE")){
						castecount++;
					}
					
				}
				if(ssccount==0){
					Object[] objArray ={"SSC","NA","NA"};
					list.add(0, objArray);
				}
				if(intercount==0){
					Object[] objArray ={"INTER","NA","NA"};
					list.add(1,objArray);
				}
				if(degreecount==0){
					Object[] objArray ={"DEGREE","NA","NA"};
					list.add(2,objArray);
				}
				if(castecount==0){
					Object[] objArray ={"CASTE","NA","NA"};
					list.add(3,objArray);
				}
				if(incomecount==0){
					Object[] objArray ={"INCOME","NA","NA"};
					list.add(4,objArray);
				}
				
				list.size();
				}
				catch(Exception exception)
				{
					logger.error("Exception raised in printCertificates Method:", exception);
				}
				finally{
					tx.commit();
					sessionObj.close();
					
				}
				return list;
	}

	@Override
	public List<BatchMaster> getBatchname() {
		Session openSession = null;
		List list=null;
		try{
			openSession = sessionFactory.openSession();
			Criteria criteria=openSession.createCriteria(BatchMaster.class,"bm");
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("bm.batchId"))
					.add(Projections.groupProperty("bm.batchName")));
			list=criteria.list();
		}catch(Exception exception){
			logger.error("Exception raised in getBatchname Method:", exception);
		}
		finally {
			openSession.close();
		}
		return list;
	}

	@Override
	public List<StudentAdmissionEntity> dropdownValueForStudent(Long batchid) {
		Session openSession = null;
		List list=null;
		try{
			openSession = sessionFactory.openSession();
			list = openSession.createSQLQuery("select admissionNumberId,studentfirstname,studentMiddleName,studentLastName from studentadmission where batchid="+batchid+"").list();
		}catch(Exception exception){
			logger.error("Exception raised in getBatchname Method:", exception);
		}
		finally {
			openSession.close();
		}
		return list;
	}

	@Override
	public List<Certificatetaken> getStudentDetailsByIdForReturn(Long sid) {
		
		List<Certificatetaken>  list=null;
		Session sessionObj=null;
		try{
		sessionObj=sessionFactory.openSession();
		list= sessionObj.createQuery("select documentid,sadmissionno,studentname,sbatchname,scertificatename,certificateno,creatdedon,returnedon,status,course from Certificatetaken where sadmissionid=? order by documentid asc").setParameter(0, sid).list();
	}catch(Exception exception){
		logger.error("Exception raised in getDocumentsByName Method:", exception);
	}
	finally {
		sessionObj.close();
	}
		
		return list;
	}

	@Override
	public void updateStatusDate(String sid, String username) {
		
		Session ses=sessionFactory.openSession();
        Transaction tx= ses.beginTransaction();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try{
   		 Date today =Calendar.getInstance().getTime();
   		 Integer id=0;
   		 if(sid!=null)
   		 {
   			 id = Integer.parseInt(sid);
   		 }
   		String date=sdf.format(today);
			Criteria createCriteria = ses.createCriteria(Certificatetaken.class);
			createCriteria.add(Restrictions.eq("documentid", id));
			Certificatetaken master = (Certificatetaken) createCriteria.uniqueResult();
			master.setStatus("Returned");
			master.setReturnedby(username);
			master.setReturnedon(date);
			ses.update(master);
			tx.commit();
        }catch(Exception e){
     	   logger.error("Exception raised in updateStatusDate Method:", e);
        }finally {
			ses.close();
		}
	}

	@Override
	public List<Certificatetaken> getCheckedAndUnCheckedDocuments() {
		List<Certificatetaken> list = null;
		Session sessionObj=null;
		try{
		sessionObj = sessionFactory.openSession();	
		Criteria criteria = sessionObj.createCriteria(Certificatetaken.class);
		criteria.setProjection(Projections.projectionList()
				.add(Projections.groupProperty("sadmissionno"))
				.add(Projections.groupProperty("studentname"))
				.add(Projections.groupProperty("sbatchname"))
				.add(Projections.groupProperty("scertificatename"))
				.add(Projections.groupProperty("certificateno"))
				.add(Projections.groupProperty("creatdedon"))
				.add(Projections.groupProperty("returnedon"))
				.add(Projections.groupProperty("returnedby"))
				.add(Projections.groupProperty("status")));
				
		 list = criteria.list();
		}catch(Exception e){
			logger.error("Exception raised in getDocByDate Method:", e);
		}finally {
			sessionObj.close();
		}
		return list;
	}

	@Override
	public List<Certificatetaken> getCheckincheckoutreportByDate(String startdate, String enddate) {
		Date fromdate=null;
		Date todate=null;
		List<Certificatetaken> list = null;
		Session sessionObj = sessionFactory.openSession();
		try{
		  if(!(startdate==null)&!(enddate==null)){
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		    fromdate=sdf.parse(startdate);
			todate=sdf.parse(enddate);
			Criteria criteria = sessionObj.createCriteria(Certificatetaken.class);
			criteria.add(Restrictions.ge("creatdedon", fromdate));
			criteria.add(Restrictions.le("creatdedon", todate));
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("sadmissionno"))
					.add(Projections.groupProperty("studentname")) 
					.add(Projections.groupProperty("sbatchname"))
					.add(Projections.groupProperty("scertificatename"))
					.add(Projections.groupProperty("certificateno"))
					.add(Projections.groupProperty("creatdedon"))
					.add(Projections.groupProperty("returnedon"))
					.add(Projections.groupProperty("returnedby"))
					.add(Projections.groupProperty("status"))
					.add(Projections.count("documentid"), "count"));
			 criteria.addOrder(Order.asc("count"));
			 list = criteria.list();
			}
		  else if(!(startdate==null)&!(startdate.isEmpty())){
			  
			  SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			    fromdate=sdf.parse(startdate);
				Criteria criteria = sessionObj.createCriteria(Certificatetaken.class);
				criteria.add(Restrictions.ge("creatdedon", fromdate));
				criteria.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("sadmissionno"))
						.add(Projections.groupProperty("studentname")) 
						.add(Projections.groupProperty("sbatchname"))
						.add(Projections.groupProperty("scertificatename"))
						.add(Projections.groupProperty("certificateno"))
						.add(Projections.groupProperty("creatdedon"))
						.add(Projections.groupProperty("returnedon"))
						.add(Projections.groupProperty("returnedby"))
						.add(Projections.groupProperty("status"))
						.add(Projections.count("documentid"), "count"));
				 criteria.addOrder(Order.asc("count"));
				 list = criteria.list();
		  }
		  else if(!(enddate==null)&!(enddate.isEmpty()))
		  {
			  SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				todate=sdf.parse(enddate);
				Criteria criteria = sessionObj.createCriteria(Certificatetaken.class);
				criteria.add(Restrictions.le("creatdedon", todate));
				criteria.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("sadmissionno"))
						.add(Projections.groupProperty("studentname")) 
						.add(Projections.groupProperty("sbatchname"))
						.add(Projections.groupProperty("scertificatename"))
						.add(Projections.groupProperty("certificateno"))
						.add(Projections.groupProperty("creatdedon"))
						.add(Projections.groupProperty("returnedon"))
						.add(Projections.groupProperty("returnedby"))
						.add(Projections.groupProperty("status"))
						.add(Projections.count("documentid"), "count"));
				 criteria.addOrder(Order.asc("count"));
				 list = criteria.list();
		  }
		  else
			{
				Criteria criteria = sessionObj.createCriteria(Certificatetaken.class);
				criteria.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("sadmissionno"))
						.add(Projections.groupProperty("studentname"))
						.add(Projections.groupProperty("sbatchname"))
						.add(Projections.groupProperty("scertificatename"))
						.add(Projections.groupProperty("certificateno"))
						.add(Projections.groupProperty("creatdedon"))
						.add(Projections.groupProperty("returnedon"))
						.add(Projections.groupProperty("returnedby"))
						.add(Projections.groupProperty("status"))
						.add(Projections.count("documentid"), "count"));
				 criteria.addOrder(Order.asc("count"));
				 list = criteria.list();
			}
		}catch(ParseException e){
			logger.error("Exception raised in getDocByDate Method:", e);
		}finally {
			sessionObj.close();
		}
		return list;
	}

	@Override
	public List<Certificatetaken> getCheckincheckoutreportByDate(Certificatetaken certificatetaken) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		List<Certificatetaken> list = null;
		Criteria criteria = session.createCriteria(Certificatetaken.class);
		if (certificatetaken.getStartDate() != null && !(certificatetaken.getStartDate().isEmpty())) {
			Date startdate1 = null;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				startdate1 = sdf.parse(certificatetaken.getStartDate());
			} catch (Exception e) {
				e.printStackTrace();
			}
			criteria.add(Restrictions.ge("creatdedon", startdate1));
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("sadmissionno"))
					.add(Projections.groupProperty("studentname")) 
					.add(Projections.groupProperty("sbatchname"))
					.add(Projections.groupProperty("scertificatename"))
					.add(Projections.groupProperty("certificateno"))
					.add(Projections.groupProperty("creatdedon"))
					.add(Projections.groupProperty("returnedon"))
					.add(Projections.groupProperty("returnedby"))
					.add(Projections.groupProperty("status"))
					.add(Projections.count("documentid"), "count"));
			 criteria.addOrder(Order.asc("count"));
			 list = criteria.list();	
		}
		else if (certificatetaken.getEndDate() != null && !(certificatetaken.getEndDate().isEmpty())) {
			Date enddate1 = null;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				enddate1 = sdf.parse(certificatetaken.getEndDate());
			} catch (Exception exception) {
				logger.error("Exception raised in leadgeneratedreportdetails Method:", exception);
			}
			criteria.add(Restrictions.le("creatdedon", enddate1));
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("sadmissionno"))
					.add(Projections.groupProperty("studentname")) 
					.add(Projections.groupProperty("sbatchname"))
					.add(Projections.groupProperty("scertificatename"))
					.add(Projections.groupProperty("certificateno"))
					.add(Projections.groupProperty("creatdedon"))
					.add(Projections.groupProperty("returnedon"))
					.add(Projections.groupProperty("returnedby"))
					.add(Projections.groupProperty("status"))
					.add(Projections.count("documentid"), "count"));
			 criteria.addOrder(Order.asc("count"));
			 list = criteria.list();
		}
		else {
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("sadmissionno"))
					.add(Projections.groupProperty("studentname")) 
					.add(Projections.groupProperty("sbatchname"))
					.add(Projections.groupProperty("scertificatename"))
					.add(Projections.groupProperty("certificateno"))
					.add(Projections.groupProperty("creatdedon"))
					.add(Projections.groupProperty("returnedon"))
					.add(Projections.groupProperty("returnedby"))
					.add(Projections.groupProperty("status"))
					.add(Projections.count("documentid"), "count"));
			 criteria.addOrder(Order.asc("count"));
			 list = criteria.list();
			
		}
		transaction.commit();
		session.close();
		return list;
	}

	@Override
	public StudentAdmissionEntity getStudentDetail(Long admissionNumberId) {
		Session openSession = sessionFactory.openSession();
		StudentAdmissionEntity studentAdmission = null;
		try{
		 studentAdmission = (StudentAdmissionEntity)openSession.get(StudentAdmissionEntity.class, admissionNumberId);
		}catch(Exception exception){
			logger.error("Exception raised in getStudentDetailsByAdmissionId Method:", exception);
		}
		finally {
			openSession.close();
		}
		return studentAdmission;
	}

	@Override
	public BatchMaster getBatchName(long batchId) {
		Session openSession = sessionFactory.openSession();
		BatchMaster batchMaster = null; 
		try{
		batchMaster= (BatchMaster) openSession.get(BatchMaster.class,batchId);
		}catch(Exception exception){
			logger.error("Exception raised in getBatchName Method:", exception);
		}
		finally {
			openSession.close();
		}
		return batchMaster;
	}
}

	

