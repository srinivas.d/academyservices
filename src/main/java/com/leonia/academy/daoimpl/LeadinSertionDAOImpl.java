package com.leonia.academy.daoimpl;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.leonia.academy.entity.DropdownMasterCourseEntity;
import com.leonia.academy.entity.DropdownMasterSourceEntity;
import com.leonia.academy.entity.LeadFollowUpDTO;
import com.leonia.academy.entity.LeadfollowupdetailsDTO;
import com.leonia.academy.idao.ILeadinSertionDAO;

@Repository
public class LeadinSertionDAOImpl implements ILeadinSertionDAO {

	@Autowired
	private SessionFactory sessionFactory;
	private static final Logger logger = Logger.getLogger(LeadinSertionDAOImpl.class);

	// this Method is Lead saveing data Method
	@Override
	public void leadinsertion(LeadfollowupdetailsDTO leadfollowupdetailsDTO) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
		session.save(leadfollowupdetailsDTO.getLeadFollowUpDTO());
		}catch (Exception exception) {
			logger.error("Exception raised in leadinsertion Method:", exception);
		} finally {
			transaction.commit();
			session.close();
		}
	}

	// this Method is DropdownMasterSourceDTO  data
	@SuppressWarnings("unchecked")
	@Override
	public List<DropdownMasterSourceEntity> MasterSource(DropdownMasterSourceEntity dropdownMasterSourceDTO) {
		Session session = sessionFactory.openSession();
		List<DropdownMasterSourceEntity> dropdownMasterSourcelist = null;
		try {
			SQLQuery query=session.createSQLQuery("select descriptiontype from dropdownmastersource");
		    dropdownMasterSourcelist = query.list();
		}catch(Exception exception){
			logger.error("Exception raised in MasterSource Method:", exception);
		}finally {
			session.close();
		}
		return dropdownMasterSourcelist;
	}

	// this Method is DropdownMasterCourseDTO  data
	@SuppressWarnings("unchecked")
	@Override
	public List<DropdownMasterCourseEntity> MasterCourse(DropdownMasterCourseEntity dropdownMasterCourseDTO) {
		Session session = sessionFactory.openSession();
		List<DropdownMasterCourseEntity> dropdownMasterCourselist = null;
		try {
		Criteria criteria = session.createCriteria(DropdownMasterCourseEntity.class).addOrder(Order.asc("Id"));
		dropdownMasterCourselist = criteria.list();
		}catch(Exception exception){
			logger.error("Exception raised in MasterCourse Method:", exception);
		}finally {
			session.close();
		}
		return dropdownMasterCourselist;
	}

	@Override
	public String saveleaddetails(LeadFollowUpDTO leadFollowUpDTO) {
		String msg ="Successfully saved";
		Session session = sessionFactory.openSession();
		Transaction tx=session.beginTransaction();
		try {

			Serializable count=session.save(leadFollowUpDTO);
			
		}catch(Exception exception){
			logger.error("Exception raised in saveleaddetails Method:", exception);
		}finally {
			tx.commit();
			 session.close();
		}
		return msg;
	}

}