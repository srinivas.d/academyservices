package com.leonia.academy.daoimpl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.leonia.academy.entity.LeadFollowUpDTO;
import com.leonia.academy.entity.LoginEntity;
import com.leonia.academy.idao.IFollowupDetailsDAO;


@Repository
public class FollowupDetailsDAOImpl implements IFollowupDetailsDAO {

	@Autowired
	private SessionFactory sessionFactory;
	private static final Logger logger = Logger.getLogger(LeadGeneratedReportDAOImpl.class);
	
	@Override
	public List<LeadFollowUpDTO> getfollowupdetails(Integer leadid) {
		Session openSession = null;
		List<LeadFollowUpDTO> list = null;
		try{
			openSession = sessionFactory.openSession();
			Criteria criteria=openSession.createCriteria(LeadFollowUpDTO.class,"leadFollowUpDTO");
			//criteria.createAlias("leadFollowUpDTO.loginEntity", "loginuser");
			criteria.add(Restrictions.eq("leadFollowUpDTO.followUpId", leadid));
			criteria.setProjection(Projections.projectionList()
				    .add(Projections.groupProperty("leadFollowUpDTO.followUpId"))
				    .add(Projections.groupProperty("leadFollowUpDTO.source"))
				    .add(Projections.groupProperty("leadFollowUpDTO.studentName"))
				    .add(Projections.groupProperty("leadFollowUpDTO.email"))
				    .add(Projections.groupProperty("leadFollowUpDTO.contact"))
				    .add(Projections.groupProperty("leadFollowUpDTO.course"))
				    .add(Projections.groupProperty("leadFollowUpDTO.allocatedUserName"))
				    .add(Projections.groupProperty("leadFollowUpDTO.datefollowup"))
				    .add(Projections.groupProperty("leadFollowUpDTO.time"))
				    .add(Projections.groupProperty("leadFollowUpDTO.remarks")));
			list=criteria.list();
		}catch(Exception exception){
			logger.error("Exception raised in getBuildingDetails Method:", exception);
		}
		finally {
			openSession.close();
		}
		return list;
	}

	@Override
	public List<LoginEntity> getUsernameList() {
		Session session = sessionFactory.openSession();
		List<LoginEntity> loginEntity = null;
		try {
			SQLQuery query=session.createSQLQuery("select username from login");
			loginEntity = query.list();
		}catch(Exception exception){
			logger.error("Exception raised in MasterSource Method:", exception);
		}finally {
			session.close();
		}
		return loginEntity;
	}

	@Override
	public String saveNewFollowUp(LeadFollowUpDTO leadFollowUpDTO) {
		String msg ="Successfully saved";
		Session session = sessionFactory.openSession();
		LeadFollowUpDTO leadFollowUpDTO1=new LeadFollowUpDTO();
		Integer followUpId=leadFollowUpDTO.getFollowUpId();
		leadFollowUpDTO1.setFollowUpId(followUpId);	
		Date foloowupDate=leadFollowUpDTO.getDatefollowup();
		String time=leadFollowUpDTO.getTime();
		String reason=leadFollowUpDTO.getRemarks();
		String username=leadFollowUpDTO.getAllocatedUserName();
		
		Transaction tx=session.beginTransaction();
		try {
			/*NewFollowupDetails newFollowupDetails= new NewFollowupDetails();
			newFollowupDetails.setNewFollowupDate(foloowupDate);
			newFollowupDetails.setTime(time);
			newFollowupDetails.setReason(reason);
			newFollowupDetails.setAllocationManager(username);
			newFollowupDetails.setStatus("open");
			newFollowupDetails.setLeadFollowUpDTO(leadFollowUpDTO1);
			session.save(newFollowupDetails);*/
			Integer followupId=leadFollowUpDTO.getFollowUpId();
			SQLQuery query=session.createSQLQuery("update LeadFollowup set datefollowup='"+leadFollowUpDTO.getDatefollowup()+"' , time='"+leadFollowUpDTO.getTime()+"' , remarks='"+leadFollowUpDTO.getRemarks()+"' , allocatedUserName='"+leadFollowUpDTO.getAllocatedUserName()+"' where followUpId='"+leadFollowUpDTO.getFollowUpId()+"'");
			query.executeUpdate();
		}catch(Exception exception){
			logger.error("Exception raised in saveleaddetails Method:", exception);
		}finally {
			tx.commit();
			 session.close();
		}
		return msg;
	}

	@Override
	public List<LeadFollowUpDTO> getNewFollowUpDetails(LeadFollowUpDTO leadFollowUpDTO) {
		Session session = sessionFactory.openSession();
		List<LeadFollowUpDTO> list = null;
		Criteria criteria = session.createCriteria(LeadFollowUpDTO.class, "lead");
		if(leadFollowUpDTO.getEndDate() != null &&leadFollowUpDTO.getStartDate() != null )
		{
			Date enddate1 = null;
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
			try {
				enddate1 = sdf1.parse(leadFollowUpDTO.getEndDate());
			} catch (Exception exception) {
				logger.error("Exception raised in leadgeneratedreportdetails Method:", exception);
			}
			Date startdate2 = null;
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
			try {
				startdate2 = sdf2.parse(leadFollowUpDTO.getStartDate());
			} catch (Exception e) {
				e.printStackTrace();
			}
			criteria.add(Restrictions.ge("datefollowup", startdate2));
			criteria.add(Restrictions.le("datefollowup", enddate1));
			criteria.setProjection(Projections.projectionList()
				    .add(Projections.groupProperty("lead.followUpId"))
				    .add(Projections.groupProperty("lead.studentName"))
				    .add(Projections.groupProperty("lead.course"))
				    .add(Projections.groupProperty("lead.contact"))
				    .add(Projections.groupProperty("lead.email"))
				    .add(Projections.groupProperty("lead.datefollowup"))
				    .add(Projections.groupProperty("lead.time"))
				    .add(Projections.groupProperty("lead.remarks"))
				    .add(Projections.groupProperty("lead.allocatedUserName"))
				    .add(Projections.groupProperty("lead.allocatedUserName")));		
			 list = criteria.list();	
		}	
		if (leadFollowUpDTO.getStartDate() != null && !(leadFollowUpDTO.getStartDate().isEmpty())) {
			Date startdate1 = null;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				startdate1 = sdf.parse(leadFollowUpDTO.getStartDate());
			} catch (Exception e) {
				e.printStackTrace();
			}
			criteria.add(Restrictions.ge("datefollowup", startdate1));
			criteria.setProjection(Projections.projectionList()
				    .add(Projections.groupProperty("lead.followUpId"))
				    .add(Projections.groupProperty("lead.studentName"))
				    .add(Projections.groupProperty("lead.course"))
				    .add(Projections.groupProperty("lead.contact"))
				    .add(Projections.groupProperty("lead.email"))
				    .add(Projections.groupProperty("lead.datefollowup"))
				    .add(Projections.groupProperty("lead.time"))
				    .add(Projections.groupProperty("lead.remarks"))
				    .add(Projections.groupProperty("lead.allocatedUserName"))
				    .add(Projections.groupProperty("lead.allocatedUserName")));		
			 list = criteria.list();	
		}
		else if (leadFollowUpDTO.getEndDate() != null && !(leadFollowUpDTO.getEndDate().isEmpty())) {
			Date enddate1 = null;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				enddate1 = sdf.parse(leadFollowUpDTO.getEndDate());
			} catch (Exception exception) {
				logger.error("Exception raised in leadgeneratedreportdetails Method:", exception);
			}
			criteria.add(Restrictions.le("datefollowup", enddate1));
			criteria.setProjection(Projections.projectionList()
				    .add(Projections.groupProperty("lead.followUpId"))
				    .add(Projections.groupProperty("lead.studentName"))
				    .add(Projections.groupProperty("lead.course"))
				    .add(Projections.groupProperty("lead.contact"))
				    .add(Projections.groupProperty("lead.email"))
				    .add(Projections.groupProperty("lead.datefollowup"))
				    .add(Projections.groupProperty("lead.time"))
				    .add(Projections.groupProperty("lead.remarks"))
				    .add(Projections.groupProperty("lead.allocatedUserName"))
				    .add(Projections.groupProperty("lead.allocatedUserName")));				
			 list = criteria.list();
		}
		else {
			criteria.setProjection(Projections.projectionList()
				    .add(Projections.groupProperty("lead.followUpId"))
				    .add(Projections.groupProperty("lead.studentName"))
				    .add(Projections.groupProperty("lead.course"))
				    .add(Projections.groupProperty("lead.contact"))
				    .add(Projections.groupProperty("lead.email"))
				    .add(Projections.groupProperty("lead.datefollowup"))
				    .add(Projections.groupProperty("lead.time"))
				    .add(Projections.groupProperty("lead.remarks"))
				    .add(Projections.groupProperty("lead.allocatedUserName"))
				    .add(Projections.groupProperty("lead.allocatedUserName")));					
			 list = criteria.list();
			
		}
		session.close();
		return list;
	}

	@Override
	public List<LeadFollowUpDTO> getFollowUpDetails() {
		Session session = sessionFactory.openSession();
		List<LeadFollowUpDTO> list = null;
		try {
		Criteria criteria = session.createCriteria(LeadFollowUpDTO.class, "lead");
		criteria.setProjection(Projections.projectionList()
			    .add(Projections.groupProperty("lead.followUpId"))
			    .add(Projections.groupProperty("lead.studentName"))
			    .add(Projections.groupProperty("lead.course"))
			    .add(Projections.groupProperty("lead.contact"))
			    .add(Projections.groupProperty("lead.email"))
			    .add(Projections.groupProperty("lead.datefollowup"))
			    .add(Projections.groupProperty("lead.time"))
			    .add(Projections.groupProperty("lead.remarks"))
			    .add(Projections.groupProperty("lead.allocatedUserName"))
			    .add(Projections.groupProperty("lead.allocatedUserName")));					
		 list = criteria.list();
		}
		catch(Exception exception)
		{
			logger.error("Exception raised in saveleaddetails Method:", exception);
		}finally {
			 session.close();
		}
		return list;
	}

	@Override
	public List<LeadFollowUpDTO> getOpenLeadFoliowupDetails() {
		Session session = sessionFactory.openSession();
		List<LeadFollowUpDTO> list = null;
		try {
		Criteria criteria = session.createCriteria(LeadFollowUpDTO.class, "lead");
		criteria.add(Restrictions.eq("status", "open"));
		criteria.setProjection(Projections.projectionList()
			    .add(Projections.groupProperty("lead.followUpId"))
			    .add(Projections.groupProperty("lead.studentName"))
			    .add(Projections.groupProperty("lead.source"))
			    .add(Projections.groupProperty("lead.course"))
			    .add(Projections.groupProperty("lead.leadDate"))
			    .add(Projections.groupProperty("lead.datefollowup"))
			    .add(Projections.groupProperty("lead.time"))
			    .add(Projections.groupProperty("lead.allocatedUserName")));					
		 list = criteria.list();
		}
		catch(Exception exception)
		{
			logger.error("Exception raised in saveleaddetails Method:", exception);
		}finally {
			 session.close();
		}
		return list;
	}

	@Override
	public List<LeadFollowUpDTO> getFollowupDetailsByDate(LeadFollowUpDTO leadFollowUpDTO) {
		Session session = sessionFactory.openSession();
		List<LeadFollowUpDTO> list = null;
		Criteria criteria = session.createCriteria(LeadFollowUpDTO.class, "lead");
		if (leadFollowUpDTO.getStartDate() != null && !(leadFollowUpDTO.getStartDate().isEmpty())) {
			Date startdate1 = null;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				startdate1 = sdf.parse(leadFollowUpDTO.getStartDate());
			} catch (Exception e) {
				e.printStackTrace();
			}
			criteria.add(Restrictions.eq("status", "open"));
			criteria.add(Restrictions.ge("datefollowup", startdate1));
			criteria.setProjection(Projections.projectionList()
				    .add(Projections.groupProperty("lead.followUpId"))
				    .add(Projections.groupProperty("lead.studentName"))
				    .add(Projections.groupProperty("lead.source"))
				    .add(Projections.groupProperty("lead.course"))
				    .add(Projections.groupProperty("lead.leadDate"))
				    .add(Projections.groupProperty("lead.datefollowup"))
				    .add(Projections.groupProperty("lead.time"))
				    .add(Projections.groupProperty("lead.allocatedUserName")));					
			 list = criteria.list();	
		}
		else
		{
			criteria.add(Restrictions.eq("status", "open"));
			criteria.setProjection(Projections.projectionList()
				    .add(Projections.groupProperty("lead.followUpId"))
				    .add(Projections.groupProperty("lead.studentName"))
				    .add(Projections.groupProperty("lead.source"))
				    .add(Projections.groupProperty("lead.course"))
				    .add(Projections.groupProperty("lead.leadDate"))
				    .add(Projections.groupProperty("lead.datefollowup"))
				    .add(Projections.groupProperty("lead.time"))
				    .add(Projections.groupProperty("lead.allocatedUserName")));					
			 list = criteria.list();
			
		}
		session.close();
		return list;
}

	@Override
	public List<LeadFollowUpDTO> getFollowUpDetailsById(Integer followupId) {
		Session openSession = null;
		List<LeadFollowUpDTO> list = null;
		try{
			openSession = sessionFactory.openSession();
			Criteria criteria=openSession.createCriteria(LeadFollowUpDTO.class,"leadFollowUpDTO");
			//criteria.createAlias("leadFollowUpDTO.loginEntity", "loginuser");
			criteria.add(Restrictions.eq("leadFollowUpDTO.followUpId", followupId));
			criteria.setProjection(Projections.projectionList()
				    .add(Projections.groupProperty("leadFollowUpDTO.followUpId"))
				    .add(Projections.groupProperty("leadFollowUpDTO.studentName"))
				    .add(Projections.groupProperty("leadFollowUpDTO.course"))
				    .add(Projections.groupProperty("leadFollowUpDTO.contact"))
				    .add(Projections.groupProperty("leadFollowUpDTO.email"))
				    .add(Projections.groupProperty("leadFollowUpDTO.datefollowup"))
				    .add(Projections.groupProperty("leadFollowUpDTO.time"))
				    .add(Projections.groupProperty("leadFollowUpDTO.remarks"))
				    .add(Projections.groupProperty("leadFollowUpDTO.allocatedUserName"))
				    .add(Projections.groupProperty("leadFollowUpDTO.allocatedUserName")));
			list=criteria.list();
		}catch(Exception exception){
			logger.error("Exception raised in getBuildingDetails Method:", exception);
		}
		finally {
			openSession.close();
		}
		return list;
	}

	@Override
	public List endlead(LeadFollowUpDTO leadFollowUpDTO) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		List list=new ArrayList();
		list.add("succesfully udated");
		try {
			Integer followupId=leadFollowUpDTO.getFollowUpId();
			SQLQuery query=session.createSQLQuery("update LeadFollowup set closestatus='"+leadFollowUpDTO.getCloseStatus()+"' , remarks='"+leadFollowUpDTO.getRemarks()+"' , status='"+leadFollowUpDTO.getStatus()+"' where followUpId='"+leadFollowUpDTO.getFollowUpId()+"'");
			query.executeUpdate();
		}catch (Exception exception) {
			logger.error("Exception raised in leadinsertion Method:", exception);
		} finally {
			transaction.commit();
			session.close();
		}
		return list;
	}

	

	/*@Override
	public List<StudentAdmission> getStudents(StudentAdmission studentAdmission) {
		Session openSession = null;
		List list=null;
		try{
			openSession = sessionFactory.openSession();
			list=openSession.createSQLQuery("select s.admissionnumberid,s.studentfirstname,b.batchname from studentadmission s INNER JOIN batchmaster b on s.batchid=b.batchid;").list();
		}catch(Exception exception){
			logger.error("Exception raised in getStudents Method:", exception);
		}
		finally {
			openSession.close();
		}
		return list;
	}*/
}
