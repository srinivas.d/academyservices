package com.leonia.academy.daoimpl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.leonia.academy.entity.LoginEntity;
import com.leonia.academy.idao.ILoginDAO;

@Repository
public class LoginDAOImpl implements ILoginDAO{

	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public LoginEntity getlogindetailes(String username) {
		Session session = sessionFactory.openSession();
		LoginEntity logindto = null;
		Criteria criteria = session.createCriteria(LoginEntity.class)
				.add(Restrictions.eq("username", username));
		logindto = (LoginEntity) criteria.uniqueResult();
		return logindto;
	}

	@Override
	public List<LoginEntity> loginCheck(LoginEntity login) {
		Session session=sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		List<LoginEntity> loginList=session.createQuery("from LoginEntity e where e.username='" + login.getUsername() + "' and e.password='" + login.getPassword() + "'").list();
		// session.getTransaction().commit();
			return loginList ;
	}

}
