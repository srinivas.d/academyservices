package com.leonia.academy.isevice;

import java.util.List;

import com.leonia.academy.entity.CalenderEvents;
import com.leonia.academy.entity.CourseMaster;
import com.leonia.academy.entity.EducationEntity;
import com.leonia.academy.entity.ParentGuardianEntity;
import com.leonia.academy.entity.RelationMaster;
import com.leonia.academy.entity.StudentAdmissionEntity;

public interface IStudentManagementService {

	List getNationalityMaster();

	List bloodGroupMaster();

	List stayCategoryMaster();

	List discountCategoryMaster();

	List religionMaster();

	List communityMaster();

	List languageMaster();

	List cityMaster();

	List getStateCountryByCityId(int cityId);

	List courseMaster();

	List getBatchNameByCourseId(Long courseId);

	List getBatchFeeByBatchId(Long batchId);

	String saveStudentAdmissionDetails(StudentAdmissionEntity studentAdmissionEntity);

	List getviewstudentDetails();

	StudentAdmissionEntity editStudentAdmissionDetails(Long admissionNumberId);

	String updateStudentAdmissionDetails(StudentAdmissionEntity studentAdmissionEntity);

	Long getAdmissionNumberId();

	List feeCategoryMaster();

	List<RelationMaster> relationsList();

	List<ParentGuardianEntity> parentDetails(Long admissionNumberId);

	String saveorupdateParentGuardian(ParentGuardianEntity parentGuardianEntity);

	List geteventtypelist();

	List<CalenderEvents> getlistcalendereventdate();

	String insertcalendereventform(CalenderEvents calenderEvents);

	List calendereventdatelist();

	StudentAdmissionEntity getStudentDetailsByAdmissionId(Long admissionId);

	List getEducatioDetails(Long admissionId);

	String storeeducationdetails(EducationEntity educationEntity);

	List universityMaster();

	List<CourseMaster> getCourseByUniversity(Long universityMasterId);

	List<StudentAdmissionEntity> getStudentsByBatchForAssignInterviews(Long vacanyDetailsId, Long batchId);

}
