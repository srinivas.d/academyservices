package com.leonia.academy.isevice;

import java.util.List;

import com.leonia.academy.entity.LoginEntity;

public interface ILoginService {

	LoginEntity getlogindetailes(String username);

	List<LoginEntity> loginCheck(LoginEntity login);

}
