package com.leonia.academy.isevice;

import java.util.List;

import com.leonia.academy.entity.DropDownMasterEntity;
import com.leonia.academy.entity.DropDownSubMasterEntity;
import com.leonia.academy.entity.InstituteMasterEntity;
import com.leonia.academy.entity.VisitEntryEntity;

public interface IAcademyVisitsService {

	@SuppressWarnings("rawtypes")
	List<DropDownMasterEntity> getInstituteType();

	String saveInstituteMasterDetails(InstituteMasterEntity instituteMasterentity);

	List<InstituteMasterEntity> getInstituteMasterList(String status);

	@SuppressWarnings("rawtypes")
	List getContactPersons(Long instituteNameId);

	@SuppressWarnings("rawtypes")
	InstituteMasterEntity editInstituteMaster(int instituteId);

	@SuppressWarnings("rawtypes")
	List getContactInfo(int instituteId);

	String updateInstituteMasterDetails(InstituteMasterEntity instituteMasterentity);

	String saveVisitEntryDetails(VisitEntryEntity visitEntryEntity);
	
	String getInstituteTypeByInstituteTypeId(Integer instituteTypeId);

	String deactivateInstitute(InstituteMasterEntity entity);

	List<VisitEntryEntity> getvisitreport(VisitEntryEntity visitEntryEntity);

	List<VisitEntryEntity> getscheduledvisitreport(VisitEntryEntity visitEntryEntity);
	List<DropDownSubMasterEntity> getDropDownSumMasterList();

}
