package com.leonia.academy.isevice;

import java.util.List;

import com.leonia.academy.entity.CardTypeMaster;
import com.leonia.academy.entity.FeeAssignToStudents;
import com.leonia.academy.entity.FeeCreation;
import com.leonia.academy.entity.FeeGenerationBatchWise;
import com.leonia.academy.entity.HostelFeeAssingToStudent;
import com.leonia.academy.entity.PaymodeMaster;
import com.leonia.academy.entity.ReceiptGenerateDTO;
import com.leonia.academy.entity.StudentAdmissionEntity;

public interface IFeeManagementService {

	List<StudentAdmissionEntity> getStudentSearchByDiscountCategory(Long discountCategoryId, String searchName);

	List discountApprovedMaster();

	List studentFeediscountDetails(Long bacthId, Long admissionNumberId);

	List feeAssignToStudentsAmountParticularFee(Long feeAssignToStudentsId);

	List hostalfeeAssignToStudentsAmountParticularFee(Long hostelFeeAssingToStudentId);

	String discountStuParticularfee(FeeAssignToStudents objStudentCategory);

	String discountStuHostelFee(FeeAssignToStudents objStudentCategory);

	List<StudentAdmissionEntity> getStudentListSearch(String searchName);

	List<FeeAssignToStudents> getFeeGenerationdetails(Long admissionNumberId, Long bacthId);

	List<HostelFeeAssingToStudent> gethostelFeeGenerationdetails(Long admissionNumberId);

	List<PaymodeMaster> getPaymentModeTypes();

	List<CardTypeMaster> getCardTypeMaster();

	List getfeepaidreceptdetails(Long admissionNumberId);

	ReceiptGenerateDTO generateReceipt(Long receiptGenerateId, Long admissionNumberId);

	String feeassigntostudentPaymentupdate(List<FeeAssignToStudents> feesettlementdtolist);

	String hostelFeeAssignToStudentPaymentUpdate(HostelFeeAssingToStudent hostelFeeAssingToStudent);

	String savePaymentDetails(com.leonia.academy.entity.ReceiptGenerateDTO receiptgeneratedtoObj);

	List getFeeDetailsById(Integer batchId);

	List feeCategory();

	List getCategoryNameByBatchId(Integer batchId);

	String saveStudentFee(FeeCreation fee);

	List getInstallmentTypes(Long batchId);

	List getInstallmentTypesdrop(Long batchId);

	List<FeeCreation> getFeeCreationBatchWiseListByBatchId(Long batchId, Long installmentTypeMasterId);

	List getAmountByParticularFee(Long feeCreationId);

	String savefeeGenarationBatchWise(FeeGenerationBatchWise generationBatchWise);

	List getFeeGenarationFeeListInstallment123(Long batchId, Long feeGenerationBatchWiseId,
			Long installmentTypeMasterId);

	List getFeeGenarationFeeListInstallment(Long batchId, Long feeGenerationBatchWiseId);

	List getStudentsListByBatchId(Long batchId, Long feeGenerationBatchWiseId, Long installmentTypeMasterId);

	String feeAssignToStudentsPost(FeeAssignToStudents feeAssignToStudents);
}
