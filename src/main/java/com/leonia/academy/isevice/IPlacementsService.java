package com.leonia.academy.isevice;

import java.util.List;

import com.leonia.academy.entity.CompanyDetails;
import com.leonia.academy.entity.InterviewAssignToStudents;
import com.leonia.academy.entity.StatusMaster;
import com.leonia.academy.entity.VacancyDetails;
import com.leonia.academy.entity.VacancyInfo;

public interface IPlacementsService {

	List getDomainMaster();

	String saveCompanyDetails(CompanyDetails companyDetails);

	List getAllCompaniesList();

	CompanyDetails editCompanyDetailsByCompanyDetailsId(Long companyDetailsId);

	String updateCompanyDetails(CompanyDetails companyDetails);

	String saveVacancyDetails(VacancyInfo vacancyInfo);

	List getDepartmentMaster();

	List getDesignationIdByDepartmentId(Long departmentId);

	List getAllVacanciesList();

	VacancyInfo editVacancyInfoByVacancyInfoId(Long vacancyInfoId);

	String updateVacancyDetails(VacancyInfo vacancyInfo);

	List getDesignationMaster();

	List<VacancyDetails> viewvacancyDetailsGrid();

	List<VacancyDetails> getVacancyDetailsById(Long vacanyDetailsId);

	String interviewAssignedToStudents(Long vacanyDetailsId, String admissionNumberId);

	List<VacancyInfo> getVacancyDetailsByCompanyDetailsId(Long companyDetailsId);

	List<VacancyDetails> getDepartmentsByVacancyInfoId(Long vacancyInfoId);

	List getStudentsByVacancyDetailsId(Long vacanyDetailsId);

	String removeStudentsByVacancyDetailsId(Long vacanyDetailsId, String admissionNumberId);

	List<StatusMaster> getStatusMaster();

	String saveDeclaredResults(InterviewAssignToStudents interviewAssignToStudents);

	List getStudentsByVacancyDetailsIdFromAttendedInterViews(Long vacanyDetailsId);

	String closingInterviewVacancyByVacancyInfoId(Long vacancyDetailsId, Long vacancyInfoId);



}
