package com.leonia.academy.isevice;

import java.util.List;

import com.leonia.academy.entity.ActiveDTO;
import com.leonia.academy.entity.LeadFollowUpDTO;

public interface ILeadGeneratedReportService {

	List<ActiveDTO> activelist();

	List<LeadFollowUpDTO> getLeadDetails(LeadFollowUpDTO leadFollowUpDTO);

	//List<LeadFollowUpDTO> searchLeadList(String sdate, String edate);

	List<LeadFollowUpDTO> searchLeadList(LeadFollowUpDTO leadFollowUpDTO);


}
