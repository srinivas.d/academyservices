package com.leonia.academy.entity;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Table
@Entity
public class FeeCreation {
	@Id
	@GeneratedValue
	private Long feeCreationId;
	@Column
	private Long universityId;
	@Column
	private Long courseId;
	@OneToMany(mappedBy="feeCreation")
	private  List<FeeGenerationBatchWise> feeGenerationBatchWise ; 
	
	@Column
	private BigDecimal feeAmount;
	
	private Boolean action;
	@ManyToOne
	@JoinColumn(name="feesCategoryId")
	private FeesCategory feesCategory;
	@ManyToOne
	@JoinColumn(name="batchId")
	private BatchMaster batchMaster;
	@Transient
	private BigDecimal totalAmount;
	
	public List<FeeGenerationBatchWise> getFeeGenerationBatchWise() {
		return feeGenerationBatchWise;
	}
	public void setFeeGenerationBatchWise(List<FeeGenerationBatchWise> feeGenerationBatchWise) {
		this.feeGenerationBatchWise = feeGenerationBatchWise;
	}
	public Long getFeeCreationId() {
		return feeCreationId;
	}
	public void setFeeCreationId(Long feeCreationId) {
		this.feeCreationId = feeCreationId;
	}
	
	public Long getUniversityId() {
		return universityId;
	}
	public void setUniversityId(Long universityId) {
		this.universityId = universityId;
	}
	public Long getCourseId() {
		return courseId;
	}
	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}
	
	public BigDecimal getFeeAmount() {
		return feeAmount;
	}
	public void setFeeAmount(BigDecimal feeAmount) {
		this.feeAmount = feeAmount;
	}
	public Boolean getAction() {
		return action;
	}
	public void setAction(Boolean action) {
		this.action = action;
	}
	public FeesCategory getFeesCategory() {
		return feesCategory;
	}
	public void setFeesCategory(FeesCategory feesCategory) {
		this.feesCategory = feesCategory;
	}
	public BatchMaster getBatchMaster() {
		return batchMaster;
	}
	public void setBatchMaster(BatchMaster batchMaster) {
		this.batchMaster = batchMaster;
	}
	
}