package com.leonia.academy.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="roomdeallocationmaster")
public class Roomdeallocationmaster {
	@Id
	@SequenceGenerator(name = "roomdeallocationrSEQ", sequenceName = "roomdeallocationid_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="roomdeallocationrSEQ")
	@Column(name="roomdeallocationid")
	private Integer roomdeallocationid;
	@Column
	private String studentname;
	@Column
	private Long admissionid;
	@Column 
	private String course;
	@Column
	private String batchname;
	@Column
	private String status;
	@Column
	private String itempresent;
	@Column
	private Integer fine;
	@Column
	private Date deallotedon;
	@Column
	private String dealocatedetails;
	public String getDealocatedetails() {
		return dealocatedetails;
	}
	public void setDealocatedetails(String dealocatedetails) {
		this.dealocatedetails = dealocatedetails;
	}
	public Date getDeallotedon() {
		return deallotedon;
	}
	public void setDeallotedon(Date deallotedon) {
		this.deallotedon = deallotedon;
	}
	public String getItempresent() {
		return itempresent;
	}
	public void setItempresent(String itempresent) {
		this.itempresent = itempresent;
	}
	public Integer getFine() {
		return fine;
	}
	public void setFine(Integer fine) {
		this.fine = fine;
	}
	public Integer getRoomdeallocationid() {
		return roomdeallocationid;
	}
	public void setRoomdeallocationid(Integer roomdeallocationid) {
		this.roomdeallocationid = roomdeallocationid;
	}
	public String getStudentname() {
		return studentname;
	}
	public void setStudentname(String studentname) {
		this.studentname = studentname;
	}
	public Long getAdmissionid() {
		return admissionid;
	}
	public void setAdmissionid(Long admissionid) {
		this.admissionid = admissionid;
	}
	public String getCourse() {
		return course;
	}
	public void setCourse(String course) {
		this.course = course;
	}
	public String getBatchname() {
		return batchname;
	}
	public void setBatchname(String batchname) {
		this.batchname = batchname;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
