package com.leonia.academy.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "LeadFollowup")
public class LeadFollowUpDTO {

	@Id
	@GeneratedValue
	private Integer followUpId;
	@Column
	private String studentName;
	@Column
	private String phoneNumber;
	@Column
	private String email;
	@Column
	private String course;
	
	@Column
	private String source;
	@Column
	@Temporal(TemporalType.DATE)
	private Date datefollowup;
	@Column
	private String time;
	@Column
	@Temporal(TemporalType.DATE)
	private Date leadDate;
	@Column
	private String parentsName;
	@Column
	private String contact;
	@Column
	@Temporal(TemporalType.DATE)
	private Date dob;
	@Column
	private String status;
	@Column
	private String remarks;
	@Column
	private int active;
	@Column
	private String allocmgr;
	@Column
	private String allocatedUserName;
	@Column
	private String closeStatus;
	
	@Transient
	private String startDate;
	@Transient
	private String endDate;
	
	/*@ManyToOne
	@JoinColumn(name="uesrId")
	private LoginEntity loginEntity;
*/
	
	public String getAllocmgr() {
		return allocmgr;
	}

	public String getAllocatedUserName() {
		return allocatedUserName;
	}

	public void setAllocatedUserName(String allocatedUserName) {
		this.allocatedUserName = allocatedUserName;
	}

	public void setAllocmgr(String allocmgr) {
		this.allocmgr = allocmgr;
	}

	/*public LoginEntity getLoginEntity() {
		return loginEntity;
	}

	public void setLoginDTO(LoginEntity loginEntity) {
		this.loginEntity = loginEntity;
	}*/

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "leadFollowUpDTO")
	private Set<NewFollowupDetails> newFollowupDetails;

	public Set<NewFollowupDetails> getLeadfollowupdetailsDTO() {
		return newFollowupDetails;
	}

	public void setLeadfollowupdetailsDTO(Set<NewFollowupDetails> leadfollowupdetailsDTO) {
		this.newFollowupDetails = newFollowupDetails;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Date getLeadDate() {
		return leadDate;
	}

	public void setLeadDate(Date leadDate) {
		this.leadDate = leadDate;
	}

	public String getParentsName() {
		return parentsName;
	}

	public void setParentsName(String parentsName) {
		this.parentsName = parentsName;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Integer getFollowUpId() {
		return followUpId;
	}

	public void setFollowUpId(Integer followUpId) {
		this.followUpId = followUpId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Date getDatefollowup() {
		return datefollowup;
	}

	public void setDatefollowup(Date datefollowup) {
		this.datefollowup = datefollowup;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getCloseStatus() {
		return closeStatus;
	}

	public void setCloseStatus(String closeStatus) {
		this.closeStatus = closeStatus;
	}

	public Set<NewFollowupDetails> getNewFollowupDetails() {
		return newFollowupDetails;
	}

	public void setNewFollowupDetails(Set<NewFollowupDetails> newFollowupDetails) {
		this.newFollowupDetails = newFollowupDetails;
	}

	
}