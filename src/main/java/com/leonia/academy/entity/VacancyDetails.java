package com.leonia.academy.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name="leoacademy_leo_companyvacancydetails")
public class VacancyDetails {
	
	@Id
	@SequenceGenerator(name="seq",sequenceName="vacanyDetailsId",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
	private Long vacanyDetailsId;
	private Long designationId;
	private Long departmentId;
	private String noOfVacancies;
	private String locationOfWork;
	private String requirement;
	private String additionalInfo;
	private Boolean status;
	@Transient
	private String departmentName;
	@Transient
	private String designationName;
	@Transient
	private String companyName;
	@Transient
	private Integer assignedStudentsCount;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="vacancyInfoId")
	private VacancyInfo vacancyInfo;
	
	/*@OneToMany(mappedBy="vacancyDetails")
	private List<InterviewAssignToStudents> interviewAssignToStudents;*/
	
	/*@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="companyDetailsId")
	private CompanyDetails companyDetails;*/
	
	public Long getVacanyDetailsId() {
		return vacanyDetailsId;
	}
	public void setVacanyDetailsId(Long vacanyDetailsId) {
		this.vacanyDetailsId = vacanyDetailsId;
	}
	
	public Long getDesignationId() {
		return designationId;
	}
	public void setDesignationId(Long designationId) {
		this.designationId = designationId;
	}
	public Long getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}
	public String getNoOfVacancies() {
		return noOfVacancies;
	}
	public void setNoOfVacancies(String noOfVacancies) {
		this.noOfVacancies = noOfVacancies;
	}
	public String getLocationOfWork() {
		return locationOfWork;
	}
	public void setLocationOfWork(String locationOfWork) {
		this.locationOfWork = locationOfWork;
	}
	public String getRequirement() {
		return requirement;
	}
	public void setRequirement(String requirement) {
		this.requirement = requirement;
	}
	public String getAdditionalInfo() {
		return additionalInfo;
	}
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	/*public CompanyDetails getCompanyDetails() {
		return companyDetails;
	}
	public void setCompanyDetails(CompanyDetails companyDetails) {
		this.companyDetails = companyDetails;
	}*/
	public VacancyInfo getVacancyInfo() {
		return vacancyInfo;
	}
	public void setVacancyInfo(VacancyInfo vacancyInfo) {
		this.vacancyInfo = vacancyInfo;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public String getDesignationName() {
		return designationName;
	}
	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	/*public List<InterviewAssignToStudents> getInterviewAssignToStudents() {
		return interviewAssignToStudents;
	}
	public void setInterviewAssignToStudents(List<InterviewAssignToStudents> interviewAssignToStudents) {
		this.interviewAssignToStudents = interviewAssignToStudents;
	}*/
	public Integer getAssignedStudentsCount() {
		return assignedStudentsCount;
	}
	public void setAssignedStudentsCount(Integer assignedStudentsCount) {
		this.assignedStudentsCount = assignedStudentsCount;
	}
	
	

	
}
