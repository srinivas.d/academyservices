package com.leonia.academy.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.FetchMode;

@Entity
@Table(name="citymaster")
public class CityMaster {
	
	@Id
	@SequenceGenerator(name = "citySEQ", sequenceName = "city_id_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="citySEQ")
	@Column(name="city_id")
	private int cityid;
    @Column(name="city_name")
    private String cityName;
    
    @Column
	private Boolean action;
    
    
    @Column(name="city_status")
    private boolean cityStatus;
    
    @ManyToOne
	@JoinColumn(name="stateId")
	private StateMaster stateMaster;
    
	public int getCityid() {
		return cityid;
	}
	public void setCityid(int cityid) {
		this.cityid = cityid;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public boolean isCityStatus() {
		return cityStatus;
	}
	public void setCityStatus(boolean cityStatus) {
		this.cityStatus = cityStatus;
	}
	
	public StateMaster getStateMaster() {
		return stateMaster;
	}
	public void setStateMaster(StateMaster stateMaster) {
		this.stateMaster = stateMaster;
	}
	public Boolean getAction() {
		return action;
	}
	public void setAction(Boolean action) {
		this.action = action;
	}
	
}
