package com.leonia.academy.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="leoacademy_tbl_feeSettlement")
public class FeeSettlementDTO {
	@Id
	@GeneratedValue
	private Long feeSettlementId;
	@Column
	private BigDecimal paidAmount;
	@Column
	private Long admissionNumberId;
	@Column
	private Long batchId;
	@Column
	@Temporal(TemporalType.DATE)
    private Date paidDate;
	@Column
    private String creditCardName;
	@Column
    private Long creditCardNumber;
	@Column
    private String creditCardType;
	@Column
    private String debitCardName;
	@Column
    private Long debitCardNumber;
	@Column
    private String debitCardType;
	@Column
    private String bankName;
	@Column
    private String chequePayee;
	@Column
	@Temporal(TemporalType.DATE)
    private Date chequeDate;
	@Column
    private Long chequeNumber;
	
	@ManyToOne
	@JoinColumn(name="paymodeId")
	private PaymodeMaster paymodeMaster;
	
	@ManyToOne
	@JoinColumn(name="receiptGenerateId")
	private ReceiptGenerateDTO receiptGenerateDTO;

	public PaymodeMaster getPaymodeMaster() {
		return paymodeMaster;
	}
	public void setPaymodeMaster(PaymodeMaster paymodeMaster) {
		this.paymodeMaster = paymodeMaster;
	}
	public String getCreditCardName() {
		return creditCardName;
	}
	public void setCreditCardName(String creditCardName) {
		this.creditCardName = creditCardName;
	}
	public Long getCreditCardNumber() {
		return creditCardNumber;
	}
	public void setCreditCardNumber(Long creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}
	public String getCreditCardType() {
		return creditCardType;
	}
	public void setCreditCardType(String creditCardType) {
		this.creditCardType = creditCardType;
	}
	public String getDebitCardName() {
		return debitCardName;
	}
	public void setDebitCardName(String debitCardName) {
		this.debitCardName = debitCardName;
	}
	public Long getDebitCardNumber() {
		return debitCardNumber;
	}
	public void setDebitCardNumber(Long debitCardNumber) {
		this.debitCardNumber = debitCardNumber;
	}
	public String getDebitCardType() {
		return debitCardType;
	}
	public void setDebitCardType(String debitCardType) {
		this.debitCardType = debitCardType;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getChequePayee() {
		return chequePayee;
	}
	public void setChequePayee(String chequePayee) {
		this.chequePayee = chequePayee;
	}
	public Date getChequeDate() {
		return chequeDate;
	}
	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}
	public Long getChequeNumber() {
		return chequeNumber;
	}
	public void setChequeNumber(Long chequeNumber) {
		this.chequeNumber = chequeNumber;
	}
	public Date getPaidDate() {
		return paidDate;
	}
	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}
	public ReceiptGenerateDTO getReceiptGenerateDTO() {
		return receiptGenerateDTO;
	}
	public void setReceiptGenerateDTO(ReceiptGenerateDTO receiptGenerateDTO) {
		this.receiptGenerateDTO = receiptGenerateDTO;
	}
	public Long getFeeSettlementId() {
		return feeSettlementId;
	}
	public void setFeeSettlementId(Long feeSettlementId) {
		this.feeSettlementId = feeSettlementId;
	}
	public BigDecimal getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}
	public Long getAdmissionNumberId() {
		return admissionNumberId;
	}
	public void setAdmissionNumberId(Long admissionNumberId) {
		this.admissionNumberId = admissionNumberId;
	}
	public Long getBatchId() {
		return batchId;
	}
	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}
	
}
