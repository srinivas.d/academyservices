package com.leonia.academy.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="floormaster")
public class Floormaster {
@Id
@SequenceGenerator(name = "floormasterSEQ", sequenceName = "floorid_seq", allocationSize = 1)
@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="floormasterSEQ")
@Column(name="floor_id")
private Integer floorId;
@Column
private String floorName;
@Column
private Date createdOn;
@Column
private boolean status;
@OneToMany(cascade=CascadeType.ALL,mappedBy="floormaster")
private List<Roommaster> roommaster;
@ManyToOne(fetch=FetchType.LAZY)
@JoinColumn(name="blockId")
private Blockmaster blockmaster;

public Integer getFloorId() {
	return floorId;
}
public void setFloorId(Integer floorId) {
	this.floorId = floorId;
}
public String getFloorName() {
	return floorName;
}
public void setFloorName(String floorName) {
	this.floorName = floorName;
}
public Date getCreatedOn() {
	return createdOn;
}
public void setCreatedOn(Date createdOn) {
	this.createdOn = createdOn;
}
public boolean isStatus() {
	return status;
}
public void setStatus(boolean status) {
	this.status = status;
}
public List<Roommaster> getRoommaster() {
	return roommaster;
}
public void setRoommaster(List<Roommaster> roommaster) {
	this.roommaster = roommaster;
}
public Blockmaster getBlockmaster() {
	return blockmaster;
}
public void setBlockmaster(Blockmaster blockmaster) {
	this.blockmaster = blockmaster;
}

}
