package com.leonia.academy.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name="leoacademy_tbl_installmenttypes")
public class InstallmentTypes {
	@GeneratedValue
	@Id
	private Long installmentTypesId;
	@Column
	private String installmentTypeName;
	@Column
	private Boolean action;
	
	
	@OneToMany(mappedBy="installmentTypes")
	private List<InstallmentTypeMaster>  installmentTypeMaster;
	
	public Long getInstallmentTypesId() {
		return installmentTypesId;
	}
	public void setInstallmentTypesId(Long installmentTypesId) {
		this.installmentTypesId = installmentTypesId;
	}
	public String getInstallmentTypeName() {
		return installmentTypeName;
	}
	public void setInstallmentTypeName(String installmentTypeName) {
		this.installmentTypeName = installmentTypeName;
	}
	public Boolean getAction() {
		return action;
	}
	public void setAction(Boolean action) {
		this.action = action;
	}
	public List<InstallmentTypeMaster> getInstallmentTypeMaster() {
		return installmentTypeMaster;
	}
	public void setInstallmentTypeMaster(List<InstallmentTypeMaster> installmentTypeMaster) {
		this.installmentTypeMaster = installmentTypeMaster;
	}
	
	
}
