package com.leonia.academy.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="roommaster")
public class Roommaster {
	@Id
	@SequenceGenerator(name = "roommasterSEQ", sequenceName = "roomid_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="roommasterSEQ")
	@Column(name="room_id",length=100)
	private Integer roomId;
	@Column
	private String roomNumber;
	@Column
	private String roomCode;
	@Column
	private int capacity;
	@Column
	private Date createdOn;
	@Column
	private String items;
	@Column
	private Integer countoccupancy;
	@Column
	private boolean status;
	
	@OneToMany(cascade=CascadeType.ALL,mappedBy="roommaster")
	private List<Bedmaster> bedmaster;
	@ManyToOne
	@JoinColumn(name="floorId")
	private Floormaster floormaster;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="categoryid")
	private RoomCategory roomCategory;

	public Integer getRoomId() {
		return roomId;
	}

	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public String getRoomCode() {
		return roomCode;
	}

	public void setRoomCode(String roomCode) {
		this.roomCode = roomCode;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getItems() {
		return items;
	}

	public void setItems(String items) {
		this.items = items;
	}

	public Integer getCountoccupancy() {
		return countoccupancy;
	}

	public void setCountoccupancy(Integer countoccupancy) {
		this.countoccupancy = countoccupancy;
	}

	

	public RoomCategory getRoomCategory() {
		return roomCategory;
	}

	public void setRoomCategory(RoomCategory roomCategory) {
		this.roomCategory = roomCategory;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public List<Bedmaster> getBedmaster() {
		return bedmaster;
	}

	public void setBedmaster(List<Bedmaster> bedmaster) {
		this.bedmaster = bedmaster;
	}

	public Floormaster getFloormaster() {
		return floormaster;
	}

	public void setFloormaster(Floormaster floormaster) {
		this.floormaster = floormaster;
	}

	

}
