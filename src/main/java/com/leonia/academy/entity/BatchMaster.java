package com.leonia.academy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table
public class BatchMaster {
	
	@Id
	@GeneratedValue
	private Long batchId;
	
	/*@OneToMany(mappedBy="batchMaster")
	private List<FeeCreation> feeCreation;*/
	
	@Column
	private String batchName;
	
	@Column
	private String year;
	
	@Column
	private Long fees;
	
	@Column
	private String numOfStudents;
	
	/*@OneToMany(mappedBy="batchMaster")
	private List<InstallmentTypeMaster>  installmentTypeMaster;*/
	
	/*@OneToMany(cascade = CascadeType.ALL, mappedBy ="batchMaster")
	private List<CourseMaster> courseMaster; */
	
	/*@OneToMany(mappedBy="batchMaster")
	private List<StudentAdmission> studentAdmission;*/
	
	
	@ManyToOne
	@JoinColumn(name ="courseId")
	private CourseMaster courseMaster;
	
	
	/*public List<StudentAdmission> getStudentAdmission() {
		return studentAdmission;
	}

	public void setStudentAdmission(List<StudentAdmission> studentAdmission) {
		this.studentAdmission = studentAdmission;
	}*/
	
	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getNumOfStudents() {
		return numOfStudents;
	}

	public void setNumOfStudents(String numOfStudents) {
		this.numOfStudents = numOfStudents;
	}

	public CourseMaster getCourseMaster() {
		return courseMaster;
	}

	public void setCourseMaster(CourseMaster courseMaster) {
		this.courseMaster = courseMaster;
	}

	@Column
	private Boolean action;

	public Long getBatchId() {
		return batchId;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	public String getBatchName() {
		return batchName;
	}

	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}

	public Boolean getAction() {
		return action;
	}

	public void setAction(Boolean action) {
		this.action = action;
	}

	public Long getFees() {
		return fees;
	}

	public void setFees(Long fees) {
		this.fees = fees;
	}

	/*public List<FeeCreation> getFeeCreation() {
		return feeCreation;
	}

	public void setFeeCreation(List<FeeCreation> feeCreation) {
		this.feeCreation = feeCreation;
	}

	public List<InstallmentTypeMaster> getInstallmentTypeMaster() {
		return installmentTypeMaster;
	}

	public void setInstallmentTypeMaster(List<InstallmentTypeMaster> installmentTypeMaster) {
		this.installmentTypeMaster = installmentTypeMaster;
	}*/
	
	
}
