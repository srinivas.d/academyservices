package com.leonia.academy.entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="leoacademy_tbl_active")
public class ActiveDTO {
	@Id
	private int activeid;
	@Column
	private String description;
	
	public int getActiveid() {
		return activeid;
	}
	public void setActiveid(int activeid) {
		this.activeid = activeid;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
