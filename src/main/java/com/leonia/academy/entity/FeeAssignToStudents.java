package com.leonia.academy.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table
public class FeeAssignToStudents {

	@Id
	@GeneratedValue
	private Long feeAssignToStudentsId;
	/*@Column
	private Long studentAdmissionNumberId;*/
	@Column
	private Long batchId;
	@Column
	private Long feeGenerationBatchWiseId;
	@Column
	private Integer genarateAssignId;
	@Column
	private BigDecimal totalAmount;
	@Column
	private BigDecimal paidAmount;
	@Column
	private BigDecimal dueAmount;
	@Column
	@Temporal(TemporalType.DATE)
	private Date paidDate;
	@Column
	@Temporal(TemporalType.DATE)
	private Date dueDate;
	@Column
	private Boolean action;
	@Column
	private BigDecimal discountAmount;
	/*@Column
	private Long discountCatId;*/
	@Column
	private String termWiseMessage;
	@Column
	private BigDecimal termWiseAmount;
	@Column
	private Long feeCreationId;
	/*@Column
	private BigDecimal balance;*/
	
	@Column
	private BigDecimal netAmount;
	
	@Column
	private String remarks;
	
	@ManyToOne
	@JoinColumn(name="admissionNumberId")
	private StudentAdmissionEntity studentAdmissionEntity;
	
	@ManyToOne
	@JoinColumn(name="installmentTypeMasterId")
	private InstallmentTypeMaster installmentTypeMaster;
	
	@ManyToOne
	@JoinColumn(name="discountApprovedmasterId")
	private DiscountApprovedMaster discountApprovedMaster;

	@ManyToOne
	@JoinColumn(name="discountCategoryId")
	private DiscountCategoryMaster discountCategoryMaster;
	
	public InstallmentTypeMaster getInstallmentTypeMaster() {
		return installmentTypeMaster;
	}
	public void setInstallmentTypeMaster(InstallmentTypeMaster installmentTypeMaster) {
		this.installmentTypeMaster = installmentTypeMaster;
	}
	public Long getFeeAssignToStudentsId() {
		return feeAssignToStudentsId;
	}
	public void setFeeAssignToStudentsId(Long feeAssignToStudentsId) {
		this.feeAssignToStudentsId = feeAssignToStudentsId;
	}

	public Long getBatchId() {
		return batchId;
	}
	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}
	public Long getFeeGenerationBatchWiseId() {
		return feeGenerationBatchWiseId;
	}
	public void setFeeGenerationBatchWiseId(Long feeGenerationBatchWiseId) {
		this.feeGenerationBatchWiseId = feeGenerationBatchWiseId;
	}
	public Integer getGenarateAssignId() {
		return genarateAssignId;
	}
	public void setGenarateAssignId(Integer genarateAssignId) {
		this.genarateAssignId = genarateAssignId;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	public BigDecimal getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}
	public BigDecimal getDueAmount() {
		return dueAmount;
	}
	public void setDueAmount(BigDecimal dueAmount) {
		this.dueAmount = dueAmount;
	}
	public Date getPaidDate() {
		return paidDate;
	}
	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public Boolean getAction() {
		return action;
	}
	public void setAction(Boolean action) {
		this.action = action;
	}

	public StudentAdmissionEntity getStudentAdmissionEntity() {
		return studentAdmissionEntity;
	}
	public void setStudentAdmissionEntity(StudentAdmissionEntity studentAdmissionEntity) {
		this.studentAdmissionEntity = studentAdmissionEntity;
	}
	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public String getTermWiseMessage() {
		return termWiseMessage;
	}
	public void setTermWiseMessage(String termWiseMessage) {
		this.termWiseMessage = termWiseMessage;
	}
	public BigDecimal getTermWiseAmount() {
		return termWiseAmount;
	}
	public void setTermWiseAmount(BigDecimal termWiseAmount) {
		this.termWiseAmount = termWiseAmount;
	}
	public Long getFeeCreationId() {
		return feeCreationId;
	}
	public void setFeeCreationId(Long feeCreationId) {
		this.feeCreationId = feeCreationId;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public DiscountApprovedMaster getDiscountApprovedMaster() {
		return discountApprovedMaster;
	}
	public void setDiscountApprovedMaster(DiscountApprovedMaster discountApprovedMaster) {
		this.discountApprovedMaster = discountApprovedMaster;
	}
	public DiscountCategoryMaster getDiscountCategoryMaster() {
		return discountCategoryMaster;
	}
	public void setDiscountCategoryMaster(DiscountCategoryMaster discountCategoryMaster) {
		this.discountCategoryMaster = discountCategoryMaster;
	}
	public BigDecimal getNetAmount() {
		return netAmount;
	}
	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	
	
		
}
