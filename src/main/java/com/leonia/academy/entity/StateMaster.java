package com.leonia.academy.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="statemaster")
public class StateMaster {
	@Id
	@SequenceGenerator(name = "stateSEQ", sequenceName = "state_id_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="stateSEQ")
	@Column(name="state_id")
	private int stateId;
	@Column(name="state_name")
	private String stateName;
	@Column(name="state_status")
	private boolean stateStatus;
	@Column
	private Boolean action;
	
	@ManyToOne
	@JoinColumn(name="countryId")
	private CountryMaster countryMaster;
	@OneToMany(cascade = CascadeType.ALL,mappedBy="stateMaster")
	private List<CityMaster> cityMaster;
	public int getStateId() {
		return stateId;
	}
	public void setStateId(int stateId) {
		this.stateId = stateId;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public boolean isStateStatus() {
		return stateStatus;
	}
	public void setStateStatus(boolean stateStatus) {
		this.stateStatus = stateStatus;
	}
	
	public Boolean getAction() {
		return action;
	}
	public void setAction(Boolean action) {
		this.action = action;
	}
	public CountryMaster getCountryMaster() {
		return countryMaster;
	}
	public void setCountryMaster(CountryMaster countryMaster) {
		this.countryMaster = countryMaster;
	}
	public List<CityMaster> getCityMaster() {
		return cityMaster;
	}
	public void setCityMaster(List<CityMaster> cityMaster) {
		this.cityMaster = cityMaster;
	}
	
	
	
}
