package com.leonia.academy.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="leoacademy_tbl_calenderevents")
public class CalenderEvents {
	
	@Id
	@GeneratedValue
	private Long calenderId;
	@Column
	private String title;
	@Temporal(TemporalType.TIMESTAMP.DATE)
	private Date startDate;
	@Temporal(TemporalType.TIMESTAMP.DATE)
	private Date endDate;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="eventid")
	private Eventtype eventtype;
	
	public Long getCalenderId() {
		return calenderId;
	}
	public void setCalenderId(Long calenderId) {
		this.calenderId = calenderId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Eventtype getEventtype() {
		return eventtype;
	}
	public void setEventtype(Eventtype eventtype) {
		this.eventtype = eventtype;
	}
	
}
