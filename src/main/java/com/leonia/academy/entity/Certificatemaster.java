package com.leonia.academy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="certificatemaster")
public class Certificatemaster {
	@Id
	@Column(name="cert_id")
	private Integer certificateid;
	@Column
	private String certificatename;
	public Integer getCertificateid() {
		return certificateid;
	}
	public void setCertificateid(Integer certificateid) {
		this.certificateid = certificateid;
	}
	public String getCertificatename() {
		return certificatename;
	}
	public void setCertificatename(String certificatename) {
		this.certificatename = certificatename;
	}
	
	

}
