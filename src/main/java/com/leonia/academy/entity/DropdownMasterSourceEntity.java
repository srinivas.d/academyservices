package com.leonia.academy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="dropdownmastersource")
public class DropdownMasterSourceEntity {

	@Id
	@GeneratedValue
	private Integer sourceid;
	@Column
	private String description;
	@Column
	private String descriptiontype;
	
	public Integer getSourceid() {
		return sourceid;
	}
	public void setSourceid(Integer sourceid) {
		this.sourceid = sourceid;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDescriptiontype() {
		return descriptiontype;
	}
	public void setDescriptiontype(String descriptiontype) {
		this.descriptiontype = descriptiontype;
	}
	
	

}
