package com.leonia.academy.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="leoacademy_tbl_installmenthostelfee")
public class InstallmentHostelFee {
	
	@Id
	@SequenceGenerator(name="leoacademy_tbl_installmenthostelfee",sequenceName="installmentHostelFeeId",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="leoacademy_tbl_installmenthostelfee")
	private Long installmentHostelFeeId;
	
	@Column
	private BigDecimal hostelinstallAmount;
	
	@Column
	@Temporal(TemporalType.DATE)
	private Date dueDate;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="categoryid")
	private RoomCategory roomCategory;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="buildingId")
	private Buildingmaster buildingmaster;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="instid")
	private InstallmentMaster installmentMaster;
	
	@Column
	private Boolean action;

	public Boolean getAction() {
		return action;
	}

	public void setAction(Boolean action) {
		this.action = action;
	}

	public Long getInstallmentHostelFeeId() {
		return installmentHostelFeeId;
	}

	public void setInstallmentHostelFeeId(Long installmentHostelFeeId) {
		this.installmentHostelFeeId = installmentHostelFeeId;
	}

	public BigDecimal getHostelinstallAmount() {
		return hostelinstallAmount;
	}

	public void setHostelinstallAmount(BigDecimal hostelinstallAmount) {
		this.hostelinstallAmount = hostelinstallAmount;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public RoomCategory getRoomCategory() {
		return roomCategory;
	}

	public void setRoomCategory(RoomCategory roomCategory) {
		this.roomCategory = roomCategory;
	}

	public Buildingmaster getBuildingmaster() {
		return buildingmaster;
	}

	public void setBuildingmaster(Buildingmaster buildingmaster) {
		this.buildingmaster = buildingmaster;
	}

	public InstallmentMaster getInstallmentMaster() {
		return installmentMaster;
	}

	public void setInstallmentMaster(InstallmentMaster installmentMaster) {
		this.installmentMaster = installmentMaster;
	}
}
