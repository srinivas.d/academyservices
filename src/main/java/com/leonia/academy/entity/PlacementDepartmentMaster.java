package com.leonia.academy.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Table(name="leoacademy_tbl_placementdepartmentmaster")
@Entity
public class PlacementDepartmentMaster {
	
	@SequenceGenerator(name="seq",sequenceName="departmentMasterId",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="seq")
	@Id
	private Long departmentMasterId;
	
	private String departmentName;
	private String departmentShortName;
	private Boolean status;
	
	@OneToMany(mappedBy="departmentMaster")
	private List<PlacementDesignationMaster> designationMaster;
	
	/*@OneToMany(mappedBy="departmentMaster")
	private List<ReasonsMaster> reasonsMaster;
	*/
	public Long getDepartmentMasterId() {
		return departmentMasterId;
	}
	public void setDepartmentMasterId(Long departmentMasterId) {
		this.departmentMasterId = departmentMasterId;
	}
	public List<PlacementDesignationMaster> getDesignationMaster() {
		return designationMaster;
	}
	public void setDesignationMaster(List<PlacementDesignationMaster> designationMaster) {
		this.designationMaster = designationMaster;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public String getDepartmentShortName() {
		return departmentShortName;
	}
	public void setDepartmentShortName(String departmentShortName) {
		this.departmentShortName = departmentShortName;
	}
	/*public List<ReasonsMaster> getReasonsMaster() {
		return reasonsMaster;
	}
	public void setReasonsMaster(List<ReasonsMaster> reasonsMaster) {
		this.reasonsMaster = reasonsMaster;
	}*/
	
	

}
