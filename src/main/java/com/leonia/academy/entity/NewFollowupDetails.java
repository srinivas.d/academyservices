package com.leonia.academy.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class NewFollowupDetails {
@Id
@GeneratedValue
private Integer newFollowUpId;
@Column
private Date newFollowupDate;
@Column
private String reason;
@Column
private String status;
@Column
private String allocationManager;
@Column
private String time;

@ManyToOne
@JoinColumn(name="followUpId")
private LeadFollowUpDTO leadFollowUpDTO;

public Integer getNewFollowUpId() {
	return newFollowUpId;
}

public void setNewFollowUpId(Integer newFollowUpId) {
	this.newFollowUpId = newFollowUpId;
}

public Date getNewFollowupDate() {
	return newFollowupDate;
}

public void setNewFollowupDate(Date newFollowupDate) {
	this.newFollowupDate = newFollowupDate;
}

public String getReason() {
	return reason;
}

public void setReason(String reason) {
	this.reason = reason;
}

public String getStatus() {
	return status;
}

public void setStatus(String status) {
	this.status = status;
}

public String getAllocationManager() {
	return allocationManager;
}

public void setAllocationManager(String allocationManager) {
	this.allocationManager = allocationManager;
}

public String getTime() {
	return time;
}

public void setTime(String time) {
	this.time = time;
}

public LeadFollowUpDTO getLeadFollowUpDTO() {
	return leadFollowUpDTO;
}

public void setLeadFollowUpDTO(LeadFollowUpDTO leadFollowUpDTO) {
	this.leadFollowUpDTO = leadFollowUpDTO;
}
}
