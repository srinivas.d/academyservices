package com.leonia.academy.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="leoacademy_tbl_domainmaster")
public class DomainMaster {
	@Id
	@SequenceGenerator(name="seq",sequenceName="domainMasterId",  allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="seq")
	private Long domainMasterId;
	private String domainName;
	private Boolean status;
	public Long getDomainMasterId() {
		return domainMasterId;
	}
	public void setDomainMasterId(Long domainMasterId) {
		this.domainMasterId = domainMasterId;
	}
	public String getDomainName() {
		return domainName;
	}
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	

}
