package com.leonia.academy.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="leoacademy_tbl_feePaidTransactions")
public class FeePaidTransactionsDTO {
	
	@Id
	@GeneratedValue
	private Long feePaidTransactionsId;
	@Column
	private BigDecimal termWiseAmount;
	@Column
	private BigDecimal paidAmount;
	@Column
	@Temporal(TemporalType.DATE)
    private Date paidDate;
	@Column
	private String termWiseMessage;
	@Column
	private Long admissionNumberId;
	@Column
	private Long batchId;
	
	@ManyToOne
	@JoinColumn(name="receiptGenerateId")
	private ReceiptGenerateDTO receiptGenerateDTO;

	public ReceiptGenerateDTO getReceiptGenerateDTO() {
		return receiptGenerateDTO;
	}
	public void setReceiptGenerateDTO(ReceiptGenerateDTO receiptGenerateDTO) {
		this.receiptGenerateDTO = receiptGenerateDTO;
	}
	public Long getFeePaidTransactionsId() {
		return feePaidTransactionsId;
	}
	public void setFeePaidTransactionsId(Long feePaidTransactionsId) {
		this.feePaidTransactionsId = feePaidTransactionsId;
	}
	public BigDecimal getTermWiseAmount() {
		return termWiseAmount;
	}
	public void setTermWiseAmount(BigDecimal termWiseAmount) {
		this.termWiseAmount = termWiseAmount;
	}
	public BigDecimal getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}
	public Date getPaidDate() {
		return paidDate;
	}
	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}
	public Long getAdmissionNumberId() {
		return admissionNumberId;
	}
	public void setAdmissionNumberId(Long admissionNumberId) {
		this.admissionNumberId = admissionNumberId;
	}
	public Long getBatchId() {
		return batchId;
	}
	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}
	public String getTermWiseMessage() {
		return termWiseMessage;
	}
	public void setTermWiseMessage(String termWiseMessage) {
		this.termWiseMessage = termWiseMessage;
	}

}
