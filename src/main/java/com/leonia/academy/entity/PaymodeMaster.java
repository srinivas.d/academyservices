package com.leonia.academy.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="leoacademy_tbl_paymodemaster")
public class PaymodeMaster {
	@GeneratedValue
	@Id
	private int paymodeId;
	@Column
	private int sno;
	@Column
	private String payMode;
	@Column
	private boolean valid;
	@Column
	private String updatedBy;
	@Temporal(TemporalType.TIMESTAMP)
	@Column
	private Date updatedDate;
	@Column
	private String updatedIp;
	
	@OneToMany(mappedBy="paymodeMaster",fetch=FetchType.LAZY)
	private List<FeeSettlementDTO> feeSettlementDTO;
	
	public List<FeeSettlementDTO> getFeeSettlementDTO() {
		return feeSettlementDTO;
	}
	public void setFeeSettlementDTO(List<FeeSettlementDTO> feeSettlementDTO) {
		this.feeSettlementDTO = feeSettlementDTO;
	}
	public int getPaymodeId() {
		return paymodeId;
	}
	public void setPaymodeId(int paymodeId) {
		this.paymodeId = paymodeId;
	}
	public int getSno() {
		return sno;
	}
	public void setSno(int sno) {
		this.sno = sno;
	}
	public String getPayMode() {
		return payMode;
	}
	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getUpdatedIp() {
		return updatedIp;
	}
	public void setUpdatedIp(String updatedIp) {
		this.updatedIp = updatedIp;
	}
	
	

}
