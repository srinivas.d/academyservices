package com.leonia.academy.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="countrymaster")
public class CountryMaster {
	@Id
	@SequenceGenerator(name = "countrySEQ", sequenceName = "country_id_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="countrySEQ")
	@Column(name="country_id")
	private int countryId;
	@Column(name="country_name")
	private String countryName;
	@Column(name="country_status")
	private boolean cityStatus;
	@Column
	private Boolean action;
	
	@OneToMany(cascade = CascadeType.ALL,mappedBy="countryMaster")
	private List<StateMaster> stateMater;
	public int getCountryId() {
		return countryId;
	}
	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public boolean isCityStatus() {
		return cityStatus;
	}
	public void setCityStatus(boolean cityStatus) {
		this.cityStatus = cityStatus;
	}
	
	
	public List<StateMaster> getStateMater() {
		return stateMater;
	}
	public void setStateMater(List<StateMaster> stateMater) {
		this.stateMater = stateMater;
	}
	public Boolean getAction() {
		return action;
	}
	public void setAction(Boolean action) {
		this.action = action;
	}

	
}
