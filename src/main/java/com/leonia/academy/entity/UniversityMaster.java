package com.leonia.academy.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Table
@Entity
public class UniversityMaster {
	@GeneratedValue
	@Id
	private Long universityMasterId;
	@Column
	private String universityName;
	@OneToMany(mappedBy="universityMaster")
	private List<CourseMaster> courseMaster;
	
	
	public Long getUniversityMasterId() {
		return universityMasterId;
	}
	public void setUniversityMasterId(Long universityMasterId) {
		this.universityMasterId = universityMasterId;
	}
	public String getUniversityName() {
		return universityName;
	}
	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}
	public List<CourseMaster> getCourseMaster() {
		return courseMaster;
	}
	public void setCourseMaster(List<CourseMaster> courseMaster) {
		this.courseMaster = courseMaster;
	}
	
	

}
