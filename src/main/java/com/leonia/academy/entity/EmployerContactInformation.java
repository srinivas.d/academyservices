package com.leonia.academy.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="leoacademy_tbl_employercontactinformation")
public class EmployerContactInformation {
	@Id
	@SequenceGenerator(name="seq",sequenceName="employerContactInformationId",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="seq")
	private Long employerContactInformationId;
	private String employerContactPersonName;
	private String employerContactMobile;
	private String employerAlternateContactMobile;
	private String employerEmail;
	private String employerDesignation;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="companyDetailsId")
	private CompanyDetails companyDetails;
	
	
	public Long getEmployerContactInformationId() {
		return employerContactInformationId;
	}
	public void setEmployerContactInformationId(Long employerContactInformationId) {
		this.employerContactInformationId = employerContactInformationId;
	}
	public String getEmployerContactMobile() {
		return employerContactMobile;
	}
	public void setEmployerContactMobile(String employerContactMobile) {
		this.employerContactMobile = employerContactMobile;
	}
	public String getEmployerAlternateContactMobile() {
		return employerAlternateContactMobile;
	}
	public void setEmployerAlternateContactMobile(String employerAlternateContactMobile) {
		this.employerAlternateContactMobile = employerAlternateContactMobile;
	}
	public String getEmployerEmail() {
		return employerEmail;
	}
	public void setEmployerEmail(String employerEmail) {
		this.employerEmail = employerEmail;
	}
	public CompanyDetails getCompanyDetails() {
		return companyDetails;
	}
	public void setCompanyDetails(CompanyDetails companyDetails) {
		this.companyDetails = companyDetails;
	}
	public String getEmployerContactPersonName() {
		return employerContactPersonName;
	}
	public void setEmployerContactPersonName(String employerContactPersonName) {
		this.employerContactPersonName = employerContactPersonName;
	}
	public String getEmployerDesignation() {
		return employerDesignation;
	}
	public void setEmployerDesignation(String employerDesignation) {
		this.employerDesignation = employerDesignation;
	}
	
	

}
