package com.leonia.academy.entity;


import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="installment")
public class InstallmentMaster {
	
	@Id
	@GeneratedValue
	private Long instid;
	@Column
	private String installmentname;
	
	@OneToMany(mappedBy="installmentMaster")
	private List<InstallmentHostelFee> installmentHostelFee;
	
	
	public Long getInstid() {
		return instid;
	}
	public void setInstid(Long instid) {
		this.instid = instid;
	}
	public List<InstallmentHostelFee> getInstallmentHostelFee() {
		return installmentHostelFee;
	}
	public void setInstallmentHostelFee(List<InstallmentHostelFee> installmentHostelFee) {
		this.installmentHostelFee = installmentHostelFee;
	}
	public String getInstallmentname() {
		return installmentname;
	}
	public void setInstallmentname(String installmentname) {
		this.installmentname = installmentname;
	}
}
