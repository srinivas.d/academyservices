package com.leonia.academy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="login")
public class LoginEntity {
	
	@Id
	@GeneratedValue
	private Integer uesrId;
	@Column
	private String username;
	@Column
	private String password;
	@Column
	private String email;
	@Column
	private Boolean enabled;
	@Column
	private String phoneNumber;
	@Column
	private String updatedby;
	@Column
	private String updateDate;
	@Column
	private String updatedip;
	@Transient
	private Integer result;
	
	
	
	/*@OneToMany(cascade = CascadeType.ALL,mappedBy="loginDTO")
	private List<LeadFollowUpDTO> LeadFollowUpDTO ;*/
	
	/*@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="roleId")
	private RolesDTO rolesDTO;*/
	
	public Integer getResult() {
		return result;
	}
	public void setResult(Integer result) {
		this.result = result;
	}
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	
	/*public RolesDTO getRolesDTO() {
		return rolesDTO;
	}
	public void setRolesDTO(RolesDTO rolesDTO) {
		this.rolesDTO = rolesDTO;
	}
	public List<LeadFollowUpDTO> getLeadFollowUpDTO() {
		return LeadFollowUpDTO;
	}
	public void setLeadFollowUpDTO(List<LeadFollowUpDTO> leadFollowUpDTO) {
		LeadFollowUpDTO = leadFollowUpDTO;
	}*/
	public Integer getUesrId() {
		return uesrId;
	}
	public void setUesrId(Integer uesrId) {
		this.uesrId = uesrId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdatedip() {
		return updatedip;
	}
	public void setUpdatedip(String updatedip) {
		this.updatedip = updatedip;
	}
	
}
