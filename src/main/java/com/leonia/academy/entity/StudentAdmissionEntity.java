package com.leonia.academy.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name="StudentAdmission")
public class StudentAdmissionEntity {
	@Id
	@GeneratedValue
	private Long admissionNumberId;
	@Column
	private String admissionNumber;
	@Column
	private Long studentRollNumber;
	@Column
	@Temporal(TemporalType.DATE)
	private Date admissionDate;
	@Column
	private String universityAdmissionNumber;
	
	private String universityRegisterNumber;
	@Column
	private String studentFirstName;
	@Column
	private String studentMiddleName;
	@Column
	private String studentLastName;
	@Column
	@Temporal(TemporalType.DATE)
	private Date dateOfBirth;
	@Column
	private String birthPlace;
	@Column
	private String gender;
	@Column
	private String studentRoomNumber;
	@Column
	private String bedNumber;
	@Column
	private String addressLine1;
	@Column
	private String addressLine2;
	@Column
	private String country;
	
	@Column
	private String state;
	@Column
	private String pinCode;
	@Column
	private String studentPhoneNumber;
	@Column
	private String studentMobilenumber;
	@Column
	private String studentEmail;
	@Column
	private String reference;
	@Column
	private BigDecimal height;
	@Column
	private BigDecimal weight;
	@Column
	private String allergies;
	@Column
	private String uploadPhotofileName;
	@Column
	private byte[] uploadPhotodata;
	@Column
	private String addSibling;
	@Column
	private Boolean action;
	@Column
	private Long nationalityId;
	@Column
	private Long bloodGroupId;
	@Column
	private Long fees;
	@Column
	private Long motherTongueId;
	@Column
	private Long courseId;
	/*@Column
	private Long batchId;*/
	@Transient
	private String batchName;
	@Transient
	private Long batchId;
	
	private String rfidCardNumber;
	
	@ManyToOne
	@JoinColumn(name="batchId")
	private BatchMaster batchMaster;
	
	@OneToMany(cascade = CascadeType.ALL,mappedBy="studentAdmission")
	private List<RoomAllotment> roomAllotment ;
	

	@OneToMany(cascade = CascadeType.ALL,mappedBy="studentAdmission")
	private List<Totalroomallocatiodetails> totalroomallocatiodetails ;
	
	public List<Totalroomallocatiodetails> getTotalroomallocatiodetails() {
		return totalroomallocatiodetails;
	}
	public void setTotalroomallocatiodetails(List<Totalroomallocatiodetails> totalroomallocatiodetails) {
		this.totalroomallocatiodetails = totalroomallocatiodetails;
	}
	
	
	public String getBatchName() {
		return batchName;
	}
	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}
	public Long getBatchId() {
		return batchId;
	}
	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}


	@Column
	private Long stayCategoryId;
	@Column
	private Long discountCategoryId;
	@Column
	private Long religionId;
	@Column
	private Long communityId;
	@Column
	private Long feeCategoryId;
	@Column
	private Long cityId;
	@Column
	private String aadhaarCardNumber;
	@Column
	private String identificationMarksOne;
	
	@Column
	private String identificationMarksTwo;
	@Column
	private String bankName;
	@Column
	private String bankBranchName;
	@Column
	private String bankAccNumber;
	@Column
	private String bankBranchIfscCode;
	
	/*@OneToMany(mappedBy="studentAdmission")
	private List<FeeAssignToStudents> feeAssignToStudents;*/
	
	@OneToMany(cascade = CascadeType.ALL,mappedBy="StudentAdmission")
	private List<HostelFeeAssingToStudent> hostelFeeAssingToStudent;
	
	@OneToMany(cascade = CascadeType.ALL,mappedBy="studentAdmissionEntity",fetch=FetchType.LAZY)
	private List<EducationEntity> educationEntity;
	
	public Long getAdmissionNumberId() {
		return admissionNumberId;
	}
	public void setAdmissionNumberId(Long admissionNumberId) {
		this.admissionNumberId = admissionNumberId;
	}
	public String getAdmissionNumber() {
		return admissionNumber;
	}
	public void setAdmissionNumber(String admissionNumber) {
		this.admissionNumber = admissionNumber;
	}
	public Long getStudentRollNumber() {
		return studentRollNumber;
	}
	public void setStudentRollNumber(Long studentRollNumber) {
		this.studentRollNumber = studentRollNumber;
	}
	
	public String getUniversityAdmissionNumber() {
		return universityAdmissionNumber;
	}
	public void setUniversityAdmissionNumber(String universityAdmissionNumber) {
		this.universityAdmissionNumber = universityAdmissionNumber;
	}
	public String getStudentFirstName() {
		return studentFirstName;
	}
	public void setStudentFirstName(String studentFirstName) {
		this.studentFirstName = studentFirstName;
	}
	public String getStudentMiddleName() {
		return studentMiddleName;
	}
	public void setStudentMiddleName(String studentMiddleName) {
		this.studentMiddleName = studentMiddleName;
	}
	public String getStudentLastName() {
		return studentLastName;
	}
	public void setStudentLastName(String studentLastName) {
		this.studentLastName = studentLastName;
	}
	
	public Date getAdmissionDate() {
		return admissionDate;
	}
	public void setAdmissionDate(Date admissionDate) {
		this.admissionDate = admissionDate;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getBirthPlace() {
		return birthPlace;
	}
	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getStudentRoomNumber() {
		return studentRoomNumber;
	}
	public void setStudentRoomNumber(String studentRoomNumber) {
		this.studentRoomNumber = studentRoomNumber;
	}
	public String getBedNumber() {
		return bedNumber;
	}
	public void setBedNumber(String bedNumber) {
		this.bedNumber = bedNumber;
	}
	
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPinCode() {
		return pinCode;
	}
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}
	public String getStudentPhoneNumber() {
		return studentPhoneNumber;
	}
	public void setStudentPhoneNumber(String studentPhoneNumber) {
		this.studentPhoneNumber = studentPhoneNumber;
	}
	public String getStudentMobilenumber() {
		return studentMobilenumber;
	}
	public void setStudentMobilenumber(String studentMobilenumber) {
		this.studentMobilenumber = studentMobilenumber;
	}
	public String getStudentEmail() {
		return studentEmail;
	}
	public void setStudentEmail(String studentEmail) {
		this.studentEmail = studentEmail;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public BigDecimal getHeight() {
		return height;
	}
	public void setHeight(BigDecimal height) {
		this.height = height;
	}
	public BigDecimal getWeight() {
		return weight;
	}
	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}
	public String getAllergies() {
		return allergies;
	}
	public void setAllergies(String allergies) {
		this.allergies = allergies;
	}
	
	public String getUploadPhotofileName() {
		return uploadPhotofileName;
	}
	public void setUploadPhotofileName(String uploadPhotofileName) {
		this.uploadPhotofileName = uploadPhotofileName;
	}
	public byte[] getUploadPhotodata() {
		return uploadPhotodata;
	}
	public void setUploadPhotodata(byte[] uploadPhotodata) {
		this.uploadPhotodata = uploadPhotodata;
	}
	public String getAddSibling() {
		return addSibling;
	}
	public void setAddSibling(String addSibling) {
		this.addSibling = addSibling;
	}
	public Boolean getAction() {
		return action;
	}
	public void setAction(Boolean action) {
		this.action = action;
	}
	
	public Long getFees() {
		return fees;
	}
	public void setFees(Long fees) {
		this.fees = fees;
	}
	public Long getNationalityId() {
		return nationalityId;
	}
	public void setNationalityId(Long nationalityId) {
		this.nationalityId = nationalityId;
	}
	public Long getBloodGroupId() {
		return bloodGroupId;
	}
	public void setBloodGroupId(Long bloodGroupId) {
		this.bloodGroupId = bloodGroupId;
	}
	public Long getMotherTongueId() {
		return motherTongueId;
	}
	public void setMotherTongueId(Long motherTongueId) {
		this.motherTongueId = motherTongueId;
	}
	public Long getCourseId() {
		return courseId;
	}
	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}
	public BatchMaster getBatchMaster() {
		return batchMaster;
	}
	public void setBatchMaster(BatchMaster batchMaster) {
		this.batchMaster = batchMaster;
	}
	/*public Long getBatchId() {
		return batchId;
	}
	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}*/
	public Long getStayCategoryId() {
		return stayCategoryId;
	}
	public void setStayCategoryId(Long stayCategoryId) {
		this.stayCategoryId = stayCategoryId;
	}
	public Long getDiscountCategoryId() {
		return discountCategoryId;
	}
	public void setDiscountCategoryId(Long discountCategoryId) {
		this.discountCategoryId = discountCategoryId;
	}
	public Long getReligionId() {
		return religionId;
	}
	public void setReligionId(Long religionId) {
		this.religionId = religionId;
	}
	public Long getCommunityId() {
		return communityId;
	}
	public void setCommunityId(Long communityId) {
		this.communityId = communityId;
	}
	public Long getFeeCategoryId() {
		return feeCategoryId;
	}
	public void setFeeCategoryId(Long feeCategoryId) {
		this.feeCategoryId = feeCategoryId;
	}
	public Long getCityId() {
		return cityId;
	}
	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}
	public String getAadhaarCardNumber() {
		return aadhaarCardNumber;
	}
	public void setAadhaarCardNumber(String aadhaarCardNumber) {
		this.aadhaarCardNumber = aadhaarCardNumber;
	}
	public String getIdentificationMarksOne() {
		return identificationMarksOne;
	}
	public void setIdentificationMarksOne(String identificationMarksOne) {
		this.identificationMarksOne = identificationMarksOne;
	}
	public String getIdentificationMarksTwo() {
		return identificationMarksTwo;
	}
	public void setIdentificationMarksTwo(String identificationMarksTwo) {
		this.identificationMarksTwo = identificationMarksTwo;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankBranchName() {
		return bankBranchName;
	}
	public void setBankBranchName(String bankBranchName) {
		this.bankBranchName = bankBranchName;
	}
	public String getBankAccNumber() {
		return bankAccNumber;
	}
	public void setBankAccNumber(String bankAccNumber) {
		this.bankAccNumber = bankAccNumber;
	}
	public String getBankBranchIfscCode() {
		return bankBranchIfscCode;
	}
	public void setBankBranchIfscCode(String bankBranchIfscCode) {
		this.bankBranchIfscCode = bankBranchIfscCode;
	}
	
	/*public List<FeeAssignToStudents> getFeeAssignToStudents() {
		return feeAssignToStudents;
	}
	public void setFeeAssignToStudents(List<FeeAssignToStudents> feeAssignToStudents) {
		this.feeAssignToStudents = feeAssignToStudents;
	}
	*/
	public List<RoomAllotment> getRoomAllotment() {
		return roomAllotment;
	}
	
	public void setRoomAllotment(List<RoomAllotment> roomAllotment) {
		this.roomAllotment = roomAllotment;
	}
	public List<HostelFeeAssingToStudent> getHostelFeeAssingToStudent() {
		return hostelFeeAssingToStudent;
	}
	public void setHostelFeeAssingToStudent(List<HostelFeeAssingToStudent> hostelFeeAssingToStudent) {
		this.hostelFeeAssingToStudent = hostelFeeAssingToStudent;
	}
	
	
	public String getUniversityRegisterNumber() {
		return universityRegisterNumber;
	}
	public List<EducationEntity> getEducationEntity() {
		return educationEntity;
	}
	public void setEducationEntity(List<EducationEntity> educationEntity) {
		this.educationEntity = educationEntity;
	}
	public void setUniversityRegisterNumber(String universityRegisterNumber) {
		this.universityRegisterNumber = universityRegisterNumber;
	}
	public String getRfidCardNumber() {
		return rfidCardNumber;
	}
	public void setRfidCardNumber(String rfidCardNumber) {
		this.rfidCardNumber = rfidCardNumber;
	}
	
}
