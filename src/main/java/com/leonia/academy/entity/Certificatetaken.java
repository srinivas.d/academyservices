package com.leonia.academy.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name="certificatetaken")
public class Certificatetaken {
	@Id
	@SequenceGenerator(name = "certificatetakenSEQ", sequenceName = "certid_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="certificatetakenSEQ")
	@Column(name="doc_id",length=100)
	private Integer documentid;
	@Column
	private Long sadmissionid;
	@Column
	private String sadmissionno;
	@Column
	private String studentname;
	@Column
	private String sbatchname;
	@Column
	private String scertificatename;
	@Column
	private String status;
	@Column
	private String fileName;
    @Column
	private byte[] data;
    @Temporal(TemporalType.DATE)
    private  Date creatdedon;
    private String returnedby;
    @Column
    private String returnedon;
    @Column
    private String certificateno;
    @Column
    private String course;
    @Transient
	private String startDate;
	@Transient
	private String endDate;
    
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getCourse() {
		return course;
	}
	public void setCourse(String course) {
		this.course = course;
	}
	public String getCertificateno() {
		return certificateno;
	}
	public void setCertificateno(String certificateno) {
		this.certificateno = certificateno;
	}
	public Integer getDocumentid() {
		return documentid;
	}
	public void setDocumentid(Integer documentid) {
		this.documentid = documentid;
	}
	public String getSadmissionno() {
		return sadmissionno;
	}
	public void setSadmissionno(String sadmissionno) {
		this.sadmissionno = sadmissionno;
	}
	public String getStudentname() {
		return studentname;
	}
	public void setStudentname(String studentname) {
		this.studentname = studentname;
	}
	public String getSbatchname() {
		return sbatchname;
	}
	public void setSbatchname(String sbatchname) {
		this.sbatchname = sbatchname;
	}
	public String getScertificatename() {
		return scertificatename;
	}
	public void setScertificatename(String scertificatename) {
		this.scertificatename = scertificatename;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getReturnedby() {
		return returnedby;
	}
	public void setReturnedby(String returnedby) {
		this.returnedby = returnedby;
	}
	public String getReturnedon() {
		return returnedon;
	}
	public void setReturnedon(String returnedon) {
		this.returnedon = returnedon;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Date getCreatdedon() {
		return creatdedon;
	}
	public void setCreatdedon(Date creatdedon) {
		this.creatdedon = creatdedon;
	}
	public byte[] getData() {
		return data;
	}
	public void setData(byte[] data) {
		this.data = data;
	}
	public Long getSadmissionid() {
		return sadmissionid;
	}
	public void setSadmissionid(Long sadmissionid) {
		this.sadmissionid = sadmissionid;
	}
	

}
