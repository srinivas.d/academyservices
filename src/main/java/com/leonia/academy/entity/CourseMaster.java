package com.leonia.academy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table
@Entity
public class CourseMaster {
	@Id
	@GeneratedValue
	private Long courseId;
	
	@Column
	private String courseName;
	
	@Column
	private String shortCode;
	
	@Column
	private String duration;
	
	@ManyToOne
	@JoinColumn(name="universityMasterId")
	private UniversityMaster universityMaster;
	
	/*@ManyToOne
	@JoinColumn(name ="batchId")
	private BatchMaster batchMaster;*/
	
	
	/*@OneToMany(cascade = CascadeType.ALL, mappedBy ="courseMaster")
	private List<BatchCountMaster> batchCountMaster; */
	
	
	
	@Column
	private Boolean action;

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public Boolean getAction() {
		return action;
	}

	public void setAction(Boolean action) {
		this.action = action;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}


	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public UniversityMaster getUniversityMaster() {
		return universityMaster;
	}

	public void setUniversityMaster(UniversityMaster universityMaster) {
		this.universityMaster = universityMaster;
	}

	
	
}
