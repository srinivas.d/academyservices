package com.leonia.academy.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="leoacademy_tbl_discountapprovedmaster")
public class DiscountApprovedMaster {
	
	@Id
	@SequenceGenerator(name = "leoacademy_tbl_discountapprovedmaster", sequenceName = "discountApprovedmasterId", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="leoacademy_tbl_discountapprovedmaster")
	private Long discountApprovedmasterId;
	
	@Column
	private String discountApprovedmasterName;
	
	/*@OneToMany(mappedBy="discountApprovedMaster")
	private List<HostelFeeAssingToStudent> hostelFeeAssingToStudent;
	
	public List<HostelFeeAssingToStudent> getHostelFeeAssingToStudent() {
		return hostelFeeAssingToStudent;
	}

	public void setHostelFeeAssingToStudent(List<HostelFeeAssingToStudent> hostelFeeAssingToStudent) {
		this.hostelFeeAssingToStudent = hostelFeeAssingToStudent;
	}
*/
	@OneToMany(mappedBy="discountApprovedMaster")
	private List<FeeAssignToStudents> feeAssignToStudents;

	public Long getDiscountApprovedmasterId() {
		return discountApprovedmasterId;
	}

	public void setDiscountApprovedmasterId(Long discountApprovedmasterId) {
		this.discountApprovedmasterId = discountApprovedmasterId;
	}

	public String getDiscountApprovedmasterName() {
		return discountApprovedmasterName;
	}

	public void setDiscountApprovedmasterName(String discountApprovedmasterName) {
		this.discountApprovedmasterName = discountApprovedmasterName;
	}

	public List<FeeAssignToStudents> getFeeAssignToStudents() {
		return feeAssignToStudents;
	}

	public void setFeeAssignToStudents(List<FeeAssignToStudents> feeAssignToStudents) {
		this.feeAssignToStudents = feeAssignToStudents;
	}
	
}
