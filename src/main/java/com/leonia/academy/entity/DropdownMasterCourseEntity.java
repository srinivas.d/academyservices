package com.leonia.academy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="dropdownmastercourse")
public class DropdownMasterCourseEntity {
	
	@Id
	@GeneratedValue
	private Integer Id;
	@Column
	private String description;
	@Column
	private String descriptiontype;
	
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDescriptiontype() {
		return descriptiontype;
	}
	public void setDescriptiontype(String descriptiontype) {
		this.descriptiontype = descriptiontype;
	}
	
	
	

}
