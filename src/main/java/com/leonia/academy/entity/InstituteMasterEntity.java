package com.leonia.academy.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="InstituteMaster")
public class InstituteMasterEntity {
	@Id
	@GeneratedValue
	private Integer instituteId;
	@Column
	private String instituteType;
	@Column
	private String instituteName;
	@Column
	private String instituteAddress;
	@Column
	private String institutePhone;
	@Column
	private String fax;
	@Column
	private String email;
	@Column
	private String landMark;
	
	@Column
	private String status;
	
	@Column
	private String allocationManager;
	@Column
	private String contactPersonOne;
	@Column
	private String contactPersonOneDesignation;
	@Column
	private String contactPersonOneEmail;
	@Column
	private String contactPersonOnePhone;
	
	@Column
	private String contactPersonTwo;
	@Column
	private String contactPersonTwoDesignation;
	@Column
	private String contactPersonTwoEmail;
	@Column
	private String contactPersonTwoPhone;
	
	
	
	@Column
	private String contactPersonThree;
	@Column
	private String contactPersonThreeDesignation;
	@Column
	private String contactPersonThreeEmail;
	@Column
	private String contactPersonThreePhone;
	
	@Transient
	private String instituteTypeName;
	@Transient
	private String allocmngName;
	
	@OneToMany(cascade = CascadeType.ALL,mappedBy="instituteMasterEntity")
	private List<InstituteContactPersonEntity> contactPersonEntities;
	
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getContactPersonOneDesignation() {
		return contactPersonOneDesignation;
	}
	public void setContactPersonOneDesignation(String contactPersonOneDesignation) {
		this.contactPersonOneDesignation = contactPersonOneDesignation;
	}
	public String getContactPersonOneEmail() {
		return contactPersonOneEmail;
	}
	public void setContactPersonOneEmail(String contactPersonOneEmail) {
		this.contactPersonOneEmail = contactPersonOneEmail;
	}
	public String getContactPersonOnePhone() {
		return contactPersonOnePhone;
	}
	public void setContactPersonOnePhone(String contactPersonOnePhone) {
		this.contactPersonOnePhone = contactPersonOnePhone;
	}
	public String getContactPersonTwo() {
		return contactPersonTwo;
	}
	public void setContactPersonTwo(String contactPersonTwo) {
		this.contactPersonTwo = contactPersonTwo;
	}
	public String getContactPersonTwoDesignation() {
		return contactPersonTwoDesignation;
	}
	public void setContactPersonTwoDesignation(String contactPersonTwoDesignation) {
		this.contactPersonTwoDesignation = contactPersonTwoDesignation;
	}
	public String getContactPersonTwoEmail() {
		return contactPersonTwoEmail;
	}
	public void setContactPersonTwoEmail(String contactPersonTwoEmail) {
		this.contactPersonTwoEmail = contactPersonTwoEmail;
	}
	public String getContactPersonTwoPhone() {
		return contactPersonTwoPhone;
	}
	public void setContactPersonTwoPhone(String contactPersonTwoPhone) {
		this.contactPersonTwoPhone = contactPersonTwoPhone;
	}
	public String getContactPersonThree() {
		return contactPersonThree;
	}
	public void setContactPersonThree(String contactPersonThree) {
		this.contactPersonThree = contactPersonThree;
	}
	public String getContactPersonThreeDesignation() {
		return contactPersonThreeDesignation;
	}
	public void setContactPersonThreeDesignation(String contactPersonThreeDesignation) {
		this.contactPersonThreeDesignation = contactPersonThreeDesignation;
	}
	public String getContactPersonThreeEmail() {
		return contactPersonThreeEmail;
	}
	public void setContactPersonThreeEmail(String contactPersonThreeEmail) {
		this.contactPersonThreeEmail = contactPersonThreeEmail;
	}
	public String getContactPersonThreePhone() {
		return contactPersonThreePhone;
	}
	public void setContactPersonThreePhone(String contactPersonThreePhone) {
		this.contactPersonThreePhone = contactPersonThreePhone;
	}
	public Integer getInstituteId() {
		return instituteId;
	}
	public void setInstituteId(Integer instituteId) {
		this.instituteId = instituteId;
	}
	public String getInstituteType() {
		return instituteType;
	}
	public void setInstituteType(String instituteType) {
		this.instituteType = instituteType;
	}
	public String getInstituteName() {
		return instituteName;
	}
	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}
	public String getInstituteAddress() {
		return instituteAddress;
	}
	public void setInstituteAddress(String instituteAddress) {
		this.instituteAddress = instituteAddress;
	}
	public String getInstitutePhone() {
		return institutePhone;
	}
	public void setInstitutePhone(String institutePhone) {
		this.institutePhone = institutePhone;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLandMark() {
		return landMark;
	}
	public void setLandMark(String landMark) {
		this.landMark = landMark;
	}
	public String getAllocationManager() {
		return allocationManager;
	}
	public void setAllocationManager(String allocationManager) {
		this.allocationManager = allocationManager;
	}
	public String getContactPersonOne() {
		return contactPersonOne;
	}
	public void setContactPersonOne(String contactPersonOne) {
		this.contactPersonOne = contactPersonOne;
	}
	public List<InstituteContactPersonEntity> getContactPersonEntities() {
		return contactPersonEntities;
	}
	public void setContactPersonEntities(List<InstituteContactPersonEntity> contactPersonEntities) {
		this.contactPersonEntities = contactPersonEntities;
	}
	public String getInstituteTypeName() {
		return instituteTypeName;
	}
	public void setInstituteTypeName(String instituteTypeName) {
		this.instituteTypeName = instituteTypeName;
	}
	public String getAllocmngName() {
		return allocmngName;
	}
	public void setAllocmngName(String allocmngName) {
		this.allocmngName = allocmngName;
	}
	
}
