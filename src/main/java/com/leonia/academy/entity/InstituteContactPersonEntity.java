package com.leonia.academy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="leoacademy_tbl_institutecontactpersonentity")
public class InstituteContactPersonEntity {
	
	@Id
	@SequenceGenerator(name="seq",sequenceName="instituteContactPersonEntityId",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="seq")
	private Long instituteContactPersonEntityId;
	
	@Column
	private String contactPersonName;
	@Column
	private String contactPersonDesignation;
	@Column
	private String contactPersonEmail;
	@Column
	private String contactPersonPhone;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="instituteId")
	private InstituteMasterEntity instituteMasterEntity;
	
	public Long getInstituteContactPersonEntityId() {
		return instituteContactPersonEntityId;
	}
	public void setInstituteContactPersonEntityId(Long instituteContactPersonEntityId) {
		this.instituteContactPersonEntityId = instituteContactPersonEntityId;
	}
	public String getContactPersonName() {
		return contactPersonName;
	}
	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}
	public String getContactPersonDesignation() {
		return contactPersonDesignation;
	}
	public void setContactPersonDesignation(String contactPersonDesignation) {
		this.contactPersonDesignation = contactPersonDesignation;
	}
	public String getContactPersonEmail() {
		return contactPersonEmail;
	}
	public void setContactPersonEmail(String contactPersonEmail) {
		this.contactPersonEmail = contactPersonEmail;
	}
	public String getContactPersonPhone() {
		return contactPersonPhone;
	}
	public void setContactPersonPhone(String contactPersonPhone) {
		this.contactPersonPhone = contactPersonPhone;
	}
	public InstituteMasterEntity getInstituteMasterEntity() {
		return instituteMasterEntity;
	}
	public void setInstituteMasterEntity(InstituteMasterEntity instituteMasterEntity) {
		this.instituteMasterEntity = instituteMasterEntity;
	}
	
	

}
