package com.leonia.academy.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="leoacademy_tbl_interviewassigntostudents")
public class InterviewAssignToStudents {
	
	@SequenceGenerator(name="seq",sequenceName="interviewAssignToStudentsId",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="seq")
	@Id
	private Long interviewAssignToStudentsId;
	
	@ManyToOne
	@JoinColumn(name="admissionNumberId")
	private StudentAdmissionEntity studentAdmissionEntity;
	
	@ManyToOne
	@JoinColumn(name="reasonsMasterId")
	private ReasonsMaster reasonsMaster;
	
	@ManyToOne
	@JoinColumn(name="vacanyDetailsId")
	private VacancyDetails vacancyDetails;

	public Long getInterviewAssignToStudentsId() {
		return interviewAssignToStudentsId;
	}
	
	private String remarks;
	
	private String statusReport;
	
	
	
	private Boolean status;
	

	public void setInterviewAssignToStudentsId(Long interviewAssignToStudentsId) {
		this.interviewAssignToStudentsId = interviewAssignToStudentsId;
	}

	public VacancyDetails getVacancyDetails() {
		return vacancyDetails;
	}

	public void setVacancyDetails(VacancyDetails vacancyDetails) {
		this.vacancyDetails = vacancyDetails;
	}


	public String getRemarks() {
		return remarks;
	}

	public StudentAdmissionEntity getStudentAdmissionEntity() {
		return studentAdmissionEntity;
	}

	public void setStudentAdmissionEntity(StudentAdmissionEntity studentAdmissionEntity) {
	this.studentAdmissionEntity = studentAdmissionEntity;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	public String getStatusReport() {
		return statusReport;
	}

	public void setStatusReport(String statusReport) {
		this.statusReport = statusReport;
	}
	
	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public ReasonsMaster getReasonsMaster() {
		return reasonsMaster;
	}

	public void setReasonsMaster(ReasonsMaster reasonsMaster) {
		this.reasonsMaster = reasonsMaster;
	}



	
}
