package com.leonia.academy.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class NationalityMaster {
	@Id
	@GeneratedValue
	private Long nationalityId;
	
	@Column
	private String nationalityType;
	@Column
	private Boolean action;
	
	public Long getNationalityId() {
		return nationalityId;
	}
	public void setNationalityId(Long nationalityId) {
		this.nationalityId = nationalityId;
	}
	public String getNationalityType() {
		return nationalityType;
	}
	public void setNationalityType(String nationalityType) {
		this.nationalityType = nationalityType;
	}
	public Boolean getAction() {
		return action;
	}
	public void setAction(Boolean action) {
		this.action = action;
	} 
	
	
}
