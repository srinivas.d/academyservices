package com.leonia.academy.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonManagedReference;

@Entity
@Table(name="dropdownmaster")
public class DropDownMasterEntity implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Integer dropdownmasterId;
	@Column
	private String description;
	
	
	@OneToMany(cascade = CascadeType.ALL,mappedBy="downMasterEntity")
	private List<DropDownSubMasterEntity> downSubMasterEntity;
	
	public List<DropDownSubMasterEntity> getDownSubMasterEntity() {
		return downSubMasterEntity;
	}
	public void setDownSubMasterEntity(List<DropDownSubMasterEntity> downSubMasterEntity) {
		this.downSubMasterEntity = downSubMasterEntity;
	}
	public Integer getDropdownmasterId() {
		return dropdownmasterId;
	}
	public void setDropdownmasterId(Integer dropdownmasterId) {
		this.dropdownmasterId = dropdownmasterId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

}
