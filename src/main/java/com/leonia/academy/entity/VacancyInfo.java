package com.leonia.academy.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="leoacademy_tbl_vacancyinfo")
public class VacancyInfo {
	
	@Id
	@SequenceGenerator(name="seq" ,sequenceName="seq" ,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE ,generator="seq")
	private Long vacancyInfoId;
	
	@Temporal(TemporalType.DATE)
	private Date interviewScheduleDate;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="companyDetailsId")
	private CompanyDetails companyDetails;
	
	private String vacancyName;
	
	private Boolean status;
	
	@OneToMany(fetch=FetchType.LAZY , mappedBy="vacancyInfo",cascade=CascadeType.ALL)
	private List<VacancyDetails> vacancyDetails;

	public Long getVacancyInfoId() {
		return vacancyInfoId;
	}

	public CompanyDetails getCompanyDetails() {
		return companyDetails;
	}
	public void setCompanyDetails(CompanyDetails companyDetails) {
		this.companyDetails = companyDetails;
	}
	public void setVacancyInfoId(Long vacancyInfoId) {
		this.vacancyInfoId = vacancyInfoId;
	}
	public Date getInterviewScheduleDate() {
		return interviewScheduleDate;
	}

	public void setInterviewScheduleDate(Date interviewScheduleDate) {
		this.interviewScheduleDate = interviewScheduleDate;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public List<VacancyDetails> getVacancyDetails() {
		return vacancyDetails;
	}

	public void setVacancyDetails(List<VacancyDetails> vacancyDetails) {
		this.vacancyDetails = vacancyDetails;
	}

	public String getVacancyName() {
		return vacancyName;
	}

	public void setVacancyName(String vacancyName) {
		this.vacancyName = vacancyName;
	}
	
}
