package com.leonia.academy.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="blockmaster")
public class Blockmaster {
	@Id
	@SequenceGenerator(name = "blockmasterSEQ", sequenceName = "blockid_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="blockmasterSEQ")
	@Column(name="block_id")
	private Integer blockId;
	@Column
	private String blockCode;
	@Column
	private String blockName;
	@Column
	private Date createdOn;
	@Column
	private boolean status;
	
	@OneToMany(cascade=CascadeType.ALL,mappedBy="blockmaster")
	private List<Floormaster> floormaster;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="buildingId")
	private Buildingmaster buildingmaster;

	

	public Integer getBlockId() {
		return blockId;
	}

	public void setBlockId(Integer blockId) {
		this.blockId = blockId;
	}

	public String getBlockCode() {
		return blockCode;
	}

	public void setBlockCode(String blockCode) {
		this.blockCode = blockCode;
	}

	public String getBlockName() {
		return blockName;
	}

	public void setBlockName(String blockName) {
		this.blockName = blockName;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public List<Floormaster> getFloormaster() {
		return floormaster;
	}

	public void setFloormaster(List<Floormaster> floormaster) {
		this.floormaster = floormaster;
	}

	public Buildingmaster getBuildingmaster() {
		return buildingmaster;
	}

	public void setBuildingmaster(Buildingmaster buildingmaster) {
		this.buildingmaster = buildingmaster;
	}

}
