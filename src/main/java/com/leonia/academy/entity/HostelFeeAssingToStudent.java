package com.leonia.academy.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name="leoacademy_leo_hostelfeeassingtostudent")
public class HostelFeeAssingToStudent {
	
	@Id
	@SequenceGenerator(name = "leoacademy_leo_hostelfeeassingtostudent", sequenceName = "hostelFeeAssingToStudentId", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "leoacademy_leo_hostelfeeassingtostudent")
	private Long hostelFeeAssingToStudentId;
	@Column
	private Long instid;
	@Column
	private BigDecimal paidAmount;
	@Column
	private BigDecimal netAmount;
	@Column
	private BigDecimal dueAmount;
	@Column
	@Temporal(TemporalType.DATE)
	private Date paidDate;
	@Column
	private Boolean action;
	@Column
	private BigDecimal discountAmount;
	@Column
	private BigDecimal termWiseAmount;
	@Column
	@Temporal(TemporalType.DATE)
	private Date dueDate;
	 @Column
	private Long installmentHostelFeeId;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="admissionNumberId")
	private StudentAdmissionEntity StudentAdmission;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="allt_Id")
	private RoomAllotment roomAllotment ;
	
	@Transient
	private Long batchId;
	@Transient
	private String termWiseMessage;
	@Column
	private String remarks;
	
	public RoomAllotment getRoomAllotment() {
		return roomAllotment;
	}
	public void setRoomAllotment(RoomAllotment roomAllotment) {
		this.roomAllotment = roomAllotment;
	}
	public Long getHostelFeeAssingToStudentId() {
		return hostelFeeAssingToStudentId;
	}
	public void setHostelFeeAssingToStudentId(Long hostelFeeAssingToStudentId) {
		this.hostelFeeAssingToStudentId = hostelFeeAssingToStudentId;
	}
	public Long getInstid() {
		return instid;
	}
	public void setInstid(Long instid) {
		this.instid = instid;
	}
	public BigDecimal getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}
	public BigDecimal getNetAmount() {
		return netAmount;
	}
	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}
	public BigDecimal getDueAmount() {
		return dueAmount;
	}
	public void setDueAmount(BigDecimal dueAmount) {
		this.dueAmount = dueAmount;
	}
	public Date getPaidDate() {
		return paidDate;
	}
	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}
	public Boolean getAction() {
		return action;
	}
	public void setAction(Boolean action) {
		this.action = action;
	}
	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}
	public BigDecimal getTermWiseAmount() {
		return termWiseAmount;
	}
	public void setTermWiseAmount(BigDecimal termWiseAmount) {
		this.termWiseAmount = termWiseAmount;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	
	public StudentAdmissionEntity getStudentAdmission() {
		return StudentAdmission;
	}
	public void setStudentAdmission(StudentAdmissionEntity studentAdmission) {
		StudentAdmission = studentAdmission;
	}
	public Long getInstallmentHostelFeeId() {
		return installmentHostelFeeId;
	}
	public void setInstallmentHostelFeeId(Long installmentHostelFeeId) {
		this.installmentHostelFeeId = installmentHostelFeeId;
	}
	public Long getBatchId() {
		return batchId;
	}
	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}
	public String getTermWiseMessage() {
		return termWiseMessage;
	}
	public void setTermWiseMessage(String termWiseMessage) {
		this.termWiseMessage = termWiseMessage;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
}
