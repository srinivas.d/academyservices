package com.leonia.academy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class RelationMaster {
	@Id
	@GeneratedValue
	private Long relationMasterId;
	
	@Column
	private String relationType;
	
	@Column
	private Boolean action;

	

	public Long getRelationMasterId() {
		return relationMasterId;
	}

	public void setRelationMasterId(Long relationMasterId) {
		this.relationMasterId = relationMasterId;
	}

	public String getRelationType() {
		return relationType;
	}

	public void setRelationType(String relationType) {
		this.relationType = relationType;
	}

	public Boolean getAction() {
		return action;
	}

	public void setAction(Boolean action) {
		this.action = action;
	}
	
	

}
