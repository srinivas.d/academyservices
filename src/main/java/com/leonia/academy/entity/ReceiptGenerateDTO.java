package com.leonia.academy.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


@Entity
@Table(name="leoacademy_tbl_receiptGenerate")
public class ReceiptGenerateDTO {
	
	@Id
	@SequenceGenerator(name = "leoacademy_tbl_receiptGenerate", sequenceName = "receiptGenerateId", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="leoacademy_tbl_receiptGenerate")
	private Long receiptGenerateId;
	@Column
	private BigDecimal  totalAmount;
	@Column
	private Long admissionNumberId;
	
	@Column
	@Temporal(TemporalType.DATE)
    private Date paidDate;
	
	@OneToMany(cascade=CascadeType.ALL,mappedBy="receiptGenerateDTO")
	private List<FeePaidTransactionsDTO> feePaidTransactionsDTO;
	
	@OneToMany(cascade=CascadeType.ALL,mappedBy="receiptGenerateDTO")
	private List<FeeSettlementDTO> feeSettlementDTO;
	
	@Transient
	public String admissionNumber;
	@Transient
	public String studentName;
	@Transient
	private String batchName;

	public List<FeePaidTransactionsDTO> getFeePaidTransactionsDTO() {
		return feePaidTransactionsDTO;
	}

	public void setFeePaidTransactionsDTO(List<FeePaidTransactionsDTO> feePaidTransactionsDTO) {
		this.feePaidTransactionsDTO = feePaidTransactionsDTO;
	}

	public List<FeeSettlementDTO> getFeeSettlementDTO() {
		return feeSettlementDTO;
	}

	public void setFeeSettlementDTO(List<FeeSettlementDTO> feeSettlementDTO) {
		this.feeSettlementDTO = feeSettlementDTO;
	}

	public String getBatchName() {
		return batchName;
	}

	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}

	public String getAdmissionNumber() {
		return admissionNumber;
	}

	public void setAdmissionNumber(String admissionNumber) {
		this.admissionNumber = admissionNumber;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public Date getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Long getAdmissionNumberId() {
		return admissionNumberId;
	}

	public void setAdmissionNumberId(Long admissionNumberId) {
		this.admissionNumberId = admissionNumberId;
	}

	public Long getReceiptGenerateId() {
		return receiptGenerateId;
	}

	public void setReceiptGenerateId(Long receiptGenerateId) {
		this.receiptGenerateId = receiptGenerateId;
	}
}
