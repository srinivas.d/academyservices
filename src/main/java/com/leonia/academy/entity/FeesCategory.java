package com.leonia.academy.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table
public class FeesCategory {
	
	@Id
	@GeneratedValue
	private Long feesCategoryId;
	
	@Column
	private String feesCategoryName;
	
	@Column
	private String feesShortForm;
	
	public String getFeesShortForm() {
		return feesShortForm;
	}

	public void setFeesShortForm(String feesShortForm) {
		this.feesShortForm = feesShortForm;
	}

	@Column
	private Boolean action;
	
	
	public Long getFeesCategoryId() {
		return feesCategoryId;
	}

	public void setFeesCategoryId(Long feesCategoryId) {
		this.feesCategoryId = feesCategoryId;
	}

	public Boolean getAction() {
		return action;
	}

	public void setAction(Boolean action) {
		this.action = action;
	}

	public String getFeesCategoryName() {
		return feesCategoryName;
	}

	public void setFeesCategoryName(String feesCategoryName) {
		this.feesCategoryName = feesCategoryName;
	}

	
	
	
	
	
		
}
