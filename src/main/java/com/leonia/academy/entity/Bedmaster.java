package com.leonia.academy.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="bedmaster")
public class Bedmaster {
	@Id
	@SequenceGenerator(name = "bedmasterSEQ", sequenceName = "bedid_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="bedmasterSEQ")
	@Column(name="bed_id")
	private Integer bedid;
	@Column
	private String bedcode;
	@Column
	private Date createdOn;
	@Column
	private String bedstatus;
	@Column
	private boolean status;
	@ManyToOne
	@JoinColumn(name="roomId")
	private Roommaster roommaster;
	public Integer getBedid() {
		return bedid;
	}
	public void setBedid(Integer bedid) {
		this.bedid = bedid;
	}
	public String getBedcode() {
		return bedcode;
	}
	public void setBedcode(String bedcode) {
		this.bedcode = bedcode;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	public String getBedstatus() {
		return bedstatus;
	}
	public void setBedstatus(String bedstatus) {
		this.bedstatus = bedstatus;
	}
	public Roommaster getRoommaster() {
		return roommaster;
	}
	public void setRoommaster(Roommaster roommaster) {
		this.roommaster = roommaster;
	}
	
	
}
