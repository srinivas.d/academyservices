package com.leonia.academy.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class ReligionMaster {
	@Id
	@GeneratedValue
	private Long religionId;
	
	@Column
	private String religionType;
	
	@Column
	private Boolean action;
	

	public Long getReligionId() {
		return religionId;
	}

	public void setReligionId(Long religionId) {
		this.religionId = religionId;
	}

	public String getReligionType() {
		return religionType;
	}

	public void setReligionType(String religionType) {
		this.religionType = religionType;
	}

	public Boolean getAction() {
		return action;
	}

	public void setAction(Boolean action) {
		this.action = action;
	}

	
	
}
