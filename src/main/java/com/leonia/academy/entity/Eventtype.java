package com.leonia.academy.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="leoacademy_tbl_eventtype")
public class Eventtype {
	
	@Id
	@GeneratedValue
	private Long eventid;
	
	private String eventtype;
	
	private String color;
	
	private String 	textColor;
	
	@OneToMany(mappedBy="eventtype")
	private List<CalenderEvents> calenderEvents;

	public Long getEventid() {
		return eventid;
	}

	public void setEventid(Long eventid) {
		this.eventid = eventid;
	}

	public String getEventtype() {
		return eventtype;
	}

	public void setEventtype(String eventtype) {
		this.eventtype = eventtype;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getTextColor() {
		return textColor;
	}

	public void setTextColor(String textColor) {
		this.textColor = textColor;
	}

	public List<CalenderEvents> getCalenderEvents() {
		return calenderEvents;
	}

	public void setCalenderEvents(List<CalenderEvents> calenderEvents) {
		this.calenderEvents = calenderEvents;
	}
	
}
