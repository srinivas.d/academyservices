package com.leonia.academy.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="leoacademy_tbl_installmenttypemaster")
public class InstallmentTypeMaster {
	@GeneratedValue
	@Id
	private Long installmentTypeMasterId;
	
	@ManyToOne
	@JoinColumn(name="batchId")
	private BatchMaster batchMaster;
	
	@ManyToOne
	@JoinColumn(name="installmentTypesId")
	private InstallmentTypes installmentTypes;
	
	@OneToMany(mappedBy="installmentTypeMaster")
	private List<FeeGenerationBatchWise> feeGenerationBatchWise;

	@OneToMany(mappedBy="installmentTypeMaster")
	private List<FeeAssignToStudents> feeAssignToStudents;
	
	
	public Long getInstallmentTypeMasterId() {
		return installmentTypeMasterId;
	}

	public void setInstallmentTypeMasterId(Long installmentTypeMasterId) {
		this.installmentTypeMasterId = installmentTypeMasterId;
	}

	public BatchMaster getBatchMaster() {
		return batchMaster;
	}

	public void setBatchMaster(BatchMaster batchMaster) {
		this.batchMaster = batchMaster;
	}

	public InstallmentTypes getInstallmentTypes() {
		return installmentTypes;
	}

	public void setInstallmentTypes(InstallmentTypes installmentTypes) {
		this.installmentTypes = installmentTypes;
	}

	public List<FeeGenerationBatchWise> getFeeGenerationBatchWise() {
		return feeGenerationBatchWise;
	}

	public void setFeeGenerationBatchWise(List<FeeGenerationBatchWise> feeGenerationBatchWise) {
		this.feeGenerationBatchWise = feeGenerationBatchWise;
	}
	
	
	
	
	
}
