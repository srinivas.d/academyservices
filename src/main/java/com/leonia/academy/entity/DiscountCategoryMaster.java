package com.leonia.academy.entity;


import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class DiscountCategoryMaster {
	@Id
	@GeneratedValue
	private Long discountCategoryId;
	@Column
	private String discountCategoryName;
	@Column
	private Boolean action;
	
	/*@OneToMany(mappedBy="discountCategoryMaster")
	private List<FeeAssignToStudents> feeAssignToStudents;
	
	@OneToMany(mappedBy="discountCategoryMaster")
	private List<HostelFeeAssingToStudent> hostelFeeAssingToStudent;*/
	
	public Long getDiscountCategoryId() {
		return discountCategoryId;
	}
	public void setDiscountCategoryId(Long discountCategoryId) {
		this.discountCategoryId = discountCategoryId;
	}
	public String getDiscountCategoryName() {
		return discountCategoryName;
	}
	public void setDiscountCategoryName(String discountCategoryName) {
		this.discountCategoryName = discountCategoryName;
	}
	public Boolean getAction() {
		return action;
	}
	public void setAction(Boolean action) {
		this.action = action;
	}
	/*public List<FeeAssignToStudents> getFeeAssignToStudents() {
		return feeAssignToStudents;
	}
	public void setFeeAssignToStudents(List<FeeAssignToStudents> feeAssignToStudents) {
		this.feeAssignToStudents = feeAssignToStudents;
	}
	public List<HostelFeeAssingToStudent> getHostelFeeAssingToStudent() {
		return hostelFeeAssingToStudent;
	}
	public void setHostelFeeAssingToStudent(List<HostelFeeAssingToStudent> hostelFeeAssingToStudent) {
		this.hostelFeeAssingToStudent = hostelFeeAssingToStudent;
	}*/
	
	
}
