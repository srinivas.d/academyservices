
package com.leonia.academy.entity;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name="leadfollowupdetails")
public class LeadfollowupdetailsDTO {
	@Id
	@GeneratedValue
	private Integer leadfollowupdetailsId;
	@Column
	private String studentName;
	@Column
	private String email;
	@Column
	private String course;
	@Column
	private String phoneNumber;
	@Column
	private String source;
	@Column
	@Temporal(TemporalType.DATE)
	private Date followupDate;
	@Column
	private String reason;
	@Column
	private String followupperson;
	@Column
	private int active;
	@Column
	private String time;
	@Transient
	private String startDate;
	@Transient
	private String endDate;
	@Transient
	private String seachname;
	@Transient
	private String visitdate;
	@Transient
	private String selectdate;
	
	@ManyToOne
	@JoinColumn(name="followUpId")
	private LeadFollowUpDTO leadFollowUpDTO;
	
	public String getSelectdate() {
		return selectdate;
	}
	public void setSelectdate(String selectdate) {
		this.selectdate = selectdate;
	}
	public String getSeachname() {
		return seachname;
	}
	public void setSeachname(String seachname) {
		this.seachname = seachname;
	}
	public String getVisitdate() {
		return visitdate;
	}
	public void setVisitdate(String visitdate) {
		this.visitdate = visitdate;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public Date getFollowupDate() {
		return followupDate;
	}
	public void setFollowupDate(Date followupDate) {
		this.followupDate = followupDate;
	}
	public LeadFollowUpDTO getLeadFollowUpDTO() {
		return leadFollowUpDTO;
	}
	public void setLeadFollowUpDTO(LeadFollowUpDTO leadFollowUpDTO) {
		this.leadFollowUpDTO = leadFollowUpDTO;
	}
	
	public int getActive() {
		return active;
	}
	public void setActive(int active) {
		this.active = active;
	}
	public Integer getLeadfollowupdetailsId() {
		return leadfollowupdetailsId;
	}
	public void setLeadfollowupdetailsId(Integer leadfollowupdetailsId) {
		this.leadfollowupdetailsId = leadfollowupdetailsId;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCourse() {
		return course;
	}
	public void setCourse(String course) {
		this.course = course;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getFollowupperson() {
		return followupperson;
	}
	public void setFollowupperson(String followupperson) {
		this.followupperson = followupperson;
	}
}
