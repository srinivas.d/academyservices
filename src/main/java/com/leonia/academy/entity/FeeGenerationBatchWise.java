package com.leonia.academy.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table
public class FeeGenerationBatchWise {
	@GeneratedValue
	@Id
	private Long feeGenerationBatchWiseId;
	
	@Column
	private Long batchId;
	@Column
	private String termWiseMessage;
	
	@ManyToOne
	@JoinColumn(name = "feeCreationId")
	private FeeCreation feeCreation;
	
	
	@ManyToOne
	@JoinColumn(name="installmentTypeMasterId")
	private InstallmentTypeMaster installmentTypeMaster;
	/*@Column
	private Long feeCreationId;*/
	
	@Column
	private BigDecimal termWiseAmount;
	@Column
	@Temporal(TemporalType.DATE)
	private Date dueDate;
	@Column
	private Boolean action;
	
	@Transient
	private String batchName;
	
	public String getBatchName() {
		return batchName;
	}
	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}
	public Long getFeeGenerationBatchWiseId() {
		return feeGenerationBatchWiseId;
	}
	public void setFeeGenerationBatchWiseId(Long feeGenerationBatchWiseId) {
		this.feeGenerationBatchWiseId = feeGenerationBatchWiseId;
	}
	public Long getBatchId() {
		return batchId;
	}
	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}
	public String getTermWiseMessage() {
		return termWiseMessage;
	}
	public void setTermWiseMessage(String termWiseMessage) {
		this.termWiseMessage = termWiseMessage;
	}
	
	/*public Long getFeeCreationId() {
		return feeCreationId;
	}
	public void setFeeCreationId(Long feeCreationId) {
		this.feeCreationId = feeCreationId;
	}*/
	public BigDecimal getTermWiseAmount() {
		return termWiseAmount;
	}
	public void setTermWiseAmount(BigDecimal termWiseAmount) {
		this.termWiseAmount = termWiseAmount;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public Boolean getAction() {
		return action;
	}
	public void setAction(Boolean action) {
		this.action = action;
	}
	public FeeCreation getFeeCreation() {
		return feeCreation;
	}
	public void setFeeCreation(FeeCreation feeCreation) {
		this.feeCreation = feeCreation;
	}
	public InstallmentTypeMaster getInstallmentTypeMaster() {
		return installmentTypeMaster;
	}
	public void setInstallmentTypeMaster(InstallmentTypeMaster installmentTypeMaster) {
		this.installmentTypeMaster = installmentTypeMaster;
	}
	
	
	

}
