package com.leonia.academy.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class CommunityMaster {
	@Id
	@GeneratedValue
	private Long communityId;
	@Column
	private String communityType;
	@Column
	private Boolean action;
	
	public Long getCommunityId() {
		return communityId;
	}
	public void setCommunityId(Long communityId) {
		this.communityId = communityId;
	}
	public String getCommunityType() {
		return communityType;
	}
	public void setCommunityType(String communityType) {
		this.communityType = communityType;
	}
	public Boolean getAction() {
		return action;
	}
	public void setAction(Boolean action) {
		this.action = action;
	}
	
	
}
