package com.leonia.academy.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name="roomcategory")
public class RoomCategory {
	
@Id
@SequenceGenerator(name = "roomcategorySEQ", sequenceName = "categoryid_seq", allocationSize = 1)
@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="roomcategorySEQ")	
@Column(name="categoryid")
private Integer categoryid;
@Column
private String categoryname;
@Column
private double categoryfee;

@OneToMany(cascade=CascadeType.ALL,mappedBy="roomCategory")
private List<Roommaster> roommaster;

public Integer getCategoryid() {
	return categoryid;
}
public void setCategoryid(Integer categoryid) {
	this.categoryid = categoryid;
}
public String getCategoryname() {
	return categoryname;
}
public void setCategoryname(String categoryname) {
	this.categoryname = categoryname;
}
public double getCategoryfee() {
	return categoryfee;
}
public void setCategoryfee(double categoryfee) {
	this.categoryfee = categoryfee;
}
public List<Roommaster> getRoommaster() {
	return roommaster;
}
public void setRoommaster(List<Roommaster> roommaster) {
	this.roommaster = roommaster;
}

}
