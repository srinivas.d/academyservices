package com.leonia.academy.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Table(name="leoacademy_tbl_placementdesignationmaster")
@Entity
public class PlacementDesignationMaster {
	@Id
	@SequenceGenerator(name="seq",sequenceName="designationMasterId",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="seq")
	private Long designationMasterId;
	
	private String designationName;
	private String designationShortName;
	private Boolean status;
	
	@ManyToOne
	@JoinColumn(name="departmentMasterId")
	private PlacementDepartmentMaster departmentMaster;
	
	public Long getDesignationMasterId() {
		return designationMasterId;
	}
	public void setDesignationMasterId(Long designationMasterId) {
		this.designationMasterId = designationMasterId;
	}
	public String getDesignationName() {
		return designationName;
	}
	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}
	public String getDesignationShortName() {
		return designationShortName;
	}
	public void setDesignationShortName(String designationShortName) {
		this.designationShortName = designationShortName;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	

}
