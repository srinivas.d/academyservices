package com.leonia.academy.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class FeeCategoryMaster {
	@Id
	@GeneratedValue
	private Long feeCategoryMasterId;
	
	@Column
	private String feeCategoryMasterType;
	
	@Column
	private Boolean action;

	public Long getFeeCategoryMasterId() {
		return feeCategoryMasterId;
	}

	public void setFeeCategoryMasterId(Long feeCategoryMasterId) {
		this.feeCategoryMasterId = feeCategoryMasterId;
	}

	public String getFeeCategoryMasterType() {
		return feeCategoryMasterType;
	}

	public void setFeeCategoryMasterType(String feeCategoryMasterType) {
		this.feeCategoryMasterType = feeCategoryMasterType;
	}

	public Boolean getAction() {
		return action;
	}

	public void setAction(Boolean action) {
		this.action = action;
	}

	
}
