package com.leonia.academy.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="leoacademy_tbl_statusmaster")
public class StatusMaster {
	@Id
	@SequenceGenerator(name="seq" , sequenceName="", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="seq")
	private Long statusId;
	private String statusName;
	public Long getStatusId() {
		return statusId;
	}
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	
	
	
	

}
