package com.leonia.academy.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name="VisitEntry")
public class VisitEntryEntity {
	@Id
	@SequenceGenerator(name = "visitEntryIdSEQ", sequenceName = "visitEntryId_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="visitEntryIdSEQ")
	private Integer visitEntryId;
	
	@Temporal(TemporalType.DATE)
	private Date visitDate;
	private String visitTime;
	private String instituteName;
	
	private String visitPurpose;
	private String contactPerson;
	private String meetingNotes;
	private String visitRating;
	private String contactInfo;
	
	@Transient
	private String fromDate;
	@Transient
	private String toDate;
	@Transient
	private String visitPurposeName;
	
	@Transient
	private String visitRatingName;
	
	@Temporal(TemporalType.DATE)
	private Date nextVisitDate;
	private String nextVisitTime;
	
	public Integer getVisitEntryId() {
		return visitEntryId;
	}
	public void setVisitEntryId(Integer visitEntryId) {
		this.visitEntryId = visitEntryId;
	}
	public Date getVisitDate() {
		return visitDate;
	}
	public void setVisitDate(Date visitDate) {
		this.visitDate = visitDate;
	}
	public String getVisitTime() {
		return visitTime;
	}
	public void setVisitTime(String visitTime) {
		this.visitTime = visitTime;
	}
	public String getInstituteName() {
		return instituteName;
	}
	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}
	public String getVisitPurpose() {
		return visitPurpose;
	}
	public void setVisitPurpose(String visitPurpose) {
		this.visitPurpose = visitPurpose;
	}
	public String getContactPerson() {
		return contactPerson;
	}
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	public String getMeetingNotes() {
		return meetingNotes;
	}
	public void setMeetingNotes(String meetingNotes) {
		this.meetingNotes = meetingNotes;
	}
	public String getVisitRating() {
		return visitRating;
	}
	public void setVisitRating(String visitRating) {
		this.visitRating = visitRating;
	}
	public Date getNextVisitDate() {
		return nextVisitDate;
	}
	public void setNextVisitDate(Date nextVisitDate) {
		this.nextVisitDate = nextVisitDate;
	}
	public String getNextVisitTime() {
		return nextVisitTime;
	}
	public void setNextVisitTime(String nextVisitTime) {
		this.nextVisitTime = nextVisitTime;
	}
	public String getContactInfo() {
		return contactInfo;
	}
	public void setContactInfo(String contactInfo) {
		this.contactInfo = contactInfo;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getVisitPurposeName() {
		return visitPurposeName;
	}
	public void setVisitPurposeName(String visitPurposeName) {
		this.visitPurposeName = visitPurposeName;
	}
	public String getVisitRatingName() {
		return visitRatingName;
	}
	public void setVisitRatingName(String visitRatingName) {
		this.visitRatingName = visitRatingName;
	}
	
	
	
}
