package com.leonia.academy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="educationdetails")
public class EducationEntity {
	
	@Id
	@SequenceGenerator(name = "educationSEQ", sequenceName = "education_id_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="educationSEQ")
	@Column(name="education_id")
	private int educationId;
	@Column
	private String sscboardname;
	@Column
	private String ssccoursename;
	@Column
	private String sscyearofpassing;
	@Column
	private Integer sscmarkobtained;
	@Column
	private Double sscpercentage;
	
	@Column
	private String interboardname;
	@Column
	private String intercoursename;
	@Column
	private String interyearofpassing;
	@Column
	private Integer intermarkobtained;
	@Column
	private Double interpercentage;
	
	@Column
	private String degreeboardname;
	@Column
	private String degreecoursename;
	@Column
	private String degreeyearofpassing;
	@Column
	private Integer degreemarkobtained;
	@Column
	private Double degreepercentage;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="admissionNumberId")
	private StudentAdmissionEntity  studentAdmissionEntity;
	
	public int getEducationId() {
		return educationId;
	}
	public void setEducationId(int educationId) {
		this.educationId = educationId;
	}
	public String getSscboardname() {
		return sscboardname;
	}
	public void setSscboardname(String sscboardname) {
		this.sscboardname = sscboardname;
	}
	public String getSsccoursename() {
		return ssccoursename;
	}
	public void setSsccoursename(String ssccoursename) {
		this.ssccoursename = ssccoursename;
	}
	public String getSscyearofpassing() {
		return sscyearofpassing;
	}
	public void setSscyearofpassing(String sscyearofpassing) {
		this.sscyearofpassing = sscyearofpassing;
	}
	public Integer getSscmarkobtained() {
		return sscmarkobtained;
	}
	public void setSscmarkobtained(Integer sscmarkobtained) {
		this.sscmarkobtained = sscmarkobtained;
	}
	public Double getSscpercentage() {
		return sscpercentage;
	}
	public void setSscpercentage(Double sscpercentage) {
		this.sscpercentage = sscpercentage;
	}
	public String getInterboardname() {
		return interboardname;
	}
	public void setInterboardname(String interboardname) {
		this.interboardname = interboardname;
	}
	public String getIntercoursename() {
		return intercoursename;
	}
	public void setIntercoursename(String intercoursename) {
		this.intercoursename = intercoursename;
	}
	public String getInteryearofpassing() {
		return interyearofpassing;
	}
	public void setInteryearofpassing(String interyearofpassing) {
		this.interyearofpassing = interyearofpassing;
	}
	public Integer getIntermarkobtained() {
		return intermarkobtained;
	}
	public void setIntermarkobtained(Integer intermarkobtained) {
		this.intermarkobtained = intermarkobtained;
	}
	public Double getInterpercentage() {
		return interpercentage;
	}
	public void setInterpercentage(Double interpercentage) {
		this.interpercentage = interpercentage;
	}
	public String getDegreeboardname() {
		return degreeboardname;
	}
	public void setDegreeboardname(String degreeboardname) {
		this.degreeboardname = degreeboardname;
	}
	public String getDegreecoursename() {
		return degreecoursename;
	}
	public void setDegreecoursename(String degreecoursename) {
		this.degreecoursename = degreecoursename;
	}
	public String getDegreeyearofpassing() {
		return degreeyearofpassing;
	}
	public void setDegreeyearofpassing(String degreeyearofpassing) {
		this.degreeyearofpassing = degreeyearofpassing;
	}
	public Integer getDegreemarkobtained() {
		return degreemarkobtained;
	}
	public void setDegreemarkobtained(Integer degreemarkobtained) {
		this.degreemarkobtained = degreemarkobtained;
	}
	public Double getDegreepercentage() {
		return degreepercentage;
	}
	public void setDegreepercentage(Double degreepercentage) {
		this.degreepercentage = degreepercentage;
	}
	public StudentAdmissionEntity getStudentAdmissionEntity() {
		return studentAdmissionEntity;
	}
	public void setStudentAdmissionEntity(StudentAdmissionEntity studentAdmissionEntity) {
		this.studentAdmissionEntity = studentAdmissionEntity;
	}
	

	
	
}
