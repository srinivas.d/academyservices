package com.leonia.academy.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="roomallotment")
public class RoomAllotment {
	@Id
	@SequenceGenerator(name = "roomallotmentSEQ", sequenceName = "alltId_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="roomallotmentSEQ")
	@Column(name="alltId")
	private Integer alltId;
	private String allotmentCode;
	private Integer bedId;
	private Integer roomId;
	private Integer floorId;
	private Integer blockId;
	private Integer buildingId;
	private Integer categoryid;
	private Date allotdatetime;
	private String itemsallocated;
	private String roomstatus;
	private Integer status;
	@ManyToOne
	@JoinColumn(name="admissionNumberId")
	private StudentAdmissionEntity studentAdmission;
	
	@OneToMany(mappedBy="roomAllotment")
	private List<HostelFeeAssingToStudent> hostelFeeAssingToStudent;
	
	public Integer getAlltId() {
		return alltId;
	}
	
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public void setAlltId(Integer alltId) {
		this.alltId = alltId;
	}
	public String getAllotmentCode() {
		return allotmentCode;
	}
	public void setAllotmentCode(String allotmentCode) {
		this.allotmentCode = allotmentCode;
	}
	
	public Integer getBedId() {
		return bedId;
	}
	public void setBedId(Integer bedId) {
		this.bedId = bedId;
	}
	public Integer getRoomId() {
		return roomId;
	}
	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}
	public Integer getFloorId() {
		return floorId;
	}
	public void setFloorId(Integer floorId) {
		this.floorId = floorId;
	}
	public Integer getBlockId() {
		return blockId;
	}
	public void setBlockId(Integer blockId) {
		this.blockId = blockId;
	}
	public Integer getBuildingId() {
		return buildingId;
	}
	public void setBuildingId(Integer buildingId) {
		this.buildingId = buildingId;
	}
	
	
	
	public Integer getCategoryid() {
		return categoryid;
	}
	public void setCategoryid(Integer categoryid) {
		this.categoryid = categoryid;
	}
	public StudentAdmissionEntity getStudentAdmission() {
		return studentAdmission;
	}

	public void setStudentAdmission(StudentAdmissionEntity studentAdmission) {
		this.studentAdmission = studentAdmission;
	}

	public List<HostelFeeAssingToStudent> getHostelFeeAssingToStudent() {
		return hostelFeeAssingToStudent;
	}

	public void setHostelFeeAssingToStudent(List<HostelFeeAssingToStudent> hostelFeeAssingToStudent) {
		this.hostelFeeAssingToStudent = hostelFeeAssingToStudent;
	}

	public Date getAllotdatetime() {
		return allotdatetime;
	}

	public void setAllotdatetime(Date allotdatetime) {
		this.allotdatetime = allotdatetime;
	}

	public String getItemsallocated() {
		return itemsallocated;
	}

	public void setItemsallocated(String itemsallocated) {
		this.itemsallocated = itemsallocated;
	}

	public String getRoomstatus() {
		return roomstatus;
	}

	public void setRoomstatus(String roomstatus) {
		this.roomstatus = roomstatus;
	}
		
}