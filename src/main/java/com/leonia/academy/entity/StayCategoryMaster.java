package com.leonia.academy.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class StayCategoryMaster {
	@Id
	@GeneratedValue
	private Long stayCategoryMasterId;
	
	@Column
	private String stayCategoryMasterType;
	
	@Column
	private Boolean action;

	public Long getStayCategoryMasterId() {
		return stayCategoryMasterId;
	}

	public void setStayCategoryMasterId(Long stayCategoryMasterId) {
		this.stayCategoryMasterId = stayCategoryMasterId;
	}

	public String getStayCategoryMasterType() {
		return stayCategoryMasterType;
	}

	public void setStayCategoryMasterType(String stayCategoryMasterType) {
		this.stayCategoryMasterType = stayCategoryMasterType;
	}

	public Boolean getAction() {
		return action;
	}

	public void setAction(Boolean action) {
		this.action = action;
	}

}
