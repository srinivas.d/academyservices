package com.leonia.academy.entity;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonBackReference;

@Entity
@Table(name="dropdownsubmaster")
public class DropDownSubMasterEntity implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Integer dropDownSubMasterId;
	@Column
	private String description;
	
	@Column
	private Integer dropdownmasterId;

	
	@ManyToOne
	@JoinColumn(name="masterId")
	private DropDownMasterEntity downMasterEntity;
	
	public DropDownMasterEntity getDownMasterEntity() {
		return downMasterEntity;
	}
	public void setDownMasterEntity(DropDownMasterEntity downMasterEntity) {
		this.downMasterEntity = downMasterEntity;
	}
	public Integer getDropDownSubMasterId() {
		return dropDownSubMasterId;
	}
	public void setDropDownSubMasterId(Integer dropDownSubMasterId) {
		this.dropDownSubMasterId = dropDownSubMasterId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getDropdownmasterId() {
		return dropdownmasterId;
	}
	public void setDropdownmasterId(Integer dropdownmasterId) {
		this.dropdownmasterId = dropdownmasterId;
	}
	

}
