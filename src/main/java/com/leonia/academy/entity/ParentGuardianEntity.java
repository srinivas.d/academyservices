package com.leonia.academy.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Table(name="parentguardian")
@Entity
public class ParentGuardianEntity {
	@Id
	@GeneratedValue
	private Long parentGuardianId;
	@Column
	private String parentFirstName;
	@Column
	private String parentSureName;
	@Column
	private Long relationWithStudent;
	@Column
	@Temporal(TemporalType.DATE)
	private Date parentDateOfBirth;
	@Column
	private String parentEducation;
	@Column
	private String parentOccupation;
	@Column
	private String parentAnnualIncomeInLakhs;
	@Column
	private String parentEmail;
	@Column
	private String parentAddressLine1;
	@Column
	private String parentAddressLine2;
	@Column
	private String city;
	@Column
	private String state;
	@Column
	private String country;
	@Column
	private String parentPhoneNumber1;
	@Column
	private String parentPhoneNumber2;
	@Column
	private String parentMobileNumber;
	@Column
	private String parentType;
	@Column
	private Boolean action;
	
	@Column
	private String impatient;
	
	
    private Long admissionNumberId;
	
	public Long getAdmissionNumberId() {
		return admissionNumberId;
	}
	public void setAdmissionNumberId(Long admissionNumberId) {
		this.admissionNumberId = admissionNumberId;
	}
	/*@ManyToOne
	@JoinColumn(name="admissionNumberId")
	private StudentAdmissionEntity studentAdmissionEntity;
	
	public StudentAdmissionEntity getStudentAdmissionEntity() {
		return studentAdmissionEntity;
	}
	public void setStudentAdmissionEntity(StudentAdmissionEntity studentAdmissionEntity) {
		this.studentAdmissionEntity = studentAdmissionEntity;
	}*/
	public Boolean getAction() {
		return action;
	}
	public void setAction(Boolean action) {
		this.action = action;
	}
	public String getParentType() {
		return parentType;
	}
	public void setParentType(String parentType) {
		this.parentType = parentType;
	}
	public Long getParentGuardianId() {
		return parentGuardianId;
	}
	public void setParentGuardianId(Long parentGuardianId) {
		this.parentGuardianId = parentGuardianId;
	}
	public String getParentFirstName() {
		return parentFirstName;
	}
	public void setParentFirstName(String parentFirstName) {
		this.parentFirstName = parentFirstName;
	}
	
	public String getParentSureName() {
		return parentSureName;
	}
	public void setParentSureName(String parentSureName) {
		this.parentSureName = parentSureName;
	}
	
	
	public Long getRelationWithStudent() {
		return relationWithStudent;
	}
	public void setRelationWithStudent(Long relationWithStudent) {
		this.relationWithStudent = relationWithStudent;
	}
	public String getParentEducation() {
		return parentEducation;
	}
	public void setParentEducation(String parentEducation) {
		this.parentEducation = parentEducation;
	}
	public String getParentOccupation() {
		return parentOccupation;
	}
	public void setParentOccupation(String parentOccupation) {
		this.parentOccupation = parentOccupation;
	}
	public String getParentAnnualIncomeInLakhs() {
		return parentAnnualIncomeInLakhs;
	}
	public void setParentAnnualIncomeInLakhs(String parentAnnualIncomeInLakhs) {
		this.parentAnnualIncomeInLakhs = parentAnnualIncomeInLakhs;
	}
	public String getParentEmail() {
		return parentEmail;
	}
	public void setParentEmail(String parentEmail) {
		this.parentEmail = parentEmail;
	}
	public String getParentAddressLine1() {
		return parentAddressLine1;
	}
	public void setParentAddressLine1(String parentAddressLine1) {
		this.parentAddressLine1 = parentAddressLine1;
	}
	public String getParentAddressLine2() {
		return parentAddressLine2;
	}
	public void setParentAddressLine2(String parentAddressLine2) {
		this.parentAddressLine2 = parentAddressLine2;
	}
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getParentPhoneNumber1() {
		return parentPhoneNumber1;
	}
	public void setParentPhoneNumber1(String parentPhoneNumber1) {
		this.parentPhoneNumber1 = parentPhoneNumber1;
	}
	public String getParentPhoneNumber2() {
		return parentPhoneNumber2;
	}
	public void setParentPhoneNumber2(String parentPhoneNumber2) {
		this.parentPhoneNumber2 = parentPhoneNumber2;
	}
	public String getParentMobileNumber() {
		return parentMobileNumber;
	}
	public void setParentMobileNumber(String parentMobileNumber) {
		this.parentMobileNumber = parentMobileNumber;
	}
	 
	public Date getParentDateOfBirth() {
		return parentDateOfBirth;
	}
	public void setParentDateOfBirth(Date parentDateOfBirth) {
		this.parentDateOfBirth = parentDateOfBirth;
	}
	public String getImpatient() {
		return impatient;
	}
	public void setImpatient(String impatient) {
		this.impatient = impatient;
	}
	
	
	/*public String getGuardian1FirstName() {
		return guardian1FirstName;
	}
	public void setGuardian1FirstName(String guardian1FirstName) {
		this.guardian1FirstName = guardian1FirstName;
	}
	public String getGuardian1SureName() {
		return guardian1SureName;
	}
	public void setGuardian1SureName(String guardian1SureName) {
		this.guardian1SureName = guardian1SureName;
	}
	public String getGuardian1relationWithStudent() {
		return guardian1relationWithStudent;
	}
	public void setGuardian1relationWithStudent(String guardian1relationWithStudent) {
		this.guardian1relationWithStudent = guardian1relationWithStudent;
	}
	public String getGuardian1DateOfBirth() {
		return guardian1DateOfBirth;
	}
	public void setGuardian1DateOfBirth(String guardian1DateOfBirth) {
		this.guardian1DateOfBirth = guardian1DateOfBirth;
	}
	public String getGuardian1Education() {
		return guardian1Education;
	}
	public void setGuardian1Education(String guardian1Education) {
		this.guardian1Education = guardian1Education;
	}
	public String getGuardian1Occupation() {
		return guardian1Occupation;
	}
	public void setGuardian1Occupation(String guardian1Occupation) {
		this.guardian1Occupation = guardian1Occupation;
	}
	public String getGuardian1AnnualIncomeInLakhs() {
		return guardian1AnnualIncomeInLakhs;
	}
	public void setGuardian1AnnualIncomeInLakhs(String guardian1AnnualIncomeInLakhs) {
		this.guardian1AnnualIncomeInLakhs = guardian1AnnualIncomeInLakhs;
	}
	public String getGuardian1Email() {
		return guardian1Email;
	}
	public void setGuardian1Email(String guardian1Email) {
		this.guardian1Email = guardian1Email;
	}
	public String getGuardian1AddressLine1() {
		
		
		return guardian1AddressLine1;
	}
	public void setGuardian1AddressLine1(String guardian1AddressLine1) {
		this.guardian1AddressLine1 = guardian1AddressLine1;
	}
	public String getGuardian1AddressLine2() {
		return guardian1AddressLine2;
	}
	public void setGuardian1AddressLine2(String guardian1AddressLine2) {
		this.guardian1AddressLine2 = guardian1AddressLine2;
	}
	public String getCity1() {
		return city1;
	}
	public void setCity1(String city1) {
		this.city1 = city1;
	}
	public String getState1() {
		return state1;
	}
	public void setState1(String state1) {
		this.state1 = state1;
	}
	public String getCountry1() {
		return country1;
	}
	public void setCountry1(String country1) {
		this.country1 = country1;
	}
	public String getGuardian1PhoneNumber1() {
		return guardian1PhoneNumber1;
	}
	public void setGuardian1PhoneNumber1(String guardian1PhoneNumber1) {
		this.guardian1PhoneNumber1 = guardian1PhoneNumber1;
	}
	public String getGuardian1PhoneNumber2() {
		return guardian1PhoneNumber2;
	}
	public void setGuardian1PhoneNumber2(String guardian1PhoneNumber2) {
		this.guardian1PhoneNumber2 = guardian1PhoneNumber2;
	}
	public String getGuardian1MobileNumber() {
		return guardian1MobileNumber;
	}
	public void setGuardian1MobileNumber(String guardian1MobileNumber) {
		this.guardian1MobileNumber = guardian1MobileNumber;
	}
	public String getGuardian2FirstName() {
		return guardian2FirstName;
	}
	public void setGuardian2FirstName(String guardian2FirstName) {
		this.guardian2FirstName = guardian2FirstName;
	}
	public String getGuardian2SureName() {
		return guardian2SureName;
	}
	public void setGuardian2SureName(String guardian2SureName) {
		this.guardian2SureName = guardian2SureName;
	}
	public String getGuardian2relationWithStudent() {
		return guardian2relationWithStudent;
	}
	public void setGuardian2relationWithStudent(String guardian2relationWithStudent) {
		this.guardian2relationWithStudent = guardian2relationWithStudent;
	}
	public String getGuardian2DateOfBirth() {
		return guardian2DateOfBirth;
	}
	public void setGuardian2DateOfBirth(String guardian2DateOfBirth) {
		this.guardian2DateOfBirth = guardian2DateOfBirth;
	}
	public String getGuardian2Education() {
		return guardian2Education;
	}
	public void setGuardian2Education(String guardian2Education) {
		this.guardian2Education = guardian2Education;
	}
	public String getGuardian2Occupation() {
		return guardian2Occupation;
	}
	public void setGuardian2Occupation(String guardian2Occupation) {
		this.guardian2Occupation = guardian2Occupation;
	}
	public String getGuardian2AnnualIncomeInLakhs() {
		return guardian2AnnualIncomeInLakhs;
	}
	public void setGuardian2AnnualIncomeInLakhs(String guardian2AnnualIncomeInLakhs) {
		this.guardian2AnnualIncomeInLakhs = guardian2AnnualIncomeInLakhs;
	}
	public String getGuardian2Email() {
		return guardian2Email;
	}
	public void setGuardian2Email(String guardian2Email) {
		this.guardian2Email = guardian2Email;
	}
	public String getGuardian2AddressLine1() {
		return guardian2AddressLine1;
	}
	public void setGuardian2AddressLine1(String guardian2AddressLine1) {
		this.guardian2AddressLine1 = guardian2AddressLine1;
	}
	public String getGuardian2AddressLine2() {
		return guardian2AddressLine2;
	}
	public void setGuardian2AddressLine2(String guardian2AddressLine2) {
		this.guardian2AddressLine2 = guardian2AddressLine2;
	}
	public String getCity2() {
		return city2;
	}
	public void setCity2(String city2) {
		this.city2 = city2;
	}
	public String getState2() {
		return state2;
	}
	public void setState2(String state2) {
		this.state2 = state2;
	}
	public String getCountry2() {
		return country2;
	}
	public void setCountry2(String country2) {
		this.country2 = country2;
	}
	public String getGuardian2PhoneNumber1() {
		return guardian2PhoneNumber1;
	}
	public void setGuardian2PhoneNumber1(String guardian2PhoneNumber1) {
		this.guardian2PhoneNumber1 = guardian2PhoneNumber1;
	}
	public String getGuardian2PhoneNumber2() {
		return guardian2PhoneNumber2;
	}
	public void setGuardian2PhoneNumber2(String guardian2PhoneNumber2) {
		this.guardian2PhoneNumber2 = guardian2PhoneNumber2;
	}
	public String getGuardian2MobileNumber() {
		return guardian2MobileNumber;
	}
	public void setGuardian2MobileNumber(String guardian2MobileNumber) {
		this.guardian2MobileNumber = guardian2MobileNumber;
	}
	public String getImpatient() {
		return impatient;
	}
	public void setImpatient(String impatient) {
		this.impatient = impatient;
	}*/
	/*public String getImpatient1() {
		return impatient1;
	}
	public void setImpatient1(String impatient1) {
		this.impatient1 = impatient1;
	}
	public String getImpatient2() {
		return impatient2;
	}
	public void setImpatient2(String impatient2) {
		this.impatient2 = impatient2;
	}*/
	
}
