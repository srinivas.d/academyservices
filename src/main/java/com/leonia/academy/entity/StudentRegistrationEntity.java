package com.leonia.academy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="studentregistration")
public class StudentRegistrationEntity {
	@Id
	@GeneratedValue
	private Integer studentId;
	@Column
	private String studentName;
	@Column
	private String phoneNumber;
	@Column
	private String email;
	@Column
	private String course;
	@Column
	private String username;
	@Column
	private String password;
	@Column
	private String parentName;
	@Column
	private String parentContact;
	@Column
	private String updatedby;
	@Column
	private String updateDate;
	@Column
	private String updatedip;
	public Integer getStudentId() {
		return studentId;
	}
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCourse() {
		return course;
	}
	public void setCourse(String course) {
		this.course = course;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdatedip() {
		return updatedip;
	}
	public void setUpdatedip(String updatedip) {
		this.updatedip = updatedip;
	}

	
	
}
