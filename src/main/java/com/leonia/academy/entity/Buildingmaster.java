package com.leonia.academy.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="buildingmaster")
public class Buildingmaster {
	@Id
	@SequenceGenerator(name = "buildingmasterSEQ", sequenceName = "buildingid_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="buildingmasterSEQ")
	@Column(name="building_id")
	private Integer buildingId;
	@Column
	private String buildingName;
	@Column
	private String buildingCode;
	@Column
	private String gendertype;
	@Column
	private Date createdOn;
	@Column
	private boolean status;
	
	@OneToMany(cascade=CascadeType.ALL,mappedBy="buildingmaster")
	private List<Blockmaster> blockmaster;

	
	public Integer getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(Integer buildingId) {
		this.buildingId = buildingId;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public String getBuildingCode() {
		return buildingCode;
	}

	public void setBuildingCode(String buildingCode) {
		this.buildingCode = buildingCode;
	}

	public String getGendertype() {
		return gendertype;
	}

	public void setGendertype(String gendertype) {
		this.gendertype = gendertype;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public List<Blockmaster> getBlockmaster() {
		return blockmaster;
	}

	public void setBlockmaster(List<Blockmaster> blockmaster) {
		this.blockmaster = blockmaster;
	}
	
}
