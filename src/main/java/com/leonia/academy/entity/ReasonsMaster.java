package com.leonia.academy.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="leoacademy_tbl_reasonsmaster")
public class ReasonsMaster {
	@Id
	@SequenceGenerator(name="seq",sequenceName="reasonsMasterId" ,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="seq")
	private Long reasonsMasterId;
	
	private String reasonsName;
	private Boolean status;
	
	@ManyToOne
	@JoinColumn(name="departmentMasterId")
	private PlacementDepartmentMaster departmentMaster;

	@OneToMany(cascade = CascadeType.ALL,mappedBy="reasonsMaster")
	private List<InterviewAssignToStudents> interviewAssignToStudents;
	
	public Long getReasonsMasterId() {
		return reasonsMasterId;
	}
	public void setReasonsMasterId(Long reasonsMasterId) {
		this.reasonsMasterId = reasonsMasterId;
	}
	public String getReasonsName() {
		return reasonsName;
	}
	public void setReasonsName(String reasonsName) {
		this.reasonsName = reasonsName;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public PlacementDepartmentMaster getDepartmentMaster() {
		return departmentMaster;
	}
	public void setDepartmentMaster(PlacementDepartmentMaster departmentMaster) {
		this.departmentMaster = departmentMaster;
	}
	

}
