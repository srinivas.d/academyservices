package com.leonia.academy.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="leoacademy_tbl_companydetails")
public class CompanyDetails {
	
	@Id
	@SequenceGenerator(name = "seq", sequenceName = "companyDetailsId", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="seq")
	private Long companyDetailsId;
	private String companyName;
	private String companyShortName;
	private String companyAddress;
	private String companyPhone;
	private String companyDomainName;
	private String companyContactPerson;
	private String companyEmail;
	private String companyLandMark;
	private Long domainMasterId;
	private String otherInfo;
	
	@OneToMany(fetch = FetchType.LAZY,mappedBy="companyDetails",cascade=CascadeType.ALL)
	private List<EmployerContactInformation> employerContactInformation = new ArrayList<>();
	
	@OneToMany(cascade=CascadeType.ALL,mappedBy="companyDetails")
	private List<VacancyInfo> vacancyInfo;
	
	public List<VacancyInfo> getVacancyInfo() {
		return vacancyInfo;
	}
	public void setVacancyInfo(List<VacancyInfo> vacancyInfo) {
		this.vacancyInfo = vacancyInfo;
	}
	public Long getCompanyDetailsId() {
		return companyDetailsId;
	}
	public void setCompanyDetailsId(Long companyDetailsId) {
		this.companyDetailsId = companyDetailsId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanyAddress() {
		return companyAddress;
	}
	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}
	public String getCompanyDomainName() {
		return companyDomainName;
	}
	public void setCompanyDomainName(String companyDomainName) {
		this.companyDomainName = companyDomainName;
	}
	public String getCompanyContactPerson() {
		return companyContactPerson;
	}
	public void setCompanyContactPerson(String companyContactPerson) {
		this.companyContactPerson = companyContactPerson;
	}
	
	public String getCompanyEmail() {
		return companyEmail;
	}
	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}
	public String getCompanyShortName() {
		return companyShortName;
	}
	public void setCompanyShortName(String companyShortName) {
		this.companyShortName = companyShortName;
	}
	public String getCompanyPhone() {
		return companyPhone;
	}
	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}
	public String getCompanyLandMark() {
		return companyLandMark;
	}
	public void setCompanyLandMark(String companyLandMark) {
		this.companyLandMark = companyLandMark;
	}
	public List<EmployerContactInformation> getEmployerContactInformation() {
		return this.employerContactInformation;
	}
	public void setEmployerContactInformation(List<EmployerContactInformation> employerContactInformation) {
		this.employerContactInformation = employerContactInformation;
	}
    
	/*public VacancyInfo getVacancyInfo() {
		return vacancyInfo;
	}
	public void setVacancyInfo(VacancyInfo vacancyInfo) {
		this.vacancyInfo = vacancyInfo;
	}*/
	public Long getDomainMasterId() {
		return domainMasterId;
	}
	public void setDomainMasterId(Long domainMasterId) {
		this.domainMasterId = domainMasterId;
	}
	public String getOtherInfo() {
		return otherInfo;
	}
	public void setOtherInfo(String otherInfo) {
		this.otherInfo = otherInfo;
	}
	
	

}
